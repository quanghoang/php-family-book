<?php

/**
 * App environment (dev, stag, prod, etc.)
 */
define('APP_ENV', 'dev');

switch (APP_ENV) {
    case 'dev':
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        return include('app/config/dev.php');

    case 'prod':
        return include('app/config/prod.php');
}