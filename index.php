<?php
include 'start.php';

$app          = System\App::instance();
$app->request = System\Request::instance();
$app->route   = System\Route::instance($app->request);
$route        = $app->route;

$route->get(['/', '/login'], 'App\Controller\Login@index');

$route->get('/logout', 'App\Controller\Login@logout');

$route->post('/', 'App\Controller\Login@login');

$route->get('/registry', 'App\Controller\Login@registry');

$route->get('/forgot-password', 'App\Controller\Login@forgotPassword');

$route->get('/doi-loi', 'App\Controller\Home@index');

$route->get('/pha-ky/{id}?/{title}?', 'App\Controller\Blog@index');

$route->get('/pha-he', 'App\Controller\Genealogy@index');

$route->get('/pha-do', 'App\Controller\Genealogy@familyTree');

$route->get('/chu-pha', 'App\Controller\Genealogy@appendix');

$route->get('/*', 'App\Controller\Error@index');

$route->end();