<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/13/17
 * Time: 2:45 PM
 */

include 'start.php';

$data   = json_decode(file_get_contents('php://input'), true);
$data   = !$data ? $_REQUEST : $data;
$ajax   = isset($data['ajax']) ? $data['ajax'] : array();
$folder = isset($ajax['folder']) ? $ajax['folder'] : '';
$class  = sprintf('\App\API\%s', isset($ajax['class']) ? $ajax['class'] : '');


if (!class_exists($class)) {
	$ajaxClass = isset($ajax['class']) ? $ajax['class'] : '';
	echo json_encode(array(
		'error'   => true,
		'message' => empty($ajaxClass) ? 'API Class is required.' : "API Class '$ajaxClass' not found."
	));
	exit();
}

unset($data['ajaxClass']);
$object = new $class($data);
$object->process();
