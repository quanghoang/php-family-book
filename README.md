Setup Project:

- Go to web root folder.
- Run: git clone https://quanghoang@bitbucket.org/quanghoang/php-family-book.git
- cd php-family-book/
- php composer.phar update
- vim .gitignore
  Add: config.php and .htaccess. 
- vim config.php
  Replace APP_ENV from dev to prod.
- vim .htaccess
  Remove /php-family-book/
- cd js
- vim config.js
  Remove /php-family-book/ in baseURL.
- jspm update

