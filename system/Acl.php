<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/18/17
 * Time: 5:31 PM
 */

namespace System;
use App\Model;

/**
 * Class Access Control Layer
 * @package System
 */
class Acl {

    /**
     * Role array
     * @var array
     */
    public $roles;

    /**
     * Resource array
     * @var array
     */
    public $resources;

    /**
     * Permit array
     * @var array
     */
    public $permits;


	private static $instance;

	/**
	 * Get instance
	 * @return Acl
	 */
	public static function instance() {
		if (!Acl::$instance) {
			Acl::$instance = new Acl();
		}
		return Acl::$instance;
	}

    public function __construct() {
        $this->roles     = array();
        $this->resources = array();
        $this->permits   = array();
	    $this->loadPermits();
    }

    /**
     * Add Role(s)
     * @param string|array $roles Roles
     */
    public function addRole($roles) {
        foreach (\Helper\Arrays::toArray($roles) as $role) {
            $this->roles[] = $role;
        }
    }

    /**
     * Add resource(s)
     * @param string|array $resources
     */
    public function addResource($resources) {
        foreach (\Helper\Arrays::toArray($resources) as $resource) {
            $this->resources[] = $resource;
        }
    }

    /**
     * Set permit
     * @param string $role Role name
     * @param string $resource Resource name
     * @param string|array $permits Permit value, use false for deny permit, or array of permits
     * @param bool $status Permit status, true (allow) or false (deny)
     */
    public function setPermit($role, $resource, $permits, $status=true) {
        if (!isset($this->permits[$role])) {
            $this->permits[$role] = array();
        }

        if ($permits == 'all') {
            $this->permits[$role][$resource] = $status;
            return;
        }

        foreach (\Helper\Arrays::toArray($permits) as $permit) {
            if (!isset($this->permits[$role][$resource])) {
                $this->permits[$role][$resource] = array();
            }
            $this->permits[$role][$resource][$permit] = $status;
        }
    }

    /**
     * Get permit
     * @param string $role
     * @param string $resource
     * @param string $permit
     * @return bool
     */
    public function getPermit($role, $resource, $permit) {
        return isset($this->permits[$role]) &&
               isset($this->permits[$role][$resource]) &&
               $this->permits[$role][$resource] &&
               isset($this->permits[$role][$resource][$permit]) &&
               $this->permits[$role][$resource][$permit];
    }

    /**
     * Set allow permit
     * @param string|array $roles
     * @param string|array $resources
     * @param string|array $permits
     * @param bool $status true (allow), false (deny)
     */
    public function allow($roles, $resources, $permits='all', $status=true) {
        $roles     = $roles == 'all' ? $this->roles : $roles;
        $resources = $resources == 'all' ? $this->resources : $resources;

        foreach (\Helper\Arrays::toArray($roles) as $role) {
            foreach (\Helper\Arrays::toArray($resources) as $resource) {
                $this->setPermit($role, $resource, $permits, $status);
            }
        }
    }

    /**
     * Set deny permit
     * @param string|array $roles
     * @param string|array $resources
     * @param string|array $permits
     */
    public function deny($roles, $resources, $permits='all') {
        $this->allow($roles, $resources, $permits, false);
    }

    /**
     * Extend permit(s)
     * @param string $fromRole
     * @param string $toRole
     */
    public function extend($fromRole, $toRole) {
        $this->permits[$toRole] = \Helper\Arrays::copy($this->permits[$fromRole]);
    }

    /**
     * Has role or not
     * @param string $role
     * @return bool
     */
    public function hasRole($role) {
        return in_array($role, Auth::getUserRoles());
    }

    /**
     * Is current login user having permit on a resource or not
     * @param string $resource Resource name
     * @param string $permit Permit name
     * @return bool
     */
    public function hasPermit($resource, $permit) {
        foreach (Auth::getUserRoles() as $role) {
            if ($this->getPermit($role, $resource, $permit)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Can view a resource?
     * @param string $resource
     * @return bool
     */
    public function canView($resource) {
        return $this->hasPermit($resource, 'view');
    }

    /**
     * Can edit a resource?
     * @param string $resource
     * @return bool
     */
    public function canAdd($resource) {
        return $this->hasPermit($resource, 'add');
    }

    /**
     * Can edit a resource?
     * @param string $resource
     * @return bool
     */
    public function canEdit($resource) {
        return $this->hasPermit($resource, 'edit');
    }

    /**
     * Can delete a resource?
     * @param string $resource
     * @return bool
     */
    public function canDelete($resource) {
        return $this->hasPermit($resource, 'delete');
    }

	/**
	 * Load user permits
	 */
	public function loadPermits() {
		$this->addRole(Model\Acl::getAllRoles());
		$this->addResource(Model\Acl::getAllResources());

		foreach ($this->roles as $role) {
			$rolePermits = $this->getRolePermits($role);
			foreach ($rolePermits as $resource => $permits) {
				$this->allow($role, $resource, $permits);
			}
		}
	}

	/**
	 * Get role permits
	 * @param string $role Role name
	 * @param array $permits Parent permits
	 * @return array
	 */
	public function getRolePermits($role, $permits=array()) {
		$parent = Model\Acl::getRoleParentName($role);
		if ($parent) {
			$permits = $this->getRolePermits($parent, $permits);
		}

		$rows = Model\Acl::getRolePermits($role);

		//get current role permit
		$result = array();
		if ($rows) {
			foreach ($rows as $row) {
				$resource = $row['resource'];
				$permit   = $row['permit'];

				if (!isset($result[$resource])) {
					$result[$resource] = array();
				}
				$result[$resource][] = $permit;
			}
		}

		//merge role permits
		if ($permits) {
			foreach ($permits as $resource => $permitList) {
				if (!isset($result[$resource])) {
					$result[$resource] = array();
				}
				foreach ($permitList as $permit) {
					$result[$resource][] = $permit;
				}
			}
		}
		return $result;
	}
}