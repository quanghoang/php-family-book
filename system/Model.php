<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/19/17
 * Time: 4:06 PM
 */

namespace System;


class Model {

    public function __construct() {
    }

    /**
     * Get db instance
     * @param string $dbName
     * @return \Simplon\Mysql\Mysql
     */
    public static function db($dbName='default') {
        return DB::instance($dbName);
    }

    public static function now() {
    	return \Helper\DateTime::now();
    }

	/**
	 * Get current login user ID
	 * @return int
	 */
    public static function getUserId() {
    	return \Helper\User::getLoginId();
    }

	/**
	 * Create option array
	 *
	 * @param $value string Option value.
	 * @param $label string Option text.
	 * @param $display string Option display value (short label)
	 * @param string $group string
	 * @param int $selected Selected or not (1: selected)
	 * @return array
	 */
	public static function createOption($value, $label, $group='', $display=null, $selected=0) {
		$option = array(
			'value' => $value,
			'label' => $label
		);
		if ($display) {
			$option['display'] = $display;
		}
		if ($group) {
			$option['group'] = $group;
		}
		if ($selected) {
			$option['selected'] = $selected ? 1 : 0;
		}
		return $option;
	}

	/**
	 * Convert data array to options array
	 *
	 * @param $data array
	 * @param $valueField string
	 * @param $labelField string
	 * @param null $groupField string
	 * @param null $selectedField string
	 * @return array
	 */
	public static function toOptionsArray($data, $valueField, $labelField, $groupField=null, $selectedField=null) {
		$options = array();

		if (empty($data) || !is_array($data)) {
			return $options;
		}

		foreach ($data as $row) {
			$value     = $row[$valueField];
			$label     = $row[$labelField];
			$group     = $groupField ? $row[$groupField] : '';
			$selected  = $selectedField && $row[$selectedField] ? 1 : 0;
			$options[] = self::createOption($value, $label, $group, $selected);
		}
		return $options;
	}

	public static function extractColumns($rows, $columns) {
		if (!$rows || !is_array($rows)) {
			return $rows;
		}

		$result = array();

		foreach ($rows as $row) {
			$temp = array();
			foreach ($columns as $alias => $column) {
				$alias = is_numeric($alias) ? $column : $alias;
				if (isset($row[$column])) {
					$temp[$alias] = $row[$column];
				}
			}

			if ($temp) {
				$result[] = $temp;
			}
		}
		return $result;
	}

	public static function getColumnValues($rows, $column, $sort=false) {
		if (!$rows || !is_array($rows)) {
			return $rows;
		}

		$result = array();
		foreach ($rows as $row) {
			$result[] = $row[$column];
		}

		if ($sort) {
			sort($result);
			if ($sort == 'DESC') {
				array_reverse($result);
			}
		}
		return $result;
	}

	/**
	 * Get column description
	 * @param $table string Table name
	 * @param $mysqlDb string Mysql connection config name.
	 * @return array
	 */
	public static function getColumnDesc($table, $mysqlDb='default') {
		$rows = self::db($mysqlDb)->fetchRowMany("DESC `$table`");
		$result = array();
		if (!$rows) {
			return $result;
		}

		foreach ($rows as $row) {
			$field = $row['field'];
			unset($row['field']);
			$result[$field] = $row;
		}
		return $result;
	}

	/**
	 * Get nullable columns of a table
	 * @param $mysqlDb string Mysql connection config name
	 * @param $table string Table name
	 * @return array
	 */
	public static function getNullColumns($mysqlDb, $table) {
		$desc = self::getColumnDesc($mysqlDb, $table);
		$columns = array();
		if ($desc) {
			foreach ($desc as $name => $column) {
				if ($column['null'] == 'YES') {
					$columns[] = $name;
				}
			}
		}
		return $columns;
	}

	/**
	 * Convert empty values to null for nullable columns
	 * @param $rows array
	 * @param $mysqlDb string
	 * @param $table string
	 * @return array
	 */
	public static function emptyToNull($data, $mysqlDb, $table) {
		$nullColumns = self::getNullColumns($mysqlDb, $table);
		if (!$nullColumns) {
			return $data;
		}
		foreach ($nullColumns as $column) {
			if (empty($data[$column])) {
				$data[$column] = null;
			}
		}
		return $data;
	}
}