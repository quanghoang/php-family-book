<?php
/**
 * Created by PhpStorm.
 * User: quanghoang
 * Date: 4/15/17
 * Time: 1:09 PM
 */

namespace System;

class Config {

    const VAR_REGEX = '/\[\$([\w.]+){1}\]/';

    private $config;

    public static $instance = null;

    /**
     * Get instance
     * @return Config
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new Config();
        }
        return self::$instance;
    }

    /**
     * Get config value
     * @param string $name
     * @param null|mixed $default Default value
     * @return mixed
     */
    public static function get($name, $default=null) {
        return self::getInstance()->getConfig($name, $default);
    }

    /**
     * Config constructor.
     */
    public function __construct() {
        $this->config = include(__DIR__ . '/../config.php');
        $this->config = $this->parse($this->config);
    }

    /**
     * Parse variables in config values
     * @param $config array Config array
     * @return array
     */
    private function parse($config) {
        if (!is_array($config)) {
            return $config;
        }

        foreach ($config as $name => $value) {
            if (is_array($value)) {
                $config[$name] = $this->parse($value);
                continue;
            }

            if (is_string($value)) {
                $config[$name] = $this->parseValue($value);
            }
        }
        return $config;
    }

    /**
     * Parse a string value
     * @param string $value
     * @return string
     */
    private function parseValue($value) {
        $found = true;
        while  ($found) {
            $found = preg_match_all(self::VAR_REGEX, $value, $matches, PREG_SET_ORDER, 0);
            if ($found) {
                $cfgValue = $this->getConfig($matches[0][1]);
                $value    = str_replace($matches[0][0], strval($cfgValue), $value);
            }
        }
        return $value;
    }


    /**
     * Get config value
     * @param string $name Key name
     * @param mixed $default Default value
     * @return mixed
     */
    public function getConfig($name, $default=null) {
        if (!$name) {
            return $this->config;
        }
        if (isset($this->config[$name])) {
            return $this->config[$name];
        }

        $value = \Helper\Arrays::find($name, $this->config);
        if ($value) {
            return $value;
        }
        return $default;
    }
}