<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/18/17
 * Time: 1:54 PM
 */

namespace System;


class Content {

    /**
     * @var \System\App
     */
    public $app;

    /**
     * Content constructor.
     */
    public function __construct() {
        $this->app = \System\App::instance();
    }

    /**
     * Get theme instance
     * @param string $theme Theme folder name
     * @return Theme
     */
    public function theme($theme='current') {
        return Theme::instance();
    }

    /**
     * Get config value
     * @param string $key
     * @param mixed $default Default value
     * @return mixed
     */
    public function config($key, $default=null) {
        return Config::get($key, $default);
    }

    /**
     * Get current URI
     * @return mixed
     */
    public function getCurrentUri() {
        return $this->app->request->path;
    }
}