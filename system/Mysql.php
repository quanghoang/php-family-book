<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/23/17
 * Time: 10:58 PM
 */

namespace System;

use \Helper\Arrays;

class Mysql extends \Simplon\Mysql\Mysql {


	public function fetchRow(string $query, array $conds = []): ?array {
		return Arrays::toArray(parent::fetchRow($query, $conds));
	}

	public function fetchRowMany(string $query, array $conds = []): ?array {
		return Arrays::toArray(parent::fetchRowMany($query, $conds));
	}
}