<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/19/17
 * Time: 9:45 AM
 */

namespace System;

/**
 * Class Authentication
 * @package System
 */
class Auth {

    /**
     * Do login after checking username & password in database
     * @param string $user_id
     * @param array $roles
     */
    public static function doLogin($user_id, $roles) {
        $_SESSION['user_id']    = $user_id;
        $_SESSION['user_roles'] = \Helper\Arrays::toArray($roles);
    }

    /**
     * Do logout
     */
    public static function doLogout() {
        unset($_SESSION['user_id']);
        unset($_SESSION['user_roles']);
    }

    /**
     * Is user login
     * @return bool
     */
    public static function isLogin() {
        return isset($_SESSION['user_id']);
    }

    /**
     * Get current login user Id
     * @return int
     */
    public static function getUserId() {
        return isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
    }

    /**
     * Set current login user Id
     */
    public static function setUserId($userId) {
        $_SESSION['user_id'] = $userId;
    }

	/**
	 * Get current login user login name
	 * @return int
	 */
	public static function getLoginName() {
		return isset($_SESSION['user_login_name']) ? $_SESSION['user_login_name'] : 0;
	}

	/**
	 * Set current login user login name
	 */
	public static function setLoginName($userLoginName) {
		$_SESSION['user_login_name'] = $userLoginName;
	}

    /**
     * Get current login user roles
     * @return array
     */
    public static function getUserRoles() {
        return isset($_SESSION['user_roles']) ? $_SESSION['user_roles'] : array('guest');
    }

    /**
     * Set current login user roles
     */
    public static function setUserRoles($roles) {
        $_SESSION['user_roles'] = \Helper\Arrays::toArray($roles);
    }
}