<?php

namespace System;

class API {

    const UI_DEFAULT_VALUE = '[ui-default-value]'; //must be the same as Validator.empty_value (Validator.js)
	const LOG_FILE         = 'ui_api_log'; //Log filename (without .log extension), can be redefined in inherited classes.

    /**
     * Request method
     * @var string
     */
    private $method;

    /**
     * Response data type (json, html, xml)
     * @var string
     */
    private $dataType;


    /**
     * Request data
     * @var array
     */
    public $data;


    public function __construct($request=array()) {
        $ajax               = $request['ajax'] ? $request['ajax'] : array();
        $this->method       = $ajax['method'];
        $this->dataType     = $ajax['dataType'] ? $ajax['dataType'] : 'json';
        $this->request      = $request;
        $data               = isset($request['data']) ? $request['data'] : array();
        $this->data         = $data;
        $this->isCallMethod = isset($request['callMethod']) ? true : false;
        //PAC_Log::setLogFile('ui_ajax_debug');
    }

    /**
     * Process request
     */
    public function process() {
        $ignore_methods = array('process', 'error', 'info', 'response');

        if (method_exists($this, $this->method) && !in_array($this->method, $ignore_methods)) {
            //$params   = array($this->state, $this->field, $this->data);
            $method   = array($this, $this->method);
            $response = call_user_func_array($method, array($this->data));
        }
        else {
            $response = $this->error("Method '$this->method' not found.");
        }

	    $response = isset($response['error']) ? $response : array(
		    'error'  => false,
		    'data'   => $response
	    );

	    if ($this->isCallMethod) {
		    return $response;
	    }
        $this->response($response);
    }

    /**
     * Call an api method
     * @param $method string
     * @param null $data Param array
     * @return null
     */
    public function callMethod($method, $data=null) {
        $data   = $data ? $data : $this->data;
        $data   = \Helper\Arrays::toArray($data);

        $parts  = explode('/', $method);
        if (count($parts) > 2) {
            return null;
        }

        $folder = 'UI';
        if (count($parts) == 2) {
            $folder = $parts[0];
            $method = $parts[1];
        }

        $parts = explode('.', $method);
        if (count($parts) > 2) {
            return null;
        }

        $class = 'Default';
        if (count($parts) == 2) {
            $class = $parts[0];
            $method = $parts[1];
        }

        $class = "PAC_UI_{$folder}_{$class}";

        if (!class_exists($class)) {
            return null;
        }

        $request = array(
            'callMethod' => true,
            'data' => $data,
            'ajax' => array(
                'method'   => $method,
                'dataType' => 'json'
            )
        );

        $object = new $class($request);
        return $object->process();
    }


    /**
     * Return error to client site
     * @param $message string Error message
     * @param $data null|array Extra data when needed
     * @return array
     */
    public function error($message, $data=null) {
        return array(
            'error'   => true,
            'message' => $message,
            'data'    => $data
        );
    }

    /**
     * Create info message array
     * @param $message string Info message
     * @param $data null|array Extra data when needed
     * @return array
     */
    public function info($message, $data=null) {
        return array(
            'info'    => true,
            'error'   => false,
            'message' => $message,
            'data'    => $data
        );
    }

    /**
     * Create info message array
     * @param $message string Info message
     * @param $data null|array Extra data when needed
     * @return array
     */
    public function success($message, $data=null) {
        return array(
            'success' => true,
            'error'   => false,
            'message' => $message,
            'data'    => $data
        );
    }

    /**
     * Response data to client site
     * @param $data array|string
     */
    public function response($response) {
        switch ($this->dataType) {
            case 'json':
				header('Content-Type:application/json');
                echo json_encode($response);
                break;

            case 'xml':
                $xml = new \SimpleXMLElement('<root/>');
                array_walk_recursive($response, array($xml, 'addChild'));
                echo $xml->asXML();
                break;

            case 'html':
                echo $response;
                break;

            case 'png':
                header("Content-Type: image/png");
                echo $response['data'];
                break;
        }
        exit();
    }

	/**
	 * Render template file
	 * @param string $template
	 * @param null $data
	 * @param null|string $theme
	 * @return string
	 */
    public function render($template, $data=null, $theme=null) {
    	return Theme::instance($theme)->render($template, $data);
    }


	/**
	 * Get config value
	 * @param $key
	 * @param null $default
	 * @return mixed
	 */
    public function config($key, $default=null) {
	    return Config::get($key, $default);
    }

    /**
     * Get login user ID
     * @return int
     */
    public function getUserId() {
	    return \System\Auth::getUserId();
    }

    /**
     * Get user login name
     * @return null|string
     */
    public function getLoginName() {
        return \System\Auth::getLoginName();
    }

	/**
	 * Get user full name
	 * @param null|int $userId User ID, use null for current login user.
	 * @param string $unknown Unknown user value.
	 * @return null|string
	 */
    public function getUserFullName($userId=null, $unknown='Unknown') {
    	$userId = $userId ? $userId : $this->getUserId();
	    $user = \App\Model\User::getUserById($userId);
	    return $user ? $user['full_name'] : $unknown;
    }

    /**
     * Check if user has role(s) or not
     * @param $roles string|array
     * @return bool
     */
    public function hasRole($roles) {
        foreach (\Helper\Arrays::toArray($roles) as $role) {
            if (!\System\Acl::instance()->hasRole($role)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get current date & time
     *
     * @return string
     */
    public function now() {
        return date('Y-m-d H:i:s');
    }

    /**
     * Get field value
     *
     * @param $fieldName string
     * @return null|string|array
     */
    public function getValue($fieldName, $toArray=false) {
        $data = $this->data;
        $data = $data && isset($data[$fieldName]) ? $data[$fieldName] : null;
        return !$toArray ? $data : ($data ? \Helper\Arrays::toArray($data) : null);
    }


    /**
     * Create option array
     *
     * @param $value string Option value.
     * @param $label string Option text.
     * @param $display string Option display value (short label)
     * @param string $group string
     * @param int $selected Selected or not (1: selected)
     * @return array
     */
    public function createOption($value, $label, $group='', $display=null, $selected=0) {
        $option = array(
            'value' => $value,
            'label' => $label
        );
        if ($display) {
            $option['display'] = $display;
        }
        if ($group) {
            $option['group'] = $group;
        }
        if ($selected) {
            $option['selected'] = $selected ? 1 : 0;
        }
        return $option;
    }

    /**
     * Convert data array to options array
     *
     * @param $data array
     * @param $valueField string
     * @param $labelField string
     * @param null $groupField string
     * @param null $selectedField string
     * @return array
     */
    public function toOptionsArray($data, $valueField, $labelField, $groupField=null, $selectedField=null, $emptyOption=false) {
        $options = array();

        if (empty($data) || !is_array($data)) {
            return $options;
        }

        if ($emptyOption) {
        	$options[] = $this->createOption('', '');
        }

        foreach ($data as $row) {
            $value     = $row[$valueField];
            $label     = $row[$labelField];
            $group     = $groupField ? $row[$groupField] : '';
            $selected  = $selectedField && $row[$selectedField] ? 1 : 0;
            $options[] = $this->createOption($value, $label, $group, $selected);
        }
        return $options;
    }


    /**
     * Combine UI attributes
     *
     * @param $attributes array Component attributes
     * @param $overwrite array Overwritten attributes
     * @return array
     */
    public function combineUI($attributes, $overwrite) {
        //merge settings
        if (is_array($overwrite)) {
            //overwrite value
            foreach ($attributes as $field => $value) {
                if ($overwrite[$field]) {
                    $attributes[$field] = $overwrite[$field];
                }
            }
            //add new field not exist
            foreach ($overwrite as $field => $value) {
                if (!$attributes[$field]) {
                    $attributes[$field] = $value;
                }
            }
        }

        //overwrite value if found in $this->data
        $value = $this->data[$attributes['name']];
        if ($value) {
            $attributes['value'] = $value;
        }

        //load init data
        if ($attributes['preload'] && $attributes['method']) {
            $response = $this->callMethod($attributes['method']);
            $attributes['loaded'] = true;
            $attributes['options'] = $response['data'];

            if ($response['info']) {
                $attributes['info'] = $response['message'];
            }
        }

        //update value
        switch ($attributes['type']) {
            case 'date':
                $attributes['value'] = date('Y-m-d', strtotime($attributes['value']));
                break;
        }
        return $attributes;
    }

    /**
     * Create row info (created/modified datetime/user)
     * @param $createdDate string
     * @param $createdBy int UserID
     * @param $modifiedDate string
     * @param $modifiedBy int
     * @return string
     */
    public function createRowInfo($createdDate, $createdBy, $modifiedDate, $modifiedBy) {
        $createdBy  = empty($createdBy) ? '' : sprintf(' by %s', $this->getUserFullName($createdBy));
        $modifiedBy = empty($modifiedBy) ? '' : sprintf(' by %s', $this->getUserFullName($modifiedBy));
        $content    = '';

        if ($createdDate && $createdDate != '0000-00-00 00:00:00') {
            $content = sprintf('[im:file-o] Created on %s%s.<br/>', \Helper\DateTime::formatTime($createdDate), $createdBy);
        }
        else if ($createdBy) {
            $content = sprintf('[im:file-o] Created by %s.<br/>', $createdBy);
        }

        if ($modifiedDate && $modifiedDate != '0000-00-00 00:00:00') {
            $content .= sprintf('[im:pencil] Last modified on %s%s.', \Helper\DateTime::formatTime($modifiedDate), $modifiedBy);
        }
        else if ($modifiedBy) {
            $content .= sprintf('[im:pencil] Last modified by %s.', $modifiedBy);
        }
        return $content ? sprintf('<div class="ui-row-info">%s</div>', $content) : '';
    }

	public function createInfoDialog($title, $body, $alertClass='info', $size='medium') {
		return array(
			'type'        => 'dialog',
			'title'       => $title,
			'bodyPading'  => true,
			'size'        => $size,
			'buttons'     => array(array('name' => 'close', 'label' => 'Close', 'click' => 'close')),
			'body'        => array(
				'type'    => 'content',
				'content' => sprintf('[im:info-circle] %s', $body),
				'class'   => 'alert alert-' . $alertClass
			),
		);
	}

    /**
     * Parse CSV file to array
     * @param $filename
     * @return array
     */
    public function parseCSV($filename) {
        return array_map('str_getcsv', file($filename));
    }

	/**
	 * Get first not empty value in array
	 * (Eg. use for getting select value of radio buttons)
	 * @param $array
	 * @return mixed|null
	 */
    public function getFirstValue($array) {
    	$array = is_array($array) ? $array : array($array);
	    foreach ($array as $value) {
    		if (!empty($value)) {
    			return $value;
		    }
	    }
	    return null;
    }

	/**
	 * Log to file
	 * @param $message string Log message
	 * @param $filename null|string Filename (without .log extension) in the /cluster/shared/log folder.
	 * @return bool Return true if success, else false.
	 */
    public function log($message, $filename=null) {
    	$filename = $filename ? $filename : static::LOG_FILE;
    	if (!$filename) {
    		return false;
	    }
	    //TODO: Find log library to replace PAC_Log
//	    PAC_Log::setLogFile($filename);
//	    PAC_Log::tofile($message);
	    return true;
    }

	/**
	 * Log save data (add/update data)
	 * @param $objectName string Save object name.
	 * @param $data array Update data from edit dialog.
	 * @param null|string $filename Filename (see log() method)
	 */
    public function logSave($objectName, $data, $filename=null) {
	    $this->log(sprintf('%s (id:%s) %s %s: %s.',
	        $this->getUserFullName(),
		    $this->getUserId(),
            $data['id'] == 'new' ? 'Add' : 'Update',
            $objectName,
            json_encode($data)),
		    $filename);
    }
}