<?php
/**
 * Created by PhpStorm.
 * User: quanghoang
 * Date: 4/15/17
 * Time: 1:31 PM
 */

namespace System;


class Controller extends Content {

    public function __construct() {
        parent::__construct();
    }

    public function render($template='index', $data=array()) {
        return $this->theme()->render($template, $data);
    }

    public function display($template='index', $data=array()) {
        echo $this->render($template, $data);
    }
}