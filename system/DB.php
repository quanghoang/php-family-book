<?php
/**
 * Created by PhpStorm.
 * User: quanghoang
 * Date: 4/15/17
 * Time: 1:05 PM
 */

namespace System;


class DB {

    /**
     * Database config name
     * @var string
     */
    private $dbName;

    /**
     * Database connection object
     * @var \Simplon\Mysql\Mysql
     */
    private $db;

    /**
     * Static instances
     * @var array
     */
    private static $instances;

    /**
     * Get instance
     * @param string $config Database config name
     * @return \Simplon\Mysql\Mysql
     */
    public static function instance($config) {
        if (!isset(self::$instances[$config])) {
            self::$instances[$config] = new DB($config);
        }
        return self::$instances[$config]->db;
    }

    /**
     * DB constructor.
     * @param string $dbName Db config name
     * @throws \Exception
     */
    public function __construct($dbName='default') {
        $this->dbName = Config::get('db.' . $dbName);
        if (empty($this->dbName)) {
            throw new \Exception("DB configuration 'db.$dbName' not found.");
        }
        $this->connect();
    }

    /**
     * Connect database
     * @throws \Exception
     */
    public function connect() {
        $config = $this->dbName;
        switch ($config['driver']) {
            case 'mysql':
                $dsn = sprintf('mysql:host=%s;port=%s;dbname=%s;charset=%s',
                       $config['host'], $config['port'], $config['database'], $config['charset']);
                $pdo = new \PDO($dsn, $config['user'], $config['password']);
                $this->db = new \System\Mysql($pdo);
                $this->db->setFetchMode($config['fetchMode']);
                break;

            default:
                throw new \Exception('Invalid DB driver.');
                break;
        }
    }
}