<?php
/**
 * Created by PhpStorm.
 * User: quanghoang
 * Date: 4/15/17
 * Time: 3:13 PM
 */

namespace System;


class Theme {

    /**
     * Theme folder name
     * @var string
     */
    private $theme;

    /**
     * @var \Twig_Loader_Filesystem
     */
    private $loader;

    /**
     * @var \Twig_Environment
     */
    private $engine;

    /**
     * @var \Twig_TemplateWrapper array
     */
    private $template;

    private static $instances;

    /**
     * Get instance
     * @param string $theme
     * @return Theme
     */
    public static function instance($theme=null) {
	    $theme = $theme ? $theme : $_SESSION['theme'];
        if (!isset(self::$instances[$theme])) {
            self::$instances[$theme] = new Theme($theme);
        }
        return self::$instances[$theme];
    }

    /**
     * Theme constructor.
     * @param string $theme Theme folder name
     */
    public function __construct($theme='default') {
        $this->theme    = $theme;
        $this->loader   = new \Twig_Loader_Filesystem(Config::get('theme.folder') . '/' . $theme);
        $this->engine   = new \Twig_Environment($this->loader, Config::get('theme.options'));
        $this->template = array();

        if (Config::get('theme.options.debug')) {
            $this->engine->addExtension(new \Twig_Extension_Debug());
        }
    }

    /**
     * Render a template file
     * @param string $file Template file name (a file in theme folder)
     * @param array $data Data array
     * @return string
     */
    public function render($file, $data=array()) {
        $file .= strpos($file, '.') === false ? '.twig' : '';
        $this->template[$file] = $this->engine->load($file);
        return $this->template[$file]->render($this->attachData($data));
    }

    /**
     * Render a block of a template file
     * @param string $file Template file name
     * @param string $blockName Block name
     * @param array $data Data array
     * @return string
     */
    public function renderBlock($file, $blockName, $data=array()) {
        if (!isset($this->template[$file])) {
            throw new \Exception("Template for '$file' file not found.");
        }
        return $this->template[$file]->renderBlock($blockName, $this->attachData($data));
    }

    /**
     * Attach app settings data
     * @param array $data
     * @return array
     */
    private function attachData($data) {
        $data = empty($data) ? array() : $data;
        $data = is_array($data) ? $data : array($data);

        //attach app settings data
        $data['app'] = Config::get('app');
        return $data;
    }
}