<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'Test\\' => array($vendorDir . '/simplon/mysql/test'),
    'System\\' => array($baseDir . '/system', $vendorDir . '/nezamy/route/system'),
    'Simplon\\Mysql\\' => array($vendorDir . '/simplon/mysql/src'),
    'Helper\\' => array($baseDir . '/helper'),
    'App\\' => array($baseDir . '/app'),
);
