# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.35)
# Database: ui-library
# Generation Time: 2017-04-20 22:51:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acl_permit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_permit`;

CREATE TABLE `acl_permit` (
  `permit_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `role` varchar(100) NOT NULL DEFAULT '',
  `resource` varchar(100) NOT NULL DEFAULT '',
  `permit` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`permit_id`),
  KEY `role` (`role`),
  KEY `resource` (`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_permit` WRITE;
/*!40000 ALTER TABLE `acl_permit` DISABLE KEYS */;

INSERT INTO `acl_permit` (`permit_id`, `archived`, `role`, `resource`, `permit`)
VALUES
	(1,0,'guest','home','view'),
	(2,0,'guest','blog','view'),
	(3,0,'guest','shop','view'),
	(4,0,'user','blog','add'),
	(5,0,'user','blog','delete'),
	(6,0,'user','blog','edit'),
	(7,0,'user','blog','comment'),
	(8,0,'user','shop','edit');

/*!40000 ALTER TABLE `acl_permit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_resource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_resource`;

CREATE TABLE `acl_resource` (
  `resource_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`resource_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_resource` WRITE;
/*!40000 ALTER TABLE `acl_resource` DISABLE KEYS */;

INSERT INTO `acl_resource` (`resource_id`, `archived`, `name`, `label`, `description`)
VALUES
	(1,0,'home','Home',NULL),
	(2,0,'blog','Blogs',NULL),
	(3,0,'shop','Shop',NULL),
	(4,0,'user','User',NULL);

/*!40000 ALTER TABLE `acl_resource` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_role`;

CREATE TABLE `acl_role` (
  `role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_role` WRITE;
/*!40000 ALTER TABLE `acl_role` DISABLE KEYS */;

INSERT INTO `acl_role` (`role_id`, `parent_id`, `archived`, `name`, `label`, `description`)
VALUES
	(1,0,0,'guest','Guest',NULL),
	(2,1,0,'user','User',NULL),
	(3,2,0,'admin','Administrator',NULL),
	(4,3,0,'dev','Developer',NULL);

/*!40000 ALTER TABLE `acl_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_user_role`;

CREATE TABLE `acl_user_role` (
  `user_role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_user_role` WRITE;
/*!40000 ALTER TABLE `acl_user_role` DISABLE KEYS */;

INSERT INTO `acl_user_role` (`user_role_id`, `user_id`, `role_id`)
VALUES
	(1,1,2),
	(2,1,3);

/*!40000 ALTER TABLE `acl_user_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) DEFAULT '0',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`user_id`, `archived`, `first_name`, `last_name`, `username`, `password`, `email`)
VALUES
	(1,0,'Quang','Hoang','quang','aa41efe0a1b3eeb9bf303e4561ff8392','hodawa@gmail.com');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
