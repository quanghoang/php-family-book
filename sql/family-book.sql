CREATE TABLE `member` (
  `member_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `father_id` int(11) unsigned NOT NULL DEFAULT '0',
  `mother_id` int(11) unsigned NOT NULL DEFAULT '0',
  `spouse_id` int(11) unsigned NOT NULL DEFAULT '0',
  `full_name` varchar(200) NOT NULL DEFAULT '',
  `other_name` varchar(200) NOT NULL DEFAULT '',
  `gender` smallint(11) NOT NULL DEFAULT '0' COMMENT '1: male, 2:female',
  `alive` smallint(11) NOT NULL DEFAULT '1',
  `married` smallint(11) NOT NULL DEFAULT '0',
  `birth_date_solar` varchar(20) DEFAULT NULL,
  `birth_date_lunar` varchar(20) DEFAULT NULL,
  `dead_date_solar` varchar(20) DEFAULT NULL,
  `dead_date_lunar` varchar(20) DEFAULT NULL,
  `burial_place` varchar(200) DEFAULT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `note` text,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;