# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: family-book
# Generation Time: 2017-11-02 22:10:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acl_permit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_permit`;

CREATE TABLE `acl_permit` (
  `permit_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `role` varchar(100) NOT NULL DEFAULT '',
  `resource` varchar(100) NOT NULL DEFAULT '',
  `permit` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`permit_id`),
  KEY `role` (`role`),
  KEY `resource` (`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_permit` WRITE;
/*!40000 ALTER TABLE `acl_permit` DISABLE KEYS */;

INSERT INTO `acl_permit` (`permit_id`, `archived`, `role`, `resource`, `permit`)
VALUES
	(1,0,'guest','home','view'),
	(2,0,'guest','blog','view'),
	(3,0,'guest','shop','view'),
	(4,0,'user','blog','add'),
	(5,0,'user','blog','delete'),
	(6,0,'user','blog','edit'),
	(7,0,'user','blog','comment'),
	(8,0,'user','shop','edit');

/*!40000 ALTER TABLE `acl_permit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_resource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_resource`;

CREATE TABLE `acl_resource` (
  `resource_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`resource_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_resource` WRITE;
/*!40000 ALTER TABLE `acl_resource` DISABLE KEYS */;

INSERT INTO `acl_resource` (`resource_id`, `archived`, `name`, `label`, `description`)
VALUES
	(1,0,'home','Home',NULL),
	(2,0,'blog','Blogs',NULL),
	(3,0,'shop','Shop',NULL),
	(4,0,'user','User',NULL);

/*!40000 ALTER TABLE `acl_resource` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_role`;

CREATE TABLE `acl_role` (
  `role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_role` WRITE;
/*!40000 ALTER TABLE `acl_role` DISABLE KEYS */;

INSERT INTO `acl_role` (`role_id`, `parent_id`, `archived`, `name`, `label`, `description`)
VALUES
	(1,0,0,'guest','Guest',NULL),
	(2,1,0,'user','User',NULL),
	(3,2,0,'admin','Administrator',NULL),
	(4,3,0,'dev','Developer',NULL);

/*!40000 ALTER TABLE `acl_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_user_role`;

CREATE TABLE `acl_user_role` (
  `user_role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_user_role` WRITE;
/*!40000 ALTER TABLE `acl_user_role` DISABLE KEYS */;

INSERT INTO `acl_user_role` (`user_role_id`, `user_id`, `role_id`)
VALUES
	(1,1,2),
	(2,1,3);

/*!40000 ALTER TABLE `acl_user_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `member_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `generation` int(11) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `in_family` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `father_id` int(11) unsigned NOT NULL DEFAULT '0',
  `mother_id` int(11) unsigned NOT NULL DEFAULT '0',
  `spouse_id` int(11) unsigned NOT NULL DEFAULT '0',
  `full_name` varchar(200) NOT NULL DEFAULT '',
  `note` varchar(500) DEFAULT NULL,
  `gender` tinyint(11) NOT NULL DEFAULT '0' COMMENT '1: male, 2:female',
  `daughter_in_law` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Only use for female (1: daugther in law 2: daugther)',
  `son_in_law` tinyint(4) NOT NULL DEFAULT '0',
  `num_son` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `num_daughter` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `dob` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dob_lunar` varchar(20) DEFAULT NULL,
  `dod` varchar(20) DEFAULT NULL,
  `dod_lunar` varchar(20) DEFAULT NULL,
  `burial_place` varchar(200) DEFAULT NULL,
  `photo_id` int(200) unsigned NOT NULL DEFAULT '0',
  `other_info` text,
  `address` varchar(500) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `website` varchar(500) DEFAULT NULL,
  `row_active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT '0',
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;

INSERT INTO `member` (`member_id`, `generation`, `position`, `in_family`, `father_id`, `mother_id`, `spouse_id`, `full_name`, `note`, `gender`, `daughter_in_law`, `son_in_law`, `num_son`, `num_daughter`, `dob`, `dob_lunar`, `dod`, `dod_lunar`, `burial_place`, `photo_id`, `other_info`, `address`, `phone`, `email`, `website`, `row_active`, `created_date`, `created_by`, `modified_date`, `modified_by`)
VALUES
	(1,1,1,1,0,0,0,'Hoàng Ngọc Khắc','Hiệu Tinh Bà',1,0,0,2,0,X'',NULL,'',NULL,'',0,'<p>Ng&agrave;i sinh trưởng v&agrave;o khoảng giữa thế kỷ thứ XVI ở l&agrave;ng Hiền Lương. Nhưng khi lớn l&ecirc;n, Ng&agrave;i về lập nghiệp v&agrave; x&acirc;y dựng x&oacute;m l&agrave;ng Kế M&ocirc;n, tổng Vĩnh Xương, huyện Phong Điền, tỉnh Thừa Thi&ecirc;n. Ng&agrave;i c&oacute; c&ocirc;ng khai canh khai khẩn lập l&agrave;ng, cũng như người đầu ti&ecirc;n lập n&ecirc;n họ Ho&agrave;ng Kế M&ocirc;n.</p>\n<p>Ng&agrave;i l&agrave; Thỉ Tổ của Họ Ho&agrave;ng Kế M&ocirc;n-Thừa Thi&ecirc;n.</p>\n<p>Cũng như th&acirc;n phụ, ng&agrave;i rất c&oacute; c&ocirc;ng trong việc kiến tạo l&agrave;ng x&oacute;m, n&ecirc;n v&agrave;o đời vua Duy T&acirc;n &acirc;n chiếu sắc phong:</p>\n<p style=\"text-align: center;\"><strong>&ldquo;Thỉ Tổ Thượng Khai Canh</strong></p>\n<p style=\"text-align: center;\"><strong>Nhật Bảo Trung Hưng-Linh Ph&ograve; Chi Thần&rdquo;</strong></p>\n<p>&nbsp;</p>\n<p>Đời Vua Khải Định, l&uacute;c triều đ&igrave;nh l&agrave;m tứ tuần, cũng được &acirc;n chiếu sắc phong:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n<p style=\"text-align: center;\"><strong>&ldquo;Gia Tăng Đoan T&uacute;c T&ocirc;n Thần&rdquo;</strong></p>\n<p>&nbsp;</p>\n<p>Hai điệu sắc phong n&agrave;y đều do họ Ho&agrave;ng tr&ecirc;n nhận l&atilde;nh phụng thờ.</p>\n<p>Mộ của Ng&agrave;i được an t&aacute;ng tại xứ Huyền V&otilde;, gần đại lộ l&agrave;ng Kế M&ocirc;n, toạ T&yacute;, hướng Ngọ.</p>\n<p>C&ograve;n B&agrave;, kh&ocirc;ng r&otilde; t&ecirc;n họ v&agrave; ng&agrave;y kỵ giổ.</p>\n<p>Ng&agrave;i sanh hạ được hai người con Trai:&nbsp;&nbsp;</p>\n<ol>\n<li>HO&Agrave;NG NGỌC CA</li>\n<li>HO&Agrave;NG NGỌC CỬA</li>\n</ol>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:00:28',1,'2017-11-01 22:03:53',1),
	(2,2,1,1,1,0,0,'Hoàng Ngọc Ca','',1,0,0,1,0,X'',NULL,'',NULL,'',0,'<p>Đ&ocirc;ng thường t&ugrave;ng Ti&ecirc;n Ch&uacute;a - Đặc tấn Thượng Trụ Quốc C&ocirc;ng. Đại phu khai canh khai khẩn Hải M&ocirc;n, c&oacute; c&ocirc;ng lớn với nước nh&agrave;.</p>\n<p>- Đời Vua Duy T&acirc;n: &Acirc;n chiếu sắc phong Thuỷ Tổ Khai Canh. Nhật Bảo Trung Hưng&nbsp; - Linh Ph&ograve; Chi Thần.</p>\n<p>- Đời Vua Khải Định: &Acirc;n chiếu sắc phong trước Gia Tặng&nbsp; Đoan T&uacute;c T&ocirc;n Thần. Triều Đ&igrave;nh đ&atilde; thiết lập miếu thờ c&uacute;ng tại bến đ&ograve; Ca C&uacute;t ( gần cửa Thuận An)</p>\n<p>- Mộ an t&aacute;ng tại xứ T&acirc;y Huyền V&otilde;, L&agrave;ng Kế M&ocirc;n.</p>\n<p>- Hai Điệu sắc phong do họ Ho&agrave;ng (tr&ecirc;n) nhận l&atilde;nh phụng thờ.</p>\n<p>- Ng&agrave;i sanh hạ được một người con Trai:&nbsp; Ho&agrave;ng Ngọc Ti&ecirc;n</p>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:02:29',1,'2017-11-01 22:05:05',0),
	(3,2,2,1,1,0,0,'Hoàng Ngọc Cửa','',1,0,0,1,0,X'',NULL,'',NULL,'',0,'<p>Đ&ocirc;ng thường t&ugrave;ng Ti&ecirc;n Ch&uacute;a- Đặc Tấn Thượng Trụ Quốc C&ocirc;ng. Đại Phu khai canh- khai khẩn- khai Tiểu Hải M&ocirc;n. C&oacute; c&ocirc;ng lớn với nước nh&agrave;. Đời Vua Duy T&acirc;n, được &acirc;n chiếu sắc phong: Thuỷ Tổ Khai Canh, Nhật Trung Hưng Linh Ph&ograve; Chi Thần. Đời Vua Khải Định, được &acirc;n chiếu sắc phong: Gia Tặng Đoan T&uacute;c T&ocirc;n Thần. Được triều đ&igrave;nh Huế cho thiết lập miếu thờ phụng tại xứ Tam Giang, gần bến đ&ograve; Ca C&uacute;t. Mộ an t&aacute;ng tại B&agrave;u R&ecirc;u L&agrave;ng Kế M&ocirc;n. Ng&agrave;y Kỵ &Ocirc;ng v&agrave; B&agrave; đều kh&ocirc;ng r&otilde;.</p>\n<p>Ng&agrave;i sanh hạ được 3 người con Trai:&nbsp;&nbsp;</p>\n<ol>\n<li>Ho&agrave;ng Ngọc Qu&acirc;n</li>\n<li>Ho&agrave;ng Ngọc Lượng</li>\n<li>Ho&agrave;ng Ngọc Đ&agrave;m</li>\n</ol>\n<p>Mộ của cả 3 Ng&agrave;i được an t&aacute;ng chung một huyệt- Mộ Ng&agrave;i Ngọc Qu&acirc;n ở giữa cao một m&eacute;t.- Mộ Ng&agrave;i Ngọc Lượng ở ph&iacute;a tả v&agrave; Ngọc Đ&agrave;m ph&iacute;a hửu.</p>\n<p>Mộ nằm tại Bắc B&agrave;u M&ocirc;n- Xứ Thượng động toạ c&agrave;n hương tốn ki&ecirc;m Hợi Tỵ. Ng&agrave;y Kỵ kh&ocirc;ng r&otilde;.</p>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:03:53',1,'2017-11-01 15:10:36',1),
	(4,3,1,1,2,0,0,'Hoàng Ngọc Tiên','',1,0,0,1,0,X'',NULL,'',NULL,'',0,'',NULL,NULL,NULL,NULL,1,'2017-11-01 15:05:05',1,'2017-11-01 22:07:26',0),
	(5,3,1,1,3,0,0,'Hoàng Ngọc Quân','',1,0,0,1,0,X'',NULL,'',NULL,'',0,'<p>Sanh được một người con Trai:&nbsp;&nbsp;</p>\n<ol>\n<li>Ho&agrave;ng Ngọc Tường (Hu&yacute; Chưởng)</li>\n<li>Ho&agrave;ng Ngọc Lượng (V&Ocirc; TỰ)</li>\n<li><span style=\"font-size: 11pt;\">Ho&agrave;ng Ngọc Đ&agrave;m&nbsp;</span>(V&Ocirc; TỰ)</li>\n</ol>\n<p>Mộ của ba Ng&agrave;i được ch&ocirc;n cất tại ph&iacute;a Đ&ocirc;ng Độn Lệ Tải gần B&agrave;u M&ocirc;n, mỗi mộ c&aacute;ch nhau 5 thước. ba ng&ocirc;i mộ n&agrave;y hiện nay được l&agrave;m chung th&agrave;nh một ng&ocirc;i mộ lớn gọi l&agrave; :&rdquo;Ba Chừng&rdquo;</p>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:06:16',1,'2017-11-01 15:10:21',1),
	(6,4,1,1,4,0,0,'Hoàng Ngọc Nam','',1,0,0,1,0,X'',NULL,'',NULL,'',0,'<p>Sanh hạ được một người con Trai:&nbsp;</p>\n<ol>\n<li>Ho&agrave;ng Ngọc Mậu</li>\n</ol>\n<p>* Th&agrave;nh lập Họ Ho&agrave;ng tr&ecirc;n (vai Anh)</p>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:07:25',1,'2017-11-01 22:11:21',1),
	(7,4,1,1,5,0,0,'Hoàng Ngọc Tướng','Huý Chủng',1,0,0,1,0,X'',NULL,'',NULL,'',0,'<p>Sanh hạ được 3 Trai:&nbsp;&nbsp;</p>\n<ol>\n<li>Ho&agrave;ng Ngọc Diễm&nbsp; (Hu&yacute; Khảm)</li>\n<li><span style=\"font-size: 11pt;\">Ho&agrave;ng Ngọc Thạ </span><span style=\"font-size: 11pt;\">(Hu&yacute; Dưỡng)</span></li>\n<li>Ho&agrave;ng Ngọc Bằng (Hu&yacute; Tề)&nbsp;</li>\n</ol>\n<p>Ng&agrave;i c&oacute; nu&ocirc;i một C&ocirc;ng Ch&uacute;a t&ecirc;n Ho&agrave;ng Thị Chương, kh&ocirc;ng r&otilde; ng&agrave;y mất. Mộ được ch&ocirc;n dưới ch&acirc;n Mộ Ng&agrave;i, tục gọi Mộ B&agrave; Ch&uacute;a.</p>\n<p>* Kỵ &Ocirc;ng ng&agrave;y 25/8 v&agrave; 26/8 &Acirc;m lịch- Kỵ B&agrave; ng&agrave;y 12/9 -13/9</p>\n<p>* Mộ &Ocirc;ng v&agrave; Mộ B&agrave; ch&ocirc;n chung tại xứ B&agrave;u M&ocirc;n, toạ Đ&ocirc;ng&nbsp; Bắc, hướng T&acirc;y Nam.&nbsp;</p>\n<p><strong>Ch&uacute; th&iacute;ch:</strong></p>\n<p>Họ Ho&agrave;ng L&agrave;ng Kế M&ocirc;n đến đời thứ Tư, th&igrave; c&aacute;c Ng&agrave;i chia ra th&agrave;nh 2 Chi lớn, rồi sau đ&oacute; lập th&agrave;nh 2 Họ.</p>\n<p>* Họ Ho&agrave;ng &ldquo;ph&iacute;a tr&ecirc;n&rdquo; do Ng&agrave;i Ho&agrave;ng Ngọc Nam (ch&aacute;u của Ng&agrave;i Ho&agrave;ng Ngọc Ca ) đứng đầu lập họ, tại gần r&uacute; tr&ecirc;n x&oacute;m họ Nguyễn. Ban đầu nh&agrave; Thờ lợp bằng tranh. Về sau, con ch&aacute;u của Ng&agrave;i Ho&agrave;ng Ngọc Mậu đứng ra lập gia phả để lưu chiếu. Nơi đ&acirc;y cũng lưu giữ hai Sắc Phong để phụng thờ.&nbsp;</p>\n<p>* Họ Ho&agrave;ng &ldquo;ph&iacute;a dưới &ldquo; vai em, do Ng&agrave;i Ho&agrave;ng Ngọc Tướng&nbsp; (ch&aacute;u của Ng&agrave;i Ho&agrave;ng Ngọc Cửa ) đứng đầu lập họ. Nh&agrave; Thờ được lợp bằng ng&oacute;i rất khang trang, nằm s&aacute;t Đường Ngang, sau Đ&igrave;nh L&agrave;ng, cạnh họ Trần Duy.Về sau, do c&aacute;c con ch&aacute;u của 3 Ng&agrave;i : Ho&agrave;ng Ngọc Diễm, Ho&agrave;ng Ngọc Thạt v&agrave; Ho&agrave;ng Ngọc Bằng, đứng ra lập gia phả để lưu chiếu. Gia phả mới hiện nay đ&atilde; được chỉnh đốn lại (1986) v&agrave; được l&agrave;m bằng vải để Thờ ở Họ. Ngo&agrave;i ra, c&ograve;n được sao ch&eacute;p th&agrave;nh nhiều tập, viết tay hoặc in bằng chử quốc ngữ để lưu giử.</p>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:09:38',1,'2017-11-02 10:08:59',1),
	(8,5,1,1,6,0,0,'Hoàng Ngọc Mậu','',1,0,0,0,0,X'',NULL,'',NULL,'',0,'<p>* Ng&agrave;i c&ugrave;ng th&acirc;n phụ đứng ra th&agrave;nh lập họ Ho&agrave;ng tr&ecirc;n.</p>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:11:21',1,'2017-11-01 22:11:21',0),
	(9,5,1,1,7,0,0,'Hoàng Ngọc Diễm','Huý Khảm',1,0,0,0,0,X'',NULL,'',NULL,'',0,'<p>Sanh hạ được 3 Trai:&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n<ol>\n<li>Ho&agrave;ng Ngọc Thập &nbsp;(Hu&yacute; Lượng)</li>\n<li>Ho&agrave;ng Ngọc Nhị&nbsp; (Hu&yacute; Cận)</li>\n<li>Ho&agrave;ng Ngọc Tam&nbsp; (Hu&yacute; Nhật)</li>\n</ol>\n<p>* Mộ được an t&aacute;ng tại bắc B&agrave;u M&ocirc;n c&ugrave;ng Mộ B&agrave;, toạ qu&yacute;&nbsp; hướng Đinh</p>\n<p>* Kỵ &Ocirc;ng ng&agrave;y 25 v&agrave; 26 th&aacute;ng 11 &Acirc;m Lịch</p>\n<p>* Kỵ B&agrave; khồng r&otilde;.</p>',NULL,NULL,NULL,NULL,1,'2017-11-01 15:12:46',1,'2017-11-01 22:12:46',0);

/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) DEFAULT '0',
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`user_id`, `archived`, `full_name`, `email`, `username`, `password`)
VALUES
	(1,0,'Hoàng Đăng Quang','hodawa@gmail.com','admin','f30ce8b85395a84f75461a5081e7b391'),
	(3,0,'Hoàng Ngọc Huy','hoangcqhuy@gmail.com','hoangcqhuy@gmail.com','68d321b9cb274462034bee6cc658d50a'),
	(4,0,'Hoàng Đăng Khoa','hoangdangquang@yahoo.com','hoangdangquang@yahoo.com','f30ce8b85395a84f75461a5081e7b391');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
