# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.35)
# Database: family-book
# Generation Time: 2017-11-01 03:24:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acl_permit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_permit`;

CREATE TABLE `acl_permit` (
  `permit_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `role` varchar(100) NOT NULL DEFAULT '',
  `resource` varchar(100) NOT NULL DEFAULT '',
  `permit` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`permit_id`),
  KEY `role` (`role`),
  KEY `resource` (`resource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_permit` WRITE;
/*!40000 ALTER TABLE `acl_permit` DISABLE KEYS */;

INSERT INTO `acl_permit` (`permit_id`, `archived`, `role`, `resource`, `permit`)
VALUES
	(1,0,'guest','home','view'),
	(2,0,'guest','blog','view'),
	(3,0,'guest','shop','view'),
	(4,0,'user','blog','add'),
	(5,0,'user','blog','delete'),
	(6,0,'user','blog','edit'),
	(7,0,'user','blog','comment'),
	(8,0,'user','shop','edit');

/*!40000 ALTER TABLE `acl_permit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_resource
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_resource`;

CREATE TABLE `acl_resource` (
  `resource_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`resource_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_resource` WRITE;
/*!40000 ALTER TABLE `acl_resource` DISABLE KEYS */;

INSERT INTO `acl_resource` (`resource_id`, `archived`, `name`, `label`, `description`)
VALUES
	(1,0,'home','Home',NULL),
	(2,0,'blog','Blogs',NULL),
	(3,0,'shop','Shop',NULL),
	(4,0,'user','User',NULL);

/*!40000 ALTER TABLE `acl_resource` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_role`;

CREATE TABLE `acl_role` (
  `role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL DEFAULT '',
  `label` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_role` WRITE;
/*!40000 ALTER TABLE `acl_role` DISABLE KEYS */;

INSERT INTO `acl_role` (`role_id`, `parent_id`, `archived`, `name`, `label`, `description`)
VALUES
	(1,0,0,'guest','Guest',NULL),
	(2,1,0,'user','User',NULL),
	(3,2,0,'admin','Administrator',NULL),
	(4,3,0,'dev','Developer',NULL);

/*!40000 ALTER TABLE `acl_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_user_role`;

CREATE TABLE `acl_user_role` (
  `user_role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acl_user_role` WRITE;
/*!40000 ALTER TABLE `acl_user_role` DISABLE KEYS */;

INSERT INTO `acl_user_role` (`user_role_id`, `user_id`, `role_id`)
VALUES
	(1,1,2),
	(2,1,3);

/*!40000 ALTER TABLE `acl_user_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `member_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `generation` int(11) unsigned NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `in_family` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `father_id` int(11) unsigned NOT NULL DEFAULT '0',
  `mother_id` int(11) unsigned NOT NULL DEFAULT '0',
  `spouse_id` int(11) unsigned NOT NULL DEFAULT '0',
  `full_name` varchar(200) NOT NULL DEFAULT '',
  `note` varchar(500) DEFAULT NULL,
  `gender` tinyint(11) NOT NULL DEFAULT '0' COMMENT '1: male, 2:female',
  `daughter_in_law` tinyint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Only use for female (1: daugther in law 2: daugther)',
  `son_in_law` tinyint(4) NOT NULL DEFAULT '0',
  `num_son` tinyint(11) unsigned NOT NULL DEFAULT '0',
  `num_daughter` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `dob` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `dob_lunar` varchar(20) DEFAULT NULL,
  `dod` varchar(20) DEFAULT NULL,
  `dod_lunar` varchar(20) DEFAULT NULL,
  `burial_place` varchar(200) DEFAULT NULL,
  `photo_id` int(200) unsigned NOT NULL DEFAULT '0',
  `other_info` text,
  `row_active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT '0',
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;

INSERT INTO `member` (`member_id`, `generation`, `position`, `in_family`, `father_id`, `mother_id`, `spouse_id`, `full_name`, `note`, `gender`, `daughter_in_law`, `son_in_law`, `num_son`, `num_daughter`, `dob`, `dob_lunar`, `dod`, `dod_lunar`, `burial_place`, `photo_id`, `other_info`, `row_active`, `created_date`, `created_by`, `modified_date`, `modified_by`)
VALUES
	(1,1,1,1,0,0,0,'Hoàng Ngọc Khắc','',1,0,0,2,0,X'',NULL,'',NULL,'',0,'',1,'2017-10-27 07:41:54',1,'2017-10-30 07:10:49',1),
	(2,2,1,1,1,0,0,'Hoàng Ngoc Ca','',1,0,0,1,0,X'',NULL,'',NULL,'',0,'<p>aaaaaaaaaaaaaaaaaa</p>',1,'2017-10-27 07:50:56',1,'2017-10-28 22:08:03',1),
	(3,2,2,1,1,0,0,'Hoàng Ngọc Cửa','',1,0,0,3,0,X'',NULL,'',NULL,'',0,'',1,'2017-10-27 07:52:10',1,'2017-10-29 06:12:21',1),
	(4,3,1,1,2,0,0,'Hoàng Ngoc Tiên','',1,0,0,0,0,X'',NULL,'',NULL,'',0,'',1,'2017-10-29 06:08:03',1,'2017-10-28 22:08:03',0),
	(5,3,1,1,3,0,0,'Hoàng Ngọc Quân','',1,0,0,0,0,X'',NULL,'',NULL,'',0,'',1,'2017-10-29 06:11:24',1,'2017-10-28 22:11:24',0),
	(6,3,2,1,3,0,0,'Hoàng Ngọc Lượng','',1,0,0,0,0,X'',NULL,'',NULL,'',0,'',1,'2017-10-29 06:11:45',1,'2017-10-28 22:11:45',0),
	(7,3,3,1,3,0,0,'Hoàng Ngọc Đàm','',1,0,0,2,1,X'',NULL,'',NULL,'',0,'',1,'2017-10-29 06:12:01',1,'2017-10-31 19:22:09',1);

/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archived` tinyint(4) DEFAULT '0',
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`user_id`, `archived`, `full_name`, `email`, `username`, `password`)
VALUES
	(1,0,'Hoàng Đăng Quang','hodawa@gmail.com','admin','aa41efe0a1b3eeb9bf303e4561ff8392');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
