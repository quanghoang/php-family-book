<?php

include 'vendor/autoload.php';
$detect = new Mobile_Detect();
$theme  = isset($_GET['ios']) ? 'ios' : (isset($_GET['android']) ? 'material' :
          ($detect->isiOS()   ? 'ios' : ($detect->isAndroidOS()  ? 'material' : 'ios')));

//App\Test::hello();

$settings = array(
    'app_title' => 'Mobile App',
    'theme'     => $theme
);
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="application-name" content="<?= $settings['app_title'] ?>"/>
    <meta name="msapplication-task" content="name=Login;action-uri=login.php;icon-uri=favicon.ico"/>

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="media/icon/apple-icon-57x57-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="media/icon/apple-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="media/icon/apple-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="media/icon/apple-icon-144x144-precomposed.png" />
    <title><?= $settings['app_title'] ?></title>
    <link rel="stylesheet" type="text/css" href="media/fortawesome/4.7.0/css/font-awesome.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="media/framework7/icons/0.8.9/css/framework7-icons.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/mobile.css" media="screen" />

    <script src="js/jspm_packages/system.js"></script>
    <script src="js/config.js"></script>

    <script type="text/javascript">
        var settings = <?= json_encode($settings) ?>;
        switch (settings.theme) {
            case 'ios':
                System.import('app/base/ui-f7-ios');
                break;

            case 'material':
                System.import('app/base/ui-f7-android');
                break;
        }
    </script>
</head>

<body>
    <div id="flash-screen">
        <div class="content">
            <h1><?= $settings['app_title'] ?></h1>
            <p><i class="fa fa-spin fa-spinner fa-2x"></i></p>
        </div>
    </div>
</body>
</html>