<?php
return array(
    'app' => array(
        'env'      => APP_ENV,
        'title'    => 'Gia Phả Hoàng Ngọc',
        'host'     => 'http://localhost:8888',
        'rootDir'  => realpath(__DIR__ . '/../../'),
        'relUrl'   => '/php-family-book/',
        'absUrl'   => '[$app.host][$app.relUrl]',
        'mediaUrl' => '[$app.relUrl]meida/',
	    'homePage' => 'doi-loi',
	    'version'  => '1.0.0'
    ),

    'menu' => include('menu.php'),

    'theme' => array(
        'default' => 'default',
        'folder'  => '[$app.rootDir]/app/theme',
        'options' => array(
            'debug'  => true,
            'cache'  => false,
        )
    ),

    'db' => array(
        'default' => array(
            'driver'     => 'mysql',
            'host'       => '127.0.0.1',
            'user'       => 'root',
            'password'   => 'root',
            'database'   => 'family-book',
            'fetchMode'  => \PDO::FETCH_ASSOC,
            'charset'    => 'utf8',
            'port'       => 8889
        ),

        'opencart' => array(
            'driver'     => 'mysql',
            'host'       => 'localhost',
            'user'       => 'root',
            'password'   => 'root',
            'database'   => 'opencart',
            'fetchMode'  => \PDO::FETCH_ASSOC,
            'charset'    => 'utf8',
            'port'       => 8889
        )
    )
);