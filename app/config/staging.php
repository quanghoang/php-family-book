<?php
return array(
	'app' => array(
		'env'      => APP_ENV,
		'title'    => 'Gia Phả Hoàng Ngọc',
		'host'     => 'http://giapha.duckdns.org:8000',
		'rootDir'  => realpath(__DIR__ . '/php-family-book/'),
		'relUrl'   => '/',
		'absUrl'   => '[$app.host][$app.relUrl]',
		'mediaUrl' => '[$app.relUrl]meida/',
		'homePage' => 'gioi-thieu',
	),

	'menu' => include('menu.php'),

	'theme' => array(
		'default' => 'default',
		'folder'  => '[$app.rootDir]/app/theme',
		'options' => array(
			'debug'  => true,
			'cache'  => false,
		)
	),

	'db' => array(
		'default' => array(
			'driver'     => 'mysql',
			'host'       => '127.0.0.1',
			'user'       => 'root',
			'password'   => 'AsdfA1234',
			'database'   => 'family-book',
			'fetchMode'  => \PDO::FETCH_ASSOC,
			'charset'    => 'utf8',
			'port'       => 3306
		),
    )
);