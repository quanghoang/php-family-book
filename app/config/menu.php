<?php

switch (APP_ENV) {
	case 'dev':
		return array(
			'doi-loi' => 'Đôi lời',
			'pha-ky' => 'Phả ký',
			'pha-he' => 'Phả hệ',
			'pha-do' => 'Phả đồ',
			'chu-pha' => 'Chú phả', //Phả phụ, chú phả
		);

	case 'prod':
		return array(
			'doi-loi' => 'Đôi lời',
			'pha-ky' => 'Phả ký',
			'pha-he' => 'Phả hệ',
			'pha-do' => 'Phả đồ',
			'chu-pha' => 'Chú phả',
		);
}

