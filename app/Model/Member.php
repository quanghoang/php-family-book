<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/19/17
 * Time: 4:41 PM
 */

namespace App\Model;


class Member extends \System\Model {

	public static function getMemberList() {
		$sql = 'SELECT member_id, generation, position, full_name, note, gender, num_son, num_daughter, father_id, 
					   mother_id, spouse_id, dob, dod, photo_id,  burial_place, other_info, daughter_in_law, son_in_law,
					   address, phone, email, website
				FROM member WHERE row_active=1 AND in_family=1
				ORDER BY generation, father_id, position';
		return self::db()->fetchRowMany($sql);
	}

	public static function getAllMaleFamilyMembers() {
		$sql = 'SELECT * FROM member 
				WHERE row_active=1 AND daughter_in_law=0 AND son_in_law=0
				ORDER BY generation, father_id, position';
		return self::db()->fetchRowMany($sql);
	}

	public static function getDaughterInLawList() {
		$sql = 'SELECT * FROM member WHERE row_active=1 AND daughter_in_law=1';
		return self::db()->fetchRowMany($sql);
	}

	public static function getMemberInLawList() {
		$sql = 'SELECT * FROM member WHERE row_active=1 AND (daughter_in_law=1 OR son_in_law=1)';
		return self::db()->fetchRowMany($sql);
	}

	public static function getMemberById($memberId) {
		$sql = 'SELECT * FROM member WHERE member_id = :member_id';
		return self::db()->fetchRow($sql, [
			'member_id' => $memberId
		]);
	}

	public static function getChildrenList($memberId) {
		$sql = 'SELECT * FROM member 
				WHERE row_active=1 AND (father_id=:member_id OR mother_id=:member_id)
				ORDER BY  position, mother_id';
		return self::db()->fetchRowMany($sql, [
			'member_id' => $memberId
		]);
	}

	public static function getChildrenNames($memberId) {
		$children   = self::getChildrenList($memberId);
		$childNames = [];
		foreach ($children as $child) {
			$childNames[] = $child['full_name'];
		}
		return $childNames;
	}

	public static function getMemberNameById($memberId) {
		$member = self::getMemberById($memberId);
		return $member ? $member['full_name'] : '';
	}

	public static function getFatherNames() {
		$sql  = 'SELECT DISTINCT full_name FROM member WHERE row_active=1 AND gender=1 
				 ORDER BY generation, position';
		$rows = self::db()->fetchRowMany($sql);
		return \Helper\Arrays::extractValues($rows, 'full_name');
	}

	public static function getMemberByFatherId($fatherId) {
		if (!$fatherId) {
			return [];
		}
		$sql = 'SELECT * FROM member WHERE row_active=1 AND father_id=:father_id ORDER BY position';
		return self::db()->fetchRowMany($sql, array(
			'father_id' => $fatherId
		));
	}

	public static function getMemberByMotherId($motherId) {
		if (!$motherId) {
			return [];
		}
		$sql = 'SELECT * FROM member WHERE row_active=1 AND mother_id=:mother_id ORDER BY position';
		return self::db()->fetchRowMany($sql, array(
			'mother_id' => $motherId
		));
	}

	public static function getMemberBySpouseId($memberId) {
		$member = self::getMemberById($memberId);
		if (!$member) {
			return [];
		}

		if ($member['daughter_in_law'] || $member['son_in_law']) {
			$row = self::getMemberById($member['spouse_id']);
			return $row ? [$row] : [];
		}

		$sql = 'SELECT * FROM member 
			    WHERE row_active=1 AND spouse_id=:spouse_id 
			    ORDER BY position, member_id';
		return self::db()->fetchRowMany($sql, array(
			'spouse_id' => $memberId
		));
	}

	public static function saveMember($data) {
		if ($data['id'] == 'new') {
			$data['member_id'] = self::addMember($data);
		}
		else {
			self::updateMember($data);
		}
		return $data;
	}

	public static function countChildrenByFatherId($fatherId) {
		$rows = self::getMemberByFatherId($fatherId);
		return $rows ? count($rows) : 0;
	}

	public static function countSpousesByMemberId($memberId) {
		$rows = self::getMemberBySpouseId($memberId);
		return $rows ? count($rows) : 0;
	}

	public static function getNextChildPosition($parentId) {
		return count(self::getChildrenList($parentId)) + 1;
	}

	public static function getNextSpousePosition($memberId) {
		return self::countSpousesByMemberId($memberId) + 1;
	}

	public static function countChildren($memberId) {
		$result = ['son' => 0, 'daughter' => 0];
		$rows   = self::getChildrenList($memberId);
		if (!$rows) {
			return $result;
		}

		foreach ($rows as $row) {
			$result['son']      += $row['gender'] == 1 ? 1 : 0;
			$result['daughter'] += $row['gender'] == 2 ? 1 : 0;
		}
		return $result;
	}

	public static function updateChildren($memberId) {
		$count = self::countChildren($memberId);
		self::db()->update('member', ['member_id' => $memberId], [
			'num_son'      => $count['son'],
			'num_daughter' => $count['daughter'],
		]);
	}

	public static function addMember($data) {
		return self::db()->insert('member', [
			'generation'      => $data['generation'],
			'position'        => $data['position'],
			'in_family'       => $data['in_family'],
			'father_id'       => $data['father_id'],
			'mother_id'       => $data['mother_id'],
			'spouse_id'       => $data['spouse_id'],
			'full_name'       => $data['full_name'],
			'note'            => $data['note'],
			'gender'          => $data['gender'],
			'daughter_in_law' => $data['daughter_in_law'],
			'son_in_law'      => $data['son_in_law'],
			'num_son'         => 0,
			'num_daughter'    => 0,
			'dob'             => $data['dob'],
			'dod'             => $data['dod'],
			'burial_place'    => $data['burial_place'],
			'photo_id'        => $data['photo_id'],
			'other_info'      => $data['other_info'],
			'address'         => $data['address'],
			'email'           => $data['email'],
			'phone'           => $data['phone'],
			'website'         => $data['website'],
			'created_date'    => self::now(),
			'created_by'      => self::getUserId()
		]);
	}

	public static function updateMember($data) {
		self::db()->update('member', ['member_id' => $data['member_id']], [
			'generation'      => $data['generation'],
			'position'        => $data['position'],
			'in_family'		  => $data['in_family'],
			'father_id'       => $data['father_id'],
			'mother_id'       => $data['mother_id'],
			'spouse_id'       => $data['spouse_id'],
			'full_name'       => $data['full_name'],
			'note'            => $data['note'],
			'gender'          => $data['gender'],
			'daughter_in_law' => $data['daughter_in_law'],
			'son_in_law'      => $data['son_in_law'],
			'num_son'         => $data['num_son'],
			'num_daughter'    => $data['num_daughter'],
			'dob'             => $data['dob'],
			'dod'             => $data['dod'],
			'burial_place'    => $data['burial_place'],
			'photo_id'        => $data['photo_id'],
			'other_info'      => $data['other_info'],
			'address'         => $data['address'],
			'email'           => $data['email'],
			'phone'           => $data['phone'],
			'website'         => $data['website'],
			'modified_date'   => self::now(),
			'modified_by'     => self::getUserId()
		]);
	}

	public static function deleteMember($memberId) {
		self::db()->update('member', ['member_id' => $memberId], [
			'row_active'    => 0,
			'modified_date' => self::now(),
			'modified_by'   => self::getUserId()
		]);
	}

	public static function getMaxGeneration() {
		$sql = "SELECT max(generation) max_generation FROM member WHERE row_active=1 AND in_family=1";
		$row = self::db()->fetchRow($sql);
		return $row ? $row['max_generation'] : 0;
	}
}