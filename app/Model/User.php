<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/19/17
 * Time: 4:05 PM
 */

namespace App\Model;


class User extends \System\Model {

    public static function getUser($username, $password) {
        $sql = 'SELECT * FROM user WHERE (username = :username OR email = :username) 
					  AND password = :password AND archived=0';
        return self::db()->fetchRow($sql, array(
            'username' => $username,
            'password' => $password
        ));
    }

	public static function getUserById($userId) {
		$sql = 'SELECT * FROM user WHERE user_id = :user_id';
		return self::db()->fetchRow($sql, array('user_id' => $userId));
	}

	public static function getUserByEmail($email) {
		$sql = 'SELECT * FROM user WHERE email = :email and archived=0';
		return self::db()->fetchRow($sql, array('email' => $email));
	}

	public static function getUserByName($name) {
		$sql = 'SELECT * FROM user WHERE full_name = :full_name and archived=0';
		return self::db()->fetchRow($sql, array('full_name' => $name));
	}

	public static function addUser($data) {
    	self::db()->insert('user', array(
    		'full_name' => $data['name'],
			'email'     => $data['email'],
		    'username'  => $data['email'],
		    'password'  => md5($data['password'])
	    ));
	}
}