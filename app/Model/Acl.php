<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/20/17
 * Time: 12:12 PM
 */

namespace app\Model;


class Acl extends \System\Model {

    public static function getAllRoles() {
        $sql = 'SELECT `name` FROM acl_role WHERE archived=0';
        return self::db()->fetchColumnMany($sql);
    }

    public static function getAllResources() {
        $sql = 'SELECT `name` FROM acl_resource WHERE archived=0';
        return self::db()->fetchColumnMany($sql);
    }

    public static function getRolePermits($role) {
        $sql = 'SELECT role, resource, permit FROM acl_permit WHERE role=:role AND archived=0';
        return self::db()->fetchRowMany($sql, array('role' => $role));
    }

    public static function getRoleByName($role) {
        $sql = 'SELECT * FROM acl_role WHERE `name` = :name';
        return self::db()->fetchRow($sql, array('name' => $role));
    }

    public static function getRoleById($roleId) {
        $sql = 'SELECT * FROM acl_role WHERE role_id = :role_id';
        return self::db()->fetchRow($sql, array('role_id' => $roleId));
    }

    public static function getRoleParentName($role) {
        $row = self::getRoleByName($role);
        if (!$role || !$row['parent_id']) {
            return null;
        }
        $row = self::getRoleById($row['parent_id']);
        return $row ? $row['name'] : null;
    }

    public static function getUserRoles($userId) {
        $sql = 'SELECT r.name FROM acl_user_role u 
                JOIN acl_role r ON u.role_id = r.role_id
                WHERE user_id = :user_id AND r.archived=0';
        return self::db()->fetchColumnMany($sql, array('user_id' => $userId));
    }
}