<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/13/17
 * Time: 2:49 PM
 */

namespace App\API;

class Test extends \System\API {

	public function hello() {
		return [
			'type' => 'text',
			'label' => 'Hello',
			'name' => 'hello',
			'container' => 'form_container'
		];
	}

}