<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/13/17
 * Time: 6:31 PM
 */

namespace App\API;

use \App\Model;

class Login extends \System\API {

	public function getRegistryForm() {
		return [
			'type'           => 'form',
			'container'      => 'form_container',
			'validateMethod' => 'Login.validateRegistryForm',
			'saveMethod'     => 'Login.saveRegistryForm',
			'ui' => [
				['type' => 'text', 'name' => 'email', 'label' => 'Địa chỉ Email'],
				['type' => 'text', 'name' => 'name', 'label' => 'Họ và Tên'],
				['type' => 'password', 'name' => 'password', 'label' => 'Mật khẩu'],
				['type' => 'password', 'name' => 'confirm-password', 'label' => 'Nhập lại mật khẩu'],
				['type' => 'flow', 'class' => 'text-right', 'ui' => [
					['type' => 'submit', 'name' => 'registry', 'label' => 'Đăng ký', 'class' => 'primary'],
					['type' => 'button', 'name' => 'cancel', 'label' => 'Huỷ bỏ'],
				]]
			]
		];
	}

	public function validateRegistryForm($data) {
		$valid     = true;
		$message   = [];
		$highlight = [];

		if (!$data['email']) {
			$valid       = false;
			$message[]   = 'Vui lòng nhập Địa chỉ Email.';
			$highlight[] = 'email';
		}
		else if (!\Helper\Validator::isEmail($data['email'])) {
			$valid       = false;
			$message[]   = 'Địa chỉ Email không hợp lệ.';
			$highlight[] = 'email';
		}
		else if (Model\User::getUserByEmail($data['email'])) {
			$valid       = false;
			$message[]   = 'Địa chỉ Email đã được sử dụng. Vui lòng sử dụng email khác.';
			$highlight[] = 'email';
		}

		$data['name'] = trim($data['name']);
		if (strlen($data['name']) < 5) {
			$valid       = false;
			$message[]   = 'Họ và Tên phải có tối thiểu 5 ký tự.';
			$highlight[] = 'name';
		}
		else if (Model\User::getUserByName($data['name'])) {
			$valid       = false;
			$message[]   = 'Họ và Tên đã được đăng ký.';
			$highlight[] = 'name';
		}

		if (strlen($data['password']) < 5) {
			$valid       = false;
			$message[]   = 'Mật khẩu phải có tối thiểu 5 ký tự.';
			$highlight[] = 'password';
		}
		else if ($data['password'] != $data['confirm-password']) {
			$valid       = false;
			$message[]   = 'Nhập lại mật khẩu không khớp.';
			$highlight[] = 'confirm-password';
		}

		return ['valid' => $valid, 'message' => $message, 'highlight' => $highlight];
	}

	public function saveRegistryForm($data) {
		if (!$this->validateRegistryForm($data)['valid']) {
			return false;
		}
		\App\Model\User::addUser($data);
		return true;
	}


	public function getForgotPasswordForm() {
		return [
			'type'           => 'form',
			'container'      => 'form_container',
			'validateMethod' => 'Login.validateForgotPasswordForm',
			'saveMethod'     => 'Login.saveForgotPasswordForm',
			'ui' => [
				['type' => 'text', 'name' => 'email', 'label' => 'Địa chỉ Email',
			        'holder' => 'Nhập email đã đăng ký để nhận thông tin mật khẩu'],
				['type' => 'flow', 'class' => 'text-right', 'ui' => [
					['type' => 'submit', 'name' => 'registry', 'label' => 'Đồng ý', 'class' => 'primary'],
					['type' => 'button', 'name' => 'cancel', 'label' => 'Huỷ bỏ'],
				]]
			]
		];
	}

	public function validateForgotPasswordForm($data) {
		$valid     = true;
		$message   = [];
		$highlight = [];

		if (!$data['email']) {
			$valid       = false;
			$message[]   = 'Vui lòng nhập Địa chỉ Email.';
			$highlight[] = 'email';
		}
		else if (!\Helper\Validator::isEmail($data['email'])) {
			$valid       = false;
			$message[]   = 'Địa chỉ Email không hợp lệ.';
			$highlight[] = 'email';
		}
		else if (!Model\User::getUserByEmail($data['email'])) {
			$valid       = false;
			$message[]   = 'Không tìm thấy Địa chỉ Email trong hệ thống.';
			$highlight[] = 'email';
		}
		return ['valid' => $valid, 'message' => $message, 'highlight' => $highlight];
	}

	public function saveForgotPasswordForm($data) {
		//TODO: Send forgot-password email
	}
}