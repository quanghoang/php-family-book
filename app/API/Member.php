<?php
/**
 * Created by PhpStorm.
 * User: quanghoang
 * Date: 10/17/17
 * Time: 10:55 PM
 */

namespace App\API;
use \App\Model;

class Member extends \System\API {

	public function getMemberGrid() {
		return [
			'type'      => 'panel',
			'label'     => 'Phả hệ Gia phả Hoàng Ngọc',
			'container' => 'grid-container',
			'accordion' => true,
			'ui'        => [
				[
					'type'             => 'editgrid',
					'loadGridMethod'   => 'Member.loadGrid',
					'loadDialogMethod' => 'Member.getMemberDialog',
					'validateMethod'   => 'Member.validateMember',
					'saveMethod'       => 'Member.saveMember',
					'deleteMethod'     => 'Member.deleteMember',
					'deleteMessage'    => 'Bạn có muốn xoá thành viên này không?',
				]
			]
		];
	}

	public function getGridData() {
		$members = Model\Member::getMemberList();

		foreach ($members as &$member) {
			if ($member['father_id']) {
				$member['father_name'] = Model\Member::getMemberNameById($member['father_id']);
			}
			if ($member['mother_id']) {
				$member['mother_name'] = Model\Member::getMemberNameById($member['mother_id']);
			}
		}

		//update member spouses
		$daughterInLaws = Model\Member::getMemberInLawList();
		$spouseIds = \Helper\Arrays::extractDistinctValues($daughterInLaws, 'spouse_id');
		foreach ($spouseIds as $spouseId) {
			$spouses = Model\Member::getMemberBySpouseId($spouseId);

			if ($spouses) {
				foreach ($members as &$member) {
					if ($member['member_id'] == $spouseId) {
						$spouseNames = \Helper\Arrays::extractDistinctValues($spouses, 'full_name');
						$member['spouses'] = $spouses;
						$member['spouse_name'] = implode(', ', $spouseNames);
						break;
					}
				}
			}
		}
		return $members;
	}


	public function getMemberRow($data) {
		$memberId = isset($data['member_id']) ? $data['member_id'] : 0;
		$member   = Model\Member::getMemberById($memberId);
		if (!$member || !$member['row_active']) {
			return [];
		}

		$spouses               = Model\Member::getMemberBySpouseId($memberId);
		$spouseNames           = \Helper\Arrays::extractDistinctValues($spouses, 'full_name');
		$member['father_name'] = Model\Member::getMemberNameById($member['father_id']);
		$member['mother_name'] = Model\Member::getMemberNameById($member['mother_id']);
		$member['spouses']     = $spouses;
		$member['spouse_name'] = implode(', ', $spouseNames);
		return $member;
	}

	private function createMemberOption($member) {
		if (!$member) {
			return $this->createOption(0, '-');
		}
		$label = sprintf('Đời thứ %s: %s', $member['generation'], $member['full_name']);
		return $this->createOption($member['member_id'], $label);
	}

	private function createMemberOptionList($members) {
		$result  = [$this->createOption('', '')];
		foreach ($members as $member) {
			$result[] = $this->createMemberOption($member);
		}
		return $result;
	}

	private function createMemberOptionById($memberId) {
		$member = Model\Member::getMemberById($memberId);
		return $this->createMemberOption($member);
	}

	/**
	 * Get father dropdown option list
	 * @param $data
	 * @return array
	 */
	public function getFatherOptionList($data) {
		/*
		Case 1: Add Member from toolbar button:
		- 1.1. Gender = Male (Son): List of all male family members.
		- 1.2. Gender = Female:
		    + 1.2.1. Daughter:  List of all male family members.
		    + 1.2.2. Daughter In Law: N/A.

		Case 2: Add Member from grid button:
		- 2.1. Add from Male row:
		    + 2.1.1. Gender = Male (Son): Only the male member (disable select).
		    + 2.1.2. Gender = Female:
		           - 2.1.2.1. Daughter: Only the male member (disable select).
		           - 2.1.2.2. Daughter In Law: N/A.
		- 2.2. Add from Female row:
		    + 2.2.1. Gender = Male:
		          - 2.2.1.1. Son: List of spouses from the female member.
		          - 2.2.1.1. Son In Law: N/A.
		    + 2.2.2. Gender = Female (Daughter): List of spouses from the female member.
		*/

		$addMemberId = isset($data['add_member_id']) ? $data['add_member_id'] : 0;
		$addMember   = Model\Member::getMemberById($addMemberId);

		//Case 1: Add from toolbar
		if (!$addMemberId) {
			//1.1. Add son
			if ($data['gender'] == 1) {
				return $this->createMemberOptionList(Model\Member::getAllMaleFamilyMembers());
			}
			else {
				////1.2.1. Add daughter
				if (isset($data['daughter_in_law']) && $data['daughter_in_law']) {
					return $this->createMemberOptionList(Model\Member::getAllMaleFamilyMembers());
				}
			}
			return [];
		}

		if (!$addMember) {
			return [];
		}

		//Case 2: Add from grid button
		//2.1. Add from Male row
		if ($addMember['gender'] == 1) {
			return $this->createMemberOptionList([$addMember]);
		}

		//2.2. Add from Female row
		return $this->createMemberOptionList( Model\Member::getMemberBySpouseId($addMemberId));
	}

	/**
	 * Get mother list
	 * @param array $data Request data array.
	 * @return array
	 */
	public function getMotherOptionList($data) {
		/*
		Case 1: Add Member from toolbar button:
		- 1.1. Gender = Male (Son): List of mothers from father_id.
		- 1.2. Gender = Female:
			+ 1.2.1. Daughter: List of mothers from father_id.
			+ 1.2.2. Daughter In Law: N/A.

		Case 2: Add Member from grid button:
		- 2.1. Add from Male row:
			+ 2.1.1. Gender = Male (Son):  List of mothers from father_id.
			+ 2.1.2. Gender = Female:
				   - 2.1.2.1. Daughter:  List of mothers from father_id.
				   - 2.1.2.2. Daughter In Law: N/A.
		- 2.2. Add from Female row:
			+ 2.2.1. Gender = Male:
				  - 2.2.1.1. Son: List of spouses from the female member.
				  - 2.2.1.1. Son In Law: N/A.
			+ 2.2.2. Gender = Female (Daughter):
		*/

		$addMemberId = isset($data['add_member_id']) ? $data['add_member_id'] : 0;
		$addMember   = Model\Member::getMemberById($addMemberId);

		//Case 1: Add from toolbar
		if (!$addMemberId) {
			return $this->createMemberOptionList(Model\Member::getMemberBySpouseId($data['male_father_id']));
		}

		if (!$addMember) {
			return [];
		}

		//Case 2: Add from grid button
		//2.1. Add from Male row
		if ($addMember['gender'] == 1) {
			return $this->createMemberOptionList(Model\Member::getMemberBySpouseId($data['male_father_id']));
		}

		//2.2. Add from Female row
		return $this->createMemberOptionList( Model\Member::getMemberBySpouseId($addMemberId));
	}


	public function loadGrid() {
		return [
			'type'         => 'grid',
			'data'         => $this->getGridData(),
			'method'       => 'Member.getGridData',
			'columns'      => [
				//['name' => 'details'],
				[
					'name'      => 'add',
					'label'     => ' ',
					'width'     => 40,
					'filter'    => 'none',
					'formatter' => 'add',
					'align'     => 'center'
				],
				[
					'name'      => 'generation',
					'label'     => 'Đời Thứ',
					'width'     => 70,
					'filter'    => 'select',
					'align'     => 'center',
					'formatter' => 'zerodash',
				],
				[
					'name'         => 'position',
					'label'        => 'Thứ Bậc',
					'width'        => 70,
					'filter'       => 'select',
					'filterLabels' => [0 => '-'],
					'align'        => 'center',
					'formatter'    => 'zerodash',
				],
				[
					'name'      => 'full_name',
					'label'     => 'Họ và Tên',
					'width'     => 200,
					'filter'    => 'text',
				],
				[
					'name'      => 'note',
					'label'     => 'Ghi Chú',
					'width'     => 200,
					'filter'    => 'text',
					'formatter' => 'dash',
				],
				[
					'name'         => 'gender',
					'label'        => 'Giới Tính',
					'width'        => 90,
					'filter'       => 'select',
					'formatter'    => 'gender',
					'align'        => 'center',
					'filterLabels' => [1 => 'Nam', 2 => 'Nữ']
				],
				[
					'name'      => 'num_son',
					'label'     => 'Con Trai',
					'width'     => 90,
					'filter'    => 'number',
					'formatter' => 'zerodash',
					'align'     => 'center'
				],
				[
					'name'      => 'num_daughter',
					'label'     => 'Con Gái',
					'width'     => 90,
					'filter'    => 'number',
					'formatter' => 'zerodash',
					'align'     => 'center'
				],
				[
					'name'      => 'father_name',
					'label'     => 'Họ Tên Cha',
					'width'     => 200,
					'filter'    => 'multiselect',
					'formatter' => 'father_name',
				],
				[
					'name'      => 'mother_name',
					'label'     => 'Họ Tên Mẹ',
					'width'     => 200,
					'filter'    => 'multiselect',
				],
				[
					'name'      => 'spouse_name',
					'label'     => 'Họ Tên Vợ / Chồng',
					'width'     => 300,
					'filter'    => 'text',
				]
			]
		];
	}

	public function getPreviewDialog($data) {
		$memberId = isset($data['member_id']) ? $data['member_id'] : 0;
		$member   = $memberId ? Model\Member::getMemberById($memberId) : null;
		if (!$member) {
			$this->error('Thành viên không tồn tại.');
		}

		//format values (empty to dash)
		$fields = ['dob', 'dod', 'burial_place', 'address', 'email', 'phone', 'website'];
		foreach ($fields as $field) {
			$member[$field] = empty($member[$field]) ? '-' : $member[$field];
		}

		$gender  = $member['gender'] == 1 ? 'Ông' : 'Bà';
		$inLaw   = $member['daughter_in_law'] ? 'Dâu, ' : ($member['son_in_law'] ? 'Rễ, ' : '');
		$note    = $member['note'] ? ' - ' . $member['note'] : '';
		$title   = sprintf('<h3 class="name">%s %s%s</h3><div class="info">(%sĐời thứ: %d, Thứ bậc: %d)</div>',
		 				   $gender, $member['full_name'], $note, $inLaw, $member['generation'], $member['position']);
		$spouses = Model\Member::getMemberBySpouseId($memberId);



		//render body content
		$body = $this->render('dialog/preview', [
			'member'      => $member,
			'spouseLabel' => $member['gender'] == 1 ? 'Vợ' : 'Chồng',
			'spouses'     => $spouses,
			'father'      => Model\Member::getMemberById($member['father_id']),
			'mother'      => Model\Member::getMemberById($member['mother_id']),
			'children'    => Model\Member::getChildrenList($memberId),
		]);

 		return [
			'type' => 'dialog',
		    'class' => 'preview-dialog',
		    'size' => 'large',
		    'height' => 600,
			'title' => $title,
			'body' => $body,
		    'buttons' => [['name' => 'close', 'label' => 'Đóng', 'click' => 'close']]
		];
	}


	public function getMemberDialog($data) {
		$isNew  = $data['id'] == 'new';
		$member = isset($data['member_id']) ? Model\Member::getMemberById($data['member_id']) : null;

		if ($isNew) {
			$data = array(
				'id'              => 'new',
				'member_id'       => '',
				'full_name'       => '',
				'note'            => '',
				'gender'          => 1,
				'add_member_id'   => isset($data['add_member_id']) ? $data['add_member_id'] : 0,
				'father_id'       => isset($data['father_id']) ? $data['father_id'] : 0,
				'mother_id'       => isset($data['mother_id']) ? $data['mother_id'] : 0,
				'spouse_id'       => isset($data['spouse_id']) ? $data['spouse_id'] : 0,
				'generation'      => 0,
				'position'        => 0,
				'daughter_in_law' => 0,
				'son_in_law'      => 0,
				'dob'             => '',
				'dod'             => '',
				'burial_place'    => '',
				'photo_id'        => 0,
				'other_info'      => '',
				'num_son'         => 0,
				'num_daughter'    => 0,
				'address'         => '',
				'phone'           => '',
				'email'           => '',
				'website'         => '',
			);
		}
		else {
			if (!$member) {
				return $this->error('Thành viên không tồn tại.');
			}

			$data['gender']          = $member['gender'];
			$data['add_member_id']   = !$member['in_family'] && !$member['spouse_id'] ? $member['mother_id'] : $member['father_id'];
			$data['daughter_in_law'] = $member['daughter_in_law'];
			$data['son_in_law']      = $member['son_in_law'];
		}

		$addMember = Model\Member::getMemberById($data['add_member_id']);
		$addGender = $addMember ? $addMember['gender'] : 1;

		return [
			'type' => 'form',
			'ui'   => [
				[
					'type'  => 'hidden',
					'name'  => 'id',
					'value' => $data['id']
				],
				[
					'type'  => 'hidden',
					'name'  => 'member_id',
					'value' => $data['member_id']
				],
				[
					'type'  => 'hidden',
					'name'  => 'num_son',
					'value' => $data['num_son']
				],
				[
					'type'  => 'hidden',
					'name'  => 'num_daughter',
					'value' => $data['num_daughter']
				],
				[
					'type'  => 'hidden',
					'name'  => 'photo_id',
					'value' => $data['photo_id']
				],
				[
					'type'  => 'hidden',
					'name'  => 'position',
					'value' => $data['position']
				],
				[
					'type'  => 'hidden',
					'name'  => 'add_member_id',
					'value' => $data['add_member_id']
				],

				[
					'type'      => 'fieldset',
					'label'     => '[im:user] Thông tin chung',
					'accordion' => true,
					'ui'        => [
						[
							'type' => 'panel',
							'columns' => 2,
							'ui' => [
								[
									'type'  => 'text',
									'name'  => 'full_name',
									'label' => 'Họ và tên',
									'value' => $data['full_name'],
									'focus' => true
								],
								[
									'type'   => 'text',
									'name'   => 'note',
									'label'  => 'Ghi chú',
									'value'  => $data['note'],
									'holder' => 'Danh hiệu hay tên gọi khác'
								],
								[
									'type'     => 'select',
									'name'     => 'gender',
									'label'    => 'Giới tính',
									'options'  => [['label' => 'Nam', 'value' => 1], ['label' => 'Nữ', 'value' => 2]],
									'value'    => $data['gender'],
									'disabled' => !$isNew,
								],
								[
									'type'   => 'date',
									'name'   => 'dob',
									'label'  => 'Ngày sinh',
									'value'  => $data['dob'],
									'holder' => 'Chọn ngày dương lịch',
								],
							]
						],

						$addGender == 1 ? $this->createMalePanel($data) : $this->createFemalePanel($data),

						[
							'type' => 'panel',
							'columns' => 2,
							'ui' => [
								[
									'type'   => 'date',
									'name'   => 'dod',
									'label'  => 'Ngày mất',
									'value'  => $data['dod'],
									'holder' => 'Chọn ngày dương lịch',
								],
								[
									'type'    => 'text',
									'name'    => 'burial_place',
									'label'   => 'Nơi an táng',
									'value'   => $data['burial_place'],
									'prepend' => '[i:map-marker]',
								],
							]
						],
					]
				],

				$this->createChildrenPanel($data),


				[
					'type'      => 'fieldset',
					'columns'   => 2,
					'label'     => '[im:address-card-o] Thông tin liên lạc',
					'accordion' => true,
					'ui'        => [
						[
							'type'    => 'text',
							'name'    => 'address',
							'label'   => 'Địa chỉ',
							'value'   => $data['address'],
							'prepend' => '[i:map-marker]',
						],
						[
							'type'    => 'text',
							'name'    => 'phone',
							'label'   => 'Số điện thoại',
							'value'   => $data['phone'],
							'prepend' => '[i:phone]',
						],
						[
							'type'    => 'text',
							'name'    => 'email',
							'label'   => 'Địa chỉ email',
							'value'   => $data['email'],
							'prepend' => '[i:envelope-o]',
						],
						[
							'type'    => 'text',
							'name'    => 'website',
							'label'   => 'Trang cá nhân',
							'value'   => $data['website'],
							'holder'  => 'Trang web cá nhân hay facebook',
							'rule'    => 'url',
							'prepend' => '[i:link]',
						],
					]
				],

				[
					'type'      => 'fieldset',
					'label'     => '[im:file-text-o] Thông tin khác',
					'accordion' => true,
					'ui'        => [
						[
							'type'   => 'textarea',
							'name'   => 'other_info',
							'rows'   => 6,
							'editor' => true,
							'value'  => $data['other_info'],
						],
					]
				],


				array(
					'type'    => 'content',
					'visible' => !$isNew,
					'content' => $this->createRowInfo($member['created_date'],  $member['created_by'],
													  $member['modified_date'], $member['modified_by'])
				)
			] //ui
		];
	}

	/**
	 * Create parent & spouse panel when adding from male row on grid or from add button on toolbar.
	 * @param array $data Form data.
	 * @return array
	 */
	private function createMalePanel($data) {
		$isNew   = $data['id'] == 'new';
		$fathers = $isNew ? $this->getFatherOptionList($data) : [$this->createMemberOptionById($data['father_id'])];
		$spouses = [$this->createMemberOptionById($data['spouse_id'])];

		if (!$isNew && $data['son_in_law']) {
			return [
				'type'    => 'panel',
				'visible' => 'gender = 1',
				'columns' => 2,
				'ui'      => [
					[
						'type'     => 'select',
						'name'     => 'spouse_id',
						'label'    => 'Vợ',
						'value'    => $data['spouse_id'],
						'options'  => $spouses,
						'disabled' => true,
					],
				]
			];
		}

		return [
			'type' => 'panel',
			'ui' => [
				[
					'type'    => 'panel',
					'visible' => 'gender = 1',
					'columns' => 2,
					'ui'      => [
						[
							'type'     => 'select',
							'name'     => 'male_father_id',
							'label'    => 'Cha',
							'value'    => $data['father_id'],
							'options'  => $fathers,
							'disabled' => !$isNew || ($isNew && $data['add_member_id']),
							'visible'  => $isNew  || $data['generation'] > 1,
							'class'    => 'father-1',
						],
						[
							'type'    => 'select',
							'name'    => 'male_mother_id',
							'label'   => 'Mẹ',
							'value'   => $data['mother_id'],
							'depend'  => 'male_father_id',
							'method'  => 'Member.getMotherOptionList',
							'visible' => $isNew || $data['generation'] > 1,
							'class'   => 'mother-1',
						],
					]
				],
				[
					'type'    => 'panel',
					'visible' => 'gender = 2',
					'columns' => 2,
					'ui'      => [
						[
							'type'     => 'select',
							'name'     => 'daughter_in_law',
							'label'    => 'Con / Dâu',
							'value'    => $data['daughter_in_law'],
							'options'  => [['label' => 'Con', 'value' => 0], ['label' => 'Dâu', 'value' => 1]],
							'disabled' => !$isNew
						],
						[
							'type'     => 'select',
							'name'     => 'female_father_id',
							'label'    => 'Cha',
							'value'    => $data['father_id'],
							'options'  => $fathers,
							'visible'  => 'daughter_in_law = 0',
							'disabled' => !$isNew || ($isNew && $data['add_member_id']),
							'focus'    => true,
							'class'    => 'father-2',
						],
						[
							'type'    => 'select',
							'name'    => 'female_mother_id',
							'label'   => 'Mẹ',
							'value'   => $data['mother_id'],
							'visible' => 'daughter_in_law = 0',
							'depend'  => 'male_father_id',
							'method'  => 'Member.getMotherOptionList',
							'class'   => 'mother-2',
						],
						[
							'type'     => 'select',
							'name'     => 'spouse_id',
							'label'    => 'Chồng',
							'visible'  => 'daughter_in_law = 1',
							'value'    => $data['spouse_id'],
							'options'  => $isNew ? $fathers : $spouses,
							'focus'    => true,
							'disabled' => !$isNew || ($isNew && $data['add_member_id']),
						],
					]
				],
			]
		];
	}


	/**
	 * Create parent & spouse panel when adding from female row on grid.
	 * @param array $data Form data.
	 * @return array
	 */
	private function createFemalePanel($data) {
		$isNew   = $data['id'] == 'new';
		$mothers = [$this->createMemberOptionById($data['mother_id'])];
		$spouses = [$this->createMemberOptionById($data['spouse_id'])];

		return [
			'type' => 'panel',
			'ui' => [
				[
					'type'    => 'panel',
					'visible' => 'gender = 1',
					'columns' => 2,
					'ui'      => [
						[
							'type'    => 'select',
							'name'    => 'son_in_law',
							'label'   => 'Con / Rễ',
							'value'   => $data['son_in_law'],
							'options' => [['label' => 'Con', 'value' => 0], ['label' => 'Rễ', 'value' => 1]],
						],
						[
							'type'     => 'select',
							'name'     => 'male_father_id',
							'label'    => 'Cha ',
							'value'    => $data['father_id'],
							'method'   => 'Member.getFatherOptionList',
							'visible'  => 'son_in_law = 0',
							'disabled' => $isNew && !$data['add_member_id'],
							'focus'    => true,
							'class'    => 'father-3',
						],
						[
							'type'     => 'select',
							'name'     => 'male_mother_id',
							'label'    => 'Mẹ',
							'value'    => $data['mother_id'],
							'visible'  => 'son_in_law = 0',
							'options'  => $mothers,
							'disabled' => true,
							'class'    => 'mother-3',
						],
						[
							'type'     => 'select',
							'name'     => 'spouse_id',
							'label'    => 'Vợ',
							'visible'  => 'son_in_law = 1',
							'value'    => $data['spouse_id'],
							'options'  => $spouses,
							'disabled' => true,
						],
					]
				],

				[
					'type' => 'panel',
					'columns' => 2,
					'visible' => 'gender = 2',
					'ui' => [
						[
							'type'     => 'select',
							'name'     => 'female_father_id',
							'label'    => 'Cha',
							'value'    => $data['father_id'],
							'method'   => 'Member.getFatherOptionList',
							'disabled' => !$isNew,
							'class'    => 'father-4',
						],
						[
							'type'     => 'select',
							'name'     => 'female_mother_id',
							'label'    => 'Mẹ',
							'value'    => $data['mother_id'],
							'options'  => $mothers,
							'disabled' => true,
							'class'    => 'mother-4',
						],
					]
				],
			]
		];
	}


	private function createChildrenPanel($data) {
		$data     = $this->parseFormData($data);
		$children = Model\Member::getChildrenList($data['member_id']);
		$isMale   = $data['gender'] == 1;
		$parentId = $isMale ? 'mother_id' : 'father_id';

		if ($data['is_new'] || !$children) {
			return null;
		}

		$data = [];
		foreach ($children as $index => $child) {
			$name   = sprintf('<a onclick="Member.openMemberDialog(%d)">%s</a>', $child['member_id'], $child['full_name']);
			$spouse = Model\Member::getMemberById($child[$parentId]);
			$spouse = !$spouse ? '' : sprintf('<a onclick="Member.openMemberDialog(%d)">%s</a>', $spouse['member_id'], $spouse['full_name']);

			$data[] = [
				'gen'    => $child['generation'],
				'pos'    => $child['position'],
				'name'   => $name,
				'note'   => $child['note'],
				'gender' => $child['gender'] == 1 ? 'Nam' : 'Nữ',
				'spouse' => $spouse
			];
		}

		return [
			'type'      => 'fieldset',
			'label'     => '[im:users] Thông tin con',
			'accordion' => true,
			'ui'        => [
				//['type' => 'content', 'class' => 'children-list', 'content' => $content]

				[
					'type'       => 'table',
					'pagination' => false,
					'data'       => $data,
					'columns'    => [
						['name' => 'gen', 'label' => 'Đời thứ', 'width' => 60, 'align' => 'center'],
						['name' => 'pos', 'label' => 'Thứ bậc', 'width' => 60, 'align' => 'center'],
						['name' => 'gender', 'label' => 'Giới tính', 'width' => 60, 'align' => 'center'],
						['name' => 'name', 'label' => 'Họ và tên con'],
						['name' => 'note', 'label' => 'Ghi chú'],
						['name' => 'spouse', 'label' => 'Họ và tên ' . ($isMale ? 'mẹ' : 'cha')],
					],
				]
			]
		];
	}


	public function validateMember($data) {
		$data      = $this->parseFormData($data);
		$valid     = true;
		$message   = [];
		$highlight = [];
//		$isMale    = $data['gender'] == 1;
//		$member    = Model\Member::getMemberById($data['member_id']);
//		$father    = Model\Member::getMemberById($data['father_id']);
		$email     = trim($data['email']);

		if (empty(trim($data['full_name']))) {
			$valid       = false;
			$message[]   = 'Vui lòng nhập Họ và Tên.';
			$highlight[] = 'full_name';
		}

		if ($email && !\Helper\Validator::isEmail($email)) {
			$valid       = false;
			$message[]   = 'Vui lòng nhập địa chỉ Email hợp lệ.';
			$highlight[] = 'email';
		}

		/*if (!$isNew && $father && $member) {
			if ($father['generation'] >= $member['generation']) {
				$valid       = false;
				$message[]   = 'Đời của cha phải trước đời của con.';
				$highlight[] = $isMale ? 'male_father_id' : 'female_father_id';
			}
		}*/

//		if (!$isMale && isset($data['daughter_in_law']) && $data['daughter_in_law'] == '') {
//			$valid       = false;
//			$message[]   = 'Vui lòng chọn mối quan hệ Dâu/Con.';
//			$highlight[] = 'daughter_in_law';
//		}

		return ['valid' => $valid, 'message' => $message, 'highlight' => $highlight];
	}

	private function parseFormData($data) {
		$defaults = [
			'add_member_id'    => 0,
			'generation'       => 0,
			'position'         => 0,
			'spouse_id'        => 0,
			'male_father_id'   => 0,
			'female_father_id' => 0,
			'daughter_in_law'  => 0,
			'son_in_law'       => 0,
			'photo_id'         => 0,
			'parent_prefix'    => 'male',
		];

		foreach ($defaults as $field => $value) {
			if (!isset($data[$field])) {
				$data[$field] = $value;
			}
		}

		$isNew          = $data['id'] == 'new';
		$memberRow      = $this->getMemberRow($data);

		$gender         = (int) $data['gender'];

		$daughterInLaw  = (int) $data['daughter_in_law'];

		$sonInLaw       = (int) $data['son_in_law'];

		$isChildren     = !$daughterInLaw && !$sonInLaw;

		$memberId		= $data['member_id'];
		$member			= Model\Member::getMemberById($memberId);

		$parentPrefix   = $gender == 1 ? 'male' : 'female';

		$fatherId       = $isChildren && isset($data[$parentPrefix . '_father_id']) ? $data[$parentPrefix . '_father_id'] : 0;
		$father         = Model\Member::getMemberById($fatherId);

		$motherId       = $isChildren && isset($data[$parentPrefix . '_mother_id']) ? $data[$parentPrefix . '_mother_id'] : 0;
		$motherId       = $motherId ? $motherId : 0;
		$mother         = Model\Member::getMemberById($motherId);

		$addMemberId    = $data['add_member_id'];
		$addMemberId    = $addMemberId ? $addMemberId : ($fatherId ? $fatherId : 0);
		$addMemberId    = $addMemberId ? $addMemberId : ($motherId ? $motherId : 0);

		$addMember      = Model\Member::getMemberById($addMemberId);

		$spouseId       = $daughterInLaw || $sonInLaw ? $data['spouse_id'] : 0;
		$spouseId       = $spouseId ? $spouseId : 0;
		$spouse         = Model\Member::getMemberById($spouseId);

		$memberType     = $sonInLaw ? 'son_in_law' : ($daughterInLaw ? 'daughter_in_law' : ($gender == 1 ? 'son' : 'daughter'));

		$inFamily 		= $member ? $member['in_family'] : $isChildren;
		$inFamily		= $inFamily && $addMember && $addMember['gender'] == 2 ? 0 : $inFamily; //children of daughters not considered as family members

		$generation     = (int) $data['generation'];
		$generation     = $generation ? $generation : ($spouse ? $spouse['generation'] : 0);
		$generation     = $generation ? $generation : ($addMember ? $addMember['generation'] + 1 : 0);
		$generation     = $generation ? $generation : ($father ? $father['generation'] + 1 : 0);
		$generation     = $generation ? $generation : ($mother ? $mother['generation'] + 1 : 1);

		$position       = $member   ? $member['position'] : 0;
		$position       = $position ? $position : ($inFamily ? $position = Model\Member::getNextChildPosition($fatherId) : 0);
		$position       = $position ? $position : ($isChildren ? Model\Member::getNextChildPosition($motherId) : 0);
		$position       = $position ? $position : Model\Member::getNextSpousePosition($spouseId);
		$position       = $position ? $position : 1;

		$numSon         = $isNew ? ($spouse ? $spouse['num_son'] : 0)      : ($memberRow ? $memberRow['num_son'] : 0);
		$numDaughter    = $isNew ? ($spouse ? $spouse['num_daughter'] : 0) : ($memberRow ? $memberRow['num_daughter'] : 0);

		$photoId        = $member ? $member['photo_id'] : $data['photo_id'];
		$photoId        = $photoId ? $photoId : 0;

		return [
			'member_type'     => $memberType,
			'add_member_id'   => $addMemberId,
			'is_new'          => $isNew,
			'id'              => $data['id'],
			'member_id'       => $data['member_id'],			
			'generation'      => $generation,
			'position'        => $position,
			'is_children'     => $isChildren,
			'in_family'		  => $inFamily,
			'father_id'       => $isChildren ? $fatherId : 0,
			'mother_id'       => $isChildren ? $motherId : 0,
			'spouse_id'       => $inFamily ? 0 : $spouseId,
			'full_name'       => $data['full_name'],
			'note'            => $data['note'],
			'gender'          => $data['gender'],
			'son_in_law'      => $sonInLaw,
			'daughter_in_law' => $daughterInLaw,
			'num_son'         => $numSon,
			'num_daughter'    => $numDaughter,
			'dob'             => $data['dob'],
			'dod'             => $data['dod'],
			'burial_place'    => $data['burial_place'],
			'photo_id'        => $photoId,
			'other_info'      => $data['other_info'],
			'address'         => $data['address'],
			'email'           => $data['email'],
			'phone'           => $data['phone'],
			'website'         => $data['website'],
		];
	}

	public function saveMember($data) {
		$data = $this->parseFormData($data);
		$data = Model\Member::saveMember($data);
		$data = $this->updateChildren($data);
		$row  = $this->getMemberRow($data);
		if ($row) {
			foreach ($row as $column => $value) {
				$data[$column] = $value;
			}
		}
		
		$data['id'] = $data['in_family'] ? $data['id'] : null; //not update current row if not family members
		$data = $this->attachUpdateRows($data);
		return $data;
	}

	public function deleteMember($data) {
		$data = $this->parseFormData($data);
		$data['delete'] = true;
		Model\Member::deleteMember($data['member_id']);
		$data = $this->updateChildren($data);
		$data = $this->attachUpdateRows($data);

		$data['id'] = $data['in_family'] ? $data['id'] : null; //not update current row if not family members
		return $data;
	}

	/**
	 * Update total children (sons & daughters) on parent rows
	 * @param $data
	 * @return mixed
	 */
	private function updateChildren($data) {
		$parentSpouseId = 0;
		$father = Model\Member::getMemberById($data['father_id']);
		$mother = Model\Member::getMemberById($data['mother_id']);

		//update children on father row
		if ($father) {
			Model\Member::updateChildren($father['member_id']);
			$parentSpouseId = $father['spouse_id'];
		}

		//update children on mother row
		if ($mother) {
			Model\Member::updateChildren($mother['member_id']);
			$parentSpouseId = $mother['spouse_id'];
		}

		//update children on parent spouse rows
		if ($parentSpouseId) {
			$spouses = Model\Member::getMemberBySpouseId($parentSpouseId);
			foreach ($spouses as $spouse) {
				Model\Member::updateChildren($spouse['member_id']);
			}
		}
		return $data;
	}

	/**
	 * Attach updated rows to return array to update on grid
	 * @param $data
	 * @return mixed
	 */
	private function attachUpdateRows($data) {
		/*
		Case 1: Add Member
		1.1. Son: Update son row (new row) + Father row (num_son column).
		1.2. Daughter: Update daughter row (new row) + Father row (num_daughter column).
		1.3. Son In Law: Update spouse row (spouse_name column).
		1.4. Daughter In Law: Update spouse row (spouse_name column).

		Case 2: Update Member
		2.1. Son: Update son row (current row) + Children rows (father_name column).
		2.2. Daughter: Update daughter row (current row) + Children rows (mother_name column).
		2.3. Son In Law: Update spouse row (spouse_name column).
		2.4. Daughter In Law: Update spouse row (spouse_name column).

		Case 3: Delete Member
		2.1. Son: Remove son row (current row) + Update father rows (num_son column).
		2.2. Daughter: Remove daughter row (current row) + Update father rows (num_daughter column).
		2.3. Son In Law: Find & update daughter row (spouse_name column) + Update children rows (father_name column).
		2.4. Daughter In Law: Find & update son row (spouse_name column) + Update children rows (mother_name column).
		*/

		$update = [];
		$member = Model\Member::getMemberById($data['member_id']);
		$father = Model\Member::getMemberById($data['father_id']);
		$mother = Model\Member::getMemberById($data['mother_id']);
		$action = $data['is_new'] == 'new' ? 'add' : (isset($data['delete']) ? 'delete' : 'update');
		$type   = $data['member_type'];
		
		if (!$member) {
			return $data;
		}

		//Case 1: Add Member
		if ($action == 'add') {
			switch ($type) {
				case 'son':
				case 'daughter':
					if ($father) {
						$update[] = [
							'member_id'    => $father['member_id'],
							'num_son'      => $father['num_son'],
							'num_daughter' => $father['num_daughter'],
						];
					}

					if ($mother) {
						$update[] = [
							'member_id'    => $mother['member_id'],
							'num_son'      => $mother['num_son'],
							'num_daughter' => $mother['num_daughter'],
						];
					}
				break;

				case 'son_in_law':
				case 'daughter_in_law':
					$row = $this->getMemberRow(['member_id' => $data['spouse_id']]);
					$update[] = [
						'member_id'   => $data['spouse_id'],
						'spouse_name' => $row['spouse_name'],
						'spouses'     => $row['spouses'],
					];
					break;
			}
		}

		//Case 2: Update Member
		if ($action == 'update') {
			switch ($type) {
				case 'son':
					$children = Model\Member::getMemberByFatherId($data['member_id']);
					foreach ($children as $m) {
						$update[] = [
							'member_id'   => $m['member_id'],
							'father_name' => $data['full_name']
						];
					}
					break;

				case 'daughter':
					$children = Model\Member::getMemberByMotherId($data['member_id']);
					foreach ($children as $m) {
						$update[] = [
							'member_id'   => $m['member_id'],
							'mother_name' => $data['full_name']
						];
					}
					break;

				case 'son_in_law':
				case 'daughter_in_law':
					$row = $this->getMemberRow(['member_id' => $data['spouse_id']]);
					$update[] = [
						'member_id'   => $data['spouse_id'],
						'spouse_name' => $row['spouse_name'],
						'spouses'     => $row['spouses'],
					];

					$children = Model\Member::getMemberByMotherId($data['member_id']);
					foreach ($children as $m) {
						$update[] = [
							'member_id'   => $m['member_id'],
							'mother_name' => $member['full_name']
						];
					}
					break;
			}
		}

		//Case 3: Delete Member
		if ($action == 'delete') {
			switch ($type) {
				case 'son':
				case 'daughter':
					if ($father) {
						$update[] = [
							'member_id'    => $father['member_id'],
							'num_son'      => $father['num_son'],
							'num_daughter' => $father['num_daughter']
						];
					}
					break;

				case 'son_in_law':				
				case 'daughter_in_law':
					//Only update on current row, no need to update other rows
					$row = $this->getMemberRow(['member_id' => $data['spouse_id']]);
					$update[] = [
						'member_id'   => $data['spouse_id'],
						'spouse_name' => $row['spouse_name'],
						'spouses'     => $row['spouses'],
					];
					break;
			}
		}

		$data['update_rows'] = $update;
		return $data;
	}
}