<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 11/4/17
 * Time: 4:28 PM
 */

namespace App\API;

use App\Model\Member;

class FamilyTree extends \System\API {

	public function getTreeData($data) {
		$from        = isset($data['from']) ? $data['from'] : 1;
		$to          = isset($data['to']) ? $data['to'] : Member::getMaxGeneration();
		$tree        = [];
		$allMembers  = Member::getMemberList();
		$generations = [];

		if ($from > $to || !$allMembers) {
			return $tree;
		}

		//create generations array (generation => members)
		foreach ($allMembers as $member) {
			$generation = $member['generation'];
			if (!isset($generations[$generation])) {
				$generations[$generation] = [];
			}
			$generations[$generation][] = $member;
		}

		//create tree array
		for ($generation = $from; $generation <= $to; $generation++) {
			$members = $generations[$generation];

			foreach ($members as $member) {
				$gender  = $member['gender'] == 1 ? 'male' : 'female';
				$link    = sprintf('<a data-id="%s">%s</a>', $member['member_id'], $member['full_name']);
				$label   = sprintf('%d.%d. <span class="%s-member">[i:%s]</span> %s',
									$member['generation'], $member['position'],
									$gender, $gender, $link);

				$node = [
					'member_id' => $member['member_id'],
					'father_id' => $member['father_id'],
					'label'     => $label,
					'child'     => []
				];

				if ($generation == $from) {
					$tree[] = $node;
					continue;
				}
				$this->addNode($tree, $node);
			}
		}
		return $tree;
	}

	private function addNode(&$root, $node) {
		if (!$root) {
			return false;
		}

		foreach ($root as &$r) {
			if ($r['member_id'] == $node['father_id']) {
				$r['child'][] = $node;
				return true;
			}

			if ($r['child']) {
				if ($this->addNode($r['child'], $node)) {
					return true;
				}
			}
		}
		return false;
	}


	public function getTree($data) {
		$tree = [
			'type'         => 'tree',
			'expandLevels' => 0,
			'data'         => $this->getTreeData($data)
		];

		return [
			'type'      => 'panel',
			'container' => 'tree-container',
			'label'     => 'Phả đồ ' . $this->config('app.title'),
			'ui'        => [$tree]
		];
	}
}