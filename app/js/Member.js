System.import('app/ui').then(function (ui) {
    ui.factory('Member.getMemberGrid', {}, panel => {
        var grid = panel.get('@editgrid');
        Member.init(ui, grid);
        panel.render();
    });
});

var Member = {
    init(ui, grid) {
        this.ui             = ui;
        this.grid           = grid;
        grid.editObjectName = this.editObjectName;
        grid.formatters     = this.formatters;

        grid.on('row.click', e => {
            this.onRowClick(e.column, e.data);
        });

        grid.on(['form.save', 'row.delete'], data => {
            this.updateRows(data);
        });

        grid.on('dialog.open', e => {
            e.dialog.showButton('delete', e.row.num_son + e.row.num_daughter == 0);
        });
    },

    onRowClick(column, data) {
        switch (column.id) {
            case 'add':
                this.grid.openDialog({
                    id:            'new',
                    add_member_id: data.member_id,
                    father_id:     data.gender == 1 ? data.member_id : 0,
                    mother_id:     data.gender == 2 ? data.member_id : 0,
                    spouse_id:     data.member_id,
                });
                break;
        }
    },

    updateRows(data) {
        $.each(data.update_rows, (index, updateRow) => {
            var row = this.grid.findRow('member_id', updateRow.member_id);
            if (row) {
                let merge = Object.assign(row, updateRow);
                this.grid.updateRow(merge);
            }
        });
    },

    openMemberDialog(memberId) {
        var row = this.grid.findRow('member_id', memberId);
        if (row) {
            this.grid.dialog.close();
            this.grid.openDialog(row);
            return;
        }

        this.ui.call('Member.getMemberRow', {member_id: memberId}, response => {
            var row = Object.assign(response.data, {id: 'member'});
            this.grid.dialog.close();
            this.grid.openDialog(row);
        });
    },

    formatters: {
        add: (rowIndex, colIndex, value) => {
            return `<i class="fa fa-user-plus add-child-icon" title="Thêm thành viên"></i>`;
        },

        gender: (rowIndex, colIndex, value) => {
            return value == 1 ? 'Nam' : (value == 2 ? 'Nữ' : '-');
        },

        full_name:(rowIndex, columnIndex, value, column, data) => {
            return value ? `<a onclick="Member.openMemberDialog(${data.member_id})">${value}</a>` : '-';
        },

        father_name:(rowIndex, columnIndex, value, column, data) => {
            return value ? `<a onclick="Member.openMemberDialog(${data.father_id})">${value}</a>` : '-';
        },

        mother_name:(rowIndex, columnIndex, value, column, data) => {
            return value ? `<a onclick="Member.openMemberDialog(${data.mother_id})">${value}</a>` : '-';
        },

        spouse_name: (rowIndex, columnIndex, value, column, data) => {
            var spouses = [];
            if (data.spouses) {
                $.each(data.spouses, (index, row) => {
                    var link = `<a onclick="Member.openMemberDialog(${row.member_id})">${row.full_name}</a>`;
                    spouses.push(link);
                });
            }
            return spouses ? spouses.join(', ') : '-';
        }
    },

    editObjectName(row) {
        var objName = '!Thêm Thành Viên';
        if (row) {
            var gender = row.gender == 1 ? 'Ông' : 'Bà';
            var info   = row.daughter_in_law == 1 ? 'Dâu - ' : (row.son_in_law == 1 ? 'Rễ - ' : '');
            objName = `!${gender} ${row.full_name} - ${info}Đời thứ ${row.generation}`;
        }
        return objName
    },
};