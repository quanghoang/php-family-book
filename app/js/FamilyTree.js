System.import('app/ui').then(function (ui) {
    ui.factory('FamilyTree.getTree', {}, panel => {
        panel.render();
        var tree = panel.get('@tree');
        FamilyTree.init(ui, tree);
    });
});

var FamilyTree = {
    init(ui, tree) {
        this.ui = ui;
        this.tree = tree;
        this.dialog = null;

        tree.getContainer().click(e => {
            let target = $(e.target);
            if (target[0].tagName == 'A') {
                var memberId = target.data('id');
                this.openMemberDialog(memberId);
            }
        });
    },

    openMemberDialog(memberId) {
        this.ui.render('Member.getPreviewDialog', {member_id: memberId}, dialog => {
            if (this.dialog) {
                this.dialog.close();
            }
            this.dialog = dialog;
            dialog.open();
        });
    }
};