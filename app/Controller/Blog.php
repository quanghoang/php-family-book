<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/18/17
 * Time: 2:45 PM
 */

namespace App\Controller;


class Blog extends Base {

    /**
     * Blog index
     * @param string $id
     * @param string $title
     */
    public function index($id='', $title='') {
        $this->menu('pha-ky');

        if (!$id) {
            $this->listing();
            return;
        }
        $this->blog($id, $title);
    }

    /**
     * Listing blogs
     */
    public function listing() {
        $this->render('pha-ky');
        $this->display();
    }

    /**
     * Display a blog
     * @param string $id
     * @param string $title
     */
    public function blog($id, $title) {
        echo "Blog id=$id, title=$title.";
    }
}