<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/18/17
 * Time: 2:32 PM
 */

namespace App\Controller;

use App\Model\User;

class Base extends \System\Controller {

    /**
     * ACL instance
     * @var \System\Acl
     */
    public $acl;

    public $data;

    /**
     * Base constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->acl = \System\Acl::instance();
        $user      = $this->getLoginUser();

        $this->data = array(
        	'appVersion' => $this->config('app.version'),
	        'absUrl'     => $this->absUrl(),
	        'homeUrl'    => $this->homeUrl(),
	        'menu'       => $this->config('menu'),
	        'loginName'  => $user ? $user['full_name'] : '',
	        'content'    => '',
        );
    }

    /**
     * Set active menu
     * @param string $menu Menu name
     */
    public function menu($menu) {
        $this->data['activeMenu'] = $menu;
    }

    /**
     * Set page content
     * @param string $content
     */
    public function content($content) {
        $this->data['content'] = $content;
    }

    /**
     * Has roles
     * @param array $roles List of roles to check
     * @return bool
     */
    public function hasRoles($roles) {
        foreach (\Helper\Arrays::toArray($roles) as $role) {
            if (!$this->acl->hasRole($role)) {
                return false;
            }
        }
        return true;
    }

    public function render($template = 'index', $data = array()) {
        $data = is_array($data) ? $data : array();
        $data = array_merge($data, $this->data);
        $content = parent::render($template, $data);
	    $this->data['content'] .= $content;
        return $content;
    }

    public function isLogin() {
    	return isset($_SESSION['user_id']);
    }

    public function loginRequired() {
	    if (!$this->isLogin()) {
		    $this->redirect('/');
	    }
    }

    public function getLoginId() {
	    return isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
    }

    public function getLoginUser() {
    	return $this->isLogin() ? User::getUserById($this->getLoginId()) : [];
    }

    public function absUrl() {
    	return $this->config('app.absUrl');
    }

    public function homeUrl() {
    	return $this->absUrl() . $this->config('app.homePage');
    }

    public function redirect($url) {
    	if (!\Helper\URL::create($url)->has_protocol()) {
		    $url = rtrim(rtrim($this->absUrl(), '/') . $url, '/');
	    }
    	echo sprintf('<script>window.location.replace("%s")</script>', $url);
    	die();
    }
}