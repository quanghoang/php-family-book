<?php
/**
 * Created by PhpStorm.
 * User: quanghoang
 * Date: 10/17/17
 * Time: 10:22 PM
 */

namespace App\Controller;


class Genealogy extends Base {

	public function index() {
		$this->loginRequired();

		$this->menu('pha-he');
		$this->render('pha-he');
		$this->display();
	}

	public function familyTree() {
		$this->loginRequired();

		$this->menu('pha-do');
		$this->render('pha-do');
		$this->display();
	}

	public function appendix() {
		$this->loginRequired();

		$this->menu('chu-pha');
		$this->render('chu-pha');
		$this->display();
	}
}