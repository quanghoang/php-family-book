<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/18/17
 * Time: 11:12 AM
 */

namespace App\Controller;
use App\Model;

class Home extends Base {

    public function index() {
		$this->loginRequired();
	    $this->menu('doi-loi');
    	$this->render('doi-loi');
        $this->display();
    }
}