<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/18/17
 * Time: 3:19 PM
 */

namespace App\Controller;
use \App\Model;

class Login extends Base{

	public function index() {
		if ($this->isLogin()) {
			$this->redirect($this->homeUrl());
		}

		$this->data['invalid'] = isset($_GET['invalid']);
		$this->render('form/login');
		$this->display('login');
	}

	public function login() {
		$user = Model\User::getUser($_POST['username'], md5($_POST['password']));
		if (!$user) {
			$this->redirect('/?invalid');
		}

		$_SESSION['user_id'] = $user['user_id'];
		$this->redirect($this->homeUrl());
	}

	public function logout() {
		unset($_SESSION['user_id']);
		$this->redirect('/');
	}

	public function registry() {
		$this->render('form/registry');
		$this->display('login');
	}

	public function forgotPassword() {
		$this->render('form/forgot-password');
		$this->display('login');
	}
}