<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 4/18/17
 * Time: 2:47 PM
 */

namespace App\Controller;


class Error extends Base {

    public function index() {
        echo "Page {$this->getCurrentUri()} Not Found";
    }
}