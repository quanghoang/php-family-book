/**
 * Created by hoangq on 2/13/17.
 */

const Config = {
    /**
     * Default ajax settings
     */
    ajax: {
        url:      'request.php',    //Ajax handler file
        folder:   'UI',             //Sub-folder in App/UI folder.
        class:    'Default',        //Default class
        method:   'POST',           //GET, POST, DELETE, PUT
        dataType: 'json',           //xml, json, script, html
    },

    /**
     * Form settings
     */
    form: {
        // Number of columns for label of horizontal layout
        horizontal_label_columns: 3,
    },


    /**
     * Mobile app settings
     */
    mobile: {
        render_framework: 'framework7'
    }
};

export default Config;