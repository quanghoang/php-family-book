/**
 * Created by hoangq on 2/24/17.
 */

import Util from 'lib/UI/Helper/Util';
import BaseMenu from '../Base/Menu';
import MenuGroup from 'lib/UI/Menu/Framework7/MenuGroup';

export default class Menu extends BaseMenu {

    constructor(attributes={}) {
        super(attributes);
        this.containerClass = 'f7 uc uc-menu';
    }

    /**
     * Add menu groups & items
     * @param menus
     */
    addMenu(menus) {
        if (Util.isEmpty(menus)) {
            return;
        }
        for (let menu of Util.toArray(menus)) {
            if (menu && menu.type == 'group') {
                this.components.push(new MenuGroup(menu));
            }
        }
    }

    createSearchBar() {
        return `
        <div class="menu-header content-block-title">            
            <h2>${this.label}</h2>            
            <a class="close-panel"><i class="f7-icons">close</i></a>
        </div> 

        <div class="content-block">
            <div class="form-group">
                <div class="icon-addon addon-md">                
                    <input type="text" placeholder="Search..." class="form-control ui-search-bar">                                                                        
                    <label for="search" class="fa fa-search" rel="tooltip" title="Search"></label>
                </div>
            </div>        
        </div>`;
    }
}