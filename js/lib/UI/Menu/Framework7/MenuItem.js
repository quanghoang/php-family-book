/**
 * Created by hoangq on 2/27/17.
 */

import BaseMenuItem from '../Base/MenuItem';

export default class MenuItem extends BaseMenuItem {

    constructor(attributes = {}) {
        super(attributes);
    }

    markup() {
        let icon   = `<div class="item-media"><i class="fa fa-${this.icon}"></i></div>`;
        let active = this.active ? 'item-active' : '';
        return `
         <a class="item-link item-content ${active}" data-url="${this.url}" data-id="${this.id}">                                 
            ${this.icon ? icon : ''}
            <div class="item-inner no-arrow">
                <div class="item-title"> ${this.label}</div>
            </div>                        
        </a>`;
    }
}