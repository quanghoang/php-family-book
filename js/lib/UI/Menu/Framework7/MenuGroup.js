/**
 * Created by hoangq on 2/27/17.
 */

import Util from 'lib/UI/Helper/Util';
import BaseMenuGroup from '../Base/MenuGroup';
import MenuItem from './MenuItem';

export default class MenuGroup extends BaseMenuGroup {

    constructor(attributes = {}) {
        super(attributes);
    }

    addMenuItem(items) {
        if (Util.isEmpty(items)) {
            return;
        }
        for (let menu of Util.toArray(items)) {
            if (menu.type == 'item') {
                menu.parent = this;
                this.components.push(new MenuItem(menu));
            }
        }
    }

    markup() {
        let content = '';
        for (let item of this.searchItems(this.search)) {
            content += `<li>${item.createWrapper(item.markup())}</li>`;
        }
        return !content ? '' : `
        <div class="content-block-title">${this.label}</div>
        <div class="list-block">
            <ul>${content}</ul>
        </div>`;
    }
}