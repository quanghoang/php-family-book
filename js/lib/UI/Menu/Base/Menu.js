/**
 * Created by hoangq on 2/24/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import MenuGroup from 'lib/UI/Menu/Base/MenuGroup';
import MenuItem from 'lib/UI/Menu/Base/MenuItem';

export default class Menu extends Component {

    constructor(attributes={}) {
        super(attributes);
        this.method         = attributes.method || null;
        this.searchBar      = Util.toBool(attributes.searchBar, false);
        this.containerClass = 'uc uc-menu';
        this.components     = [];
        this.keyword        = '';

        this.addMenu(attributes.menu);
    }

    init() {
        let container = this.getContainer();

        let input = container.find('input').on('change keyup', () => {
            let keyword = input.val();
            if (keyword != this.keyword) {
                this.search(keyword);
            }
        });

        this.getMenuContent().click(e => {
            let a = $(e.target).closest('a');
            if (!a.length) {
                return false;
            }

            let item = this.findMenuItemById(a.data('id'));
            if (item.active) {
                this.trigger('close-left-menu');
                return;
            }
            this.setActiveItem(item);
            this.trigger('click', {url: a.data('url'), item: item});
        });
    }

    /**
     * Add menu groups & items
     * @param menus
     */
    addMenu(menus) {
        if (Util.isEmpty(menus)) {
            return;
        }

        for (let menu of Util.toArray(menus)) {
            if (menu && menu.type == 'group') {
                this.components.push(new MenuGroup(menu));
            }
        }
    }

    /**
     * Get menu items
     * @returns {Array}
     */
    getMenuItems() {
        let items = [];
        for (let group of this.components) {
            for (let item of group.components) {
                items.push(item);
            }
        }
        return items;
    }

    /**
     * Find menu by Id
     * @param itemId string
     * @returns {*}
     */
    findMenuItemById(itemId) {
        for (let item of this.getMenuItems()) {
            if (item.id == itemId) {
                return item;
            }
        }
        return null;
    }

    /**
     * Find menu by name
     * @param itemName string
     * @returns {*}
     */
    findMenuItemByName(itemName) {
        for (let item of this.getMenuItems()) {
            if (item.name == itemName) {
                return item;
            }
        }
        return null;
    }

    /**
     * Search menu items
     * @param keyword
     */
    search(keyword) {
        this.keyword = keyword;

        for (let menu of this.components) {
            menu.search = this.keyword;
        }
        this.updateMenuContent();
    }


    /**
     * Set active menu item
     * @param item MenuItem|string MenuItem object or menu item name
     */
    setActiveItem(item) {
        item = Util.isString(item) ? this.findMenuItemByName(item) : item;
        if (!(item instanceof MenuItem)) {
            return;
        }

        for (let it of this.getMenuItems()) {
            it.setActive(it.id == item.id);
        }
    }


    /**
     * Load menu data from server
     * @param callback function
     */
    loadData(callback) {
        if (!this.method) {
            return;
        }

        this.call(this.method, null, response => {
            this.loaded = true;
            this.addMenu(response.data);
            this.updateMenuContent();

            if (Util.isCallable(callback)) {
                callback(response);
            }
        });
    }

    /**
     * Get menu content
     * @returns {*}
     */
    getMenuContent() {
        return this.getContainer().find('.menu-content');
    }

    /**
     * Update/refresh menu content
     * @returns {string}
     */
    updateMenuContent() {
        let content = '';
        for (let group of this.components) {
            content += group.createWrapper(group.markup());
        }

        if (this.isRendered) {
            this.getMenuContent().html(content);
        }
        return content;
    }


    /**
     * Create search bar, TBD in inherited classes
     * @returns {string}
     */
    createSearchBar() {
        return '';
    }

    /**
     * Markup
     * @returns {string}
     */
    markup() {
        let content = this.updateMenuContent();
        return this.createSearchBar() + `<div class="menu-content">${content}</div>`;
    }
}