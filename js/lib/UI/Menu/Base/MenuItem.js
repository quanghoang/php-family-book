/**
 * Created by hoangq on 2/27/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';

export default class MenuItem extends Component {

    constructor(attributes = {}) {
        super(attributes);
        this.type   = 'MenuItem';
        this.parent = attributes.parent || null;
        this.icon   = attributes.icon || null;
        this.url    = attributes.url || attributes.name;
        this.active = Util.toBool(attributes.active, false);
    }

    setActive(active) {
        if (this.active == active) {
            return;
        }

        this.active = active;
        let a = this.getContainer().find('a');
        if (!a.length) {
            return;
        }
        a.removeClass('item-active');
        if (active) {
            a.addClass('item-active');
        }
    }
}