/**
 * Created by hoangq on 2/27/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import MenuItem from './MenuItem';

export default class MenuGroup extends Component {

    constructor(attributes = {}) {
        super(attributes);
        this.type       = 'MenuGroup';
        this.parent     = attributes.parent || null;
        this.icon       = attributes.icon   || null;
        this.search     = attributes.search || '';
        this.components = [];

        this.addMenuItem(attributes.items || attributes.ui || attributes.components);
    }

    addMenuItem(items) {
        if (Util.isEmpty(items)) {
            return;
        }

        for (let menu of Util.toArray(items)) {
            if (menu.type == 'item') {
                menu.parent = this;
                this.components.push(new MenuItem(menu));
            }

            if (menu.type == 'group') {
                menu.parent = this;
                this.components.push(new MenuGroup(menu));
            }
        }
    }

    /**
     * Search menu items
     * @param keyword
     * @returns {Array}
     */
    searchItems(keyword) {
        let items  = [];
        if (!keyword) {
            return this.components;
        }
        let regexp = new RegExp(Util.escapeRegExp(keyword), 'i');
        for (let item of this.components) {
            if (regexp.test(item.label)) {
                items.push(item);
            }
        }
        return items;
    }
}