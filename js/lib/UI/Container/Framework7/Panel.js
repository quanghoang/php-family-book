/**
 * Created by hoangq on 3/1/17.
 */

import Container from './Container';

export default class Panel extends Container {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Panel';
        this.containerClass = 'uc uc-panel ' + this.class;
    }
}