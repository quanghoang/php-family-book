/**
 * Created by hoangq on 2/10/17.
 */

import Container from './Container';

export default class Flow extends Container {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Flow';
        this.containerClass = 'uc uc-flow';
    }

    /**
     * Markup label
     * @param content
     * @returns {string}
     */
    createLabel(content) {
        let form = this.closest('@Form');
        if (form) {
            if (this.layout == 'horizontal' ) {
                let col     = form.label_columns;
                let classes = `col-sm-${col}`;
                let label   = `<label class="control-label ${classes}">${this.label}</label>`;
                content     = `<div class="col-sm-${12 - col}">${content}</div>`;
                return `<div class="form-group">${label} ${content}</div>`;
            }
        }
        return super.createLabel(content);
    }
}