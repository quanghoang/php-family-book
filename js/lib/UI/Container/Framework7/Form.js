/**
 * Created by hoangq on 2/21/17.
 */

import Component from 'lib/UI/Component';
import Container from './Container';
import Input from 'lib/UI/Input/Framework7/Input';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Config from 'lib/UI/Config';
import Validator from 'lib/UI/Helper/Validator';

export default class Form extends Container {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Form';
        this.containerClass = 'f7 uc uc-form';
        this.title          = attributes.title  || '';
        this.method         = attributes.method || '';
        this.action         = attributes.action || '';
        this.label_columns  = attributes.label_columns || Config['form.horizontal_label_columns'];
        this.error          = []; //validation error message array
        this.validated      = false;
    }

    init() {
        super.init();

        //Init dependents & validations
        for (let component of this.components) {
            if (component instanceof Input) {
                component.initDependents(this);

                if (component.rule) {
                    component.on('change', () => {
                        if (this.validated) {
                            this.validate();
                        }
                    });
                }
            }
        }
    }

    markup() {
        let title   = this.title ? `<div class="content-block-title">${this.title}</div>` : '';
        let content = `
            <div class="list-block">
                ${title}
                <div class="list-block inset ui-error-message hidden"></div>
                <ul><li><span class="progressbar-infinite hidden"></span></li>`;

        for (let component of this.components) {
            if (component instanceof Component) {
                let classes = component.alignTop ? 'align-top' : '';
                content += `<li class="${classes}">${component.createWrapper()}</li>`;
                continue;
            }
            content += `<li class="uc">${Html.icon(String(component))}</li>`;
        }
        content += '</ul></div>';
        content   = Html.createForm(this.id, this.name, content, this.class, this.method, this.action);
        return content;
    }

    /**
     * Show/hide progress bar
     * @param status bool
     */
    showProgressbar(status=true) {
        let bar = this.getContainer().find('.progressbar-infinite').addClass('hidden');
        if (status) {
            bar.removeClass('hidden');
        }
    }

    /**
     * Disabled all input components in form
     * @param status bool
     */
    disable(status=true) {
        let items = this.getContainer('li.item-content').removeClass('disabled');
        if (status) {
            items.addClass('disabled');
        }
    }

    showError() {
        let container = this.getContainer().find('.ui-error-message').addClass('hidden');
        if (!this.error.length) {
            return;
        }

        let content = '';
        for (let error of this.error) {
            content += `<li><i class="fa fa-close fa-li"></i> ${error}</li>`;
        }

        content = `
        <ul>
            <li><i class="fa fa-exclamation-triangle fa-li"></i> Please check following field(s) again.</li>
            ${content}
        </ul>`;

        container.html(content);
        container.removeClass('hidden');
    }


    validate() {
        let valid      = true;
        this.validated = true;
        this.error     = [];
        let depend     = {};

        for (let input of this.getInputs()) {
            if (input.validate()) {
                continue;
            }

            valid = false;
            if (!depend[input.name]) {
                this.error.push(input.error);
            }

            for (let rule of Validator.parseRule(input.rule)) {
                if (rule.name == 'required_depend') {
                    depend[input.name] = rule.param;
                    depend[rule.param] = input.name;
                }
            }
        }
        this.showError();
        return valid;
    }
}