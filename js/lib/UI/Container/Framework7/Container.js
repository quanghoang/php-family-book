/**
 * Created by hoangq on 3/2/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import {factory} from 'lib/UI/Factory/Framework7/Factory';
import BaseContainer from '../Base/Container';

export default class Container extends BaseContainer {

    constructor(attributes={}) {
        super(attributes);
        this.containerClass = 'f7 uc uc-container ' + this.class;
    }

    add(components) {
        if (Util.isEmpty(components)) {
            return;
        }
        for (let component of Util.toArray(components)) {
            component = component instanceof Component ? component : factory(component);
            component.layout = this.layout;
            component.parent = this;
            this.components.push(component);
        }
    }
}