/**
 * Created by hoangq on 2/22/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Container extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Container';
        this.isContainer    = true;
        this.containerClass = 'uc uc-container ' + this.class;
        this.layout         = attributes.layout || 'vertical';
        this.width          = attributes.width  || 0;
        this.height         = attributes.height || 0;
        this.value          = attributes.value  || null;
        this.scrollbar      = Util.toBool(attributes.scrollbar, true);
    }

    add(components) {
        if (Util.isEmpty(components)) {
            return;
        }
        for (let component of Util.toArray(components)) {
            component.layout = this.layout;
            component.parent = this;
            this.components.push(component);
        }
    }

    removeAll() {
        for (let component of this.components) {
            component.destroy();
        }
        this.components = [];
        this.setContent('');
    }

    markup() {
        return this.createLabel(this.markupComponents());
    }

    markupComponents(components=null) {
        let content = '';
        components  = components || this.components || [];
        for (let component of Util.toArray(components)) {
            if (component instanceof Component) {
                content += component.createWrapper();
                continue;
            }
            content += `<div class="uc">${Html.icon(String(component))}</div>`;
        }
        return content;
    }

    renderComponents(components=null) {
        components = components || this.components || [];
        for (let component of Util.toArray(components)) {
            if (component instanceof Component) {
                component.render();
            }
        }
    }

    render() {
        if (this.value) {
            this.setValue(this.value);
        }

        let container = this.setContent(this.markup());
        if (this.width) {
            container.css('width', this.width);
        }
        if (this.height) {
            container.css('height', this.height);
            if (this.scrollbar) {
                container.css('overflow-y', 'scroll');
            }
        }

        this.renderComponents();
        this.init();
    }

    refresh() {
        super.refresh();

        for (let component of this.components) {
            component.render();
        }
    }
}