/**
 * Created by hoangq on 3/1/17.
 */

import Container from './Container';
import Component from 'lib/UI/Component';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';
import Bootstrap from 'lib/UI/Helper/Bootstrap';

export default class Panel extends Container {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Panel';
        this.containerClass = 'uc uc-panel ' + this.class + (this.label ? ' ui-with-header' : '');
        this.columns        = attributes.columns || 1;
        this.accordion      = attributes.accordion || false;
        this.collapsed      = Util.toBool(attributes.collapsed, false); //collapse accordion or not
        this.accordionIcons = {'open': '[i:caret-up]', 'close': '[i:caret-down]'};
    }

    init() {
        super.init();

        if (this.accordion) {
            let accordion  = $(`#${this.id}a`);
            let toggle = $(`#${this.id}t`);
            let toggleIcon = toggle.find('.ui-accordion-toggle');

            toggle.click(() => {
                this.collapsed = !this.collapsed;
                toggleIcon.html(Html.icon(this.accordionIcons[this.collapsed ? 'close' : 'open']));
            });

            toggleIcon.click(e => {
                toggle.click();
                return false;
            });
        }
    }

    markup() {
        if (this.columns < 0) {
            throw 'Number of columns must be greater than 0.';
        }
        if (this.columns == 1) {
            return this.createPanel(this.markupComponents());
        }

        let column   = Math.floor(12.0 / this.columns);
        let cssClass = `col-sm-12 col-md-${column}`;
        let content  = '<div class="row">';

        for (let component of this.components) {

            if (component instanceof Component) {
                content += component.createWrapper('', cssClass);
                continue;
            }
            content += `<div class="uc ${cssClass}">${Html.icon(String(component))}</div>`;
        }
        content += '</div>';
        return this.createPanel(content);
    }

    createPanel(content) {
        if (!this.label) {
            return content;
        }

        let accordionIcon  = this.accordionIcons[this.collapsed ? 'close' : 'open'];
        let accordionToggle = this.accordion ? `data-toggle="collapse" data-target="#${this.id}a"` : '';

        return `
        <div class="panel ${this.class || 'panel-default'}">    	
			<div id="${this.id}t" class="panel-heading ${this.accordion ? 'ui-accordion' : ''}" ${accordionToggle}>
                ${Html.icon(this.label)}
                <span class="ui-accordion-toggle ${this.accordion ? '' : 'ui-hide'}">${Html.icon(accordionIcon)}</span>
            </div>
			<div id="${this.id}a" class="panel-body ${this.accordion ? 'collapse' : ''} ${this.collapsed ? '' : 'in'}">            
                ${content}
            </div>                
        </div>`;
    }

    render() {
        super.render();
        Bootstrap.initToggles(this.getContainer());
    }
}