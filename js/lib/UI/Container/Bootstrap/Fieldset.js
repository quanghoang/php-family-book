/**
 * Created by hoangq on 8/8/17.
 */

import Panel from './Panel';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';

export default class Fieldset extends Panel {

    constructor(attributes={}) {
        super(attributes);

        this.type            = 'Fieldset';
        this.containerClass += ' uc-fieldset ';
        this.accordionIcons  = {'open': '[i:angle-up]', 'close': '[i:angle-down]'};
    }

    createPanel(content) {
        if (!this.label) {
            return content;
        }

        let accordionIcon  = this.accordionIcons[this.collapsed ? 'close' : 'open'];
        let accordionToggle = this.accordion ? `data-toggle="collapse" data-target="#${this.id}a"` : '';

        return `
        <fieldset>
            <legend id="${this.id}t" ${accordionToggle} class="${this.accordion ? 'ui-accordion' : ''}">
                ${Html.icon(this.label)}
                <span class="ui-accordion-toggle ${this.accordion ? '' : 'ui-hide'}">${Html.icon(accordionIcon)}</span>
            </legend>
            <div id="${this.id}a" class="fieldset-body ${this.accordion ? 'collapse' : ''} ${this.collapsed ? '' : 'in'}">
               ${content}
            </div>
        </fieldset>`;
    }
}