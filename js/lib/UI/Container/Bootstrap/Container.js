/**
 * Created by hoangq on 2/22/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import {factory} from 'lib/UI/Factory/Bootstrap/Factory';
import BaseContainer from '../Base/Container';


export default class Container extends BaseContainer {

    constructor(attributes={}) {
        super(attributes);
        this.containerClass = 'b3 uc uc-container ' + this.class;
    }

    add(components) {
        if (Util.isEmpty(components)) {
            return;
        }
        for (let component of Util.toArray(components)) {
            component = component instanceof Component ? component : Container.factory(component);
            component.layout = this.layout;
            component.parent = this;
            this.components.push(component);
        }
    }

    static factory(component) {
        return factory(component);
    }
}