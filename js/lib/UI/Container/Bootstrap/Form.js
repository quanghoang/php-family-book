/**
 * Created by hoangq on 2/10/17.
 */

import Component from 'lib/UI/Component';
import Container from './Container';
import Input from 'lib/UI/Input/Bootstrap/Input';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Config from 'lib/UI/Config';
import Validator from 'lib/UI/Helper/Validator';
import Bootstrap from 'lib/UI/Helper/Bootstrap';

export default class Form extends Container {

    constructor(attributes={}) {
        super(attributes);

        this.type              = 'Form';
        this.containerClass    = 'b3 uc uc-form';
        this.title             = attributes.title || '';
        this.method            = attributes.method || ''; //loading method
        this.action            = attributes.action || '';
        this.value             = attributes.value || null;
        this.submitMethod      = attributes.submitMethod || 'GET'; //GET or POST
        this.enctype           = attributes.enctype || null; //Eg. multipart/form-data
        this.label_columns     = attributes.label_columns || Config['form.horizontal_label_columns'];
        this.error             = []; //validation error message array
        this.validated         = false; //form was validated or not (validation status)
        this.validateMethod    = attributes.validateMethod || null;   //custom validate on server side
        this.validateCallback  = attributes.validateCallback || null; //custom validate on client side
        this.validateOnSubmit  = Util.toBool(attributes.validateOnSubmit, true); //validate on submit form
        this.validateInvisible = Util.toBool(attributes.validateInvisible, false);  //validate invisible inputs or not
        this.saveMethod        = attributes.saveMethod || null; //save form method
        this.canSubmit         = Util.toBool(attributes.canSubmit, true);
    }

    init() {
        super.init();
        Bootstrap.initToggles(this.getContainer());

        //Init dependents & validations
        for (let input of this.getInputs()) {
            input.initDependents(this);
            if (input.rule) {
                input.on('change', e => {
                    if (this.validated) {
                        this.validate();
                    }
                });
            }
        }

        if (this.validateMethod) {
            this.initRemoteValidation();
        }

        this.isSubmitted = false;
        this.getContainer().find('button[type=submit]').click(() => {
            this.isSubmitted = true;
        });

        this.getContainer().find('form').submit(e => {
            this.trigger('submit', e);

            if (!this.canSubmit) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            return true;
        });

        if (this.validateOnSubmit) {
            this.on('submit', e => {
                if (!this.validate()) {
                    e.preventDefault();
                }
            });
        }

        if (this.saveMethod) {
            this.canSubmit = false; //disable form submit when saveMethod is used.
            this.on('validated', result => {
                if (result.valid && this.isSubmitted) {
                    this.call(this.saveMethod, this.getValue(), response => {
                        this.trigger('saved', response);
                    });
                }
                this.isSubmitted = false;
            });
        }

        this.trigger('form.init', this);
    }

    initRemoteValidation() {
        this.validateOnSubmit = false;
        let submitInput       = null;

        for (let input of this.getInputs()) {
            if (input.type == 'Submit') {
                input.getInput().on('click', () => {
                    submitInput = input.getInput();
                    this.validate();
                    return false;
                });
            }
        }

        this.on('validated', e => {
            if (submitInput && e.valid && !e.error.length) {
                submitInput.off('click').click();
            }
            submitInput = null;
        })

    }

    getFormContainer() {
        return $('#' + this.id);
    }

    submit() {
        this.getFormContainer().submit();
    }

    markup() {
        if (this.layout == 'horizontal') {
            this.class += 'form-horizontal';
        }

        let content = `
        ${this.title ? `<h3 class="ui-title">${this.title}</h3>` : ''}
        <div class="ui-error-message alert alert-danger hidden"></div>
        ${super.markup()}`;

        return Html.createForm(this.id, this.name, content, this.class, this.submitMethod, this.action, this.enctype);
    }


    /**
     * Disabled all input components in form
     * @param status bool
     */
    disable(status=true) {
        for (let component of this.components) {
            if (component instanceof Input) {
                component.enableInput(!status);
            }
        }
    }

    setFocus(focus=true) {
        for (let input of this.getInputs()) {
            if (input.focus) {
                input.setFocus(focus);
                break;
            }
        }
    }

    /**
     * Show error messages
     */
    showError() {
        let container = this.getContainer().find('.ui-error-message').addClass('hidden');
        if (!this.error.length) {
            return;
        }

        let content = '';
        for (let error of this.error) {
            content += `<li><i class="fa fa-close fa-li"></i> ${error}</li>`;
        }

        //<li><i class="fa fa-exclamation-triangle fa-li"></i> Please check following field(s) again.</li>
        content = `<ul>${content}</ul>`;

        container.html(content);
        container.removeClass('hidden');
    }

    clearError() {
        this.error = [];
    }

    addError(message) {
        this.error.push(message);
    }

    setError(message, fieldName=null) {
        this.error.push(message);

        if (fieldName) {
            let input = this.getInput(fieldName);
            if (input) {
                input.highlightError(false);
            }
        }
    }

    /**
     * Show progress bar
     * @param status
     */
    showProgressbar(status=true) {
    }

    /**
     * Validate form
     * @returns {boolean}
     */
    validate() {
        let valid      = true;
        this.validated = true;
        this.error     = [];
        let depend     = {};


        //validate on client side
        for (let input of this.getInputs()) {
            if (!this.validateInvisible && !input.isVisible()) {
                continue;
            }
            if (input.validate()) {
                continue;
            }

            valid = false;
            if (!depend[input.name]) {
                this.error.push(input.error);
            }

            for (let rule of Validator.parseRule(input.rule)) {
                if (rule.name == 'required_depend') {
                    depend[input.name] = rule.param;
                    depend[rule.param] = input.name;
                }
            }
        }

        if (Util.isCallable(this.validateCallback)) {
            valid &= this.validateCallback();
        }

        //validate on server side
        if (this.validateMethod && !this.loading) {
            this.loading = true;
            this.call(this.validateMethod, this.getValue(), response => {
                this.loading = false;
                if (response.error) {
                    this.trigger('validate.error', response);
                    return;
                }

                let result = response.data;
                result = result ? result : {valid: true};
                valid &= result.valid;

                for (let error of Util.toArray(result.message)) {
                    this.error.push(error);
                }

                for (let field of Util.toArray(result.highlight)) {
                    let input = this.getInput(field);
                    if (input) {
                        input.highlightError(false);
                    }
                }
                this.showError();
                this.trigger('validated', {valid: Util.toBool(valid), error: this.error, remote: true});
            });
            return true;
        }

        this.showError();
        this.trigger('validated', {valid, error: this.error, remote: false});
        return valid;
    }

    /**
     * Close current opening dropdown if exist
     * (used to hide dropdowns when closing dialogs)
     */
    closeDropdown() {
        for (let input of this.getInputs()) {
            switch (input.type) {
                case 'Select':
                    input.closeDropdown();
                    break;

                case 'Textarea':
                    if (input.html) {
                        top.tinymce.activeEditor.windowManager.close();
                    }
                    break;
            }
        }
    }
}