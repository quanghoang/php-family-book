/**
 * Created by hoangq on 1/17/17.
 */

import Component from 'lib/UI/Component';
import Container from './Container';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Dialog extends Container {

    constructor(attributes={}) {
        super(attributes);

        this.type        = 'Dialog';
        this.title       = attributes.title || '';
        this.body        = '';
        this.size        = attributes.size || 'medium'; //'small', 'medium', 'large', 'larger', 'full', 'all'
        this.height      = attributes.height || 0; //max height of the body content, use 0 for unlimited
        this.class       = attributes.class || 'default'; //header class, 'default', 'primary', etc.
        this.align       = attributes.class || 'top'; //vertical align: 'top' or 'center'
        this.parseIcon   = Util.toBool(attributes.parseIcon, true); //parse icons in body content or not
        this.titleIcon   = attributes.titleIcon || '';
        this.effect      = attributes.effect || ''; //Eg. fade
        this.bodyPadding = Util.toBool(attributes.bodyPadding, false);
        this.buttons     = [];
        this.level       = attributes.level || 1; //popup level, used for restoring page scrollbar after closing dialog
        this.tabIndex    = attributes.tabIndex || 0;

        let body = attributes.body || attributes.ui || attributes.components || null;
        if (body) {
            if (!Component.isComponent(body) && Util.isObject(body)) {
                body = Container.factory(body);
            }
            this.setBody(body);
        }

        if (attributes.buttons) {
            this.addButton(attributes.buttons);
        }
    }

    /**
     * Init dialog toggles
     * @param container jquery
     */
    static initToggles(container=null) {
        if (!container || !container.length) {
            return;
        }

        container.find('[data-ui-dialog]').each((index, item) => {
            let element = $(item);
            if (element.data('init')) {
                return; ////skip this element if already initialized
            }

            element.data('init', true);
            let param  = element.data('param');
            let method = element.data('ui-dialog');

            element.on('click', e => {
                //loading dialog if not loaded
                if (!element.data('loading')) {
                    element.data('loading', true);
                    Dialog.callMethod(method, param, response => {
                        element.data('loading', false);

                        let attributes = response.data;
                        if (!Util.isObject(attributes)) {
                            return;
                        }
                        attributes.body = Util.isObject(attributes.body) ? Dialog.factory(attributes.body) : attributes.body;
                        let dialog      = new Dialog(attributes);
                        dialog.open();
                    });
                }
            });
        });
    }

    /**
     * Display alert dialog
     * @param title string
     * @param body string|Component
     * @param size string
     * @param classes string
     * @param id string
     * @returns {Dialog}
     */
    static alert(title, body, size='small', classes='danger') {
        let dialog = new Dialog({
            title:   title,
            body:    body,
            'class': classes,
            size:    size
        });
        dialog.addButton({name: 'close', click: 'destroy'});
        dialog.open();
    }

    /**
     * Display info dialog
     * @param title
     * @param body
     * @param size
     * @param classes
     * @param id
     */
    static info(title, body, size='small', classes='info') {
        Dialog.alert(title, body, size, classes);
    }

    /**
     * Confirm dialog
     * @param title
     * @param body
     * @param callback Callback function to handle Yes button click
     * @param size
     * @param classes
     */
    static confirm(title, body, callback, size='medium', classes='success') {
        let dialog = new Dialog({
            title:   title,
            body:    body,
            'class': classes,
            size:    size
        });
        dialog.addButton([
            {name: 'yes', class: 'primary', click: () => {
                if (Util.isCallable(callback)) {
                    callback();
                }
                dialog.destroy();
            }},
            {name: 'no', click: 'destroy'}
        ]);
        dialog.render();
        dialog.open();
    }

    /**
     * Add button
     * @param button object|array Button object, eg. {name: 'OK', click: () => { alert('OK Clicked'); }}
     */
    addButton(button) {
        for (let btn of Util.toArray(button)) {
            btn = btn || {};
            if (!btn.name) {
                throw new Error('Button name is required.');
            }

            let click = btn.click || null;
            click = click === 'close'   ? () => {this.close()}   : click;
            click = click === 'destroy' ? () => {this.destroy()} : click;

            if (btn.hide) {
                continue;
            }

            this.buttons.push({
                id:        btn.id || this.id + '-button-' + btn.name,
                name:      btn.name,
                label:     btn.label || Util.ucFirst(btn.name),
                class:     btn.class || (btn.default ? 'primary' : 'default'),
                click:     Util.isCallable(click) ? click : null,
                default:   Util.toBool(btn.default, false),
                align:     btn.align || '',
                tooltip:   btn.tooltip || '',
                disabled:  Util.toBool(btn.disabled, false),
                visible:   Util.toBool(btn.visible, true),
            });
        }
    }

    /**
     * Get button by name
     * @param name string
     * @returns {*}
     */
    getButton(name) {
        for (let button of this.buttons) {
            if (button.name == name) {
                return button;
            }
        }
        return null;
    }

    /**
     * Get button input
     * @param name
     * @returns {void|*|jQuery|HTMLElement}
     */
    getButtonInput(name) {
        let button = this.getButton(name);
        return $('#' + button.id);
    }

    /**
     * Show button(s)
     * @param buttonName string|array
     */
    showButton(buttonName, show=true) {
        for (let name of Util.toArray(buttonName)) {
            let button = this.getButtonInput(name).hide();
            if (show) {
                button.show();
            }
        }
    }

    /**
     * Enable button(s)
     * @param buttonName string|array
     * @param enable
     */
    enableButton(buttonName, enable=true) {
        for (let name of Util.toArray(buttonName)) {
            this.getButtonInput(name).prop('disabled', !enable);
        }
    }

    isButtonEnabled(buttonName) {
        let button  = this.getButtonInput('save');
        return button && !button.prop('disabled');
    }

    /**
     * Get dialog wrapper
     * @returns {jQuery}
     */
    getDialog() {
        return $('#' + this.id);
    }

    /**
     * Open dialog
     */
    open() {
        if (!this.isRendered) {
            this.render();
        }
        this.trigger('before.open');

        this.getDialog().on('shown.bs.modal', e => {
            this.trigger('shown');
        });

        this.getDialog().modal({show: true, backdrop: true, keyboard: true}).on('hidden.bs.modal', e => {
            this.trigger('close');
        });

        this.trigger('open');
    }

    /**
     * Close dialog
     */
    close() {
        if (!this.isRendered) {
            return;
        }
        this.trigger('before.close');
        this.getDialog().modal('hide');

        if (this.level > 1) {
            $('body').addClass('modal-open');
        }
    }

    /**
     * Destroy dialog (close and remove dialog from DOM)
     */
    destroy() {
        if (!this.isRendered) {
            return;
        }
        this.close();
        this.removeContainer();
        super.destroy();
        this.trigger('destroy');
    }

    /**
     * Call button click callback function
     * @param buttonName string
     */
    click(buttonName) {
        for (let button of this.buttons) {
            if (button.name == buttonName && Util.isCallable(button.click)) {
                button.click();
                break;
            }
        }
    }

    /**
     * Set dialog title
     * @param title string
     */
    setTitle(title) {
        this.title = Html.icon(title);

        if (this.isRendered) {
            this.getContainer().find('.modal-title .ui-title').html(title);
        }
    }

    /**
     * Set dialog title icon
     * @param title string
     */
    setTitleIcon(icon) {
        this.titleIcon = Html.icon(icon);

        if (this.isRendered) {
            let titleIcon = this.getContainer().find('.modal-title .ui-title-icon');
            titleIcon.html(this.titleIcon);
            titleIcon.removeClass('ui-hide');
            if (!icon) {
                titleIcon.addClass('ui-hide');
            }
        }
    }

    /**
     * Get body container
     * @returns {*}
     */
    getBodyContainer() {
        return this.getContainer().find('.modal-body');
    }

    /**
     * Set dialog body
     * @param body string
     */
    setBody(body) {
        let isComponent = body instanceof Component;
        let content     = isComponent ? body.createWrapper() : body;
        this.body       = this.parseIcon && !isComponent ? Html.icon(body) : body;

        if (this.isRendered) {
            content = this.parseIcon ? Html.icon(content) : content;
            let bodyContainer = this.getBodyContainer().html(content);

            if (this.height) {
                bodyContainer.css('overflow', 'auto');
                bodyContainer.css('max-height', this.height);
            }
            if (isComponent) {
                body.render();
            }
        }
        return this;
    }

    /**
     * Scroll body content to top (to show error messages)
     * @param top
     */
    scrollTop(top=0) {
        this.getBodyContainer().scrollTop(top);
    }

    /**
     * Get dialog size (convert from text to bootstrap class name)
     * @returns {*|string}
     */
    getSize() {
        let sizes = {small: 'sm', medium: 'md', large: 'lg', larger: 'xl', full: 'full', all: 'all'};
        return sizes[this.size] || 'md';
    }

    /**
     * Init dialog
     */
    init() {
        //handle button click event
        this.getContainer().find('.btn').click(e => {
            let btn = $(e.currentTarget);
            let name = btn.attr('name');
            let button = this.getButton(name);

            if (button && Util.isCallable(button.click)) {
                this.trigger('button.click', {button: name});
                button.click();
            }
            this.trigger(`button.${name}.click` , {});
        });

        //handle submit event for default button
        for (let button of this.buttons) {
            if (button.default) {
                this.getContainer().find('form').submit(() => {
                    this.getContainer().find(`.btn[name=${button.name}]`).click();
                    return false;
                });
                break;
            }
        }

        // let body = $('body');
        // if (!body.data('init-dialog')) {
        //     body.data('init-dialog', true);
        //     body.keyup(e => {
        //         if (e.keyCode == 27) {
        //             this.close();
        //         }
        //     });
        // }
    }

    /**
     * Create markup
     * @returns {string}
     */
    markup() {
        let buttons = '';
        for (let button of this.buttons) {
            let cssClass = 'btn-' + button.class + (button.align ? ' float-' + button.align : '');
            buttons += Html.createButton(button.id, button.name, button.label, cssClass, false, button.tooltip, button.disabled, button.visible);
        }

        let header  = `
            <div class="modal-header modal-header-${this.class}">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">
                    <span class="ui-title-icon ${this.titleIcon ? '' : 'ui-hide'}">${Html.icon(this.titleIcon)}</span>
                    <span class="ui-title">${Html.icon(this.title)}</span>
              </h4>
            </div>`;

        let footer      = `<div class="modal-footer">${buttons}</div>`;
        let modalClass  = this.size == 'full' || this.size == 'all' ? 'modal-fullscreen' : '';
        modalClass     += this.size == 'all' ? ' all' : '';
        let tabIndex    = this.tabIndex ? `talogin_namebindex="${this.tabIndex}"` : '';
        let bodyPadding = this.bodyPadding ? 'ui-modal-body-padding' : '';
        let bodyHeight  = this.height ? `style="overflow:auto;max-height:${this.height}px"` : '';

        return `
        <div class="b3 ui-dialog">
            <div class="modal ${modalClass} ${this.effect}" 
                 id="${this.id}" role="dialog" ${tabIndex}>
                <div class="${this.align == 'center' ? 'vertical-alignment-helper' : ''}">
                    <div class="modal-dialog modal-${this.getSize()} ${this.align == 'center' ? 'vertical-align-center ' : ''}">
                      <div class="modal-content">
                        ${this.title ? header : ''}
                        <div class="modal-body ${bodyPadding}" ${bodyHeight}>
                            ${this.body instanceof Component ? this.body.createWrapper() : this.body}
                        </div>
                        ${this.buttons.length ? footer : ''}
                      </div>
                    </div>
                </div>
            </div>
        </div>`;
    }

    /**
     * Render content
     */
    render() {
        if (!this.hasContainer()) {
            this.createContainer();
        }
        this.setContent(this.markup());
        this.init();

        if (this.body instanceof Component) {
            this.body.render();
        }
        this.trigger('ready');
    }
}