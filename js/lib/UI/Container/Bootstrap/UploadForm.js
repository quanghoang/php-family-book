import Form from './Form';

export default class UploadForm extends Form {
    constructor(attributes={}) {
        attributes.submitMethod = 'POST';
        attributes.enctype      = "multipart/form-data";
        super(attributes);
    }
}