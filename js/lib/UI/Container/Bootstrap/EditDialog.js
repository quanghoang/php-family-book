import Container from './Container';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class EditDialog extends Container {

    constructor(attributes = {}) {
        super(attributes);

        this.type            = 'EditDialog';
        this.containerClass  = 'uc-edit-dialog';
        this.detachContainer = attributes.container ? false : true;
        this.title           = attributes.title || '';
        this.size            = attributes.size || 'medium';
        this.height          = attributes.height || 0;
        this.saveMethod      = attributes.saveMethod || null;
        this.validateMethod  = attributes.validateMethod || null;
        this.deleteMethod    = attributes.deleteMethod || null;
        this.canSave         = Util.toBool(attributes.canSave, true);
        this.hideSave        = Util.toBool(attributes.hideSave, false);
        this.isSubmit        = false;
        this.body            = attributes.body || null;
        this.buttons         = attributes.buttons || {save: {}, cancel: {}};
        this.level           = attributes.level || 1; //dialog level

        this.initDialog(attributes);
    }

    initDialog(attributes) {
        attributes.form   = attributes.form   || {};
        attributes.dialog = attributes.dialog || {};

        //init form
        this.form = Container.factory({
            type:           'form',
            canSubmit:      false,
            validateMethod: this.validateMethod,
            ui:             this.body,
            ...attributes.form
        });

        //init buttons
        let buttons = {
            save:   {name: 'save',   label: 'Save',   default: true, disabled: !this.canSave, hide: this.hideSave},
            cancel: {name: 'cancel', label: 'Cancel', click: 'close'},
            delete: {name: 'delete', label: '[i:trash]', align: 'left'}
        };
        if (this.buttons.save) {
            buttons.save   = {...buttons.save, ...this.buttons.save};
        }
        if (this.buttons.cancel) {
            buttons.cancel = {...buttons.cancel, ...this.buttons.cancel};
        }
        if (this.buttons.delete) {
            buttons.delete = {...buttons.delete, ...this.buttons.delete};
        }

        let dialogButtons = [];
        if (this.deleteMethod) {
            dialogButtons.push(buttons.delete);
        }
        if (this.canSave && this.saveMethod) {
            dialogButtons.push(buttons.save);
        }
        dialogButtons.push(buttons.cancel);

        //init dialog
        this.dialog = Container.factory({
            type:    'dialog',
            title:   this.title,
            size:    this.size,
            height:  this.height,
            level:   this.level,
            class:   this.class,
            buttons: dialogButtons,
            ...attributes.dialog
        });
    }

    init() {
        this.dialog.one('close', () => {
            this.setContent('');
            if (this.detachContainer) {
                this.removeContainer();
            }
            this.form.destroy();
            this.dialog.destroy();
            this.trigger('close');
            this.destroy();
        });

        this.dialog.one('button.save.click', () => {
            this.submit();
        });

        this.dialog.one('button.delete.click', () => {
            this.delete();
        });

        this.form.one('submit', () => {
            this.submit();
        });

        this.form.one('validated', result => {
            if (this.isSubmit && result.valid) {
                this.save();
            }
            this.isSubmit = false;
        });

        this.dialog.one('shown', () => {
            this.trigger('shown');
            this.form.setFocus();
        });
    }

    open() {
        if (!this.isRendered) {
            this.render();
        }
        this.dialog.open();
        this.trigger('open');
    }

    close() {
        this.dialog.close();
    }

    submit() {
        this.isSubmit = true;
        this.form.validate();
    }

    save() {
        let data = this.form.getValue();
        this.trigger('save', data);

        if (this.saveMethod) {
            this.call(this.saveMethod, data, response => {
                this.trigger('saved', response.data);
                this.close();
            });
            return;
        }
        this.close();
    }

    delete() {
        let data = this.form.getValue();
        this.trigger('delete', data);

        if (this.deleteMethod) {
            this.call(this.deleteMethod, data, response => {
                this.trigger('deleted', response.data);
                this.close();
            });
            return;
        }
        this.close();
    }

    render() {
        this.setContent('');
        this.dialog.container = this.getContainerId();
        this.dialog.setBody(this.form);
        this.dialog.render();
        this.init();
    }

    showButton(buttonName, show=true) {
        this.dialog.showButton(buttonName, show);
    }
}