/**
 * Created by hoangq on 7/17/17.
 */

import Container from './Container';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Bootstrap from 'lib/UI/Helper/Bootstrap';

export default class Popover extends Container {

    constructor(attributes = {}) {
        super(attributes);
    }

    /**
     * Init popover toggles
     * @param container jquery
     */
    static initToggles(container) {
        if (!container || !container.length) {
            return;
        }

        //init bootstrap popovers
        container.find('[data-toggle="popover"]').each((index, item) => {
            let element = $(item);
            if (!element.data('init')) {
                element.data('init', true);
                element.attr('tabindex', 0);
                element.data('html', 'true');
                element.popover();
            }
        });

        //init ui popovers
        container.find('[data-ui-popover]').each((index, item) => {
            let element = $(item);
            if (element.data('init')) {
                return; //skip this element if already initialized
            }

            element.data('init', true);
            let param  = element.data('param');
            let method = element.data('ui-popover');

            element.on('click', e => {
                //loading popover if not loadded
                if (!element.data('loaded') && !element.data('loading')) {
                    element.data('loading', true);
                    Popover.callMethod(method, param, response => {
                        element.data('loaded', true);
                        element.data('loading', false);

                        let options = response.data;
                        if (!Util.isObject(options)) {
                            return;
                        }
                        element.popover($.extend(options, {trigger: 'manual', html: true})); //merge options
                        container.find('[data-ui-popover]').popover('hide'); //hide all current opening popovers
                        element.popover('show'); //show this popover
                    });
                }

                //show popover if already loaded
                if (element.data('loaded')) {
                    let shown = element.next('div.popover:visible').length;
                    element.popover(shown ? 'hide' : 'show');
                }
            });

            //init toggles in this popover content if found
            element.on('shown.bs.popover', e => {
                let popup = $('#' + element.attr('aria-describedby'));
                Bootstrap.initToggles(popup);
            });
        });

        //hide popover when clicking outside
        let body = $('body');
        if (!body.data('ui-popover')) {
            body.data('ui-popover', true);
            body.on('click', function (e) {
                $('[data-ui-popover]').each(function () {
                    let element = $(this);
                    if (!element.is(e.target) && element.has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        element.popover('hide');
                    }
                });
            });
        }
    }
}