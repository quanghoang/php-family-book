/**
 * Created by hoangq on 8/8/17.
 */

import Container from './Container';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';
import Bootstrap from 'lib/UI/Helper/Bootstrap';

export default class Tab extends Container {

    constructor(attributes = {}) {
        super(attributes);

        this.type           = 'Tab';
        this.containerClass = 'uc uc-tab ';
        this.position       = attributes.position  || 'top'; //top, left, bottom, right
        this.tabHeight      = attributes.tabHeight || 0; //0 for auto
        this.justified      = Util.toBool(attributes.justified, false);
        this.tabs           = [];
        this.activeIndex    = 0; //active tab index

        this.addTab(attributes.tabs);
    }

    init() {
        super.init();

        this.getContainer().find('.nav-tabs a').on('shown.bs.tab', e => {
            let index = $(e.target).data('index');
            let tab = this.tabs[index];
            this.activeIndex = index;

            this.trigger('tab.shown', tab);
            this.trigger(`tab.${tab.name}.shown`, tab);

            Bootstrap.initToggles(this.getContainer());
        });
    }

    addTab(tabs) {
        if (!tabs) {
            return;
        }

        let count = 0;
        let active = 0;
        for (let tab of Util.toArray(tabs)) {
            count++;
            active         = tab.active ? count : active;
            let components = tab.ui || tab.components || [];
            let ui         = [];

            for (let component of Util.toArray(components)) {
                if (Container.isComponent(component)) {
                    ui.push(component);
                }
                else {
                    ui.push(Container.factory(component));
                }
            }

            this.tabs.push({
                id:       tab.id || this.id + '-tab-' + count,
                active:   tab.active || false,
                name:     tab.name || 'tab-' + count,
                label:    tab.label || 'Tab ' + count,
                disabled: Util.toBool(tab.disabled, false),
                ui:       ui
            });

            if (this.tabs && !active) {
                this.tabs[0].active = true;
            }
        }
    }

    createTabs() {
        let navigator = '';
        let content   = '';
        let index     = 0;

        for (let tab of this.tabs) {
            let ui        =  this.markupComponents(tab.ui);
            let active    = tab.active ? 'active' : '';
            let disabled  = tab.disabled ? 'disabled' : '';
            let height    = this.tabHeight ? `style="height: ${this.tabHeight}px; overflow-y: scroll;"` : '';

            content      += `<div class="tab-pane ${active}" 
                                 id="${tab.id}" ${height}>${ui}</div>`;
            navigator    += `<li class="${active} ${disabled}">
                                 <a href="#${tab.id}" data-toggle="tab" data-index="${index++}">${tab.label}</a>
                            </li>`;
        }
        let justified = this.justified && ['top', 'bottom'].includes(this.position) ? 'nav-justified' : '';
        navigator = `<ul class="nav nav-tabs ${justified}">${navigator}</ul>`;
        content   = `<div class="tab-content">${content}</div>`;
        return {navigator, content};
    }

    markup() {
        let tabs = this.createTabs();
        let content = this.position == 'bottom' ? tabs.content + tabs.navigator : tabs.navigator + tabs.content;
        return `<div class="tabbable tabs-${this.position}">${content}</div>`;
    }

    render() {
        super.render();

        for (let tab of this.tabs) {
            if (tab.ui) {
                this.renderComponents(tab.ui);
            }
        }
    }
}