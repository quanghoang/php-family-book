/**
 * Created by hoangq on 3/22/17.
 */

import BaseChart from 'lib/UI/Chart/Base/Chart';
import Highcharts from 'highcharts5-0-2';

export default class Chart extends BaseChart {

    constructor(attributes={}) {
        super(attributes);
        this.containerClass = 'uc uc-chart uc-highchart';
    }

    init() {
        if (this.options) {
            this.chart = Highcharts.chart(this.id + '-chart', this.options);
        }
    }
}