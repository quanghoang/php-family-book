/**
 * Created by hoangq on 3/22/17.
 */

import Component from 'lib/UI/Component';

export default class Chart extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.containerClass = 'uc uc-chart';
        this.options = attributes.options || {};
    }

    markup() {
        return `<div id="${this.id}-chart"></div>`;
    }
}