import Content from './Content';
import Util from '../Helper/Util';
import Html from '../Helper/Html';

export default class Alert extends Content {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Alert';
        this.containerClass = 'uc uc-alert ' + (this.class ? ' ' + this.class : '');
        this.alert          = attributes.alert || 'info'; //Can be info, success, warning, danger
        this.parseIcon      = Util.toBool(attributes.icon, true);
        this.content        = attributes.content || attributes.message || '';
        this.updateMethod   = attributes.updateMethod || ''; //update message from server side when form changed
        this.icons          = {info: 'info-circle', success: 'check-circle', warning: 'exclamation-circle', danger: 'exclamation-triangle'};
    }

    init() {
        super.init();
    }

    markup() {
        let content = this.parseIcon ? Html.icon(this.content) : this.content;
        let icon    = this.icons[this.alert] || 'question-circle';
        icon        = `[im:${icon}]`;
        return `<div class="alert alert-${this.alert}">${Html.icon(icon)} ${content}</div>`;
    }
}