/**
 * Created by hoangq on 2/13/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Content extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Content';
        this.containerClass = 'uc uc-content' + (this.class ? ' ' + this.class : '');
        this.parseIcon      = Util.toBool(attributes.icon, true);
        this.content        = attributes.content || '';
    }

    init() {
        if (Util.isEmpty(this.content) && this.method) {
            this.reload();
        }
    }

    reload() {
        this.call(this.method, this.loadParams, content => {
            this.initLoad(content);
        }, 'html');
    }

    initLoad(content) {
        this.content = content;
        this.setContent(this.markup());
    }

    markup() {
        return this.parseIcon ? Html.icon(this.content) : this.content;
    }

    render() {
        let content = this.markup();
        this.setContent(content);
        this.init();
    }
}