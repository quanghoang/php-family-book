/**
 * Created by hoangq on 1/19/17.
 */

import Text from './Column/Text';

export default class Factory {


    static createColumn(attributes={}) {
        attributes.type = attributes.type || 'text';

        switch (attributes.type.toLowerCase()) {
            case 'text':
                return new Text(attributes);

            default:
                throw `Invalid Attribute Type '${attributes.type}'.`;
        }
    }
}