/**
 * Created by hoangq on 1/18/17.
 */

import Component from '../Component';
import Storage from 'lib/UI/Helper/Storage';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Validator from 'lib/UI/Helper/Validator';
import Factory from './Factory';
import Pagination from '../Input/Bootstrap/Pagination';
//import Dialog from 'lib/UI/Container/Bootstrap/Dialog';
import Arrays from 'lib/UI/Helper/Arrays';
import Bootstrap from 'lib/UI/Helper/Bootstrap';

export default class Table extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.maxNumPages  = 5; //max pages listed in the pagination
        this.method       = attributes.method || null;
        this.layout       = attributes.layout || 'default'; //'default', 'hide-columns', 'vertical', 'horizontal'.
        this.class        = attributes.class || 'table-bordered table-striped';
        this.pagination   = Util.toBool(attributes.pagination, true);
        this.validation   = Util.toBool(attributes.validation, true);
        this.itemsPerPage = attributes.itemsPerPage || 10;
        this.height       = attributes.height || null;
        this.selectRows   = Util.toBool(attributes.selectRows, false);
        this.activePage   = this.pagination ? 1 : 0;
        this.startPage    = 1;
        this.keyword      = '';
        this.columns      = [];
        this.storage      = new Storage();
        this.validator    = new Validator();
        this.screen       = Util.getBootstrapScreen();
        this.editDialog   = null;
        this.selectedRows = {};
        this.searchBar    = Util.toBool(attributes.searchBar, true);

        //pagination
        let pagination = {
            activePage:   this.activePage,
            itemsPerPage: this.itemsPerPage,
            maxNumPages:  this.maxNumPages,
            onChange:     status => this.onPageChanged(status)
        };
        this.pageTop      = new Pagination({...pagination, container: this.id + '-page-top'});
        this.pageBottom   = new Pagination({...pagination, container: this.id + '-page-bottom'});

        //init table
        if (attributes.columns) {
            this.addColumn(attributes.columns);
        }
        if (attributes.data) {
            this.setData(attributes.data);
        }
    }

    /**
     * Add column
     * @param columns
     */
    addColumn(columns) {
        if (Util.isEmpty(columns)) {
            return;
        }
        for (let column of Util.toArray(columns)) {
            column.table = this;
            this.columns.push(Factory.createColumn(column));
        }
    }

    /**
     * Set data
     * @param data
     */
    setData(data) {
        if (!Util.isArray(data)) {
            return;
        }

        //get column having formatter method
        let formatColumns = [];
        for (let column of this.columns) {
            if (column.formatter) {
                formatColumns.push(column);
            }
        }

        //create new field start with '_' for formatted values (for cache purpose)
        for (let column of formatColumns) {
            let name = column.name;
            for (let row of data) {
                row['_' + name] = column.formatValue(row);
            }
        }
        this.storage.setData(data);
    }

    /**
     * Get data
     * @returns {*}
     */
    getData() {
        return this.storage.getData();
    }

    /**
     * Handle page change event
     * @param status
     */
    onPageChanged(status) {
        this.activePage = status.activePage;
        this.startPage  = status.startPage;
        this.refresh();
    }

    /**
     * Update showing message
     */
    updateShowingMessage() {
        let message;
        if (this.pagination) {
            let status = this.pageTop.getStatus();
            message = status.totalItems ? `Showing ${status.startItem} to ${status.endItem} of ${status.totalItems}` : '';
        }
        else {
            let totalItems = this.getData().length;
            message = totalItems ? `Showing ${Math.min(totalItems, 1)} to ${totalItems} of ${totalItems}` : '';
        }
        this.getContainer().find('.showing-message').html(message);
    }

    /**
     * Update pagination status
     */
    updatePagination() {
        let totalItems               = this.getData().length;
        this.pageTop.totalItems      = totalItems;
        this.pageTop.activePage      = this.activePage;
        this.pageTop.itemsPerPage    = this.itemsPerPage;
        this.pageTop.startPage       = this.startPage;
        this.pageBottom.totalItems   = totalItems;
        this.pageBottom.activePage   = this.activePage;
        this.pageBottom.itemsPerPage = this.itemsPerPage;
        this.pageBottom.startPage    = this.startPage;

        if (this.pagination) {
            this.pageTop.render();
            this.pageBottom.render();
        }
        this.updateShowingMessage();
    }

    /**
     * Get current page data
     * @returns {*}
     */
    getCurrentPage() {
        if (!this.pagination) {
            return this.getData();
        }
        return this.storage.getPage(this.activePage, this.itemsPerPage);
    }

    /**
     * Get row by id
     * @param rowId int
     * @returns {null}|{object}
     */
    getRow(rowId) {
        return this.storage.getRowById(rowId);
    }

    /**
     * Get row wrapper
     * @param row object
     * @returns {*}
     */
    getRowWrapper(row) {
        return this.getContainer().find(`tr[data-id=${row._id}]`);
    }

    /**
     * Get column header wrapper
     * @param columnName string
     * @returns {*}
     */
    getHeaderWrapper(columnName) {
        return this.getContainer().find(`th[data-name=${columnName}]`);
    }

    /**
     * Get column by name
     * @param colName
     * @returns {null}|{object}
     */
    getCol(columnName) {
        for (let column of this.columns) {
            if (column.name == columnName) {
                return column;
            }
        }
        return null;
    }

    /**
     * Search on table
     * @param keyword string
     */
    search(keyword) {
        this.keyword               = keyword;
        this.activePage            = 1;
        this.pageTop.activePage    = this.activePage;
        this.pageBottom.activePage = this.activePage;

        this.storage.setKeyword(keyword);
        this.refresh();
    }

    /**
     * Sort table
     * @param sort object,  Eg. {first_name: 'ASC', age: 'DESC'}
     */
    sort(sort) {
        this.storage.setSort(sort);
        this.refresh();
    }

    /**
     * Get current sort object
     * @returns {{}}
     */
    getSortObject() {
        let sort = {};
        for (let column of this.columns) {
            if (column.sort != 'none') {
                sort[column.name] = column.sort.toUpperCase();
            }
        }
        sort = Util.isEmpty(sort) ? {_id: 'ASC'} : sort;
        return sort;
    }

    /**
     * Update sort indicator
     */
    updateSortIndicators() {
        for (let column of this.columns) {
            if (!column.sortable) {
                continue;
            }
            let th = this.getHeaderWrapper(column.name);
            th.removeClass('ui-sort-none ui-sort-asc ui-sort-desc');
            th.addClass('ui-sort-' + column.sort);
        }
    }

    /**
     * Create show selector
     * @returns {string}
     */
    createShowSelector() {
        let itemsPerPage = [10, 20, 50, 100, 500, 1000];
        if (!itemsPerPage.includes(this.itemsPerPage)) {
            let index = 0;
            for (let value of itemsPerPage) {
                if (value > this.itemsPerPage) {
                    break;
                }
                index++;
            }
            itemsPerPage.splice(index, 0, this.itemsPerPage);
        }
        let options = itemsPerPage.map(value => {
            return {value: value, label: value};
        });
        return Html.createSelect(null, 'show', options, this.itemsPerPage, [], false, 'form-control ui-show-selector');
    }

    /**
     * Render toolbar (search bar, layout selector, show selector, pagination)
     * @returns {string}
     */
    renderToolbar() {
        let searchOptions = '';
        for (let column of this.columns) {
            searchOptions += `<li data-value="${column.name}"><a href="#">${column.label}</a></li>`;
        }

        let searchInput = `
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="icon-addon addon-md">                
                    <input type="text" placeholder="Search..." class="form-control ui-search-bar">                                                                        
                    <label for="search" class="glyphicon glyphicon-search" rel="tooltip" title="Search"></label>
                </div>
            </div>
        </div>`;

        return `
        <div class="ui-toolbar">
            <div class="row">                            
                ${this.searchBar ? searchInput : ''}                                                                
                <div class="col-sm-9 col-md-8 col-xs-12 pages hidden-xs">                    
                    <div id="${this.id}-page-top"></div>
                </div>        
                    
                <div class="visible-xs col-xs-12 showing">
                    <div class="showing-message pull-left"></div>
                    
                    <ul class="ui-layout-selector">                       
                        <li data-value="default"><i class="fa fa-table"></i></li>
                        <li data-value="vertical"><i class="fa fa-align-justify"></i></li>
                        <li data-value="horizontal"><i class="fa fa-columns"></i></li>
                    </ul>                    
                </div>
            </div>                            
        </div>`;
    }


    /**
     * Render table footer (pagination + show selector)
     * @returns {string}
     */
    renderFooter() {
        return `
        <div class="ui-footer row">           
            <div class="pages col-sm-7 col-sm-push-5 col-xs-12">
                 <div id="${this.id}-page-bottom" class="${this.pagination ? '' : 'hidden'}"></div>
            </div>
            <div class="showing col-sm-5 col-sm-pull-7 col-xs-12">   
                  <div class="items-per-page form-group ${this.pagination ? '' : 'hidden'}">
                        <span class="hidden-xs">Show</span> 
                        ${this.createShowSelector()}
                        <div class="showing-message"></div>
                  </div>                 
            </div>            
        </div>`;
    }

    /**
     * Render table header (column title)
     * @returns {string}
     */
    renderHeader() {
        let content = '';

        if (this.selectRows) {
            content += '<th class="ui-select-column"><i class="fa fa-square-o"></i></th>';
        }

        for (let column of this.columns) {
            let classes  = '';
            let editable = '';
            let style    = '';

            if (column.editable) {
                classes = 'ui-col-editable';
                editable = '*';
            }

            if (column.align) {
                classes += ' ui-text-' + column.align;
            }

            let width = column.width ? `width="${column.width}"` : '';

            content += `
            <th data-name="${column.name}" style="${style}" ${width}>
                <div class="${classes}">
                    <span class="ui-col-label" title="${column.editable ? 'Editable column' : ''}">
                        ${column.label}${editable}
                    </span>                
                    <span class="ui-col-control ${column.sortable ? '' : 'hidden'}">
                        <span class="ui-col-sort ui-sort-${column.sort}">
                            <i class="ui-icon-asc fa fa-caret-up"></i> 
                            <i class="ui-icon-desc fa fa-caret-down"></i>
                        </span>
                    </span>                
                </div>                
            </th>`;
        }
        return `<thead><tr data-id="header">${content}</tr></thead>`;
    }

    /**
     * Render table body
     * @returns {string}
     */
    renderBody() {
        let content = '';
        let data = this.getCurrentPage();

        for (let row of data) {
            let columns = this.renderRow(row);
            let selected = this.selectedRows[row._id];

            if (this.selectRows) {
                columns = `<td class="ui-select-column"><i class="fa fa-${selected ? 'check-square-o' : 'square-o'}"></i></td>` + columns;
            }
            content += `<tr data-id="${row._id}" class="${selected ? 'ui-selected' : ''}">${columns}</tr>`;
        }
        return `<tbody>${content}</tbody>`;
    }

    /**
     * Refresh selected rows
     * @param row array|null Row object or null for refreshing  all rows in current page
     */
    refreshSelectedRows(row=null) {
        if (!this.selectRows) {
            return;
        }

        let data = row ? [row] : this.getCurrentPage();
        for (let row of data) {
            let tr = this.getRowWrapper(row);
            let selected = this.selectedRows[row._id];

            tr.removeClass('ui-selected');
            if (selected) {
                tr.addClass('ui-selected');
            }
            tr.find('.ui-select-column').html(`<i class="fa fa-${selected ? 'check-square-o' : 'square-o'}"></i>`);
        }
    }

    /**
     * Render row
     * @param row object
     * @returns {string}
     */
    renderRow(row) {
        let content = '';
        for (let column of this.columns) {
            let classes = column.editable ? 'ui-col-editable' : '';
            content += this.renderColumn(column, row);
        }
        return content;
    }

    /**
     * Render column
     * @param column object
     * @param row object
     * @returns {string}
     */
    renderColumn(column, row) {
        let classes  = '';
        let editable = '';
        let style    = '';

        if (column.editable) {
            classes = 'ui-col-editable';
            editable = '*';
        }

        if (column.width) {
            if (['lg', 'md'].includes(this.screen) || ['horizontal'].includes(this.layout)) {
                style = `width: ${column.width}px`;
            }
        }

        if (column.align) {
            classes += ' ui-text-' + column.align;
        }

        return `
            <td data-title="${column.label}${editable}" data-name="${column.name}">
                <div class="ui-cell ${classes}" style="${style}">${column.render(row)}</div>                        
            </td>`;
    }

    /**
     * Refresh row
     * @param row object
     */
    refreshRow(row) {
        let tr = this.getRowWrapper(row);
        tr.html(this.renderRow(row));
    }

    /**
     * Refresh column
     * @param column object
     */
    refreshColumn(column) {
        let data = this.getCurrentPage();
        for (let row of data) {
            let tr = this.getRowWrapper(row);
            tr.find(`td[data-name=${column.name}]`).replaceWith(this.renderColumn(column, row));
        }
    }

    /**
     * Update layout selector
     */
    updateLayoutSelector() {
        let selector = this.getContainer().find('.ui-layout-selector');
        selector.find('li').removeClass('ui-selected').each((i, li) => {
            li = $(li);
            if (li.data('value') == this.layout) {
                li.addClass('ui-selected');
            }
        });
    }

    /**
     * Init table, called one after table rendered
     */
    init() {
        let container = this.getContainer();

        //init pagination
        this.updatePagination();

        //handle search keyword change event
        container.find('.ui-search-bar').on('keyup change', e => {
            let keyword = $(e.currentTarget).val();
            if (keyword == this.keyword) {
                return;
            }
            this.search(keyword);
        });

        //handle change show selector event
        container.find('.ui-show-selector').change(e => {
            this.itemsPerPage = $(e.currentTarget).val();
            container.find('.ui-show-selector').val(this.itemsPerPage);
            this.refresh();
        });


        //handle change layout selector event
        container.find('.ui-layout-selector li').click(e => {
            this.layout = $(e.currentTarget).data('value');
            this.updateLayoutSelector();

            let tc = this.getContainer().find('.ui-table-container');
            tc.removeClass('table-responsive');
            if (this.layout == 'default') {
                tc.addClass('table-responsive');
            }

            container.find('.ui-table').
            removeClass('table-default table-vertical table-horizontal').
            addClass('table-' + this.layout);
        });
        this.updateLayoutSelector();


        //handle window resize event
        $(window).resize(() => {
            let screen = Util.getBootstrapScreen();
            if (screen != this.screen) {
                this.screen = screen;
                this.updateColumnWidth();
            }
        });


        //handle events on table body
        this.initBody();
    }

    /**
     * Update columns width
     */
    updateColumnWidth() {
        let container = this.getContainer();
        container.removeClass('ui-table-lg ui-table-md ui-table-sm ui-table-xs').addClass('ui-table-' + this.screen);

        if (['lg', 'md'].includes(this.screen)) {
            this.refresh();
        }
    }

    /**
     * Init body, called when body changed
     */
    initBody() {
        //handle click event on table
        this.getContainer().click(e => {
            let target  = $(e.target);
            let td      = $(target.closest('td,th'));
            let tr      = $(target.closest('tr'));
            let rowId   = tr.data('id');
            let colName = td.data('name');
            let column  = this.getCol(colName);
            let cell    = td.find('.ui-cell');

            if (rowId == 'header') {
                return this.onHeaderClicked(tr, td, column);
            }
            return this.onBodyClicked(target, tr, td, cell, this.getRow(rowId), column);
        });

        //re-init toggles
        this.on('body.change', e => {
            Bootstrap.initToggles(this.getContainer());
        });
    }

    /**
     * Handle click event on header columns
     */
    onHeaderClicked(tr, th, column) {
        let next    = {none: 'asc', asc: 'desc', desc: 'none'};
        let sorts   = {};

        if (th.hasClass('ui-select-column')) {
            return;
        }

        if (!column.sortable) {
            return;
        }
        column.sort = next[column.sort];

        //reset other columns (sort single column)
        for (let col of this.columns) {
            if (col.name != column.name) {
                col.sort = 'none';
            }
        }
        this.updateSortIndicators();
        this.sort(this.getSortObject());
    }

    /**
     * Handle click event on body columns
     */
    onBodyClicked(el, tr, td, cell, row, column) {
        //click on edit cells
        if (cell.hasClass('ui-col-editable') && !cell.hasClass('editing')) {
            this.closeEditForms();
            this.showEditColumnForm(cell, row, column);
            return false;
        }

        //toggle selected rows
        if (this.selectRows && row) {
            let selected = Util.toBool(this.selectedRows[row._id], false);
            this.selectedRows[row._id] = !selected;
            this.refreshSelectedRows(row);
            this.trigger('select-row', {selected: !selected, row});
        }
    }

    /**
     * Open edit column form
     * @param cell object
     * @param row object
     * @param column object
     */
    showEditColumnForm(cell, row, column) {
        let content = column.renderEdit(row);

        switch (column.getEditMode()) {
            case 'inline':
                cell.addClass('editing');
                cell.html(content);
                break;

            case 'dialog':
                /*this.editDialog = new Dialog({
                    id:     this.id + '-edit-dialog',
                    size:   'medium',
                    body:   content,
                    buttons: [
                        {name: 'save', 'default': true, click: () => {
                            if (column.updateEditValue(row)) {
                                this.editDialog.destroy();
                            }
                        }},
                        {name: 'close', click: 'destroy'}
                    ]
                });
                this.editDialog.open();*/
                break;
        }
        column.initEdit(row);
    }


    /**
     * Close edit forms
     */
    closeEditForms() {
        for (let column of this.columns) {
            let cells = this.getContainer().find(`td[data-name=${column.name}] .ui-cell.editing`);

            $(cells).each((i, cell) => {
                cell    = $(cell);
                let tr  = $(cell.closest('tr'));
                let row = this.getRow(tr.data('id'));

                cell.removeClass('editing');
                cell.html(column.renderView(row));
                column.initView(row);
            });
        }
    }

    /**
     * Create markup
     * @returns {string}
     */
    markup() {
        let table = this.renderHeader();
        let layout = this.layout == 'default' || this.layout == 'hide-columns' ? 'table-responsive' : '';
        let fixedHeight = this.height ? 'ui-fixed-height' : '';
        let height = `height: ${this.height}px`;
        table += this.renderBody();
        table  = `            
            <div class="ui-table-container ${layout} ${fixedHeight}" style="${height}">
                <table class="table ${this.class}">
                    ${table}
                </table>                
            </div>`;

        let content = this.renderToolbar();
        content += table;
        content += this.renderFooter();

        let classes = `ui-table table-${this.layout} ui-table-${this.screen} ${this.selectRows ? 'ui-select-rows' : ''}`;
        return `<div class="${classes}">${content}</div>`;
    }

    /**
     * Render table
     */
    render() {
        super.render();
        this.trigger('body.change');
    }


    /**
     * Refresh table content
     */
    refresh() {
        let body = this.renderBody();
        this.getContainer().find('tbody').replaceWith(body);
        this.updatePagination();

        this.trigger('table.refresh');
        this.trigger('body.change');
        this.triggerRoot(`table.${this.name}.refresh`, {table: this}, true);
    }


    /**
     * Select rows
     * @param callback string|function|object
     */
    select(callback, send_events=true, status=true) {

        if (Util.isObject(callback)) {
            let data  = callback;
            callback = row => {
                for (let name of Object.keys(data)) {
                    if (row[name] != data[name]) return false;
                    else return true;
                }
            };
        }

        if (Util.isString(callback)) {
            switch (callback) {
                case 'all':
                    callback = () => {return true};
                    status = true;
                    break;

                case 'none':
                    callback = () => {return true};
                    status = false;
                    break;
            }
        }

        if (!Util.isCallable(callback)) return;

        for (let row of this.getData()) {
            if (callback(row)) {
                //trigger events if row select changes
                if (send_events && status && !this.selectedRows[row._id])
                    this.trigger('select-row', {selected: status, row});
                else if (send_events && !status && this.selectedRows[row._id])
                    this.trigger('select-row', {selected: status, row});

                this.selectedRows[row._id] = status;  //set current row select value
            }
        }

        if (this.isRendered) {
            this.refreshSelectedRows();
        }

    }

    deselect(callback, send_events=true) {
        this.select(callback, send_events, false);
    }

    /**
     * Get selected rows
     * @returns {Array}
     */
    getSelectedRows() {
        let data = this.getData();
        let result = [];

        for (let row of data) {
            if (this.selectedRows[row._id]) {
                result.push(row);
            }
        }
        return result;
    }
}