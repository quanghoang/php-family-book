/**
 * Created by hoangq on 1/19/17.
 */

import Column from '../Column';
import Html from 'lib/UI/Helper/Html';

export default class Text extends Column {


    constructor(attributes) {
        super(attributes);
    }

    renderEdit(row) {
        let content = Html.createText(this.createId(row, 'input'), 'value', this.getValue(row), 'form-control');
        return this.createEditForm(row, content);
    }

    initEdit(row) {
        super.initEdit(row);

        let input = this.getInput(row);
        input.val(input.val());
    }
}