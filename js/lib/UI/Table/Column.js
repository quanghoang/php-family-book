/**
 * Created by hoangq on 1/19/17.
 */

import Util from 'lib/UI/Helper/Util';

export default class Column {

    constructor(attributes) {
        //const
        this.empty_value = '-';

        //static
        Column.counter   = Column.counter ? Column.counter + 1 : 1;

        //public
        attributes       = attributes || {};
        this.id          = `col${Util.padString(Column.counter, 3)}`;
        this.type        = 'Column';
        this.table       = attributes.table || null;
        this.label       = attributes.label || null;
        this.name        = attributes.name  || null;
        this._name       = this.name ? '_' + this.name : null; //used for formatted value
        this.rule        = attributes.rule  || null;
        this.width       = attributes.width || 0;
        this.formatter   = Util.isCallable(attributes.formatter) ? attributes.formatter : null;
        this.editable    = Util.toBool(attributes.editable, false);
        this.sortable    = Util.toBool(attributes.sortable, false);
        this.sort        = attributes.sort || 'none'; //sort: 'none', 'asc', 'desc'
        this.editMode    = attributes.editMode || 'auto'; //'auto', 'inline', 'dialog'
        this.align       = attributes.align || null;

        if (Util.isEmpty(this.name)) {
            throw new Error(`Column name is required.`);
        }
    }

    /**
     * Create ID based on column id and row id
     * @param row object
     * @param type string ID type, eg. input
     * @returns {string}
     */
    createId(row, type) {
        return `${Util.padString(this.id, 3)}-row${Util.padString(row._id, 3)}-${type}`;
    }

    /**
     * Get column value
     * @param row
     * @returns {*}
     */
    getValue(row) {
        if (Util.isEmpty(row)) {
            return null;
        }
        return row[this.name] || row[this._name] || null;
    }

    /**
     * Get edit mode
     * @returns {string}
     */
    getEditMode() {
        if (this.editMode != 'auto') {
            return this.editMode;
        }
        return ['lg', 'md'].includes(this.table.screen) ? 'inline' : 'dialog';
    }

    /**
     * Format column value
     * @param row
     * @returns {null}
     */
    formatValue(row) {
        return Util.isCallable(this.formatter) ? this.formatter(row) : null;
    }

    /**
     * Render view
     * @param row
     * @returns {*|string}
     */
    renderView(row) {
        return row[this._name] || row[this.name] || this.empty_value;
    }

    /**
     * Render edit (TDB in inherited classes)
     * @param row
     * @returns {*|string}
     */
    renderEdit(row) {
        return null;
    }

    /**
     * Render
     * @param row object
     * @param mode string Render mode, 'view' or 'edit'
     * @returns {*|string}
     */
    render(row, mode='view') {
        switch (mode) {
            case 'view':
                return this.renderView(row);

            case 'edit':
                return this.renderEdit(row);
        }
    }

    /**
     * Create edit form (for inline edit)
     * @param row
     * @param content string
     * @returns {string}
     */
    createEditForm(row, content) {
        content = this.getEditMode() == 'inline' ? this.createInlineEditForm(row, content) : this.createDialogEditForm(row, content);
        return `<form id="${this.id + '-edit-form'}" class="ui-table-form">${content}</form>`;
    }

    /**
     * Create inline edit form
     * @param row
     * @param content string
     * @returns {string}
     */
    createInlineEditForm(row, content) {
        return `             
            <small class="ui-edit-error"></small>
            <div class="form-group">
                ${content}
            </div>
            <div class="ui-edit-buttons arrow-box">
                <a class="btn btn-primary" name="save"><i class="fa fa-check"></i></a>               
                <a class="btn btn-default" name="cancel"><i class="fa fa-times"></i></a>
            </div>`;
    }

    /**
     * Create dialog edit form
     * @param row
     * @param content string
     * @returns {string}
     */
    createDialogEditForm(row, content) {
        let label = this.label ? `<label for="${this.createId(row, 'input')}" class="ui-noselect">${this.label}</label>` : '';
        return `            
            <div class="form-group"> 
                ${label}
                ${content}
                <small class="help-block ui-edit-error"></small>
            </div>`;
    }

    /**
     * Get edit form wrapper
     * @param row object
     * @returns {jQuery}
     */
    getEditForm(row) {
        return $('#' + this.id + '-edit-form');
    }

    /**
     * Init edit form
     * @param row
     */
    initEdit(row) {
        if (this.getEditMode() == 'inline') {
            this.initInlineEdit(row);
        }
        else {
            this.initDialogEdit(row);
        }
    }

    /**
     * Init inline edit form
     * @param row
     */
    initInlineEdit(row) {
        this.getInput(row).focus();

        let form = this.getEditForm(row).submit(() => {
            this.updateEditValue(row);
            return false;
        });

        form.keyup(e => {
            if (e.keyCode == 27) {
                this.table.closeEditForms();
            }
        });

        form.find('.btn').click(e => {
            let btn = $(e.currentTarget);
            switch (btn.attr('name')) {
                case 'save':
                    this.updateEditValue(row);
                    break;

                case 'cancel':
                    this.table.closeEditForms();
                    break;
            }
        });
    }

    /**
     * Init dialog edit form
     * @param row
     */
    initDialogEdit(row) {
        this.getInput(row).focus();
    }


    /**
     * Init view (TBD in inherited classes)
     * @param row
     */
    initView(row) {

    }

    /**
     * Get edit input
     * @returns {jQuery}
     */
    getInput(row) {
        return $('#' + this.createId(row, 'input'));
    }

    /**
     * Get input value
     * @param row
     * @returns {*}
     */
    getInputValue(row) {
        return this.getInput(row).val();
    }

    /**
     * Update edit value
     * @param row
     */
    updateEditValue(row) {
        let oldValue  = row[this.name];
        let newValue  = this.getInputValue(row);
        let input     = this.getInput(row);
        let group     = input.closest('.form-group');
        let validator = this.table.validator;
        let editForm  = this.getEditForm(row);
        let error     = editForm.find('.ui-edit-error');

        //show error message
        group.removeClass('has-error');
        error.html('').hide();
        if (!validator.validate(newValue, this.rule, this.label)) {
            error.html(validator.error).css('display', 'block');
            group.addClass('has-error');
            input.focus();
            return false;
        }

        //update if changed
        if (oldValue != newValue) {
            row[this.name] = newValue;

            if (this.formatter) {
                row[this._name] = this.formatValue(row);
            }
            this.table.trigger('update', {column: this.name, row});
        }

        this.table.refreshRow(row);
        return true;
    }
}