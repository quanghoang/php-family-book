/**
 * Created by hoangq on 3/6/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import App from 'lib/UI/App/Framework7/App';
import BaseList from '../Base/List';

export default class List extends BaseList {

    constructor(attributes={}) {
        attributes.containerClass = 'f7 uc uc-list';
        super(attributes);
    }

    init() {
        super.init();

        if (Dom7('.searchbar').length) {
            this.initSearchBar();
        }
    }

    loadData(callback=null) {
        this.call(this.method, null, response => {
            if (!response.error) {
                this.items = [];
                this.addItem(response.data);
                this.refresh();
                this.initSearchBar();
                this.initItems();

                if (Util.isCallable(callback)) {
                    callback(response);
                }
            }
        });
    }

    initItems() {
        let items = this.getContainer().find('.item-content');
        if (!items.length) {
            return;
        }

        items.click(e => {
            let target = $(e.currentTarget);
            let id = target.data('id');
            let item = this.getItemById(id);
            if (item) {
                this.selectItem(item.id, true);
                this.trigger('item.click', item);
            }
        });
    }

    initSearchBar() {
        let searchIn = [];
        for (let field of this.searchIn.split(',')) {
            searchIn.push('.item-' + field.trim());
        }

        this.search = App.getF7Instance().searchbar('.searchbar', {
            searchList: '.list-block-search',
            searchIn:   searchIn.join(','),
            hideGroups: true
        });
    }

    /**
     * Highlight selected item
     * @param id string
     * @param select bool
     */
    selectItem(id, select=true) {
        let container = this.getContainer();
        let li = container.find(`a.item-link[data-id=${id}]`).parent();
        container.find('div.list-block li').removeClass('ui-selected');
        if (select) {
            li.addClass('ui-selected');
        }
    }

    markup() {
        let content = '';
        let items = this.getItems();
        for (let group of this.findGroups(items)) {
            content += this.createGroup(group, items);
        }
        return `
        <div class="list-block media-list ${this.searchbar ? 'list-block-search searchbar-found' : ''}">
            ${this.title ? `<div class="content-block-title">${this.title}</div>` : ''}
            ${content}
            ${this.label ? `<div class="list-block-label">${this.label}</div>` : ''}
        </div>`;
    }


    /**
     * Create group items
     * @param title string Group title
     * @param items array
     * @returns {string}
     */
    createGroup(title, items) {
        let content = '';
        for (let item of items) {
            if (item.group == title) {
                content += '<li>' + this.createItem(item) + '</li>';
            }
        }
        return !content ? '' : `
        ${title ? `<div class="content-block-title">${title}</div>` : ''}        
        <ul>${content}</ul>`;
    }

    /**
     * Create item
     * @param item object
     * @returns {string}
     */
    createItem(item) {
        let subtitle = '';
        if (item.subtitle) {
            for (let sub of Util.toArray(item.subtitle)) {
                subtitle += sub ? `<div class="item-subtitle">${sub}</div>` : '';
            }
        }
        let content = `
        ${item.media ? `<div class="item-media">${item.media}</div>` : ''}
        <div class="item-inner">
            <div class="item-title-row">
                <div class="item-title">${item.title}</div>
                ${item.after ? `<div class="item-after">${item.after}</div>` : ''}
            </div>
            ${subtitle}            
            ${item.text ? `<div class="item-text">${item.text}</div>` : ''}
        </div>`;

        let data = `data-id="${item.id}"`;
        return `
        ${item.link ? `<a href="${item.url || '#'}" class="item-link item-content" ${data}>` : 
                      `<div class="item-content" ${data}>`}
            ${Html.icon(content)}
        ${item.link ? '</a>' : '</div>'}`;
    }
}