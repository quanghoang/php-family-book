/**
 * Created by hoangq on 3/22/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class List extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'List';
        this.containerClass = attributes.containerClass || 'uc uc-list';
        this.title          = attributes.title || '';
        this.sortGroup      = attributes.sortGroup || ''; //Can be '', 'asc', 'desc'
        this.sortItem       = attributes.sortItem || ''; //Can be '', 'asc', 'desc'

        this.searchbar      = Util.toBool(attributes.searchbar, true);
        this.searchIn       = 'title, subtitle, text';
        this.keyword        = '';
        this.items          = [];
        this.addItem(attributes.items);
    }

    init() {
        if (this.method && !this.loaded) {
            this.loadData();
        }
        this.initItems();
    }

    loadData() {
        this.call(this.method, null, response => {
            if (!response.error) {
                this.items = [];
                this.addItem(response.data);
                this.refresh();
                this.initItems();
            }
        });
    }

    /**
     * Init items events
     */
    initItems() {
        let items = this.getContainer().find('.item');
        if (!items.length) {
            return;
        }

        items.click(e => {
            let id = $(e.currentTarget).data('id');
            let item = this.getItemById(id);
            if (item) {
                this.trigger('item.click', item);
            }
        });
    }

    /**
     * Get item by id
     * @param id string
     * @returns {*}
     */
    getItemById(id) {
        for (let item of this.items) {
            if (item.id == id) {
                return item;
            }
        }
        return null;
    }

    /**
     * Add item(s)
     *
     * Item: {title, after, subtitle, text, media, link, url, group, data}
     * - title:     Item title (required).
     * - after:     Additional text after the title (align to the right).
     * - subtitle:  Subtitle under the title.
     * - text:      Text under the subtitle.
     * - media:     Icon or Image (alight to the left).
     * - link:      Is item a link? (true/false) (add an arrow on the right of the item)
     * - url:       Link URL for link item if applied, using '#' by default.
     * - group:     Item group title.
     *
     * @param items object|array
     */
    addItem(items) {
        if (Util.isEmpty(items)) {
            return;
        }
        for (let item of Util.toArray(items)) {
            item = Util.isString(item) ? {title: item} : item;
            this.items.push({
                id:       item.id       || 'item-' + (this.items.length + 1),
                title:    item.title    || '',
                after:    item.after    || '',
                subtitle: item.subtitle || '',
                text:     item.text     || '',
                media:    item.media    || '',
                link:     item.link     || false,
                url:      item.url      || '',
                group:    item.group    || ''
            });
        }
    }

    getItems() {
        return this.items;
    }

    /**
     * Find groups
     * @param items array
     * @returns {[string]}
     */
    findGroups(items) {
        let groups = [''];
        let found  = {};

        for (let item of items) {
            let group = item.group;
            if (group && !found[group]) {
                found[group] = true;
                groups.push(group);
            }
        }

        //sort group
        switch (this.sortGroup.toUpperCase()) {
            case 'ASC':
                groups = groups.sort();
                break;

            case 'DESC':
                groups = groups.sort().reverse();
                break;
        }
        return groups;
    }
}