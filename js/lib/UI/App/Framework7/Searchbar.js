/**
 * Created by hoangq on 3/7/17.
 */

import Component from 'lib/UI/Component';

export default class Searchbar extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.notFound = attributes.notFound || 'Nothing found';
    }

    init() {
        super.init();

    }

    markup() {
        return `
        <form id="${this.container}" class="searchbar">
            <div class="searchbar-input">
                <input type="search" placeholder="Search">
                <a href="#" class="searchbar-clear"></a>
            </div>
            <a href="#" class="searchbar-cancel">Cancel</a>
        </form>                
        <div class="searchbar-overlay"></div>`;
    }

    markupNotFound() {
        return `<div class="content-block searchbar-not-found">${this.notFound}</div>`;
    }
}