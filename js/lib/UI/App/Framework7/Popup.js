/**
 * Created by hoangq on 3/7/17.
 */

import Component from 'lib/UI/Component';
import App from 'lib/UI/App/Framework7/App';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';

export default class Popup extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.title       = attributes.title || 'Popup title';
        this.content     = attributes.content || '';
        this.leftButton  = attributes.leftButton || '';
        this.rightButton = attributes.rightButton || '';
        this.floatButton = attributes.floatButton || null;
        this.f7app       = App.getF7Instance();
    }

    init() {
        this.getContainer().find('.navbar a').click(e => {
            let name = $(e.currentTarget).attr('name');
            this.trigger('button.click', name);
        });

        this.getContainer().find('.floating-button').click(e => {
            let name = $(e.currentTarget).attr('name');
            this.trigger('button.click', name);
        });
    }

    markup() {
        let floatButton = this.floatButton;
        floatButton = Util.isString(floatButton) ? {color: 'pink', label: floatButton} : floatButton;
        floatButton = !floatButton ? '' :
            `<a href="#" class="floating-button color-${floatButton.color || 'pink'}" 
                name="${floatButton.name || 'float'}">${Html.icon(floatButton.label)}</a>`;

        return `
        <div id="${this.container}" class="popup popup-mypopup">
           <div class="view">
              <div class="pages navbar-fixed">              
                <div class="page">
                
                    <div class="navbar">
                        <div class="navbar-inner">               
                             <div class="left">
                                ${this.leftButton || `
                                <a href="#" class="back link close-popup" name="close">
                                    <i class="f7-icons">close</i><span>Close</span>
                                </a>`}
                            </div>
                            <div class="center sliding">${this.title}</div>
                            <div class="right">
                                ${this.rightButton || '<a href="#" class="link" name="save">Save</a>'}                                
                            </div>
                        </div>    
                    </div>
                    
                    ${floatButton}
                                                                                                                                
                    <div class="page-content">
                        <span class="progressbar-infinite"></span>
                        <div class="content"></div>
                    </div>
                    
                 </div>
              </div>
           </div>                                           
        </div>`;
    }

    /**
     * Get page content container
     * @returns {*}
     */
    getPageContainer() {
        return this.getContainer().find('.page-content .content');
    }

    /**
     * Show/hide progressbar
     * @param status bool
     */
    showProgressbar(status=true) {
        let bar = this.getContainer().find('.progressbar-infinite').addClass('hidden');
        if (status) {
            bar.removeClass('hidden');
        }
    }

    /**
     * Open popup
     */
    open() {
        this.f7app.popup(this.markup(), true);
        this.f7app.addView(`#${this.container} .view`);
        this.getPageContainer().html(this.content);
        this.init();
    }

    /**
     * Close popup
     */
    close() {
        this.f7app.closeModal();
    }
}