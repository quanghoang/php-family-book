/**
 * Created by hoangq on 2/17/17.
 */

import Util from 'lib/UI/Helper/Util';
import Component from 'lib/UI/Component';
import Page from './Page';
import App from './App';

export default class View extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.name    = attributes.name      || '';
        this.title   = attributes.title     || '';
        this.pages   = attributes.pages     || {};
        this.options = attributes.options   || null;
        this.f7app   = App.getF7Instance();
        this.f7view  = null;
    }

    /**
     * Add page
     * @param pages
     */
    addPage(pages) {
        if (Util.isEmpty(pages)) {
            return;
        }

        for (let page of Util.toArray(pages)) {
            if (!page.name) {
                throw 'Page name is required!';
            }
            if (!Util.isObject(page)) {
                throw 'Invalid page object.';
            }

            page = page instanceof Page ? page : new Page(page);
            this.pages[page.name] = page;
        }
    }

    removePage(pageName) {
        $('.views .view .page').each((i, page) => {
            page = $(page);
            if (page.data('page') == pageName) {
                page.remove();
            }
        });
    }

    /**
     * Open page
     * @param pageName
     */
    open(pageName) {
        if (!this.pages[pageName]) {
            throw `Page ${pageName} not found.`;
        }
    }

    init() {
        let options = this.options || {
            dynamicNavbar:      true,
            allowDuplicateUrls: false
        };
        this.f7view = this.f7app.addView('.view-' + this.name, options);
    }

    markup() {
        // Material / Android view
        if (this.f7app.params.material) {
            return `
            <div class="view view-${this.name}">                       
                <div class="pages"></div>
            </div>`;
        }

        // iOS view
        return `
        <div class="view view-${this.name}">       
            <div class="navbar"></div>
            <div class="pages navbar-fixed" style="background: #efeff4"></div>
        </div>`;
    }
}