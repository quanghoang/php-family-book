/**
 * Created by hoangq on 2/17/17.
 */

import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Component from 'lib/UI/Component';
import UI from 'lib/UI/Factory/Framework7/UI';
import Searchbar from './Searchbar';
import Toolbar from './Toolbar';
import App from 'lib/UI/App/Framework7/App';

export default class Page extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.name        = attributes.name || '';
        this.title       = attributes.title || '';
        this.content     = attributes.content || '';
        this.params      = attributes.params || {};
        this.toolbar     = attributes.toolbar || null;
        this.searchbar   = attributes.searchbar || null;
        this.backButton  = Util.isString(attributes.backButton) ? attributes.backButton :
                           Util.toBool(attributes.backButton, false);
        this.ui          = UI.getInstance('mobile');
        this.f7app       = attributes.f7app || App.getF7Instance();
        this.app         = attributes.app   || null;
        this.permit      = Util.toBool(attributes.permit, true);
        this.initialized = false;
    }

    /**
     * Create instance from attribute values
     */
    createInstances() {
        if (!this.permit) {
            return;
        }

        //create searchbar instance
        if (this.searchbar && !(this.searchbar instanceof Searchbar)) {
            let attributes = Util.isObject(this.searchbar) ? this.searchbar : {};
            this.searchbar = new Searchbar(attributes);
        }

        //create toolbar instance
        if (this.toolbar && !(this.toolbar instanceof Toolbar)) {
            let attributes = Util.isObject(this.toolbar) ? this.toolbar :
                             Util.isString(this.toolbar) ? {content: this.toolbar} : {};
            this.toolbar   = new Toolbar(attributes);
        }
    }

    /**
     * Create page navigator bar
     * @returns {string}
     */
    createNavbar() {
        let backButton = `
            <div class="left">
                <a href="#" class="back link">
                    <i class="icon icon-back"></i>
                    <span>${Util.isString(this.backButton) ? this.backButton : 'Back'}</span>
                </a>
            </div>`;
        return `
        <div class="navbar">
            <div class="navbar-inner" data-page="${this.name}">                
                ${this.backButton ? backButton : ''}
                <div class="center sliding">${this.title}</div>
                <div class="right">
                    <i class="fa fa-spin fa-spinner hidden"></i>
                    <a href="#" class="link icon-only open-panel"><i class="icon icon-bars"></i></a>
                </div>
            </div>
        </div>`;
    }


    /**
     * Create page content (TBD in inherited classes)
     */
    createContent() {
        return '';
    }

    /**
     * Get page content
     */
    getContent() {
        let content = this.createContent() || this.content || '';
        return Html.icon(content);
    }

    markup() {
        this.createInstances();
        return `          
        <div class="pages">
            <div data-page="${this.name}" class="f7 page ${this.searchbar ? 'has-search' : ''}">
                ${this.createNavbar()}
                ${this.permit && this.toolbar ? this.toolbar.markup() : ''}
                ${this.permit && this.searchbar ? this.searchbar.markup() : ''}
                <div class="page-content">                    
                    ${this.permit && this.searchbar ? this.searchbar.markupNotFound() : ''}
                    <div class="list-block inset ui-error-message" style="display: none"></div>                                        
                    ${this.permit ? this.getContent() : ''}                                        
                </div>
            </div>
        </div>`;
    }

    /**
     * Show error message
     * @param message string
     */
    showError(message) {
        let content = `<ul><li><i class="fa fa-exclamation-triangle fa-li"></i> ${message}</li></ul>`;
        this.getContainer().find('.ui-error-message').html(content).fadeIn(1000);
    }

    /**
     * Hide error message
     */
    hideError() {
        this.getContainer().find('.ui-error-message').fadeOut(1000);
    }

    /**
     * Show loading icon
     */
    showLoadingIcon() {
        this.getContainer().find('.navbar .fa-spin').removeClass('hidden');
    }

    /**
     * Hide loading icon
     */
    hideLoadingIcon() {
        this.getContainer().find('.navbar .fa-spin').addClass('hidden');
    }

    /**
     * Get page container
     * @returns {void|jQuery|HTMLElement}
     */
    getContainer() {
        return $(`.pages div[data-page=${this.name}]`);
    }

    /**
     * Init page, call every time this page is opened
     * (used for init page content)
     */
    init() {
    }

    /**
     * Page ready, call only one time, when page instance is created
     * (used for init toolbars or popups)
     */
    ready() {
    }


    render(view, params) {
        let content = this.markup();
        this.params = params;
        view.router.load({content: content, query: params});

        if (!this.permit) {
            this.showError(`Permission is required to access to the ${this.title} page.`)
            return;
        }

        if (this.searchbar) {
            this.searchbar.init();
        }
        if (this.toolbar) {
            this.toolbar.init();
        }
    }
}