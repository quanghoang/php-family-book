
import View from './View';
import Page from './Page';
import Menu from 'lib/UI/Menu/Framework7/Menu';

import Request from 'lib/UI/Helper/Request';
import Util from 'lib/UI/Helper/Util';
import UI   from 'lib/UI/Factory/Framework7/UI';

export default class App {

    constructor(attributes={}) {
        this.title        = attributes.title || 'Mobile App';
        this.f7app        = App.getF7Instance(attributes.options);
        this.menu         = new Menu(attributes.menu);
        this.hasLogin     = Util.toBool(attributes.hasLogin, false);
        this.signedin     = Util.toBool(attributes.signedin, false);
        this.permit       = attributes.permit || {};
        this.loginMethod  = attributes.loginMethod  || 'Mobile/App.login';
        this.logoutMethod = attributes.logoutMethod || 'Mobile/App.logout';
        this.startPage    = attributes.startPage || null;
        this.theme        = attributes.options.material ? 'material' : 'ios';
        this.appUrl       = attributes.appUrl;
        this.$7           = Dom7;
        this.views        = {};
        this.pages        = {};
        this.pageClass    = {};
        this.currentPage  = null;
        this.ui           = UI.getInstance('mobile');
        this.firstSubmit  = true; //use for fixing bug with login password autocomplete on chrome
    }


    /**
     * Get Framework 7 app instance
     * @returns {*}
     */
    static getF7Instance(options=null) {
        options = options || {};

        if (!App.f7Instance) {
            App.f7Instance = new Framework7(options);
        }
        return App.f7Instance;
    }

    init() {
        this.initApp();
        this.initMenu();
        this.initLogin();
        this.initToggle();
    }

    initApp() {
        // Init views
        for (let view in this.views) {
            this.views[view].init();
        }

        // Init pages
        this.f7app.onPageInit('*', e => {
            let page = this.pages[e.name];
            if (page) {
                this.initPage(page.name);

                if (Util.isMobile() && this.theme == 'material') {
                    //fix issue with Android keyboard overlay on bottom text/textarea
                    document.body.style.height = window.outerHeight + 'px';
                }
            }
        });

        // Set active menu after clicking on back button
        this.f7app.onPageAfterAnimation('*', e => {
            let page = this.pages[e.name];
            if (page && page.name != this.currentPage) {
                this.setTitle(page.title);
                this.currentPage = page.name;
                this.menu.setActiveItem(page.name);
            }
        });
    }

    initMenu() {
        // Load menu data if not being loaded
        if (this.signedin && !this.menu.loaded) {
            this.menu.loadData(() => {
                this.menu.setActiveItem(this.startPage);
            });
        }

        // Handle click event on menu items
        this.menu.on('click', data => {
            this.closeMenu();

            if (data.url == 'logout') {
                this.logout();
                return;
            }
            this.openUrl(data.url);
        });

        // Handle close menu event
        this.menu.on('close-left-menu', () => {
            this.closeMenu();
        });
    }

    initLogin() {
        let form = $('#login-form');
        let loginButton = $('#login-button').click(() => {
            if (loginButton.hasClass('disabled')) {
                return;
            }
            form.submit();
        });

        form.submit(e => {
            let data = Util.toObjectData(form.serializeArray());
            if (data.password == '' && this.firstSubmit) {
                //fix chrome issue with password autocomplete
                this.firstSubmit = false;
                setTimeout(() => {
                    form.submit();
                }, 100);
                return false;
            }

            this.login(data.username, data.password);
            this.firstSubmit = false;
            return false;
        });

        $('#login-form input').keyup(e => {
            if (e.keyCode == 13) {
                form.submit();
            }
        });
    }

    /**
     * Init toggles (open page, open popup, etc.)
     */
    initToggle() {
        $('body').click(e => {
            let target = $(e.target);
            let url    = target.data('url');
            if (url) {
                this.openUrl(url);
            }
        });
    }

    /**
     * Login to the system
     * @param username string
     * @param password string
     */
    login(username, password) {
        if (!this.loginMethod) {
            console.error('Login method is required.');
            return;
        }

        let button  = $('#login-button').addClass('disabled');
        let loading = $('#login-loading').show();
        let message = $('#login-message').hide();

        Request.call(this.loginMethod, {username, password}, response => {
            loading.hide();
            button.removeClass('disabled');

            if (response.data.signedin) {
                this.signedin = true;
                this.permit   = response.data.permit;

                this.menu.loadData(() => {
                    this.closeLogin();
                    this.openPage(this.startPage);
                });
                return;
            }
            message.show();
        });
    }

    /**
     * Logout of the system
     */
    logout() {
        let yes = () => {
            Request.call(this.logoutMethod, null, response => {
                location.reload();
            });
        };
        let no = () => {
            this.menu.setActiveItem(this.currentPage);
        }
        this.f7app.confirm('Do you want to logout?', 'Logout', yes, no);
    }

    /**
     * Close/hide left menu
     */
    closeMenu() {
        this.f7app.closePanel('left');
    }

    /**
     * Open/show left menu
     */
    openMenu() {
        this.f7app.openPanel('left');
    }

    /**
     * Open page by URL
     * @param url string
     * @param viewName string
     */
    openUrl(url, viewName='main') {
        let index  = url.indexOf('?');
        let name   = url;
        let params = null;

        if (index > 0) {
            name   = url.substr(0, index);
            params = this.$7.parseUrlQuery(url);
        }
        this.openPage(name, params, viewName);
    }

    /**
     * Open page
     * @param pageName string
     * @param viewName string
     * @param params null|object
     */
    openPage(pageName, params=null, viewName='main') {
        if (!pageName) {
            return;
        }

        let view = this.view(viewName);
        if (!view) {
            console.error('View not found.');
            return;
        }

        let page = this.pages[pageName];

        //create instance
        if (!page && this.pageClass[pageName]) {
            page                 = new this.pageClass[pageName]();
            page.app             = this;
            page.permit          = Util.toBool(this.permit[pageName], false);
            page.f7app           = this.f7app;
            this.pages[pageName] = page;
        }

        if (!page) {
            throw new Error(`Page '${pageName}' not found.`);
        }
        page.render(view.f7view, params);
        this.setTitle(page.title);
        this.menu.setActiveItem(page.name);

        this.currentPage = pageName;
    }

    /**
     * Close page
     * @param pageName string
     * @param viewName string
     */
    closePage(pageName, viewName='main') {
        let view = this.views[viewName];
        let page = this.pages[pageName];
        if (!view || !page) {
            return;
        }
        view.removePage(pageName);
        this.pages[pageName] = null;
    }

    /**
     * Set app title
     * @param pageTitle
     */
    setTitle(pageTitle) {
        //ignore dashboard page title for adding to home screen feature
        if (pageTitle == 'Dashboard') {
            document.title = this.title;
            return;
        }
        document.title = pageTitle + ' - ' + this.title;
    }

    openModal(name, params=null) {
        this.f7app.openModal(name);
    }

    closeModal(name=null) {
        this.f7app.closeModal(name);
    }

    openPopup(popupName, params=null) {
        //this.f7app.
    }

    closePopup(popupName=null) {

    }

    /**
     * Show login screen
     */
    showLogin() {
        this.f7app.loginScreen();
    }

    /**
     * Close/hide login screen
     */
    closeLogin() {
        this.closeModal();
    }

    /**
     * Add view(s)
     * @param views object|View
     */
    addView(views) {
        if (Util.isEmpty(views)) {
            return;
        }

        for (let view of Util.toArray(views)) {
            if (!view.name) {
                console.error('View name is required!');
                continue;
            }
            if (!Util.isObject(view)) {
                console.error('Invalid view object.');
                continue;
            }

            view = view instanceof View ? view : new View(view);
            this.views[view.name] = view;
        }
    }

    /**
     * Add page(s) classes
     * @param pageClass object Pairs of name and class (not instance)
     */
    addPage(pageClass) {
        if (Util.isEmpty(pageClass)) {
            return;
        }

        for (let name of Object.keys(pageClass)) {
            this.pageClass[name] = pageClass[name];
        }
    }

    /**
     * Get view
     * @param viewName string
     * @returns {*|null}
     */
    view(viewName='main') {
        return this.views[viewName] || null;
    }

    /**
     * Create login screen
     * @returns {string}
     */
    createLoginScreen() {
        return `
        <div class="login-screen modal-in">          
          <div class="view">
            <div class="page">              
              <div class="page-content login-screen-content">              
                <div class="login-screen-title">${this.title}</div>                
                    <div id="login-message">
                        <div class="message">
                            <i class="fa fa-exclamation-circle"></i> Invalid username or password.                            
                        </div>                            
                    </div>
                    <form id="login-form">                         
                      <div class="list-block">
                        <ul>
                          <li class="item-content">
                            <div class="item-inner">
                              <div class="item-title label">Username</div>
                              <div class="item-input">
                                <input type="text" name="username" placeholder="Username"  />
                              </div>
                            </div>
                          </li>
                          <li class="item-content">
                            <div class="item-inner">
                              <div class="item-title label">Password</div>
                              <div class="item-input">
                                <input type="password" name="password" placeholder="Password" />
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div class="list-block">
                        <ul>
                          <li style="text-align: center">
                            <a id="login-button" href="#" class="item-link list-button">Sign In</a>
                          </li>
                        </ul>                    
                        <div id="login-loading"><i class="fa fa-spinner fa-2x fa-spin"></i></div>                                                                        
                      </div>
                    </form>
              </div>
            </div>
          </div>
        </div>`;
    }

    /**
     * Create app markup
     * @returns {string}
     */
    markup() {
        let views = '';
        for (let view in this.views) {
            views += this.views[view].markup();
        }

        return `                
        <div class="statusbar-overlay"></div>
        <div class="panel-overlay"></div>
        
        <div id="main-menu" class="panel panel-left panel-cover layout-dark">                       
        </div>        
        
        <div class="views">
            ${views}           
        </div>
        ${this.hasLogin ? this.createLoginScreen() : ''}`;
    }

    initPage(pageName) {
        let page = this.pages[pageName];
        if (!page || !this.permit[pageName]) {
            return;
        }

        page.init();
        if (!page.initialized) {
            page.ready();
        }
        page.initialized = true;
    }

    /**
     * App ready event
     */
    ready() {
        if (this.hasLogin) {
            if (this.signedin) {
                this.closeLogin();
                this.openPage(this.startPage);
            }
        }
        else {
            this.openPage(this.startPage);
        }
    }

    render() {
        $('body').html(this.markup()).addClass(this.theme);
        this.menu.render();
        this.init();
        this.ready();
    }
}