/**
 * Created by hoangq on 3/7/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';

export default class Toolbar extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.floatRight = Util.toBool(attributes.floatRight, false);
        this.content    = attributes.content || '';
    }

    init() {
        this.getContainer().find('a').click(e => {
            let name = $(e.currentTarget).attr('name');
            this.trigger('button.click', name);
        });
    }

    markup() {
        return `
        <div id="${this.container}" class="toolbar toolbar-bottom ${this.floatRight ? 'toolbar-right' : ''}">        
            <div class="toolbar-inner">                                                 
                ${this.content}
            </div>
        </div>`;
    }
}