/**
 * Created by hoangq on 3/16/17.
 */

import Util from 'lib/UI/Helper/Util';
import Component from 'lib/UI/Component';

export default class Factory {

    static getInstance(types) {
        if (!Factory.instance) {
            Factory.instance = new Factory(types);
        }
        return Factory.instance;
    }

    constructor(types) {
        this.types = types;
    }

    /**
     * Create component instance(s)
     * @param components array
     * @param parent null|Component
     * @returns {*}
     */
    create(components, parent=null) {
        if (Util.isEmpty(components)) {
            return null;
        }
        parent = parent || Component.getRoot();

        for (let attributes of Util.toArray(components)) {
            if (!attributes) {
                continue;
            }
            //child components
            let children = attributes.ui || attributes.components;
            if (children) {
                delete attributes.ui;
                delete attributes.components;
            }

            //convert string to Content component
            if (Util.isString(attributes)) {
                attributes = {
                    type:    'Content',
                    content: attributes
                };
            }

            //determine type
            let type = this.types[attributes.type.toLowerCase()] || null;
            if (!type) {
                console.error(`Invalid type '${attributes.type}'.`);
                return null;
            }
            //create parent component object
            let component = new type(attributes);
            if (!component.parent) {
                component.parent = parent;
                component.parent.add(component);
            }

            //create child component objects
            if (children) {
                for (let child of children) {
                    if (child) {
                        child.parent = component;
                        component.add(this.create(child, component));
                    }
                }
            }
            return component;
        }
    }

    /**
     * Component factory
     * @param components array|string
     * @param data null|object
     * @param callback null|function
     * @returns {*}
     */
    factory(components, data=null, callback=null) {
        if (Util.isString(components)) {
            this.call(components, data, response => {
                return this.factory(response.data, data, callback);
            });
            return;
        }

        let component = this.create(components);
        if (Util.isCallable(callback)) {
            callback(component);
        }
        return component;
    }

    /**
     * Create & Render component(s) from array
     * @param components array
     * @param data object|string Post data for render from server side or framework for render in client side.
     * @param callback function Used when loading UI components from server side
     * @returns {*}
     */
    render(components, data, callback=null) {
        if (Util.isString(components)) {
            this.call(components, data, response => {
                return this.render(response.data, data, callback);
            });
            return;
        }

        let component = this.factory(components);
        if (component) {
            component.render();
        }

        if (Util.isCallable(callback)) {
            callback(component);
        }
        return component;
    }

    open(components, data, callback=null) {
        this.render(components, data, object => {
            if (!Util.isCallable(object.open)) {
                throw 'Open() method not found.';
            }

            object.open();
            if (Util.isCallable(callback)) {
                callback(object);
            }
        });
    }

    /**
     * Get child component(s)
     * @param selector string
     * @returns {*}
     */
    get(selector) {
        return Component.getRoot().get(selector);
    }

    /**
     * Call server side method
     * @param method string method name (syntax: [folder/][class.]<method>)
     * @param data object|null Param data
     * @param callback function
     * @param dataType string Response datatype (json, xml, html)
     */
    call(method, data, callback, dataType='json') {
        Component.getRoot().call(method, data, response => {
            if (Util.isCallable(callback)) {
                return callback(response);
            }
        }, dataType);
    }
}