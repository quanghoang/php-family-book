/**
 * Created by hoangq on 3/16/17.
 */

import Content from 'lib/UI/Content/Content';
import Alert from 'lib/UI/Content/Alert';
import Flow from 'lib/UI/Container/Bootstrap/Flow';
import Panel from 'lib/UI/Container/Bootstrap/Panel';
import Fieldset from 'lib/UI/Container/Bootstrap/Fieldset';
import Tab from 'lib/UI/Container/Bootstrap/Tab';
import Dialog from 'lib/UI/Container/Bootstrap/Dialog';
import EditDialog from 'lib/UI/Container/Bootstrap/EditDialog';
import Form from 'lib/UI/Container/Bootstrap/Form';
import UploadForm from 'lib/UI/Container/Bootstrap/UploadForm';
import Text from 'lib/UI/Input/Bootstrap/Text';
import Password from 'lib/UI/Input/Bootstrap/Password';
import Currency from 'lib/UI/Input/Bootstrap/Currency';
import Date from 'lib/UI/Input/Bootstrap/Date';
import Textarea from 'lib/UI/Input/Bootstrap/Textarea';
import Checkbox from 'lib/UI/Input/Bootstrap/Checkbox';
import Radio from 'lib/UI/Input/Bootstrap/Radio';
import Switch from 'lib/UI/Input/Bootstrap/Switch';
import Button from 'lib/UI/Input/Bootstrap/Button';
import Submit from 'lib/UI/Input/Bootstrap/Submit';
import Select from 'lib/UI/Input/Bootstrap/Select';
import Autocomplete from 'lib/UI/Input/Bootstrap/Autocomplete';
import Hidden from 'lib/UI/Input/Bootstrap/Hidden';
import File from 'lib/UI/Input/Bootstrap/File';
import Filter from 'lib/UI/Filter/Bootstrap/Filter';
import Table from 'lib/UI/Table/Table';
import Grid from 'lib/UI/Grid/Bootstrap/Grid';
import EditGrid from 'lib/UI/Grid/Bootstrap/EditGrid';
import Tree from 'lib/UI/Tree/Bootstrap/Tree';

// import PivotGrid from 'lib/UI/Grid/Bootstrap/PivotGrid';

const types = {
    panel:        Panel,
    fieldset:     Fieldset,
    tab:          Tab,
    form:         Form,
    uploadform:   UploadForm,
    flow:         Flow,
    dialog:       Dialog,
    editdialog:   EditDialog,
    content:      Content,
    alert:        Alert,
    text:         Text,
    password:     Password,
    currency:     Currency,
    date:         Date,
    textarea:     Textarea,
    button:       Button,
    submit:       Submit,
    select:       Select,
    autocomplete: Autocomplete,
    checkbox:     Checkbox,
    radio:        Radio,
    switch:       Switch,
    hidden:       Hidden,
    file:         File,
    filter:       Filter,
    table:        Table,
    grid:         Grid,
    editgrid:     EditGrid,
    tree:         Tree

    //list:         List,
    // pivotgrid:    PivotGrid
};

import Factory from '../Base/Factory';
let instance = new Factory.getInstance(types);

function factory(components, data=null, callback=null) {
    return instance.factory(components, data, callback);
}

function render(components, data=null, callback=null) {
    return instance.render(components, data, callback);
}

function open(components, data=null, callback=null) {
    instance.open(components, data, callback);
}

function get(selector) {
    return instance.get(selector);
}

function call(method, data, callback, dataType='json') {
    return instance.call(method, data, callback, dataType);
}

export {factory, render, get, call};