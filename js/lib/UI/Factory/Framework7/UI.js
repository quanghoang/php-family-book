/**
 * Created by hoangq on 3/1/17.
 */

import {factory, render, get, call} from './Factory';
import Config from 'lib/UI/Config';
import Util from 'lib/UI/Helper/Util';

export default class UI {

    /**
     * Create UI instance
     * @param name string Instance name
     * @param attributes object
     * @returns {*}
     */
    static getInstance(name='default', attributes={}) {
        if (!UI.instance) {
            UI.instance = {};
        }

        if (!UI.instance[name]) {
            UI.instance[name] = new UI(attributes);
        }
        return UI.instance[name];
    }

    constructor(attributes={}) {
        //this.defaultFramework = attributes.framework || 'bootstrap';
    }

    // /**
    //  * Set default framework
    //  * @param framework string Can be 'bootstrap' or 'framework7'
    //  * @returns {UI}
    //  */
    // framework(framework) {
    //     this.defaultFramework = framework;
    //     return this;
    // }

    /**
     * Get config value
     * @param name Config name
     * @returns {*}
     */
    config(name) {
        const flatten = Util.flattenObject(Config);
        if (!name) {
            return Config;
        }
        if (Config[name] !== undefined) {
            return Config[name];
        }
        if (flatten[name] !== undefined) {
            return flatten[name];
        }
        return null;
    }

    /**
     * Create component(s) from array
     * @param components array
     * @returns {*}
     */
    factory(components) {
        //return factory(components, framework || this.defaultFramework);
        return factory(components);
    }

    /**
     * Create & Render component(s) from array
     * @param components array
     * @param data object|string Post data for render from server side or framework for render in client side.
     * @param callback function Used when loading UI components from server side
     * @returns {*}
     */
    render(components, data, callback=null) {
        return render(components, data, callback);
    }

    /**
     * Get child component(s)
     * @param selector string
     * @returns {*}
     */
    get(selector) {
        return get(selector);
    }

    /**
     * Call server side method
     * @param method string method name (syntax: [folder/][class.]<method>)
     * @param data object|null Param data
     * @param callback function
     * @param label string Used for debug or displaying progressions.
     */
    call(method, data, callback, label='') {
        return call(method, data, callback, label);
    }
}