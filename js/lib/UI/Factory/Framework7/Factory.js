/**
 * Created by hoangq on 3/16/17.
 */

import Util from 'lib/UI/Helper/Util';
import Component from 'lib/UI/Component';
import Content from 'lib/UI/Content/Content';
import Flow from 'lib/UI/Container/Framework7/Flow';
import Panel from 'lib/UI/Container/Framework7/Panel';
import Chart from 'lib/UI/Chart/Framework7/HighChart';

import Form from 'lib/UI/Container/Framework7/Form';
import Text from 'lib/UI/Input/Framework7/Text';
import Textarea from 'lib/UI/Input/Framework7/Textarea';
import Date from 'lib/UI/Input/Framework7/Date';
import Button from 'lib/UI/Input/Framework7/Button';
import Select from 'lib/UI/Input/Framework7/Select';
import Autocomplete from 'lib/UI/Input/Framework7/Autocomplete';
import Checkbox from 'lib/UI/Input/Framework7/Checkbox';
import Switch from 'lib/UI/Input/Framework7/Switch';
import Hidden from 'lib/UI/Input/Framework7/Hidden';
import List from 'lib/UI/List/Framework7/List';


const types = {
    panel:        Panel,
    chart:        Chart,
    form:         Form,
    flow:         Flow,
    content:      Content,
    text:         Text,
    textarea:     Textarea,
    date:         Date,
    button:       Button,
    select:       Select,
    autocomplete: Autocomplete,
    checkbox:     Checkbox,
    switch:       Switch,
    hidden:       Hidden,
    list:         List,
};

import Factory from '../Base/Factory';
let instance = new Factory.getInstance(types);

function factory(components, data=null, callback=null) {
    return instance.factory(components, data, callback);
}

function render(components, data=null, callback=null) {
    return instance.render(components, data, callback);
}

function get(selector) {
    return instance.get(selector);
}

function call(method, data, callback, label='') {
    instance.call(method, data, callback, label);
}

export {factory, render, get, call};