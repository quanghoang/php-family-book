/**
 * Created by hoangq on 1/6/17.
 */

import Util from './Util';
import DateTime from './DateTime';

export default class Validator {


    /**
     * Get instance
     * @returns {Validator}
     */
    static getInstance() {
        if (!Validator.instance) {
            Validator.instance = new Validator();
        }
        return Validator.instance;
    }

    /**
     * Validate value
     * @param value string
     * @param rules array|string
     * @param label
     * @param data
     * @returns {{valid: boolean, error: (string|*)}}
     */
    static validate(value, rules, label, data={}) {
        let instance = Validator.getInstance();
        let valid    = instance.validate(value, rules, label, data);
        return {
            valid: valid,
            error: instance.error
        };
    }

    static parseRule(rules) {
        return Validator.getInstance().parseRule(rules);
    }


    constructor() {
        //const
        this.empty_value     = '[ui-default-value]'; //must be the same as PAC_UI_API::UI_DEFAULT_VALUE
        this.rule_delimiter  = ',';
        this.param_delimiter = ':';

        //error message
        this.error = '';

        //validate messages
        this.messages = {
            required:        'The {label} field is required.',
            required_depend: 'The {label} or {label2} field is required.',
            number:          'The {label} field must be a number.',
            currency:        'The {label} field must be a number.',
            max_length:      'The {label} field must have at most {param} characters.',
            min_length:      'The {label} field must have at least {param} characters.',
            email:           'The {label} field must be a valid email address.',
            phone:           'The {label} field must be a valid phone number.',
            date:            'The {label} field must be a valid date (yyyy-mm-dd).',
            json:            'The {label} field must be a valid JSON string.',
            javascript:      'The {label} field must be a valid JavaScript code.',
            url:             'The {label} field must be a valid URL.',
        };

        //validate functions, return true if valid.
        this.functions = {
            required: (value) => {
                return value && String(value).trim().length > 0;
            },
            required_depend: (value, otherValue) => {
                return !Util.isBlank(value) || !Util.isBlank(otherValue);
            },
            number: (value) => {
                return value == '' || Util.isNumeric(value);
            },
            currency: (value) => {
                return Util.isNumeric(value) && (value * 1 >= -1); //-1 is default value of a currency value
            },
            max_length: (value, length) => {
                return value.length <= length;
            },
            min_length: (value, length) => {
                return value.length >= length;
            },
            email: (value) => {
                return !value || Util.isEmail(value);
            },
            phone: (value) => {
                return !value || Util.isPhone(value);
            },
            date: value => {
                return !value || DateTime.isDate(value, 'yy-mm-dd');
            },
            json: value => {
                return !value || Util.isJSON(value);
            },
            javascript: value => {
                return !value || Util.isJavaScript(value);
            },
            url: value => {
                return !value || Util.isURL(value);
            }
        };
    }

    /**
     * Validate value
     * @param value string
     * @param rules array|string Validation rules. Eg. ['required', 'max_length:50', 'email'] or 'required,max_length:50,email'
     * @param label string
     * @returns {boolean}
     */
    validate(value, rules, label='', data={}) {
        if (value == this.empty_value) {
            return true;
        }

        let valid = true;
        this.error = '';

        for (let rule of this.parseRule(rules)) {
            if (!Util.isCallable(rule.func)) {
                continue;
            }

            let param  = rule.param;
            let label2 = '';
            if (data[rule.param]) {
                param  = data[rule.param].value;
                label2 = data[rule.param].label;
            }

            if (!rule.func(value, param)) {
                valid = false;
                let message = this.messages[rule.name];
                this.error = message.replace('{label}', label).replace('{label2}', label2).
                                     replace('{param}', param).replace('  ', ' ');
                break;
            }
        }
        return valid;
    }

    /**
     * Parse rule(s) from string
     * @param rules sting|array
     * @returns {Array}
     */
    parseRule(rules) {
        rules = Util.isString(rules) ? rules.split(this.rule_delimiter) : rules;

        let result = [];

        for (let item of rules) {
            let parts = item.split(this.param_delimiter);
            let rule  = parts[0];
            let param = parts[1] || null;
            let func  = this.functions[rule] || null;

            if (!func) {
                let rules = Object.keys(this.functions).map(name => ` '${name}'`);
                throw new Error(`Invalid rule (${rule}). Use${rules} instead.`);
            }
            result.push({name: rule, param, func});
        }
        return result;
    }
}