

export default class DateTime {

    /**
     * Check a string value is an invalid date or not
     * @param value string
     * @param format string
     * @returns {boolean}
     */
    static isDate(value, format='yy-mm-dd') {
        try {
            let parse = $.datepicker.parseDate(format, value);
            if (parse && Date.parse(value)) {
                return true;
            }
        }
        catch (e) {
            return false;
        }
        return false;
    }

    /**
     * Convert date to ISO Date format
     * @param date Date
     * @returns {string}
     */
    static toIsoDate(date) {
        return date instanceof Date ? date.toISOString().substr(0, 10): String(date);
    }

    static today() {
        return DateTime.toIsoDate(new Date());
    }

    /**
     * Get last month date range
     * @returns {{start: string, end: string}}
     */
    static getLastMonthRange() {
        let date  = new Date(), y = date.getFullYear(), m = date.getMonth();
        let start = new Date(y, m - 1, 1);
        let end   = new Date(y, m, 0);
        return {start: DateTime.toIsoDate(start), end: DateTime.toIsoDate(end)};
    }

    /**
     * Get last week date range
     * @returns {{start: string, DateTime: string}}
     */
    static getLastWeekRange() {
        let date = new Date();
        date.setDate(date.getDate() - (date.getDay() + 6) % 7); //Monday of this week
        date.setDate(date.getDate() - 7); //previous Monday
        let start = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1); //previous Sunday
        let end = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 5); //previous Saturday
        return {start: DateTime.toIsoDate(start), end: DateTime.toIsoDate(end)};
    }
}