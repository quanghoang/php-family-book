/**
 * Created by hoangq on 2/9/17.
 */

export default class State {

    constructor(attributes={}) {
        super();
        this.setState(attributes.state || {});
    }


    /**
     * Set state
     * @param state
     */
    setState(state) {
        this.state = state;
    }

    /**
     * Get state
     * @returns {*}
     */
    getState() {
        return this.state;
    }

    onAction(action, callback) {

    }

    dispatch(action, state) {

    }
}