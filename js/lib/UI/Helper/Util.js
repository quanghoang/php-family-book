/**
 * Created by hoangq on 1/6/17.
 */

import Papa from 'PapaParse-4-2';
import Inputmask from 'inputmask-3-3-10';

export default class Util {

    /**
     * Check a value is string or not
     * @param value
     * @returns {boolean}
     */
    static isString(value) {
        return typeof value === 'string' || value instanceof String;
    }

    /**
     * Check a value is array or not
     * @param value
     * @returns {boolean}
     */
    static isArray(value) {
        return Array.isArray(value);
    }

    // /**
    //  * Is UI component or not
    //  * @param object object
    //  * @param type null|string
    //  * @returns {boolean}
    //  */
    // static isComponent(object, type=null) {
    //     return  object instanceof Component && (!type || (object.type && type == object.type));
    // }

    /**
     * Check if object is a jQuery object or not
     * @param object
     * @returns {boolean}
     */
    static isjQuery(object) {
        return object instanceof jQuery;
    }

    /**
     * Check a value is callable or not
     * @param value
     * @returns {boolean}
     */
    static isCallable(value) {
        return typeof value === 'function';
    }

    /**
     * Check if a value is an object or not
     * @param value
     * @returns {boolean}
     */
    static isObject(value) {
        return value && !Util.isArray(value) && typeof value === 'object';
    }

    /**
     * Check if a string is a javascript code or not
     * @param code string
     * @returns {boolean}
     */
    static isJavaScript(code) {
        code = Util.trim(code);
        if  (code[0] == '{') {
            code = 'var ui_dump_obj = ' + code;
        }
        try {
            eval(code);
            return true;
        }
        catch (e) {
            return false;
        }
    }

    /**
     * Check a value is empty or not
     * @param value string|array|object
     * @returns {boolean}
     */
    static isEmpty(value) {
        if (!value || value == false) { //Note: [] == false, '' == false
            return true;
        }
        if (Util.isObject(value) && $.isEmptyObject(value)) {
            return true;
        }
        return false;
    }

    /**
     * Check a string value is blank or not
     * @param value string
     * @returns {boolean}
     */
    static isBlank(value) {
        return !value || !value.length || (Util.isString(value) && !value.trim());
    }

    /**
     * Check a string value is numeric or not
     * @param value string
     * @returns {boolean}
     */
    static isNumeric(value) {
        return !isNaN(parseFloat(value)) && isFinite(value);
    }

    /**
     * Check a string is a valid JSON string or not
     * @param value string
     * @returns {boolean}
     */
    static isJSON(value) {
        try {
            $.parseJSON(value);
            return true;
        }
        catch(err) {
            return false;
        }
    }


    /**
     * Check if mobile device or not
     * @returns {boolean}
     */
    static isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }

    /**
     * Convert string to slug
     * @param str string
     * @returns {string}
     */
    static toSlug(str) {
        let slug = '';
        let trimmed = $.trim(str);
        slug = trimmed.replace(/[^a-z0-9-]/gi, '-').replace(/-+/g, '-').replace(/^-|-$/g, '');
        return slug.toLowerCase();
    }

    static isEmail(str) {
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.exec(str) !== null;
    }

    static isPhone(str) {
        const regex = /^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$/;
        return regex.exec(str) !== null;
    }

    static isURL(str) {
        const regex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
        return regex.exec(str) !== null;
    }

    /**
     * Check if HtmlElement exist or not
     * @param selector string
     * @returns {boolean}
     */
    static exists(selector) {
        return $(selector).length > 0;
    }

    /**
     * Escape regular expression string
     * @param str string
     */
    static escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    /**
     * Trim value
     * @param value string|array
     * @returns {*}
     */
    static trim(value) {
        if (Util.isArray(value)) {
            return value.map((item) => {
                return item ? item.trim() : item;
            });
        }

        if (Util.isString(value)) {
            return value ? value.trim() : value;
        }
        return value;
    }

    /**
     * Strip all html tags in value string
     * @param value string
     * @returns string
     */
    static stripTags(value) {
        //return String($(value).text());
        return String(value).replace(/<[^>]+>/ig, '');
    }

    /**
     * Strip all space characters on value string
     * @param value string
     * @returns {string}
     */
    static stripSpaces(value) {
        return String(value).replace(/(\r\n|\n|\r)/gm, '');
    }

    /**
     * Convert serializeArray to objectData
     * @param serializeArray
     * @returns {object}
     */
    static toObjectData(serializeArray) {
        let data = {};
        serializeArray.forEach((item) => {
            let name  = item.name,
                value = item.value;

            if (!data[name]) {
                data[name] = value;
            }
            else {
                if (!Util.isArray(data[name])) {
                    data[name] = [data[name]];
                }
                data[name].push(value);
            }
        });
        return data;
    }

    /**
     * Convert value to array
     * @param value
     * @returns {*}
     */
    static toArray(value) {
        if (!value) {
            return [];
        }
        return Util.isArray(value) ? value : [value];
    }

    /**
     * Convert value to boolean value
     * @param value
     * @param defaultValue
     * @returns {boolean}
     */
    static toBool(value, defaultValue=false) {
        if (value === undefined) {
            return defaultValue;
        }
        return value ? true : false;
    }

    /**
     * Convert object array to csv content
     * @param data array List of objects
     * @param filename string Download filename
     * @returns {*}
     */
    static toCSV(data, filename=null) {
        let content = Papa.unparse(data);
        if (!filename) {
            return content;
        }
        let encodedUri = encodeURI("data:text/csv;charset=utf-8," + content);
        let link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", filename);
        document.body.appendChild(link);
        link.click();
        return content;
    }

    /**
     * Format value to mask format
     * @param string value
     * @param string|object mask
     * @returns {*}
     */
    static toMask(value, mask) {
        mask = Util.isString(mask) ? {alias: mask} : mask;
        return Inputmask.format(value, mask);
    }

    /**
     * Check if the mask is valid for the value
     * @param string value
     * @param string|object mask
     */
    static isValidMask(value, mask) {
        mask = Util.isString(mask) ? {alias: mask} : mask;
        return Inputmask.isValid(value,  mask);
    }

    /**
     * Add padding character at beginning of the value
     * (used for adding leading zeros to number, eg. 01, 001, etc.)
     * @param value number|string
     * @param width int
     * @param pad string Padding character
     * @returns {*}
     */
    static padString(value, width,  pad='0') {
        value = value + '';
        return value.length >= width ? value : new Array(width - value.length + 1).join(pad) + value;

    }

    /**
     * Convert camel case to string (eg. MenuItem --> Menu Item)
     * @param str string
     * @param separator string
     */
    static camelToCase(str, separator=' ') {
        return str.replace(/([a-z])([A-Z])/g, `$1${separator}$2`);
    }

    /**
     * Upper case first character
     * @param str
     * @returns {string}
     */
    static ucFirst(str) {
        return str[0].toUpperCase() + str.slice(1);
    }

    /**
     * Get query string data
     * @returns {{}}
     */
    static getQueryStringData() {
        let query = window.location.search.substring(1);
        let params = query.split("&");
        let result = {};

        params.forEach((param) => {
            param       = decodeURIComponent(param).split('=');
            let name    = param[0];
            let value   = param[1];
            let isArray = name.indexOf('[]') == name.length - 2;
            name        = isArray ? name.replace('[]', '') : name;

            if (!name || !value) {
                return; //continue
            }

            if (isArray) {
                if (!result[name]) {
                    result[name] = [];
                }
                result[name].push(value);
            }
            else {
                result[name] = value;
            }
        });
        return result;
    }


    /**
     * Get current screen size
     * @returns {{width: int, height: int}}
     */
    static getScreenSize() {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight|| e.clientHeight|| g.clientHeight;
        return {width: x, height: y};
    }

    /**
     * Get bootstrap screen
     * @returns {string}
     */
    static getBootstrapScreen() {
        let width = Util.getScreenSize().width;

        if (width < 768) {
            return 'xs'; //extra small screen (phone)
        }

        if (width >= 768 &&  width <= 992) {
            return 'sm'; //small screen (tablet)
        }

        if (width > 992 &&  width <= 1200) {
            return 'md'; //large screen (laptop)
        }
        return 'lg'; //extra large screen (desktop)
    }

    /**
     * Redirect
     * @param url string
     */
    static redirect(url) {
        window.location.replace(url);
    }

    /**
     * Open tab
     * @param url string
     */
    static openTab(url) {
        window.open(url, '_blank');
    }

    /**
     * Get absolute URL of the current page
     * @returns string
     */
    static getAbsUrl() {
        return window.location.origin + window.location.pathname;
    }


    /**
     * Flatten an object
     * @param object
     * @returns {{}}
     */
    static flattenObject(object) {
        let result = {};
        for (let field in object) {
            if (!object.hasOwnProperty(field)) {
                continue;
            }

            if ((typeof object[field]) == 'object') {
                let flatObject = Util.flattenObject(object[field]);
                for (let x in flatObject) {
                    if (!flatObject.hasOwnProperty(x)) {
                        continue;
                    }
                    result[field + '.' + x] = flatObject[x];
                }
                continue;
            }
            result[field] = object[field];
        }
        return result;
    }
}