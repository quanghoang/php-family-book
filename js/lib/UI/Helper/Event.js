/**
 * Created by hoangq on 2/9/17.
 */

import Util from 'lib/UI/Helper/Util';

export default class Event {

    constructor() {
        this.events = {};
    }

    /**
     * On event
     * @param event string|array Event name or list of event names
     * @param callback function
     * @param multiple bool Multiple callbacks on an event
     */
    on(event, callback, multiple=true) {
        if (Util.isArray(event)) {
            for (let eventItem of event) {
                this.on(eventItem, callback, multiple);
            }
        }

        if (!Util.isCallable(callback)) {
            return;
        }

        if (multiple) {
            if (!this.events[event]) {
                this.events[event] = [];
            }
            this.events[event].push(callback);
            return;
        }

        this.events[event] = [callback];
    }

    /**
     * On event with one/single callback
     * @param event string
     * @param callback function
     */
    one(event, callback) {
       this.on(event, callback, false);
    }

    /**
     * Trigger event
     * @param event string
     * @param data object
     */
    trigger(event, data) {
        let callbacks = this.events[event];
        data = data || {};

        if (callbacks) {
            callbacks.forEach((callback) => {
                return Util.isCallable(callback) ? callback(data) : null;
            });
        }
        return null;
    }

    clearEvents() {
        this.events = {};
    }
}