/**
 * Created by hoangq on 2/8/17.
 */

import Event from './Event';
import Util from './Util';
import Config from 'lib/UI/Config';

export default class Request extends Event {

    /**
     * Constructor
     */
    constructor(attributes={}) {
        super();

        //private
        this.currentQueue   = 'default';
        this.totalRequests  = 0;
        this.autoSend      = true;
        this.queues         = {};
        this.requests       = {};
        this.ajaxs          = {};
    }

    /**
     * Get Request instance
     * @returns {*|Request}
     */
    static getInstance() {
        Request.instance = Request.instance || new Request();
        return Request.instance;
    }

    /**
     * Add a request to queue
     * @param queue string Queue name
     * @param url string
     * @param data object|null
     * @param success callback
     * @param fail callback|null
     * @param req_method string Request method (GET, POST, PUT, DELETE)
     * @param req_datatype string Data Type (json, html, xml)
     * @param label string Label for generating progression message, eg. Loading photos.
     * @returns {Request}
     */
    static request(queue, url, data, success, fail=null, req_method='GET', req_datatype='json', label='') {
        let instance = Request.getInstance();
        queue = queue || instance.currentQueue;
        instance.add(queue, url, data, success, fail, req_method, req_datatype, label);
        return Request;
    }

    /**
     * Define a request
     * @param name Request name
     * @param url
     * @param data
     * @param success
     * @param fail
     * @param req_method
     * @param req_datatype
     * @returns {Request}
     */
    static define(name, url, data, success, fail=null, req_method='GET', req_datatype='json') {
        let instance = Request.getInstance();
        instance.requests[name] = {
            queue: instance.currentQueue,
            url,
            data,
            success,
            fail,
            req_method,
            req_datatype,
        };
        return Request;
    }

    /**
     * Call defined requests
     * @param name string Request name
     * @returns {Request}
     */
    static callDefine(name, label='') {
        let instance = Request.getInstance();
        let request  = instance.requests[name];
        if (!request) {
            throw `Request (${name}) not found.`;
        }

        Request.request(
            instance.currentQueue,
            request.url,
            request.data || {},
            request.success,
            request.fail || null,
            request.req_method || 'GET',
            request.req_datatype || 'json',
            label || name || ''
        );
        return Request;
    }

    /**
     * Add a GET request
     * @param url
     * @param data
     * @param success
     * @param fail
     * @param req_datatype
     * @param label
     * @returns {Request}
     */
    static get(url, data, success, fail=null, req_datatype='json', label='') {
        Request.request(null, url, data, success, fail, 'GET', req_datatype, label);
        return Request;
    }

    /**
     * Add a POST request
     * @param url
     * @param data
     * @param success
     * @param fail
     * @param req_datatype
     * @param label
     * @returns {Request}
     */
    static post(url, data, success, fail=null, req_datatype='json', label='') {
        Request.request(null, url, data, success, fail, 'POST', req_datatype, label);
        return Request;
    }

    /**
     * Set current queue
     * @param queue string Queue Name
     * @returns {Request}
     */
    static queue(queue='default') {
        Request.getInstance().currentQueue = queue;
        return Request;
    }


    /**
     * Hold (disable auto-send) requests
     * @returns {Request}
     */
    static hold() {
        Request.getInstance().autoSend = false;
        return Request;
    }

    /**
     * Update autoSend status & execute queue if autoSend is true
     * @param auto bool
     * @returns {Request}
     */
    static send() {
        let instance = Request.getInstance();
        instance.autoSend = true;

        Object.keys(instance.queues).forEach(queue => {
            instance.execute(queue);
        });
        return Request;
    }

    /**
     * Abort current queue
     * @returns {Request}
     */
    static abort() {
        let instance = Request.getInstance();
        instance.abort(instance.currentQueue);
        return Request;
    }

    /**
     * Clear/Empty current query
     * @returns {Request}
     */
    static clear() {
        let instance = Request.getInstance();
        instance.clear(instance.currentQueue);
        return Request;
    }

    /**
     * On Event
     * @param event string
     * @param callback
     * @returns {Request}
     */
    static on(event, callback) {
        Request.getInstance().on(event, callback);
        return Request;
    }

    /**
     * Call method on server side
     * @param method string Method name (syntax: [folderName/][className.]<methodName>).
     * @param data object Request data.
     * @param success function Success callback.
     * @param fail function|null Fail callback.
     * @param ajaxUrl string Ajax URL, use null to get default url from Config.js file.
     * @returns {boolean}
     */
    static call(method, data, success, fail=null, ajaxUrl=null) {
        ajaxUrl   = ajaxUrl || Config.ajax.url;
        let parse = Request.parse(method, this.ajaxFolder, this.ajaxClass);
        if (!parse) {
            console.error(`Invalid method syntax`);
            return false;
        }
        if (!ajaxUrl) {
            console.error(`Ajax URL is required.`);
            return false;
        }

        let label = 'Call method ' + parse.method;

        let reqData = {
            data: Util.isCallable(data) ? data() : data,
            ajax: {
                folder:   parse.folder,
                class:    parse.class,
                method:   parse.method,
                dataType: this.dataType,
            }
        };
        Request.request('call', ajaxUrl, reqData, success, fail, 'POST', 'json', label);
    }

    /**
     * Parse method, folder, class for requests (Syntax:
     * @param method string Method name (syntax: [folderName/][className.]<methodName>).
     * @param ajaxFolder string Default ajax sub-folder in PAC/UI.
     * @param ajaxClass string Default class.
     * @returns {*}
     */
    static parse(method, ajaxFolder='UI', ajaxClass='Default') {
        if (!method) {
            return null;
        }

        //parse folder
        let parts = method.split('/');
        if (parts.length > 2) {
            throw new Error(`Invalid method syntax (${method}).`);
        }

        let folder = ajaxFolder;
        if (parts.length == 2) {
            folder = parts[0];
            method = parts[1];
        }

        //parse class
        parts = method.split('.');
        if (parts.length > 2) {
            throw new Error(`Invalid method syntax (${method}).`);
        }
        return {
            folder,
            class:  parts.length == 1 ? ajaxClass : parts[0],
            method: parts.length == 1 ? parts[0]  : parts[1]
        }
    }


    /**
     * Add request
     * @param queue string Queue name.
     * @param url string Ajax URL
     * @param data object
     * @param success callback Callback for success/done event.
     * @param fail callback|null Callback for fail/error event.
     * @param req_method string Request method (GET, POST, PUT, DELETE).
     * @param req_datatype string Request data type (json, html, xml).
     * * @param name string Label for generating progression message.
     */
    add(queue, url, data, success, fail, req_method, req_datatype, label) {
        if (!this.queues[queue]) {
            this.queues[queue]   = [];
            this.ajaxs[queue]    = null;
        }

        this.totalRequests++;

        this.queues[queue].push({
            label:    label || url, //use url as label if undefined
            url:      url,
            type:     req_method,
            dataType: req_datatype,
            data:     data,
            success:  success,
            fail:     fail
        });

        if (this.autoSend && !this.isLoading(queue)) {
            this.execute(queue);
        }
    }

    /**
     * Pop an item in queue
     * @param queue string Queue name
     * @returns {object}|{null} Return request object or null if empty
     */
    pop(queue) {
        if (!this.queues[queue] || !this.queues[queue].length) {
            return null;
        }
        return this.queues[queue].shift() || null;
    }

    /**
     * Count total requests in all queues
     * @returns {number}
     */
    countTotalRequests() {
        let total = 0;
        for (let queue in this.queues) {
            total += this.queues[queue].length;
        }
        return total;
    }

    /**
     * Compute complete percentage
     * @returns {*}
     */
    getProgress() {
        let total   = this.totalRequests - this.countTotalRequests();
        let percent = this.totalRequests ? ((total / this.totalRequests) * 100) : 0;
        return !percent || percent < 100 ? percent.toFixed(2)*1 : parseInt(percent);
    }

    /**
     * Execute a queue
     * @param queue string
     */
    execute(queue) {
        let progress = this.getProgress();
        let request  = this.pop(queue);
        if (!request) {
            return;
        }

        let eventData = {
            queue: queue,
            label:  request.label,
            request: {
                method: request.type,
                url:    request.url,
                params: request.data || {}
            }
        };
        this.trigger('start', {status: 'start', ...eventData});
        this.trigger('progress', {complete: progress, message: request.label, ...eventData});

        let ajax = $.ajax({
            url:      request.url,
            type:     request.type,
            dataType: request.dataType,
            data:     request.data
        });

        //handle on success/done event
        ajax.done(data => {
            request.success(data);

            if (this.getProgress() == 100) {
                this.totalRequests = 0;
                this.trigger('progress', {complete: 100, message: 'Done.', ...eventData});
                this.trigger('done', {status: 'done'});
            }
        });

        //handle on fail/error event
        ajax.fail((xhr, status, message) => {
            if (request.fail) {
                request.fail(xhr, status, message);
            }
            this.trigger('error', {status: 'error', xhr, status, message, ...eventData});
        });

        //process next request in queue
        ajax.always(data => {
            this.ajaxs[queue] = null;
            this.execute(queue);
        });

        this.ajaxs[queue] = {
            jqXHR: ajax,
            data:  request.data
        };
    }

    /**
     * Check if queue(s) is loading?
     * @param queue string|null Queue name or use null for all queues.
     * @returns {boolean}
     */
    isLoading(queue=null) {
        let queues = queue ? [queue] : Object.keys(this.queues);
        for (let q of queues) {
            if (this.ajaxs[q]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Clear all requests in queue
     * @param queue string|null Queue name or use null for all queues.
     */
    clear(queue=null) {
        let queues = queue ? [queue] : Object.keys(this.queues);
        for (let q of queues) {
            this.queues[q] = [];
            this.trigger('clear', {queue: q});
        }
    }

    /**
     * Abort/cancel queue(s)
     * @param queue string|null Queue name or use null for all queues.
     */
    abort(queue=null) {
        let queues = queue ? [queue] : Object.keys(this.queues);
        queues.forEach(q => {
            this.clear(q);
            if (this.ajaxs[q]) {
                this.ajaxs[q].jqXHR.abort();
                this.trigger('abort', {queue: q});
            }
        });
    }
}