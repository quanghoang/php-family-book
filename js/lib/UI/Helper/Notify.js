import 'bootstrap-notify-3-1-3';
import Html from './Html';
// import $ from 'es6-shim/jquery-noglobal-3-1-1';

export default class Notify {

    static info(message, title=null, settings={}) {
        Notify.notify(message, title, settings.url, 'fa fa-info-circle', {
            type: "info",
            ...settings
        });
    }

    static success(message, title=null, settings={}) {
        Notify.notify(message, title, settings.url, 'fa fa-check-circle', {
            type: "success",
            ...settings
        });
    }

    static error(message, title=null, settings={}) {
        Notify.notify(message, title, settings.url, 'fa fa-exclamation-circle', {
            type: "danger",
            ...settings
        });
    }

    static warning(message, title=null, settings={}) {
        Notify.notify(message, title, settings.url, 'fa fa-exclamation-circle', {
            type: "warning",
            ...settings
        });
    }

    static notify(message, title=null, url=null, icon=null, settings={}) {
        let options = {
            message: Html.icon(message),
            icon:    icon  || '',
            title:   title ? title + ':' : '',
            url:     url   || '',
            target:  url ? '_blank' : ''
        };

        settings = {
            element:         'body',
            position:        null,
            type:            "info",
            allow_dismiss:   true,
            newest_on_top:   true,
            showProgressbar: false,
            placement:       {from: "top", align: "right"},
            offset:          20,
            spacing:         10,
            z_index:         1000000, //1031,
            delay:           5000,
            timer:           1000,
            url_target:      '_blank',
            mouse_over:      null,
            animate:         {enter: 'animated fadeInDown', exit: 'animated fadeOutUp'},
            onShow:          null,
            onShown:         null,
            onClose:         null,
            onClosed:        null,
            icon_type:       'class',
            template: `
                <div data-notify="container" class="ui-notify col-xs-11 col-sm-3 alert alert-{0}" role="alert">
                     <button type="button" aria-hidden="true" class="close ui-notify-close" data-notify="dismiss">×</button>
                     <span data-notify="icon" class="ui-notify-icon"></span>
                     <span data-notify="title" class="ui-notify-title">{1}</span>
                     <span data-notify="message">{2}</span>
                     <div class="progress" data-notify="progressbar">
                        <div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0"
                             aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                     </div>
                     <a href="{3}" target="{4}" data-notify="url"></a>
                 </div>`, //{0} = type, {1} = title, {2} = message, {3} = url, {4} = target
            ...settings
        };

        $.notify(options, settings);
    }
}