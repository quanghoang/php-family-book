/**
 * Created by hoangq on 7/17/17.
 */

import Dialog from 'lib/UI/Container/Bootstrap/Dialog';
import Popover from 'lib/UI/Container/Bootstrap/Popover';

export default class Bootstrap {

    static initToggles(container=null) {
        if (!container || !container.length) {
            container = $('body');
        }

        Dialog.initToggles(container);
        Popover.initToggles(container);
    }
}