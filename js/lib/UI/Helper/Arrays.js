/**
 * Created by hoangq on 1/19/17.
 */

import Util from 'lib/UI/Helper/Util';

export default class Arrays {

    /**
     * Flatten object array
     * @param data array Object array
     * @returns {*}
     */
    static flatten(data) {
        if (Util.isEmpty(data) || !Util.isArray(data)) {
            return data;
        }
        let flatData = [];
        for (let row of data) {
            flatData.push(Util.flattenObject(row));
        }
        return flatData;
    }


    /**
     * Sort object array
     * @param data array Object array
     * @param sorts object Eg. {first_name: 'ASC', age: 'DESC'}
     */
    static sort(data, sorts) {
        if (Util.isEmpty(data) || !Util.isArray(data) || Util.isEmpty(sorts) || !Util.isObject(sorts)) {
            return data;
        }

        //compare function
        function compareObject(sorts) {
            return (a, b) => {
                for (let field in sorts) {
                    let dir = sorts[field] ? 1 : -1;
                    let compare = a[field] > b[field] ? dir : (a[field] < b[field] ? -dir : 0);
                    if (compare) {
                        return compare;
                    }
                }
                return 0;
            }
        }

        //convert sort condition from text to integer number
        let result = {};
        for (let field in sorts) {
            let value = sorts[field].toUpperCase();
            value     = value == 'ASC' ? 1 : (value == 'DESC' ? 0 : false);

            if (value !== false) {
                result[field] = value;
            }
        }
        return data.sort(compareObject(result));
    }

    /**
     * Search object array by keyword
     * @param data array  Data object array. Eg. [{first_name: 'Foo', last_name: 'Bar'}, etc.]
     * @param keyword string Search keyword. Eg. 'fo'
     * @returns {*}
     */
    static search(data, keyword) {
        if (Util.isBlank(keyword) || Util.isEmpty(data) || !Util.isArray(data)) {
            return data;
        }

        let result = [];
        let regexp = new RegExp(Util.escapeRegExp(keyword), 'i');

        for (let row of data) {
            for (let col in row) {
                let value = row[col];
                if (regexp.test(value)) {
                    result.push(row);
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Filter data
     * @param data array Data array. Eg. [{first_name: 'Foo', last_name: 'Bar'}, etc.]
     * @param filters Filter array. Eg. [{name: 'first_name', value: 'foo'}, etc.]
     * @returns {*}
     */
    static filter(data, filters) {
        if (Util.isEmpty(data) || !Util.isArray(data) || Util.isEmpty(filters) || !Util.isArray(filters)) {
            return data;
        }

        //create filter regular expressions
        let regexps = {};
        for (let i=0; i < filters.length; i++) {
            regexps[filters[i].name] = new RegExp(Util.escapeRegExp(filters[i].value), 'i');
        }

        //filter rows
        let result = [];
        for (let row of data) {
            for (let col in row) {
                let value  = row[col];
                let regexp = regexps[col] || null;
                if (regexp && regexp.test(value)) {
                    result.push(row);
                    break;
                }
            }
        }
        return result;
    }
}