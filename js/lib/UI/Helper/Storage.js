/**
 * Created by hoangq on 1/18/17.
 */

import Util from './Util';
import Arrays from './Arrays';

export default class Storage {

    constructor() {
        this.data    = [];
        this.cached  = null; //cached data
        this.keyword = null; //search keyword
        this.sort    = null; //sort object
        this.filter  = null; //filter array
    }

    /**
     * Set search keyword
     * @param keyword string
     */
    setKeyword(keyword) {
        this.cached  = null;
        this.keyword = keyword;
    }

    /**
     * Set sort object
     * @param sort object Sort object. Eg. {first_name: 'ASC', age: 'DESC'}
     */
    setSort(sort) {
        this.cached = null;
        this.sort   = sort;
    }

    /**
     * Set filter array
     * @param filter array Filter array. Eg. [{name: 'first_name', value: 'foo'}, etc.]
     */
    setFilter(filter) {
        this.cached  = null;
        this.filter = filter;
    }

    /**
     * Set storage data
     * @param data array
     */
    setData(data) {
        if (!Util.isArray(data)) {
            throw new Error('Data must be an array.');
        }

        for (let i=0; i < data.length; i++) {
            data[i]['_id'] = i + 1;
        }
        this.data   = Arrays.flatten(data);
        this.cached = this.data;
    }

    /**
     * Get storage data
     * @returns {*}
     */
    getData() {
        if (this.cached) {
            return this.cached;
        }

        let data    = this.data;
        data        = Arrays.search(data, this.keyword);
        data        = Arrays.filter(data, this.filter);
        data        = Arrays.sort(data, this.sort);
        this.cached = data;
        return data;
    }


    /**
     * Get number of pages
     * @param rowsPerPage int
     * @returns {number}
     */
    getNumPages(rowsPerPage) {
        let data = this.getData();
        return !rowsPerPage ? 1 : Math.ceil(data.length / rowsPerPage);
    }

    /**
     * Get page data
     * @param page int
     * @param rowsPerPage int
     * @returns {*}
     */
    getPage(page, rowsPerPage) {
        let data = this.getData();
        if (!page || !rowsPerPage || Util.isEmpty(data)) {
            return data;
        }

        let length   = data.length;
        let numPages = this.getNumPages(rowsPerPage);
        page         = Math.min(Math.max(page, 1), numPages);
        let offset   = (page - 1) * rowsPerPage;
        let result   = [];

        for (let i=0; i < rowsPerPage && i + offset < length; i++) {
            result.push(data[i + offset]);
        }
        return result;
    }

    /**
     * Get row by id
     * @param rowId int
     * @returns {null}|{object}
     */
    getRowById(rowId) {
        let data = this.getData();
        let index = rowId - 1;
        return index < 0 || index >= data.length ? null : data[index];
    }
}