/**
 * Created by hoangq on 1/6/17.
 */

import Util from 'lib/UI/Helper/Util';
import Arrays from 'lib/UI/Helper/Arrays';

export default class Html {

    /**
     * Create Select dropdown list
     * @param id string
     * @param name string Name of the select dropdown
     * @param options array List of options [{label: 'label', value: 'value'}, ...]
     * @param value string|array Selected value
     * @param defaults array Default options, eg. None, All, etc.
     * @param multiple bool Create multiple select or not
     * @param sort_group string Sort groups. Eg. ASC, or DESC.
     * @param sort_option string Sort options. Eg. ASC, or DESC.
     * @returns {string}
     */
    static createSelect(id, name, options=[], value=null, defaults=[], multiple=false, classes='', sort_group='', sort_option='') {
        id       = id ? `id="${id}"` : '';
        value    = Util.isArray(value) ? value : (value ? [value] : null);
        defaults = Util.isArray(defaults) ? defaults : [defaults];
        options  = defaults.concat(options);

        let index = 0;
        for (let option of options) {
            if (Util.isString(option)) {
                options[index] = {label: option, value: option};
            }
            index++;
        }

        //find groups in options
        let groups = [''];
        let found  = {};
        options.forEach((option) => {
            let group = option.group;
            if (group && !found[group]) {
                found[group] = true;
                groups.push(group);
            }
        });

        //sort group
        sort_group = sort_group || '';
        switch (sort_group.toUpperCase()) {
            case 'ASC':
                groups = groups.sort();
                break;

            case 'DESC':
                groups = groups.sort().reverse();
                break;
        }


        //select options
        if (value) {
            options.forEach((option, index) => {
                options[index].selected = value.indexOf(option.value) >= 0;
            });
        }

        //create groups
        let html = '';
        groups.forEach((group) => {
            html += Html.createOptionGroup(options, group, sort_option);
        });

        multiple = multiple ? ' multiple' : '';
        return `<select ${id} name="${name}"${multiple} class="${classes}">\n${html}</select>\n`;
    }

    /**
     * Create Select Option
     * @param label string
     * @param value string
     * @param selected bool
     * @returns {string}
     */
    static createOption(label, value, selected=false) {
        selected = selected ? ' selected="selected"' : '';
        return `<option value="${value}"${selected}>${label}</option>\n`;
    }


    /**
     * Create option group
     * @param options array
     * @param group string Group name (use '' for default group)
     * @param sort_option string Sort options ASC or DESC.
     * @returns {string}
     */
    static createOptionGroup(options, group, sort_option='') {
        let html = '';
        let group_options = [];

        for (let option of options) {
            if (group == option.group || (group == '' && !option.group)) {
                group_options.push(option);
            }
        }

        //sort options
        sort_option = sort_option || '';
        group_options = Arrays.sort(group_options, {label: sort_option.toUpperCase()});

        for (let option of group_options) {
            html += Html.createOption(option.label, option.value, option.selected);
        }


        if (group && group != '') {
            html = `<optgroup label="${group}">${html}</optgroup>`;
        }
        return html;
    }

    /**
     * Create Hidden input
     * @param id
     * @param name string
     * @param value string
     * @returns {string}
     */
    static createHidden(id, name, value) {
        id = id ? `id="${id}"` : '';
        return `<input ${id} type="hidden" name="${name}" value="${value}" />\n`;
    }

    /**
     * Create Radio input
     * @param id
     * @param name
     * @param label
     * @param value
     * @param checked
     * @returns {string}
     */
    static createRadio(id, name, label, value, checked=false) {
        id      = id ? `id="${id}"` : '';
        checked = checked ? ' checked="checked"' : '';
        return `<label class="ui-noselect"><input ${id} type="radio" name="${name}" value="${value}"${checked} /> ${label}</label>\n`;
    }

    /**
     * Create Checkbox input
     * @param id
     * @param name
     * @param label
     * @param value
     * @param checked
     * @returns {string}
     */
    static createCheckbox(id, name, label, value, checked=false) {
        id      = id ? `id="${id}"` : '';
        checked = checked ? ' checked="checked"' : '';
        return `<label class="ui-field-label ui-noselect"><input ${id} type="checkbox" name="${name}" value="${value}"${checked}/> ${label}</label>\n`;
    }

    /**
     * Create Text input
     * @param id
     * @param name
     * @param value
     * @param classes
     * @param placeholder
     * @param autocomplete
     * @returns {string}
     */
    static createText(id, name, value, classes=null, placeholder=null, autocomplete=false) {
        id           = id ? `id="${id}"` : '';
        value        = value ? value : '';
        placeholder  = placeholder ? `placeholder="${placeholder}"` : '';
        autocomplete = !autocomplete ? `autocomplete="off"` : '';
        classes      = classes ? classes : '';
        return `<input type="text" ${id} name="${name}" value="${value}" class="form-control ${classes}" ${placeholder} ${autocomplete} />`;
    }

    /**
     * Create Textarea input
     * @param id
     * @param name
     * @param value
     * @param rows
     * @param placeholder
     * @param classes
     * @returns {string}
     */
    static createTextarea(id, name, value, rows=4, placeholder=null, classes=null) {
        id          = id ? `id="${id}"` : '';
        value       = value ? value : '';
        placeholder = placeholder ? `placeholder="${placeholder}"` : '';
        classes     = classes ? classes : '';
        return `<textarea ${id} name="${name}" class="form-control ${classes}" ${placeholder} rows="${rows}">${value}</textarea>`;
    }

    /**
     * Create button input
     * @param id
     * @param name
     * @param value
     * @param classes
     * @param submit
     * @param title string
     * @param disabled bool
     * @param visible
     * @returns {string}
     */
    static createButton(id, name, value, classes='btn-primary', submit=false, title='', disabled=false, visible=true) {
        id       = id ? `id="${id}"` : '';
        submit   = submit ? 'submit' : 'button';
        value    = Html.icon(value);
        title    = title ? `title="${title}"` : '';
        disabled = disabled ? `disabled` : '';
        visible  = visible ? '' : `style="display:none"`;
        return `<button ${id} name="${name}" type="${submit}" class="btn ${classes}" ${title} ${disabled} ${visible}>${value}</button>`;
    }

    /**
     * Create form
     * @param id string
     * @param name string
     * @param content string
     * @param classes string CSS classes
     * @param method string Submit method
     * @param action string Submit location
     * @param enctype string Encrypt type (Eg. multipart/form-data)
     * @returns {string}
     */
    static createForm(id, name, content, classes='', method=null, action=null, enctype=null) {
        id      = id ? `id="${id}"` : '';
        name    = name ? ` name=${name}` : '';
        method  = method ? ` method="${method}"` : '';
        action  = action ? ` action="${action}"` : '';
        enctype = enctype ? ` enctype="${enctype}"` : '';
        return `<form ${id}${name}${method}${action}${enctype} class="${classes || ''}">${content}</form>`;
    }

    /**
     * Create label for editing field
     * @param label
     * @returns {string}
     */
    static createLabel(label) {
        return `<label class="ui-label">${label}</label>`;
    }

    /**
     * Create list
     * @param items array
     * @param classes CSS classes
     * @returns {string}
     */
    static createList(items, classes='') {
        let content = '';
        items.forEach((item) => {
            content += `<li>${item}</li>`;
        });
        return `<ul class="ui-list ${classes}">${content}</ul>`;
    }


    /**
     * Create loader animation icon
     * @param show bool
     * @returns {string}
     */
    static createLoader(show=false) {
        return `<div class="ui-loading ui-loader" style="display: ${show ? 'block' : 'hidden'}"></div>`;
    }

    /**
     * Parse awesome icons in text.
     * Eg1. [i:star] ==> <i class="fa fa-star"></i>
     * Eg2. [i2|s:spinner] ==> <i class="fa fa-spin fa-spinner fa-2x"></i>
     * Eg3. [i|r90:star] ==> rotate star icon 90 degrees
     * Eg4. [i|v:bus] ==> flip bus icon vertically
     * @param text string
     */
    static icon(text) {
        if (!Util.isString(text)) {
            return text;
        }
        if (!text) {
            return '';
        }
        const regex = /\[i([12345]{0,1})[|]{0,1}([ibsqlhvrm901278]*):([\w\-~*,]+)\]/g;

        return text.replace(regex, (src, size, attributes, icon) => {
            let rotate = attributes.indexOf('r90')  >= 0 ? '90' :
                        (attributes.indexOf('r180') >= 0 ? '180' :
                        (attributes.indexOf('r270') >= 0 ? '270' : false));

            let flip =   attributes.indexOf('h') >= 0 ? 'horizontal' :
                        (attributes.indexOf('v') >= 0 ? 'vertical' : false);

            return Html.createIcon(icon, {
                size:    size || 0,
                border:  attributes.indexOf('b') >= 0,
                spin:    attributes.indexOf('s') >= 0,
                quote:   attributes.indexOf('q') >= 0,
                li:      attributes.indexOf('l') >= 0,
                inverse: attributes.indexOf('i') >= 0, //invert color/white icon
                margin:  attributes.indexOf('m') >= 0,
                rotate:  rotate,
                flip:    flip,
            });
        });
    }

    /**
     * Create awesome icon
     * @param icon string Icon name. Eg. star/fa-star (single icon) or circle*2,~twitter (stack icon)
     * @param attributes object Attributes including size, border, spin, quote, li, rotate, flip
     * @returns {string}
     */
    static createIcon(icon, attributes={}) {
        let [size, border, spin, quote, li, rotate, flip, inverse, margin] = [
            attributes.size     || 0,     //icon size from 0 to 5
            attributes.border   || false, //show border around the icon or not.
            attributes.spin     || false, //spin icon or not (used with fa-spinner, fa-circle-o-notch, fa-refresh, fa-cog, fa-pulse, etc.)
            attributes.quote    || false, //quote icon or not (used to add quote icon at beginning of a paragraph).
            attributes.li       || false, //use icon to replace list item bullets or not. (used with ul, li tags)
            attributes.rotate   || false, //rotate icon '90', '180', '270' or not (false)
            attributes.flip     || false, //flip icon 'horizontal' or 'vertical' or not (false)
            attributes.inverse  || false, //invert color / white icon
            attributes.margin   || false, //add margin-right: 10px or not
        ];

        let sizes     = ['', 'fa-lg', 'fa-2x', 'fa-3x', 'fa-4x', 'fa-5x'];
        let iconSize  = sizes[size];
        let icons     = icon.split(',');
        let stack     = [];
        let regex     = /\*([12345]+)/; //parse stacked icon size
        let isStacked = icons.length > 1;

        for (let icon of icons) {
            let stackInverse = icon.substr(0, 1) == '~';
            let stackSize    = 'fa-stack-1x';

            icon      = stackInverse ? icon.substr(1) : icon;
            icon      = icon.substr(0, 3) == 'fa-' ? icon : 'fa-' + icon;
            let match = icon.match(regex);

            if (match) {
                stackSize = 'fa-stack-' + match[1] + 'x';
                icon = icon.replace(regex, '');
            }

            if (isStacked) {
                icon = icon + ' ' + stackSize;
                icon = stackInverse ? icon + ' fa-inverse' : icon;
            }
            else {
                icon = iconSize ? icon + ' ' + iconSize : icon;
            }

            icon = border  ? icon + ' fa-border' : icon;
            icon = spin    ? icon + ' fa-spin' : icon;
            icon = quote   ? icon + ' fa-quote-left fa-pull-left' + (size ? '' : ' fa-3x') : icon;
            icon = li      ? icon + ' fa-li' : icon;
            icon = rotate  ? icon + ' fa-rotate-' + rotate : icon;
            icon = flip    ? icon + ' fa-flip-' + flip : icon;

            stack.push(icon);
        }

        inverse = inverse ? 'fa-inverse' : '';
        margin  = margin  ? 'fa-margin-right' : '';
        if (!isStacked) {
            return `<i class="fa ${stack[0]} ${inverse} ${margin}"></i>`;
        }

        let content = '';
        stack.forEach(icon => {
            content += `<i class="fa ${icon}"></i>`;
        });
        return `<span class="fa-stack ${iconSize} ${inverse} ${margin}">${content}</span>`;
    }
}