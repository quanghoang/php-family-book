import 'treeview-1-2-0';
import 'treeview-1-2-0/dist/bootstrap-treeview.min.css!';
import BaseTree from '../Base/Tree';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Bootstrap from 'lib/UI/Helper/Bootstrap';

export default class Tree extends BaseTree {

    constructor(attributes = {}) {
        super(attributes);
    }

    convertData(data) {
        if (!data || !data.length) {
            return null;
        }
        let result = [];
        for (let node of data) {
            node = this.convertNode(node);
            if (node) {
                result.push(node);
            }
        }
        return result;
    }

    convertNode(node) {
        return node ? {
            text: Html.icon(node.label || ''),
            nodes: this.convertData(node.child),
            tags: [node.tag || '']
        } : null;
    }

    init() {
        let data = this.convertData(this.data);

        this.getContainer().treeview({
            data: data,
            selectedBackColor: '#fff1ca',
            selectedColor: '#000',
            showTags: true,
            levels: this.expandLevels || 10000000
        });
    }

    markup() {
        return '&nbsb;';
    }

    expandAll() {

    }
}