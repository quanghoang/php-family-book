import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';

export default class Tree extends Component {

    constructor(attributes = {}) {
        super(attributes);

        this.type           = 'Tree';
        this.containerClass = attributes.containerClass || 'uc uc-tree';
        this.data           = attributes.data || null;
        this.expandLevels   = attributes.expandLevels === undefined ? 2 : attributes.expandLevels; //use 0 to expand all levels
    }
}
