/**
 * Created by hoangq on 12/20/16.
 */

import Filter from './Filter'
import Util from 'lib/UI/Helper/Util'

import 'lib/UI/css/ui.css!';
import 'lib/UI/css/bootstrap-chosen.css!';
import 'lib/UI/Filter/css/Filter.css!';

export default class DartFilter extends Filter {

    constructor(config={}) {
        super(config);

        this.dart_elements = {
            operator_icao_code:{
                type:        'Select',
                name:        'operator_icao_code',
                label:       'Operator',
                required:    true,
                sort_group:  'DESC',
                sort_option: 'ASC',
                method:      'Dart/Operator.loadOperators',
                operators:   ['eq']
            },
            registration_name: {
                type:        'Select',
                name:        'registration_name',
                label:       'Registration Name',
                required:    true,
                sort_group:  'ASC',
                sort_option: 'ASC',
                method:      'Dart/Aircraft.getAllAircraftList',
                depend:      'operator_icao_code',
                operators:   ['eq']
            },
            include_engineer_faults: {
                type: 'YesNo',
                name: 'include_engineer_faults',
                label: 'Include Engineer Faults',
                value: 'Yes'
            },
            month: {
                type:  'Month',
                name:  'month',
                label: 'Month'
            },
            start_date: {
                type:     'Date',
                label:    'Start Date',
                name:     'start_date',
                operator: ['eq']
            },
            end_date: {
                type:     'Date',
                label:    'End Date',
                name:     'end_date',
                operator: ['eq']
            }
        };
    }

    /**
     * Overwrite method
     * @param attributes object|array
     */
    addElement(attributes) {
        let elements = [];
        Util.toArray(attributes).forEach((element) => {
            //create element from string value
            if (Util.isString(element)) {
                if (!this.dart_elements[element]) {
                    throw new Error(`Dart element (${element}) not found.`);
                }
                elements.push(this.dart_elements[element]);
                return; //continue
            }

            //merge element with dart element
            if (element.name && this.dart_elements[element.name]) {
                let clone = $.extend({}, this.dart_elements[element.name]);
                element   = $.extend(clone, element);
                elements.push(element);
                return; //continue
            }
            elements.push(element);
        });
        super.addElement(elements);
    }
}