/**
 * Created by hoangq on 12/6/16.
 */

import Field from '../Field';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Select extends Field {

    constructor(attributes={}) {
        attributes.multiple  = Util.toBool(attributes.multiple, false);
        attributes.operators = attributes.operators || ['eq', 'ne'];
        attributes.options   = attributes.options   || [];
        attributes.defaults  = attributes.defaults  || [{label: '', value: ''}];
        super(attributes);
    }

    /**
     * Overwrite renderEditMode method
     * @returns {string}
     */
    renderEditMode() {
        //let options = this.options['info'] ? [] : this.options;
        let options = this.info ? [] : this.options;
        let content = Html.createSelect(null, 'value', options, this.value, this.defaults,
                                        this.multiple, '', this.sort_group, this.sort_option);
        return this.createEditForm(content);
    }


    /**
     * Overwrite initEditMode method
     */
    initEditMode() {
        super.initEditMode();

        //loading data if not loaded
        if (!this.loaded && Util.isEmpty(this.options)) {
            this.loadData();
        }

        let container = this.filter.getContainer().find('.edit-container');
        let select    = this.getInput().addClass('field_' + this.name);
        let okButton  = container.find('[name=ok]');
        let info      = container.find('.ui-info').hide();
        let settings  = {width: '100%'};

        if (this.loading) {
            this.showLoadingIcon(true);

            //disable select & ok button
            select.prop('disabled', true);
            okButton.prop('disabled', true);

            //change select placeholder text
            settings = {...settings, placeholder_text_single: 'Loading...', placeholder_text_multiple: 'Loading...'};
        }

        if (!Util.isEmpty(this.info)) {
            select.prop('disabled', true);
            okButton.prop('disabled', true);
            info.html(this.info.message).show();
        }

        select.chosen(settings);
    }

    /**
     * Overwrite getValue method
     * @returns object
     */
    getData() {
        let data = super.getData();
        if (this.multiple) {
            let value = data.value || null;
            data.value = !value || Util.isArray(value) ? value : [value];
        }
        return data;
    }

    /**
     * Overwrite getAttributes method
     */
    getAttributes() {
        return $.extend(super.getAttributes(), {
            options:     this.options,
            defaults:    this.defaults,
            multiple:    this.multiple,
            sort_group:  this.sort_group,
            sort_option: this.sort_option
        });
    }
}