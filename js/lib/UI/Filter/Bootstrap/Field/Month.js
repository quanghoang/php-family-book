/**
 * Created by hoangq on 12/20/16.
 */

import Select from './Select';
import Util from 'lib/UI/Helper/Util';

export  default class Month extends Select {

    constructor(attributes={}) {
        attributes.multiple           = Util.toBool(attributes.multiple, false);
        attributes.use_month_name     = Util.toBool(attributes.use_month_name, false);
        attributes.include_this_month = Util.toBool(attributes.include_this_month, true);
        attributes.num_months         = attributes.num_months || 24;
        super(attributes);

        //month names list
        this.months  = ["January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December"];

        //create month options list
        if (Util.isEmpty(this.options)) {
            this.options = this.createMonthOptions();
        }
    }

    /**
     * Create month options list
     * @returns {Array}
     */
    createMonthOptions() {
        let options   = [];
        let date      = new Date();
        let year      = date.getFullYear();
        let month     = date.getMonth();
        let numMonths = this.include_this_month ? this.num_months : this.num_month + 1;

        for (let i=0; i < numMonths; i++) {
            //create label & value
            let monthVal = Util.padString(month + 1, 2);
            let label    = this.use_month_name ? `${year} - ${this.months[month]}` : `${year}-${monthVal}`;
            let value    = `${year}-${monthVal}`;

            //move to next month
            month--;
            if (month < 0) {
                year--;
                month = 11;
            }

            if (!this.include_this_month && !i) {
                continue;
            }
            options.push({label: label, value: value});
        }
        return options;
    }

    /**
     * Overwrite method
     */
    getAttributes() {
        return $.extend(super.getAttributes(), {
            use_month_name:     this.use_month_name,
            include_this_month: this.include_this_month
        });
    }

}