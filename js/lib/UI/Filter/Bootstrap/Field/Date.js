/**
 * Created by hoangq on 12/6/16.
 */

import Text from './Text';
import Util from 'lib/UI/Helper/Util';
import DateTime from 'lib/UI/Helper/DateTime';
//import $ from 'es6-shim/jquery-noglobal-3-1-1';


export default class Date extends Text {

    constructor(attributes={}) {
        attributes.operators  = attributes.operators  || ['eq', 'ne', 'bf', 'af'];
        attributes.dateFormat = attributes.dateFormat || 'yy-mm-dd';
        super(attributes);
    }

    /**
     * Overwrite method
     * @param operator string
     * @returns {*}
     */
    toOperatorLabel(operator) {
        let labels = {eq: 'on', ne: 'not on'};
        return labels[operator] || super.toOperatorLabel(operator);
    }

    /**
     * Overwrite method
     */
    initEditMode() {
        super.initEditMode();

        let input = this.filter.getContainer().find('.edit-container input[name=value]');
        input.datepicker({dateFormat: this.dateFormat});
        input.datepicker('show');
    }


    /**
     * Overwrite method
     * @returns {boolean}
     */
    validate() {
        if (!super.validate()) {
            return false;
        }

        let value = this.getValue();
        if (!Util.isEmpty(value) && !DateTime.isDate(value, this.dateFormat)) {
            this.error = `The <b>${this.label}</b> field must be a valid date.`;
            return false;
        }
        return true;
    }
}