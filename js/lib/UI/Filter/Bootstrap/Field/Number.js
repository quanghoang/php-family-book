/**
 * Created by hoangq on 12/12/16.
 */


import Text from './Text';
import Util from 'lib/UI/Helper/Util';

export default class Number extends Text {

    constructor(attributes={}) {
        attributes.operators  = attributes.operators  || ['eq', 'ne', 'gt', 'ge', 'lt', 'le'];
        attributes.dateFormat = attributes.dateFormat || 'yy-mm-dd';
        super(attributes);
    }

    /**
     * Overwrite method
     */
    initEditMode() {
        super.initEditMode();

        //restrict keys pressed (0-9, dot, comma, plus and minus only)
        this.filter.edit_container.find('input[name=value]').keypress((e) => {
            let regex = /[0-9.,\-\+]/;
            let key   = e.keyCode || e.which;

            if (key == 27 || key == 13) {
                return; //ignore ESC and Enter key
            }
            if (!regex.test(String.fromCharCode(key))) {
                e.preventDefault();
                return false;
            }
        });
    }

    /**
     * Overwrite method
     * @param operator string
     * @returns {*}
     */
    toOperatorLabel(operator) {
        let labels = {eq: '=', ne: '!=', gt: '>', ge: '>=', lt: '<', le: '<='};
        return labels[operator] || super.toOperatorLabel(operator);
    }

    /**
     * Overwrite method
     * @returns {boolean}
     */
    validate() {
        if (!super.validate()) {
            return false;
        }

        if (!Util.isNumeric(this.getValue())) {
            this.error = `The <b>${this.label}</b> field must be a valid number.`;
            return false;
        }
        return true;
    }
}