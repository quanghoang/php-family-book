/**
 * Created by hoangq on 12/6/16.
 */

import Field from '../Field';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import DateTime from 'lib/UI/Helper/DateTime';


export default class DateRange extends Field {

    constructor(attributes={}) {
        attributes.operators  = attributes.operators || ['eq', 'ne'];
        attributes.dateFormat = attributes.dateFormat || 'yy-mm-dd';
        super(attributes);

        this.ranges = {
            last_month: {label: 'Last Month', value: 'last_month', ...DateTime.getLastMonthRange()},
            last_week:  {label: 'Last Week',  value: 'last_week',  ...DateTime.getLastWeekRange()},
            custom:     {label: 'Custom',     value: 'custom',     start: DateTime.today(), end: DateTime.today()}
        };
    }

    /**
     * Overwrite renderEditMode method
     * @returns {string}
     */
    renderEditMode() {
        let options = [];
        for (let range of Object.keys(this.ranges)) {
            options.push(this.ranges[range]);
        }

        let range = this.ranges['last_month'];
        let content = `
        <div class="row">
            <input type="hidden" name="value" value="${this.value || ''}" />
            <div class="col-sm-4">${Html.createSelect(null, 'range', options)}</div>
            <div class="col-sm-4">${Html.createText(null, 'start_date', range.start, null, 'Start Date')}</div>            
            <div class="col-sm-4">${Html.createText(null, 'end_date', range.end, null, 'End Date')}</div>
        </div>
        `;
        return this.createEditForm(content);
    }


    /**
     * Overwrite initEditMode method
     */
    initEditMode() {
        super.initEditMode();

        let container  = this.filter.getContainer().find('.edit-container');
        this.select    = container.find('select[name=range]');
        this.startDate = container.find('input[name=start_date]');
        this.endDate   = container.find('input[name=end_date]');

        this.select.chosen({width: '100%'}).change(() => {
            this.onRangeSelect()
        });

        this.startDate.datepicker({dateFormat: this.dateFormat});
        this.endDate.datepicker({dateFormat: this.dateFormat});

        this.startDate.change(() => {
            if (this.isCustomRange()) {
                this.ranges.custom.start = this.startDate.val();
                this.updateHiddenInput();
            }
        });

        this.endDate.change(() => {
            if (this.isCustomRange()) {
                this.ranges.custom.end = this.endDate.val();
                this.updateHiddenInput();
            }
        });

        let value = this.parseValue(this.value);
        if (value) {
            this.select.val(value.range).trigger('chosen:updated');
            if (this.isCustomRange()) {
                this.ranges.custom.start = value.start;
                this.ranges.custom.end   = value.end;
                this.onRangeSelect();
            }
        }

        this.onRangeSelect();
    }

    isCustomRange() {
        return this.select.val() == 'custom';
    }

    parseValue(value) {
        if (!value) {
            return null;
        }

        let parts = value.split(',');
        if (parts.length != 3) {
            return null;
        }
        return {range: parts[0], start: parts[1], end: parts[2], label: this.ranges[parts[0]].label};
    }

    updateHiddenInput() {
        this.getInput().val(this.getValue());
    }

    /**
     * Handle range select event
     */
    onRangeSelect() {
        let range    = this.select.val();
        let start    = this.ranges[range].start;
        let end      = this.ranges[range].end;
        let disabled = !this.isCustomRange();

        this.startDate.prop('disabled', disabled).val(start);
        this.endDate.prop('disabled', disabled).val(end);
        this.updateHiddenInput();
    }

    formatValue(value) {
        if (!value) {
            return 'empty';
        }
        let parse = this.parseValue(value);
        if (!parse) {
            return value;
        }
        let dateRange = `${parse.start} - ${parse.end}`;
        return parse.range == 'custom' ? dateRange : `${parse.label} (${dateRange})`;
    }


    /**
     * Overwrite getValue method
     * @returns object
     */
    getValue() {
        let range = this.select.val();
        let start = this.startDate.val();
        let end   = this.endDate.val();
        return `${range},${start},${end}`;
    }

    formatOperatorLabel(operator, value) {
        switch (operator) {
            case 'eq':
                return 'is';

            case 'ne':
                return 'is not';
        }
        return this.toOperatorLabel(operator);
    }

    validate() {
        let value = this.parseValue(this.getValue());
        if (value.range != 'custom') {
            return true;
        }

        if (Util.isBlank(value.start) || Util.isBlank(value.end)) {
            this.error = `Both Start Date and End Date field are required.`;
            return false;
        }

        if (value.end < value.start) {
            this.error = `The End Date (${value.end}) must be after the Start Date (${value.start}).`;
            return false;
        }

        return true;
    }


    /**
     * Overwrite getAttributes method
     */
    getAttributes() {
        return $.extend(super.getAttributes(), {
        });
    }
}