/**
 * Created by hoangq on 12/2/16.
 */

import Field from '../Field';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Text extends Field {

    constructor(attributes={}) {
        attributes.operators = attributes.operators || ['eq', 'ne', 'ct', 'nc', 'sw', 'ns'];
        super(attributes);
    }

    /**
     * Render edit mode
     * @returns {string}
     */
    renderEditMode() {
        let value   = this.value || '';
        let content = Html.createText(null, 'value', value);
        return this.createEditForm(content);
    }

    /**
     * Overwrite method
     */
    initInput() {
        super.initInput();

        //set cursor at the end of the text box
        let input = this.getInput();
        input.val(input.val());
    }
}