/**
 * Created by hoangq on 12/9/16.
 */


import Field from '../Field';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Checkbox extends Field {

    constructor(attributes={}) {
        attributes.operators = attributes.operators || ['eq', 'ne'];
        attributes.options   = attributes.options   || [];
        attributes.multiple  = true;
        super(attributes);
    }

    /**
     * Render edit mode
     * @returns {string}
     */
    renderEditMode() {
        let content = '';
        this.options.forEach((option) => {
            let checked = Util.isArray(this.value) ? this.value.indexOf(option.value) >= 0 : option.checked;
            content    += Html.createCheckbox(null, 'value', option.label, option.value, checked);
        });
        return this.createEditForm(content);
    }

    /**
     * Overwrite method
     * @param value
     */
    initInput() {
        //set focus on the first checkbox
        this.getInput().each((i, input) => {
            input = $(input);
            if (input.prop('checked')) {
                input.focus();
                return false; //break
            }
        });
    }

    /**
     * Overwrite getValue method
     * @returns object
     */
    getData() {
        let data = super.getData();
        let value = data.value || null;
        data.value = !value || Util.isArray(value) ? value : [value];
        return data;
    }

    /**
     * Overwrite method
     */
    getAttributes() {
        return $.extend(super.getAttributes(), {
            options:  this.options
        });
    }
}