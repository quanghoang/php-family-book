/**
 * Created by hoangq on 12/20/16.
 */

import Radio from './Radio';
import Util from 'lib/UI/Helper/Util';

export  default class YesNo extends Radio {

    constructor(attributes) {
        super(attributes);

        if (Util.isEmpty(this.options)) {
            this.options = [{label: 'Yes', value: 'Yes'}, {label: 'No', value: 'No'}];
        }
    }
}