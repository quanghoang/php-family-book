/**
 * Created by hoangq on 12/9/16.
 */

import Field from '../Field';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Radio extends Field {

    constructor(attributes={}) {
        attributes.operators = attributes.operators || ['eq', 'ne'];
        attributes.options   = attributes.options || [];
        super(attributes);
    }

    /**
     * Render edit mode
     * @returns {string}
     */
    renderEditMode() {
        let content = '';
        this.options.forEach((option) => {
            let checked = this.value ? this.value == option.value : option.checked;
            content    += Html.createRadio(null, 'value', option.label, option.value, checked);
        });
        return this.createEditForm(content);
    }

    /**
     * Overwrite method
     * @param value
     */
    initInput() {
        //set focus on the first radio
        this.getInput().each((i, input) => {
            input = $(input);
            if (input.prop('checked')) {
                input.focus();
                return false; //break
            }
        });
    }

    /**
     * Overwrite method
     */
    getAttributes() {
        return $.extend(super.getAttributes(), {
            options:  this.options
        });
    }
}