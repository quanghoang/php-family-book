/**
 * Created by hoangq on 12/7/16.
 */

import Text from './Field/Text';
import Date from './Field/Date';
import DateRange from './Field/DateRange';
import Month from './Field/Month';
import Number from './Field/Number';
import Select from './Field/Select';
import Radio from './Field/Radio';
import YesNo from './Field/YesNo';
import Checkbox from './Field/Checkbox';

export default class Factory {

    /**
     * Create field
     * @param attributes object Field attributes object
     * @returns {Field}
     */
    static createField(attributes={}) {
        let type = attributes.type.toLowerCase();

        if (!type) {
            throw new Error('Element type not found.');
        }

        let types = {
            text:      Text,
            date:      Date,
            daterange: DateRange,
            month:     Month,
            number:    Number,
            select:    Select,
            radio:     Radio,
            yesno:     YesNo,
            checkbox:  Checkbox
        };

        if (types[type]) {
            return new (types[type])(attributes);
        }
        throw new Error(`Invalid field type (${attributes.type}).`);
    }
}