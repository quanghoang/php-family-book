/**
 * Created by hoangq on 12/2/16.
 */

import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Field {

    constructor(attributes={}) {
        attributes.method   = attributes.method   || null;
        attributes.value    = attributes.value    || null;
        attributes.operator = attributes.operator || null;
        attributes.active   = Util.toBool(attributes.active, true);
        Field.counter       = attributes.id ? (Field.counter || 1) : Field.counter + 1;

        //public
        this.attributes     = attributes;
        this.operators      = [];       //list of operators (equals, does not equal, etc.)
        this.active         = true;     //active or not
        this.duplicate      = false;    //duplicated or not
        this.required       = false;    //required or not
        this.trimed         = true;     //trim value?
        this.method         = null;     //method name for loading data
        this.depend         = null;     //which field(s) this field depend on

        //private
        this.filter         = attributes.filter || null;
        this.type           = 'Field';
        this.index          = Field.counter;
        this.id             = attributes.id || `filter-field-${this.index}`;
        this.error          = ''; //error message
        this.info           = ''; //info message
        this.loading        = false;
        this.loaded         = false;

        //init attributes
        let self = this;
        $.each(attributes, function(name, value) {
            self[name] = value;
        });

        //check required attributes
        this.requireAttributes(['id', 'type', 'name', 'label', 'filter']);

        //listen & update when dependents changed
        this.depend = Util.toArray(this.depend);

        if (!Util.isEmpty(this.depend)) { //&& this.id != 'new'
            this.on('change', (event) => {
                if (this.depend.includes(event.filter.name)) {
                    this.update(event);
                }
            });
        }
    }

    /**
     * Convert operator value to label
     * @param operator string
     * @returns string
     */
    toOperatorLabel(operator) {
        let labels = {
            eq: 'equals',
            ne: 'does not equal',
            ct: 'contains',
            nc: 'does not contain',
            sw: 'starts with',
            ns: 'does not start with',
            bf: 'before',
            af: 'after',
            bt: 'between',
            gt: 'greater than',
            ge: 'greater than or equal',
            lt: 'less than',
            le: 'less than or equal'
        };
        return labels[operator] || 'unknown';
    }

    /**
     * Is not operator or not
     * @param operator string
     * @returns {boolean}
     */
    isNotOperator(operator) {
        let notOperators = ['ne', 'nc', 'ns'];
        return notOperators.indexOf(operator) >= 0;
    }

    /**
     * Get value
     * @returns {*|null}
     */
    getValue() {
        return this.trimed ? Util.trim(this.value) : this.value;
    }

    /**
     * Get attributes for cloning object
     * @returns object
     */
    getAttributes() {
        return $.extend(this.getData(), {
            label:     this.label,
            type:      this.type,
            operators: this.operators,
            duplicate: this.duplicate,
            required:  this.required,
            method:    this.method,
            depend:    this.depend
        });
    }

    /**
     * Get field data
     * @returns object
     */
    getData() {
        return {
            name:     this.name,
            value:    this.getValue(),
            operator: this.operator,
            active:   this.active,
        }
    }

    /**
     * Load data from server
     * @param callback function
     * @param data null|object
     */
    loadData(callback=null, data=null) {
        // data = {
        //     data:  data,
        //     state: this.filter.getData(),
        //     field: this.getAttributes()
        // };

        let params = data ? {data: data} : {};
        for (let field of this.filter.getData()) {
            if (field.active) {
                params[field.name] = field.value;
                params[field.name + '_op'] = field.operator;
            }
        }

        this.call(this.method, response => {
            this.initLoadData(response);
            this.loaded = true;

            if (Util.isCallable(callback)) {
                callback(response.data);
            }
        }, params);
    }

    /**
     * Init loaded data, called by loadData() method after loading data finish
     * @param response
     */
    initLoadData(response) {
        this.info = '';
        let data = response.data;

        if (response.info) {
            this.info = {
                message: response.message,
                //data:    response.data
            };
            data = [];
        }

        //refresh view mode
        let options  = data || [];
        this.options = options;
        this.refreshEditMode();

        //remove value(s) not in the options array
        let changed = false;
        let values  = Util.toArray(this.value);
        this.value  = [];
        values.forEach((value) => {
            let found = false;
            options.forEach((option) => {
                if (value == option.value) {
                    found = true;
                    return false; //break
                }
            });
            if (found) {
                this.value.push(value);
            }
            else {
                changed = true;
            }
        });
        this.value = this.multiple ? this.value : (this.value[0] || null);
        this.value = Util.isEmpty(this.value) ? null : this.value;

        //refresh view mode
        this.refreshViewMode();

        //trigger change event if values changed
        if (changed) {
            this.trigger('change', {status: 'updated', filter: this.getData()})
        }
    }


    /**
     * Update data, called if there is any dependent field changed
     * @param event object ChangeEvent data object
     */
    update(event) {
        this.loadData();
    }


    /**
     * Call method on server
     * @param method string
     * @param callback function
     * @param data null|object
     */
    call(method, callback, data=null) {
        if (!this.method) {
            return;
        }

        this.filter.call(method, data, callback);
    }

    /**
     * Reset to default value
     */
    reset() {
        this.operator = this.attributes.operator || this.operators[0];
        this.value    = this.attributes.value || null;
    }

    /**
     * Check required attributes
     * @param requiredAttributes array List of attribute names (eg. ['name', 'label', ...])
     * @returns {boolean}
     */
    requireAttributes(requiredAttributes) {
        let self = this;
        requiredAttributes.forEach(function(name) {
            if (!self[name]) {
                throw new Error(`Attribute '${name}' is required for '${self.type}' field.`);
            }
        });
        return true;
    }

    /**
     * Render Field
     * @param mode Render mode (view or edit)
     * @returns {*}
     */
    render(mode='view') {
        switch (mode) {
            case 'view':
                return this.renderViewMode();
            case 'edit':
                return this.renderEditMode();
        }
        return 'Invalid render mode.';
    }

    /**
     * Render view mode
     * @returns {string}
     */
    renderViewMode() {
        let operator       = this.formatOperatorLabel(this.operator, this.value);
        let value          = this.formatValue(this.value);
        let value_class    = this.value ? 'ui-bold' : 'ui-normal';
        let active_checked = this.active ? 'checked="checked"' : '';
        let active_class   = this.active ? '' : 'ui-inactive';
        let readonly       = this.filter.readonly ? 'ui-readonly' : '';

        //override operator value if value is empty
        operator = value == 'empty' ? 'equals' : operator;

        return `
        <div id="${this.id}" class="ui-field ${active_class} ui-noselect bg-info ${readonly}">
            <span class="ui-filter-active">
                ${this.filter.activateButton ? 
                    `<input type="checkbox" name="active" ${active_checked} value="1" title="Active/Inacive"/>` : 
                    '<input type="hidden" name="active" value="1" />'} 
                
                <input type="hidden" name="${this.name}" value="${value}" />
            </span>
            <span class="ui-text">
                <span class="ui-bold">${this.label}</span> 
                <span class="ui-normal">${operator}</span> 
                <span class="${value_class}">${value}</span>
            </span>
            ${this.filter.removeButton ? 
                '<span class="ui-close glyphicon glyphicon-remove" title="Remove"></span>' : 
                '<span style="padding: 0 5px"></span>'}             
        </div>`;
    }

    /**
     * Format value
     * @param value
     * @returns string
     */
    formatValue(value) {
        if (!value) {
            return 'empty';
        }
        if (!Util.isArray(value) || value.length == 1) {
            return value;
        }
        return '(' + value.join(', ') + ')';
    }

    /**
     * Format operator label
     * @param operator string
     * @param value string|array
     * @returns string
     */
    formatOperatorLabel(operator, value) {
        switch (operator) {
            case 'eq':
                if (Util.isArray(value) && value.length > 1) {
                    return 'any of';
                }
                break;

            case 'ne':
                if (Util.isArray(value) && value.length > 1) {
                    return 'none of';
                }
                break;
        }
        return this.toOperatorLabel(operator);
    }

    /**
     * Render edit mode
     * @returns {string}
     */
    renderEditMode() {
        return 'To be implemented by inherited classes.';
    }

    /**
     * Refresh edit mode (re-render the edit form)
     */
    refreshEditMode() {
        let content   = this.renderEditMode();
        let container = this.filter.getContainer().find('.edit-container');
        let fieldId   = container.find('form input[name=field_id]').val();

        if (fieldId == this.id) {
            container.html(content);
            this.initEditMode();
        }
    }

    /**
     * Refresh view mode
     */
    refreshViewMode() {
        let content = this.renderViewMode();
        this.filter.getContainer().find('#' + this.id).replaceWith(content);
        this.initViewMode();
    }

    /**
     * Create Operator dropdown select
     */
    createOperatorSelect() {
        if (Util.isEmpty(this.operators)) {
            throw new Error('Operators cannot be empty.');
        }

        // //not show drowdown list if having only one operator
        // if (this.operators.length == 1) {
        //     return Html.createHidden(null, 'operator', this.operator);
        // }

        let options = [];
        this.operators.forEach((operator) => {
            let label = this.toOperatorLabel(operator);
            options.push({label: label, value: operator});
        });
        return Html.createSelect(null, 'operator', options, this.operator, [], false, 'form-control');
    }

    /**
     * Init view mode
     */
    initViewMode() {
        let container = $('#' + this.id);

        //handle click event
        container.click((e) => {
            let target = $(e.target);

            //click on close button
            if (target.hasClass('ui-close')) {
                this.filter.removeFilter(this.id);
                this.filter.refresh();
                return;
            }

            //do nothing on readonly mode
            if (this.filter.readonly) {
                return;
            }

            //click on active/inactive checkbox
            if (target.is('input')) {
                this.active = target.prop('checked');
                container.removeClass('ui-inactive');
                if (!this.active) {
                    container.addClass('ui-inactive');
                }
                this.filter.hideAddForm();
                this.filter.hideEditForm();

                let status = this.active ? 'active' : 'inactive';
                this.trigger('change', {status: status,  filter: this.getData()});
                return;
            }

            //click on the filter label
            if (this.filter) {
                this.filter.editFilter(this.id);
            }
        });

        //override default background for not-operators
        if (this.isNotOperator(this.operator)) {
            container.removeClass('bg-info');
            container.addClass('ui-not-operator');
        }
    }

    /**
     * Get operator select input element
     */
    getOperatorInput() {
        return this.filter.getContainer().find('.edit-container [name=operator]');
    }

    /**
     * Get input element
     */
    getInput() {
        return this.filter.getContainer().find('.edit-container [name=value]');
    }

    /**
     * Init input value
     * @param value
     */
    initInput() {
        this.getInput().focus();
    }

    /**
     * Init edit mode
     */
    initEditMode() {
        let container = this.filter.getContainer().find('.edit-container');

        //disable operator select if having only 1 operator
        if (this.operators.length == 1) {
            this.getOperatorInput().prop('disabled', true);
        }

        //init input value
        this.initInput();

        //handel button events
        container.find('[type=button]').click((e) => {
            let operator = this.getOperatorInput().val();
            let value    = this.getInput().val();
            let added    = this.id == 'new';
            let changed  = this.operator != operator || this.value != value;

            switch ($(e.target).attr('name')) {
                case 'ok':
                    let error_msg = container.find('.ui-error');
                    let old_value = this.value;
                    this.operator = operator;
                    this.value    = this.trimed ? Util.trim(value) : value;

                    //validate value
                    error_msg.hide();
                    if (!this.validate()) {
                        this.value = old_value;
                        error_msg.html(this.error).fadeIn('fast');
                        return;
                    }

                    //save changes
                    if (added) {
                        this.filter.addFilter(this.getAttributes());
                        this.filter.hideAddForm();
                    }
                    else if (changed) {
                        this.filter.refresh();
                    }

                    this.filter.hideEditForm();
                    this.trigger('change', {status: added ? 'added' : 'updated',  filter: this.getData()});
                    break;

                case 'cancel':
                    if (added) {
                        this.filter.hideAddForm();
                    }
                    this.filter.hideEditForm();
                    break;
            }
        });

        //set default button to OK button
        container.find('form').on('submit', function(e) {
            e.preventDefault();
            container.find('[name=ok]').click();
        });


        //set ESC key as Cancel button
        container.keyup(function(e) {
            if (e.keyCode == 27) {
                e.preventDefault();
                container.find('[name=cancel]').click();
            }
        });
    }

    /**
     * Create edit form
     * @param content string
     * @returns {string}
     */
    createEditForm(content) {
        let showOperators = this.operators.length > 1;
        let body = `<div class="col-lg-2 col-sm-3 ${showOperators ? '' : 'ui-hide'}">${this.createOperatorSelect()}</div>
                    <div class="${showOperators ? 'col-lg-10 col-sm-9' : 'col-lg-12'}">${content}</div>`;

        content = `
            ${Html.createHidden(null, 'field_id', this.id)}
            ${Html.createLabel(this.label)}
            <div class="row">
                ${body}
            </div>
            <div class="ui-buttons">                    
                    ${Html.createButton(null, 'ok', 'OK')}
                    ${Html.createButton(null, 'cancel', 'Cancel', 'btn-default')}
                    ${Html.createLoader()}
                </div>
            <div class="ui-info bg-info"></div>
            <div class="ui-error bg-danger"></div>`;
        return Html.createForm(null, 'edit-form', content, `ui-${this.type.toLowerCase()}-field`);
    }

    /**
     * Disable view mode
     * @param disabled bool
     */
    disableViewMode(disabled=true) {
        let container = $('#' + this.id);
        let classes   = 'ui-button-disabled ui-state-disabled';
        container.removeClass(classes);
        if (disabled) {
            container.addClass(classes);
        }
    }

    /**
     * Show loading icon
     * @param loading
     */
    showLoadingIcon(loading) {
        let element = this.filter.getContainer().find('.edit-container .ui-loading');
        element.css('display', 'inline-block');
        if (!loading) {
            element.hide();
        }
    }

    /**
     * Validate field value, to be implemented by the inherited classes
     * @returns {boolean}
     */
    validate() {
        this.error = '';
        if (this.required && Util.isBlank(this.getValue())) {
            this.error = `The <b>${this.label}</b> field is required.`;
            return false;
        }
        return true;
    }

    /**
     * On event
     * @param event string
     * @param callback function
     */
    on(event, callback) {
        this.filter.on(event, callback);
    }

    /**
     * Trigger event
     * @param event string
     * @param data object
     */
    trigger(event, data) {
        this.filter.trigger(event, data);
    }
}