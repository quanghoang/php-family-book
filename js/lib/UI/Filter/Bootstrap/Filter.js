/**
 * Created by hoangq on 12/2/16.
 */

import Component from 'lib/UI/Component';
import Factory from './Factory';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

import 'lib/UI/Filter/css/Filter.css!';
import 'chosen1-6-2';
// import 'jqueryui1-12-1/themes/base/jquery-ui.css!';


export default class Filter extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.operatorDelimiter = '.'; //separate operator & value
        this.activeDelimiter   = '!'; //active & inactive status
        this.valueDelimiter    = ';'; //separate values in array
        this.submitButtonId    = attributes.submitButtonId || null;
        this.submitButton      = Util.toBool(attributes.submitButton, false);
        this.submitUrl         = attributes.submitUrl || null;
        this.readonly          = Util.toBool(attributes.readonly, false);
        this.removeButton      = Util.toBool(attributes.removeButton, true);
        this.activateButton    = Util.toBool(attributes.activateButton, true);
        this.filters           = [];
        this.errors            = [];
        this.containerClass    = 'b3 uc filter-container';
        this.elements          = [];

        if (attributes.elements) {
            this.addElement(attributes.elements);
        }
    }

    /**
     * Add element (template for creating filters)
     * @param attributes object|array Element attributes or an array of attributes
     */
    addElement(attributes) {
        Util.toArray(attributes).forEach((attributes) => {
            attributes        = attributes || {};
            attributes.id     = 'new';
            attributes.filter = this;
            this.elements.push(Factory.createField(attributes));
        });
    }

    /**
     * Get element by name
     * @param name string
     * @returns {*}
     */
    getElement(name) {
        let element = null;
        this.elements.forEach((e) => {
            if (e.name == name) {
                element = e;
            }
        });
        return element;
    }

    /**
     * Add Filter (can add filters not in elements list)
     * @param attributes object|array Filter attributes or an array of attributes
     */
    addFilter(attributes) {
        let attributeList = Util.isArray(attributes) ? attributes : [attributes];

        attributeList.forEach((attributes) => {
            attributes        = attributes || {};
            attributes.filter = this;

            let index   = this.findFilter('name', attributes.name);
            let element = this.getElement(attributes.name);

            if (element && !element.duplicate && index >= 0) {
                //overwrite operator & value of the duplicated filter instead of adding new one
                if (attributes.operator) {
                    this.filters[index].operator = attributes.operator;
                }
                if (attributes.value) {
                    this.filters[index].value = attributes.value;
                }
                return; //continue
            }

            //add filter
            let field = Factory.createField(attributes);
            this.filters.push(field);
        });
        this.refresh();
    }

    /**
     * Set filter value & operator (only filters in elements list)
     * @param name string
     * @param value string|array
     * @param operator null|string Use null value for default operator
     * @param active bool Active filter or not
     */
    setFilter(name, value, operator=null, active=true) {
        let element = this.getElement(name);
        if (!element) {
            throw new Error(`Element (${name}) not found.`);
        }

        let attributes      = element.getAttributes();
        attributes.operator = !operator ? element.operators[0] : operator;
        attributes.value    = value;
        attributes.active   = active;
        this.addFilter(attributes);
    }

    /**
     * Find filter
     * @param attr string attribute name
     * @param value string attribute value
     * @returns {number} Return index if found or -1 if not found
     */
    findFilter(attr, value) {
        for (let index=0; index < this.filters.length; index++) {
            if (this.filters[index][attr] == value) {
                return index;
            }
        }
        return -1;
    }

    /**
     * Get filter by id
     * @param id
     * @returns {null}|{object}
     */
    getFilter(id) {
        let index = this.findFilter('id', id);
        return index >= 0 ? this.filters[index] : null;
    }

    /**
     * Remove filter by id
     * @param id Field id
     */
    removeFilter(id) {
        let index = this.findFilter('id', id);
        if (index >= 0) {
            let data = this.filters[index].getData();
            this.filters.splice(index, 1);

            this.disableAddButton(false);
            this.hideAddForm();
            this.hideEditForm();

            this.trigger('change', {status: 'removed', filter: data});
        }
    }

    /**
     * Reset/clear filters
     */
    reset() {
        this.filters = [];
        this.refresh();
    }

    /**
     * Create data query string
     * @returns {*}
     */
    createQueryString() {
        let data = {};
        this.filters.forEach((field) => {
            let value = field.value || '';
            value = Util.isArray(value) ? value.join(this.valueDelimiter) : value;
            value = field.operator + this.operatorDelimiter + value;
            value = (field.active ? '' : this.activeDelimiter) + value;

            if (!field.duplicate) {
                data[field.name] = value;
            }
            else {
                if (!data[field.name]) {
                    data[field.name] = [];
                }
                data[field.name].push(value);
            }
        });
        return $.param(data);
    }

    /**
     * Restore filters from query string data
     */
    restoreFilters() {
        let data = Util.getQueryStringData();
        let init_filters = this.filters.concat();
        let found = false;

        //remove all init filters (filters added before render method)
        this.filters = [];

        //set filters based on query string data
        this.elements.forEach((element) => {
            let name  = element.name;
            let value = data[name];

            if (value == undefined) {
                return; //continue
            }

            let values = Util.isArray(value) ? value : [value];
            values.forEach((value) => {
                let index = value.indexOf(this.operatorDelimiter);
                if (index <= 0 || index > 3) {
                    return; //continue
                }

                let operator = value.substring(0, index);
                let active   = operator[0] == this.activeDelimiter ? false : true;
                operator     = active ? operator : operator.substring(1);
                value        = value.substring(index + this.operatorDelimiter.length);

                //validate operator & value
                if (!element.operators.includes(operator)) {
                    return; //continue
                }

                //convert value to array for multiple elements
                if (element.multiple) {
                    value = value.split(this.valueDelimiter);
                    value = value.length == 1 && value[0] == '' ? null : value;
                }

                //set filter value
                this.setFilter(name, value, operator, active);
                found = true;
            });
        });

        //set init filters back if there is no filter set from query string
        if (!found) {
            init_filters.forEach((filter) => {
                this.setFilter(filter.name, filter.value, filter.operator, filter.active);
            });
        }
    }

    /**
     * Edit filter by id
     * @param id
     */
    editFilter(id) {
        this.filters.forEach((field) => {
            let found = field.id == id;
            field.disableViewMode(found);
            if (found) {
                this.hideAddForm();
                this.showEditForm(field);
            }
        });
    }

    /**
     * Get filters data
     * @returns {Array}
     */
    getData() {
        let data = [];
        this.filters.forEach((field) => {
            data.push(field.getData());
        });
        return data;
    }

    /**
     * Hide Edit Form
     */
    hideEditForm() {
        this.getContainer().find('.edit-container').html('').hide();
        this.filters.forEach((field) => {
            field.disableViewMode(false);
        });
    }

    /**
     * Show Edit Form
     * @param field
     */
    showEditForm(field) {
        let content = field.render('edit');
        this.getContainer().find('.edit-container').html(content).show();
        field.initEditMode();
    }

    /**
     * Refresh view
     */
    refresh() {
        //can't refresh when filter container was not rendered
        if (!this.isRendered) {
            return;
        }

        //create view content
        let content = '';
        this.filters.forEach((field) => {
            content += field.render('view');
        });
        this.getContainer().find('.view-container').html(content);

        //init field events
        this.filters.forEach((field) => {
            field.initViewMode();
        });

        //update add button status
        this.disableAddButton(!this.getAvailableElements().length);
    }

    markup() {
        let content = `
        <div class="error-container bg-danger"></div>
        <div class="view-container"></div>
        <div class="add-container">
            <a class="add-button btn btn-primary ${this.readonly ? 'ui-hide' : ''}" title="Add new filter">                
                <span class="fa fa-plus"></span>
            </a>                                    
            <div class="select-filter ui-hide">
                 <select name="filter" class="form-control"></select>
            </div>
        </div>
        <div class="edit-container"></div>`;

        //create submit button
        if (!this.submitButtonId && this.submitButton) {
            let name  = this.submitButton.name  || 'submit';
            let label = Util.isString(this.submitButton) ? this.submitButton : (this.submitButton.label || 'Submit');
            content  += `<div class="ui-submit">${Html.createButton(null, name, label)}</div>`;
        }
        return content;
    }

    /**
     * Init filter events
     */
    init() {
        let container = this.getContainer();

        //handel add button event
        let add_button = container.find('.add-button');
        add_button.click(() => {
            if (!add_button.hasClass('ui-button-disabled')) {
                this.showAddForm();
            }
        });

        //handel filter select change event
        let filter_select = container.find('select[name=filter]');
        filter_select.change(() => {
            let name = filter_select.val();
            this.elements.forEach((field) => {
                if (field.name == name) {
                    field.reset();
                    this.showEditForm(field);
                }
            });
        });

        //handle submit button
        let submitButton = this.submitButtonId ? $('#' + this.submitButtonId) : container.find('.ui-submit input');
        submitButton.click(() => {
            this.submitFilters();
        });

        //restore filters from query string
        this.restoreFilters();

        //trigger ready event
        this.trigger('ready');
    }

    /**
     * Show add form
     */
    showAddForm() {
        //create filter dropdown
        let options = '';
        this.getAvailableElements().forEach((element) => {
            options += Html.createOption(element.label, element.name);
        });

        let filter_select = this.getContainer().find('select[name=filter]');
        filter_select.html(options);
        filter_select.prop('selectedIndex', 0).change();

        //display the dropdown & edit form
        let container = this.getContainer().find('.select-filter');
        container.removeClass('ui-hide');
    }

    /**
     * Get available elements
     * @returns {Array}
     */
    getAvailableElements() {
        let elements = [];
        this.elements.forEach((element) => {
            let index = this.findFilter('name', element.name);
            if (!element.duplicate && index >= 0) {
                return;
            }
            elements.push(element);
        });
        return elements;
    }

    /**
     * Disable add button
     * @param disabled bool
     */
    disableAddButton(disabled=true) {
        let classes    = 'ui-button-disabled ui-state-disabled';
        let add_button = this.getContainer().find('.add-button');

        add_button.removeClass(classes);
        if (disabled) {
            add_button.addClass(classes);
        }
    }

    /**
     * Hide add form
     */
    hideAddForm() {
        this.getContainer().find('.select-filter').addClass('ui-hide');
    }

    /**
     * Submit filters (handle submit button)
     */
    submitFilters() {
        let errorContainer = this.getContainer().find('.error-container');
        errorContainer.hide();

        if (this.validate()) {
            let url = (this.submitUrl || Util.getAbsUrl()) + '?' + this.createQueryString();
            Util.redirect(url);
            return;
        }
        errorContainer.html(Html.createList(this.errors)).fadeIn();
    }


    /**
     * Validate all filters
     * @returns {boolean}
     */
    validate() {
        let valid   = true;
        this.errors = [];

        //check required fields first
        this.elements.forEach((element) => {
            if (element.required) {
                let index = this.findFilter('name', element.name);
                if (index < 0 || !this.filters[index].active) {
                    this.errors.push(`The <b>${element.label}</b> field is required.`);
                    valid = false;
                }
            }
        });

        //check none-required fields
        this.filters.forEach((filter) => {
            if (!filter.validate() && !this.errors.includes(filter.error)) {
                this.errors.push(filter.error);
                valid = false;
            }
        });
        return valid;
    }
}