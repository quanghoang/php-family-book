/**
 * Created by hoangq on 2/14/17.
 */


import Input from './Input';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';
import Arrays from 'lib/UI/Helper/Arrays';

export default class Select extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Select';
        this.containerClass = 'b3 uc uc-text';
        this.options        = attributes.options     || [];
        this.sortGroup      = attributes.sortGroup   || ''; //Can be '', 'asc', 'desc'
        this.sortOption     = attributes.sortOption  || ''; //Can be '', 'asc', 'desc'
        this.multiple       = Util.toBool(attributes.multiple, false);
        this.clearButton    = Util.toBool(attributes.clearButton, true);
        this.selectAll      = Util.toBool(attributes.selectAll, false);
        this.emptyOption    = Util.toBool(attributes.emptyOption, !this.multiple); //add an empty option at top or not
        this.settings       = attributes.settings    ||  //settings for select2 plugin
            {placeholder: this.holder, allowClear:  this.clearButton};

        if (this.selectAll) {
            this.append = `<span class="ui-select-all-button" title="Select All">[i1:check-square-o]</span>`;
            this.settings.allowClear = true;
        }
    }

    init() {
        let input = this.getInput();
        input.on('change keyup', event => {
            this.trigger('change', {
                id:    this.id,
                name:  this.name,
                value: this.getValue(),
                event: event
            });
        });

        //init select box
        input.select2(this.settings);
        input.val(this.value).change();

        //init data
        if (this.method && !this.loaded) {
            this.loadData();
        }

        //show load info if found
        if (this.info) {
            this.setPlaceHolder(this.info);
            this.enableInput(false);
        }

        //handle select all button
        if (this.selectAll) {
            this.getContainer().find('.ui-select-all-button').click(() => {
                let values = this.options.map(option => {
                    return option.value;
                });
                values = this.multiple ? values : values[0];
                input.val(values).change();
            });
        }

        if (this.value !== null) {
            this.setValue(this.value);
        }

        if (this.focus) {
            this.setFocus();
        }

        if (this.disabled) {
            this.enableInput(false);
        }
    }

    setPlaceHolder(placeholder) {
        this.getInput().select2({...this.settings, placeholder: placeholder});
    }

    loadData() {
        this.enableInput(false);
        this.getInput().html('');
        this.setPlaceHolder('Loading...');
        super.loadData();
    }

    initData(response) {
        let input = this.getInput();

        if (response.info) {
            this.setPlaceHolder(response.message);
            return;
        }

        this.setOptions(response.data);
        this.setPlaceHolder(this.holder);
        this.enableInput(!this.disabled);

        if (this.value) {
            this.setValue(this.value);
        }
    }

    setValue(value) {
        super.setValue(value);
        this.getInput().change(); //refresh select2
    }

    setOptions(options) {
        this.options = options;
        let html = this.emptyOption ? '<option></option>' : '';
        this.getInput().html(html + this.createOptions());
    }

    markup() {
        let options = this.createOptions();
        return this.createLabel(this.createInput(options));
    }

    /**
     * Create select input element
     * @param options string Options HTML content
     */
    createInput(options) {
        let multiple = this.multiple ? ' multiple' : '';
        return ` 
        <select id="${this.id}" name="${this.name}" ${multiple} class="form-control ${this.class}" style="width: 100%">
            ${this.emptyOption ? '<option></option>' : ''}
            ${options}
        </select>`;
    }

    /**
     * Close dropdown
     */
    closeDropdown() {
        this.getInput().select2('close');
    }

    /**
     * Create Select Options content
     */
    createOptions() {
        let value   = this.value;
        value       = Util.isArray(value) ? value : (value == null ? null : [value]);
        let options = this.options || [];

        if (Util.isEmpty(options)) {
            return '';
        }

        //find groups & convert string to object option
        let groups = [''];
        let found  = {};
        let index  = -1;
        for (let option of options) {
            index++;
            if (Util.isString(option)) {
                option = {value: option, label: option};
                options[index] = option;
            }

            let group = option.group;
            if (group && !found[group]) {
                found[group] = true;
                groups.push(group);
            }
        }

        //sort group
        switch (this.sortGroup.toUpperCase()) {
            case 'ASC':
                groups = groups.sort();
                break;
            case 'DESC':
                groups = groups.sort().reverse();
                break;
        }

        //select options
        if (value) {
            let index = 0;
            for (let option of options) {
                options[index++].selected = value.indexOf(option.value) >= 0;
            }
        }

        //create groups
        let content = '';
        groups.forEach((group) => {
            content += this.createOptionGroup(options, group);
        });
        return content;
    }

    /**
     * Create option group
     * @param options array
     * @param group string Group name (use '' for default group)
     * @returns {string}
     */
    createOptionGroup(options, group) {
        let content = '';
        let group_options = [];

        for (let option of options) {
            if (group == option.group || (group == '' && !option.group)) {
                group_options.push(option);
            }
        }

        //sort options
        switch (this.sortOption.toUpperCase()) {
            case 'ASC':
                group_options = Arrays.sort(group_options, {label: this.sortOption.toUpperCase()});
                break;
            case 'DESC':
                group_options = Arrays.sort(group_options, {label: this.sortOption.toUpperCase()}).reverse();
                break;
        }

        for (let option of group_options) {
            option   = Util.isString(option) ? {value: option, label: option} : option;
            content += `<option value="${option.value}" ${option.selected ? 'selected' : ''}>${option.label}</option>`;
        }

        if (group && group != '') {
            content = `<optgroup label="${group}">${content}</optgroup>`;
        }
        return content;
    }
}