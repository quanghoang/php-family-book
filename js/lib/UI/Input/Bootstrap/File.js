import Input from './Input';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';
import 'bootstrap-filestyle-1-2-3';

export default class File extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'File';
        this.containerClass = 'b3 uc uc-file';
        this.buttonLabel    = attributes.buttonLabel || 'Browse';
        this.icon           = attributes.icon || 'folder-open-o';
        this.accept         = attributes.accept || ''; //See: https://www.w3schools.com/tags/att_input_accept.asp
    }

    init() {
        this.getInput().filestyle({
            buttonText: this.buttonLabel,
            buttonName: this.class || 'btn-default',
            iconName:   'fa fa-' + this.icon,
        });
    }

    createInput() {
        let accept = this.accept ? `accept="${this.accept}"` : '';
        return `<input type="file" class="filestyle" id="${this.id}" name="${this.name}" ${accept} />`;
    }
}