/**
 * Created by hoangq on 3/21/17.
 */

import Input from './Input';
import Util from 'lib/UI/Helper/Util';

export default class Radio extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Radio';
        this.bold           = Util.toBool(attributes.bold, true);
        this.containerClass = 'b3 uc uc-radio '  + (this.bold ? 'uc-bold' : 'uc-normal');
        this.checkValue     = attributes.checkValue; //value return when check
        this.checked        = Util.toBool(attributes.checked, false) || this.value == this.checkValue;

        if (!this.checkValue) {
            throw `Radio: ${this.name}: The checkValue attribute is required.`;
        }
    }

    createLabel(content) {
        return `<label>${content} <span>${this.label}</span></label>`;
    }

    createInput() {
        return `        
        <input type="radio" name="${this.name}" id="${this.id}" value="${this.checkValue}" 
               class="${this.class}" ${this.disabled ? 'disabled' : ''} ${this.checked ? 'checked' : ''} />`
    }

    setValue(value) {
        this.value   = value;
        this.checked = value == this.checkValue;
        let input = this.getInput();
        if (input.length) {
            input.prop('checked', this.checked);
        }
    }

    getValue() {
        return this.getInput().is(':checked') ? this.checkValue : '';
    }
}