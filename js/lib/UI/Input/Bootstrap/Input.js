/**
 * Created by hoangq on 2/9/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import Validator from 'lib/UI/Helper/Validator';
import Html from 'lib/UI/Helper/Html';
import BaseInput from '../Base/Input';

export default class Input extends BaseInput {

    constructor(attributes={}) {
        super(attributes);
    }

    /**
     * Create label
     * @param content
     * @returns {string}
     */
    createLabel(content) {
        if (this.prepend || this.append) {
            content = `
            <div class="input-group ${this.prepend ? 'has-prepend' : ''} ${this.append ? 'has-append' : ''}">
                ${this.createPrepend()}${content}${this.createAppend()}
            </div>`;
        }

        let form = this.getForm();
        if (form) {
            let classes = '';
            if (this.layout == 'horizontal') {
                let col = form.label_columns;
                classes = `col-sm-${col}`;
                content = `<div class="col-sm-${12 - col}">${content}</div>`;
            }

            let label = this.label ? `<label class="control-label ${classes}" for="${this.id}">${this.label}</label>` : '';
            let help  = this.help ? `<span class="help-block">${this.help}</span> ` : '';
            let type  = this.type.toLowerCase();
            //return `<div id="${this.id}-wp" class="el el-${type} form-group ${this.class}">${label} ${content} ${help}</div>`;
            return `${label} ${content} ${help}`;
        }
        return super.createLabel(content);
    }

    /**
     * Create prepend content
     * @returns {string}
     */
    createPrepend() {
        return this.prepend ? `<div class="input-group-addon">${Html.icon(this.prepend)}</div>` : '';
    }

    /**
     * Create append content
     * @returns {string}
     */
    createAppend() {
        return this.append ? `<div class="input-group-addon">${Html.icon(this.append)}</div>` : '';
    }
}