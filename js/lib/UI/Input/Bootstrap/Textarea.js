/**
 * Created by hoangq on 3/21/17.
 */

import Input from './Input';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';
import 'jquery-ns-autogrow-1-1-6';
import CodeMirror from 'code-mirror-npm';
import 'code-mirror-npm/lib/codemirror.css!';
import 'code-mirror-npm/addon/edit/closebrackets';
import 'code-mirror-npm/addon/edit/closetag';
import 'code-mirror-npm/addon/edit/matchbrackets';
import 'code-mirror-npm/mode/htmlmixed/htmlmixed';
import 'code-mirror-npm/mode/xml/xml';
import 'code-mirror-npm/mode/javascript/javascript';
import 'code-mirror-npm/mode/css/css';
import 'code-mirror-npm/mode/xml/xml';
import 'code-mirror-npm/mode/python/python';
import 'code-mirror-npm/mode/sql/sql';
// import 'code-mirror-npm/mode/clike/clike-modified';
// import 'code-mirror-npm/mode/php/php-modified';
import 'code-mirror-npm/theme/dracula.css!';
import 'code-mirror-npm/theme/eclipse.css!';

// import 'tinymce-4-6-4';

export default class Textarea extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Textarea';
        this.containerClass = 'b3 uc uc-textarea';
        this.autocomplete   = Util.toBool(attributes.autocomplete, false);
        this.autogrow       = Util.toBool(attributes.autogrow, false);
        this.readonly       = Util.toBool(attributes.readonly, false);
        this.resizable      = Util.toBool(attributes.resizable, false);
        this.rows           = attributes.rows || 4;
        this.editor         = attributes.editor || false; //basic (false) or full
        this.theme          = attributes.theme || 'dark'; //dark or light
        this.code           = attributes.code || null;   //support: html, xml, javascript, css, json, php, python
                                                         //see all modes: http://codemirror.net/mode/index.html
    }

    init() {
        super.init();

        this.waitForVisibility(() => {
            if (this.editor && !this.code) {
                this.initHtmlEditor();
                return;
            }

            if (this.code) {
                this.initCodeEditor();
                return;
            }

            if (this.autogrow) {
                this.getInput().autogrow({vertical: true, horizontal: false});
            }
        });
    }

    initHtmlEditor() {
        let toolbar = {
            basic: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            full: 'undo redo | insert | styleselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor',
        };
        toolbar = toolbar[this.editor] ? toolbar[this.editor] : toolbar['basic'];

        let options = {
            selector: '#' + this.id,
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            branding: false,
            height: this.rows * 50,
            mode: "exact",
            readonly: this.readonly,
            //elements: "simple_rich_text, simple_rich_text01, simple_rich_text02, simple_rich_text03, simple_rich_text04, simple_rich_text05, simple_rich_text06",
            theme: 'modern',
            plugins: [
                'preview advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample'
            ],
            toolbar1: toolbar,
            //toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            removed_menuitems: 'newdocument, cut, copy, paste',
            invalid_elements: 'script',
            paste_word_valid_elements: "b,strong,i,em,h1,h2,u,p,ol,ul,li,a[href],span,color,font-size,font-color,font-family,mark,table,tr,td",
            paste_retain_style_properties: "all",
            content_style: ".mce-content-body {font-size:11pt;}"

        };
        tinymce.init(options);
    }

    initCodeEditor() {
        let options = {
            html: {
                mode: 'htmlmixed',
                closeTag: true,
            },
            javascript: {
                mode: "text/javascript",
                matchBrackets: true,
                continueComments: "Enter",
                extraKeys: {"Ctrl-Q": "toggleComment"},
            },
            json:  {
                mode: "application/ld+json",
                matchBrackets: true,
                autoCloseBrackets: true,
                indentUnit: 2,
            },
            css:  {
                mode: "text/css",
                extraKeys: {"Ctrl-Space": "autocomplete"}
            },
            php: {
                mode: "application/x-httpd-php",
                matchBrackets: true,
                autoCloseBrackets: true,
            },
            python: {
                mode: "text/x-cython",
                matchBrackets: true,
                version: 3,
                autoCloseBrackets: true,
                singleLineStringErrors: false
            },
            sql: {
                mode: "text/x-sql",
                matchBrackets: true,
                autoCloseBrackets: true,
            }
        };
        options = options[this.code] || {};
        options = {
            theme:          this.theme == 'dark' ? 'dracula' : 'eclipse',
            lineNumbers:    true,
            indentWithTabs: false,
            lineWrapping:   true,
            indentUnit:     4,
            readOnly:       this.readonly,
            ...options
    }
        this.codeMirror = CodeMirror.fromTextArea(document.getElementById(this.id), options);
        let height = this.autogrow ? 'auto' : this.rows * 50;
        this.codeMirror.setSize('100%', height);

    }

    createInput() {
        return `
        <textarea id="${this.id}" name="${this.name}" 
            class="form-control ${this.class} ${this.resizable ? 'ui-resizeable' : 'ui-no-resize'}" 
            placeholder="${this.holder}" 
            rows="${this.rows}" 
            ${this.disabled ? 'disabled' : ''} 
            ${this.readonly ? 'readonly' : ''}>${this.value || ''}</textarea>`;
    }

    getValue() {
        if (this.codeMirror) {
            this.codeMirror.save(); //update textarea
        }
        return super.getValue();
    }
}