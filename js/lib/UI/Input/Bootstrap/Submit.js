/**
 * Created by hoangq on 8/3/17.
 */

import Button from './Button';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';

export default class Submit extends Button {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Submit';
        this.containerClass = 'b3 uc uc-button';
        this.submit         = true;
        this.reset          = false;
        this.name           = attributes.name  || 'submit';
        this.value          = attributes.value || '1';
        this.label          = attributes.label || 'Submit';
        this.class          = attributes.class || 'primary';
    }
}