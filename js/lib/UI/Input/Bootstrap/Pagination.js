/**
 * Created by hoangq on 1/19/17.
 */

import Component from 'lib/UI/Component';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';

export default class Pagination extends Component {

    constructor(attributes) {
        attributes = attributes || {};
        super(attributes);

        this.activePage   = attributes.activePage || 1;
        this.totalItems   = attributes.totalItems || 0;
        this.itemsPerPage = attributes.itemsPerPage || 10;
        this.maxNumPages  = attributes.maxNumPages || 5;
        this.onChange     = attributes.onChange || null;

        //private
        this.startPage    = 1;
        this.endPage      = 0;

        if (Util.isCallable(this.onChange)) {
            this.on('change', this.onChange);
        }
    }

    getNumPages() {
        return Math.ceil(this.totalItems / this.itemsPerPage);
    }

    getStatus() {
        return {
            activePage:   this.activePage,
            startPage:    this.startPage,
            endPage:      this.endPage,
            startItem:    (this.activePage - 1) * this.itemsPerPage + 1,
            endItem:      Math.min(this.activePage * this.itemsPerPage, this.totalItems),
            totalPages:   this.getNumPages(),
            totalItems:   this.totalItems,
            itemsPerPage: this.itemsPerPage
        };
    }

    selectPage(activePage, startPage) {
        let ul          = this.getContainer().find('ul');
        let rendered    = !ul.length || activePage > this.endPage || this.startPage != startPage;
        let numPages    = this.getNumPages();
        this.activePage = activePage;
        this.startPage  = startPage;

        this.trigger('change', this.getStatus());

        if (rendered) {
            this.render();
            return;
        }

        //highlight active page
        ul.find('li').removeClass('active');
        ul.find(`li[data-page=${activePage}]`).addClass('active');

        //update buttons status
        ul.find('.pg-first').removeClass('disabled').addClass(activePage == 1 ? 'disabled' : '');
        ul.find('.pg-last').removeClass('disabled').addClass(activePage == numPages ? 'disabled' : '');
        ul.find('.pg-next').removeClass('disabled').addClass(activePage == numPages ? 'disabled' : '');
        ul.find('.pg-previous').removeClass('disabled').addClass(activePage == 1 ? 'disabled' : '');
    }


    init() {
        this.getContainer().find('li').click(e => {
            e.preventDefault();
            let li   = $(e.currentTarget);
            let page = li.data('page');
            let start = this.startPage;

            if (li.hasClass('disabled') || li.hasClass('active')) {
                return;
            }

            if (li.hasClass('pg-first')) {
                page  = 1;
                start = 1;
            }
            if (li.hasClass('pg-last')) {
                page  = this.getNumPages();
                start = page > this.endPage ? Math.max(1, page - this.maxNumPages + 1) : start;
            }
            if (li.hasClass('pg-next')) {
                page = this.activePage + 1;
                start = page > this.endPage ? page : start;
            }
            if (li.hasClass('pg-previous')) {
                page  = this.activePage - 1;
                start = page < this.startPage ? Math.max(1, page - this.maxNumPages + 1) : start;
            }
            this.selectPage(page, start);
        });
    }

    render() {
        if (!this.totalItems || this.getNumPages() == 1) {
            this.setContent('');
            return;
        }

        let content   = '';
        let numPages  = this.getNumPages();

        //first button
        content += `<li class="pg-first ${this.activePage == 1 ? 'disabled' : ''}" title="First">
                        <a href="#1">«</a>
                    </li>`;

        //previous button
        content += `<li class="pg-previous ${this.activePage == 1 ? 'disabled' : ''}" title="Previous">
                        <a href="#${this.startPage - 1}">
                            <span class="hidden-xs">Previous</span>
                            <span class="visible-xs">&lt;</span>
                        </a>
                    </li>`;

        //page buttons
        this.endPage = this.startPage;
        for (let i=0; i < this.maxNumPages; i++) {
            this.endPage = this.startPage + i;
            content += `<li class="pg-page ${this.endPage == this.activePage ? 'active' : ''}" 
                            data-page="${this.endPage}"><a href="#${this.endPage}">${this.endPage}</a></li>`;

            if (this.endPage == numPages) {
                break;
            }
        }

        //more button
        //content += `<li class="pg-more disabled ${this.endPage < numPages ? '' : 'hidden'}" title="More"><span>...</span></li><li>`;

        //next button
        content += `<li class="pg-next ${this.activePage == numPages ? 'disabled' : ''}" title="Next">
                        <a href="#${this.activePage + 1}">
                            <span class="hidden-xs">Next</span>
                            <span class="visible-xs">&gt;</span>
                        </a>
                    </li>`;

        //last button
        content += `<li class="pg-last ${this.activePage == numPages ? 'disabled' : ''}" title="Last">
                        <a href="#${numPages}">»</a>
                    </li>`;

        //create wrapper
        content = `<ul class="pagination pagination-large">${content}</ul>`

        this.setContent(content, 'ui-pagination');
        this.init();
    }
}