/**
 * Created by hoangq on 3/21/17.
 */

import Checkbox from './Checkbox';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Switch extends Checkbox {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Switch';
        this.containerClass = 'b3 uc uc-switch';
        this.checked        = Util.toBool(attributes.checked, false) || this.value == 1;
    }

    init() {
        this.getInput().hide();

        this.getContainer().find('a.btn').click(e => {
            let button = $(e.target);
            let value  = button.data('value') * 1;
            this.setValue(value);
        });

        this.setValue(this.value);
    }

    setValue(value) {
        this.value = value;
        this.getHiddenInput().val(value);
        this.getInput().prop('checked', value == 1);
        this.updateButtons();
        this.trigger('change', {id: this.id, name: this.name, value: value});
    }

    updateButtons() {
        let value     = this.getValue() * 1;
        let btnActive = value ? '.btn-yes' : '.btn-no';
        let btnClass  = value ? 'btn-success' : 'btn-danger';
        this.getContainer().find('a.btn').removeClass('active btn-success btn-danger');
        this.getContainer().find(btnActive).addClass('active ' + btnClass);
    }

    markup() {
        return super.markup() + `
        <div class="btn-group">            
            <a class="btn btn-no btn-default" data-value="0">No</a>
            <a class="btn btn-yes btn-default" data-value="1">Yes</a>
        </div>`;
    }

    createLabel(content) {
        return `<label><span>${this.label}</span></label><div>${content}</div>`;
    }
}