/**
 * Created by hoangq on 3/1/17.
 */

import Input from './Input';
import Util from 'lib/UI/Helper/Util';

export default class Autocomplete extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Autocomplete';
        this.containerClass = 'b3 uc uc-autocomplete';
        this.readonly       = Util.toBool(attributes.readonly, false);
        this.multiple       = Util.toBool(attributes.multiple, false);
        this.restrict       = Util.toBool(attributes.restrict, false); //restrict input text (only accept autocomplete values)
        this.options        = attributes.options || [];
    }

    init() {
        super.init();

        if (this.method && !this.loaded) {
            this.loadData();
            return;
        }
        this.initAutocomplete();
    }

    initAutocomplete() {
        var options = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: this.options
        });

        // multiple values (using tags input plugin)
        if (this.multiple) {
            this.getInput().tagsinput({
                typeaheadjs: {
                    source: options,
                },
                freeInput: !this.restrict
            });
            return;
        }

        // single value (use typeahead plugin)
        this.getInput().typeahead({hint: true, highlight: true, minLength: 1}, {source: options});
    }

    getValue() {
        let value = super.getValue();
        if (Util.isEmpty(value)) {
            return this.multiple ? [] : '';
        }
        return this.multiple ? value.split(',') : value;
    }

    initData(response) {
        this.options = response.data;
        this.initAutocomplete();
        this.enableInput(true);

        this.getInput().attr('placeholder', this.holder);
        this.getContainer().find('input.tt-input').attr('placeholder', this.holder);
    }

    loadData() {
        this.enableInput(false);
        this.getInput().attr('placeholder', 'Loading...');
        super.loadData();
    }

    createInput() {
        return `
        <input type="text" id="${this.id}" name="${this.name}" value="${this.value || ''}"
            class="form-control ${this.class}" placeholder="${this.holder}" autocomplete="off"
            ${this.disabled ? 'disabled' : ''} ${this.readonly ? 'readonly' : ''} 
            ${this.multiple ? `data-role="tagsinput"` : ''} />`
    }
}