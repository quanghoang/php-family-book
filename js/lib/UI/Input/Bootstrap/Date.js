/**
 * Created by hoangq on 3/2/17.
 */

import Text from './Text';
import Util from 'lib/UI/Helper/Util';
//import $ from 'es6-shim/jquery-noglobal-3-1-1';


export default class Date extends Text {


    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Date';
        this.containerClass = 'b3 uc uc-date';
        this.autocomplete   = false;
        this.dateFormat     = attributes.dateFormat || 'yy-mm-dd';
        this.prepend        = '[i:calendar]';
        this.clearButton    = Util.toBool(attributes.clearButton, true);
        this.holder         = attributes.holder || 'Select a date';
    }

    init() {
        super.init();
        this.getInput().datepicker({
            dateFormat: this.dateFormat
        });
    }
}