/**
 * Created by hoangq on 2/9/17.
 */

import Input from './Input';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';
import Inputmask from 'inputmask-3-3-10';

export default class Text extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Text';
        this.containerClass = 'b3 uc uc-text';
        this.autocomplete   = Util.toBool(attributes.autocomplete, false);
        this.readonly       = Util.toBool(attributes.readonly, false);
        this.hasDefault     = Util.toBool(attributes.hasDefault, false);
        this.defaultValue   = attributes.defaultValue !== undefined ? attributes.defaultValue : '';
        this.isDefault      = Util.toBool(attributes.isDefault, false);
        this.value          = attributes.value === null ? this.defaultValue : attributes.value;
        this.inputValue     = this.value === null ? '' : this.value;
        this.clearButton    = Util.toBool(attributes.clearButton, false);
        this.mask           = attributes.mask || null; //usage: 9: numeric, a: alphabetical, *: alphanumeric (https://github.com/RobinHerbots/Inputmask)

        if (this.hasDefault) {
            this.append = this.append || `<input type="checkbox" class="ui-default-value" title="Check to use default value" 
                                                 ${this.isDefault ? 'checked' : ''} />`;
        }
    }

    getDefaultCheckBox() {
        return this.getContainer().find(`input[type=checkbox]`);
    }

    init() {
        super.init();

        this.getInput().on('change, keyup', e => {
            if (!this.isDefault) {
                this.inputValue = this.getInput().val();
            }
        });

        this.getDefaultCheckBox().on('change', e => {
            this.isDefault = $(e.target).prop('checked');
            this.enableInput(!this.isDefault);
            this.setValue(this.isDefault ? this.defaultValue : this.inputValue);
            this.setFocus(!this.isDefault);
        });

        if (this.clearButton) {
            $(`#${this.id}-clear`).show().click(() => {
                this.getInput().val('');
            });
        }

        if (this.mask) {
            let options = {mask: this.mask, autoUnmask: true};
            Inputmask(options).mask(this.getInput()[0]);
        }

        if (this.disabled) {
            this.enableInput(false);
        }
        else {
            this.enableInput(!this.isDefault);
        }
    }

    createInput() {
        let value = this.isDefault ? this.defaultValue : this.value;
        value = value === undefined || value === null ? '' : value;
        return `
        <div class="ui-text-wrapper">
            <input type="text" id="${this.id}" name="${this.name}" value="${value}"
                class="form-control ${this.class}" placeholder="${this.holder}"
                autocomplete="${this.autocomplete ? 'on' : 'off'}"
                ${this.disabled ? 'disabled' : ''} ${this.readonly ? 'readonly' : ''} />
            <i id="${this.id}-clear" class="fa fa-close ui-clear-button"></i>        
        </div>`;
    }

    getValue() {
        return this.isDefault ? '[ui-default-value]' : super.getValue();
    }
}