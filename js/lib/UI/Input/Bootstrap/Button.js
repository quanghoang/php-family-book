/**
 * Created by hoangq on 2/9/17.
 */

import Input from './Input';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';

export default class Button extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Button';
        this.containerClass = 'b3 uc uc-button';
        this.submit         = Util.toBool(attributes.submit, false);
        this.reset          = Util.toBool(attributes.reset, false);
        this.value          = attributes.value || 'Button';
        this.label          = attributes.label || attributes.value;

        this.on('click', attributes.click);
    }

    setLabel(label) {
        this.getInput().html(Html.icon(label));
    }

    markup() {
        let classes = this.class;
        classes     = classes ? classes : 'btn-default';
        classes     = classes.indexOf('btn-') < 0 ? 'btn-' + classes : classes;
        let label   = Html.icon(this.label);
        let type    = this.reset ? 'reset' : (this.submit ? 'submit' : 'button');

        return `<button id="${this.id}" name="${this.name}" type="${type}" value="${this.value}" 
                    class="btn ${classes}" ${this.disabled ? 'disabled' : ''}>${label}</button>`;
    }
}