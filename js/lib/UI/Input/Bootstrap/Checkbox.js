/**
 * Created by hoangq on 3/21/17.
 */

import Input from './Input';
import Util from 'lib/UI/Helper/Util';

export default class Checkbox extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Checkbox';
        this.bold           = Util.toBool(attributes.bold, true);
        this.containerClass = 'b3 uc uc-checkbox ' + (this.bold ? 'uc-bold' : 'uc-normal');
        this.value          = attributes.value || 0;
        this.checked        = Util.toBool(attributes.checked, false) || this.value == 1;
    }

    init() {
        let input = this.getInput().change(event => {
            this.checked = input.prop('checked');
            let value = this.checked ? 1 : 0;
            this.getHiddenInput().val(value);
            this.trigger('change', {id: this.id, name: this.name, value: value, event: event});
        });
    }

    createLabel(content) {
        return `<label>${content} <span>${this.label}</span></label>`;
    }

    createInput() {
        return `
        <input type="hidden" id="${this.id}-hidden" name="${this.name}" value="${this.value || 0}" />
        <input type="checkbox" id="${this.id}" class="${this.class}" ${this.disabled ? 'disabled' : ''} ${this.checked ? 'checked' : ''} />`
    }

    getHiddenInput() {
        return $(`#${this.id}-hidden`);
    }

    setValue(value) {
        this.value   = value;
        this.checked = value ? true : false;
        let input = this.getInput();
        if (input.length) {
            input.prop('checked', this.checked);
        }
    }

    getValue() {
        return this.isRendered ? this.getHiddenInput().val() : this.value;
    }
}