/**
 * Created by hoangq on 3/8/17.
 */


import Input from './Input';
import Util from 'lib/UI/Helper/Util';

export default class Hidden extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Hidden';
        this.containerClass = 'b3 uc uc-hidden';
        this.visible        = Util.toBool(attributes.visible, false);
    }

    markup() {
        return `<input type="hidden" id="${this.id}" name="${this.name}" value="${this.value || ''}" />`;
    }
}