
import Text from './Text';
import Util from 'lib/UI/Helper/Util';

export default class Password extends Text {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Password';
        this.containerClass = 'b3 uc uc-password';
        this.autocomplete   = Util.toBool(attributes.autocomplete, false);
    }

    init() {
        super.init();

        //fix browser autocomplete issue
        if (!this.autocomplete) {
            setTimeout(() => {
                this.setValue(this.value);
            }, 200);
        }
    }

    createInput() {
        return `
        <div class="ui-text-wrapper">            
            <input type="password" id="${this.id}" name="${this.name}" value="${this.value}"
                class="form-control ${this.class}" placeholder="${this.holder}"
                autocomplete="${this.autocomplete ? 'on' : 'off'}"
                ${this.disabled ? 'disabled' : ''} ${this.readonly ? 'readonly' : ''} />
            <i id="${this.id}-clear" class="fa fa-close ui-clear-button"></i>        
        </div>`;
    }
}