/**
 * Created by hoangq on 7/11/17.
 */

import Text from './Text';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';

export default class Currency extends Text {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Currency';
        this.containerClass = 'b3 uc uc-currency';
        this.autocomplete   = Util.toBool(attributes.autocomplete, false);
        this.readonly       = Util.toBool(attributes.readonly, false);
        this.decimal        = attributes.decimal || 2; //decimal digits
        this.defaultValue   = parseFloat(attributes.defaultValue || 0).toFixed(this.decimal);
        this.value          = Util.isNumeric(this.value) ? this.value : 0;
        this.value          = parseFloat(this.value).toFixed(this.decimal);
        this.prepend        = attributes.prepend || '[i:usd]';
        this.inputValue     = this.value;
    }
}