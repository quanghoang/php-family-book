/**
 * Created by hoangq on 2/9/17.
 */

import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import Validator from 'lib/UI/Helper/Validator';
import Html from 'lib/UI/Helper/Html';

export default class Input extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.type     = 'Input';
        this.isInput  = true;
        this.layout   = attributes.layout || 'vertical';
        this.value    = attributes.value === undefined ? '' : attributes.value;
        this.name     = attributes.name || this.id;
        this.label    = Html.icon(attributes.label || '') ;
        this.group    = attributes.group || '';
        this.holder   = attributes.holder || attributes.placeholder || '';
        this.help     = attributes.help || ''; //help/hint message
        this.rule     = Util.toArray(attributes.rule) || [];
        this.prepend  = attributes.prepend || '';
        this.append   = attributes.append || '';
        this.disabled = Util.toBool(attributes.disabled, false);
        this.hidden   = Util.toBool(attributes.hidden, false);
        this.focus    = Util.toBool(attributes.focus, false);

        this.error    = '';
        this.info     = attributes.info || null;
        this.method   = attributes.method || '';
        this.depend   = attributes.depend || null;
        this.loaded   = Util.toBool(attributes.loaded, false);

    }

    /**
     * Init input component
     */
    init() {
        let input = this.getInput();

        input.click(event => {
            return this.trigger('click', {
                id:    this.id,
                name:  this.name,
                event: event
            });
        });

        input.on('change keyup', event => {
            let value = this.getValue();
            if (value !== this.value) {
                this.value = value;

                this.trigger('change', {
                    id:    this.id,
                    name:  this.name,
                    value: value,
                    event: event
                });
            }
        });

        if (this.focus) {
            this.setFocus();
        }
    }

    /**
     * Set value
     * @param value
     */
    setValue(value) {
        this.value = value;

        let input = this.getInput();
        if (input.length) {
            input.val(value);
        }
    }

    /**
     * Get value
     * @returns {*}
     */
    getValue() {
        let input = this.getInput();
        if (input.length) {
            return input.val();
        }
        return this.value;
    }

    /**
     * Get input element
     * @returns {jQuery}
     */
    getInput() {
        let input = $('#' + this.id);
        if (!input.length) {
            input = this.getContainer().find('input, select, button');
        }
        return input;
    }

    /**
     * Get Form component
     * @returns {*}
     */
    getForm() {
        return this.closest('@Form');
    }

    /**
     * Validate input value
     * @returns {boolean}
     */
    validate() {
        let data = {};
        for (let rule of Validator.parseRule(this.rule)) {
            if (rule.name == 'required_depend') {
                let form = this.getForm();
                let input = form.find(rule.param);
                input = input && input[0] instanceof Input ? input[0] : null;
                if (input) {
                    data[input.name] = {value: input.getValue(), label: input.label};
                }
            }
        }

        let value  = this.getValue();
        let result = Validator.validate(value, this.rule, this.label, data);
        let valid  = result.valid;
        this.error = result.error;

        //validate mask if defined
        if (valid && this.mask) {
            let val = Util.toMask(value, this.mask);
            valid   = Util.isValidMask(val, this.mask);
            if (!valid) {
                this.error = `The ${this.label} field is invalid.`;
            }
        }

        this.highlightError(valid);
        return valid;
    }

    /**
     * Highlight error
     * @param success
     */
    highlightError(success=true) {
        this.getContainer().removeClass('has-error has-success').addClass(success ? 'has-success' : 'has-error');
    }

    markup() {
        let content = this.createInput();
        return this.renderLabel ? this.createLabel(content) : content;
    }

    /**
     * Create input html content (TBD in inherited classes)
     */
    createInput() {
    }

    /**
     * Create label
     * @param content
     * @returns {string}
     */
    createLabel(content) {
        return super.createLabel(content);
    }

    /**
     * Create prepend content
     * @returns {string}
     */
    createPrepend() {
    }

    /**
     * Create append content
     * @returns {string}
     */
    createAppend() {
    }

    /**
     * Enable/Disable input
     * @param enabled bool
     */
    enableInput(enabled=true) {
        //this.disabled = !enabled;
        this.getInput().prop('disabled', !enabled);
    }

    /**
     * Set focus
     * @param focus
     */
    setFocus(focus=true) {
        if (focus) {
            this.getInput().focus();
        }
        else {
            this.getInput().blur();
        }
    }

    /**
     * Set placeholder value
     * @param placeholder string
     */
    setPlaceHolder(placeholder) {
        this.getInput().attr('placeholder', placeholder);
    }

    /**
     * Init dependents
     * @param form {Form} Form object
     */
    initDependents(form) {
        if (!this.depend || !form) {
            return;
        }

        let depends = Util.isString(this.depend) ? this.depend.split(',') : this.depend;
        depends     = Util.trim(Util.toArray(depends));

        for (let depend of depends) {
            let components = form.find(depend);
            if (!components) {
                continue;
            }

            for (let component of components) {
                component.on('change', () => {
                    this.loadData();
                });
            }
        }
    }

    /**
     * Init loaded data, will call after loading data is done (TBD in inherited classes)
     * @param response object Response Data
     */
    initData(response) {
    }

    /**
     * Load data
     */
    loadData() {
        let form    = this.getForm();
        let loader  = form || this.getRoot();
        let data    = form.getValue() || {};
        data.params = this.loadParams;

        if (form) {
            form.showProgressbar();
        }
        loader.call(this.method, data, response => {
            if (form) {
                form.showProgressbar(false);
            }
            this.initData(response);
            this.trigger('change', {id: this.id, name: this.name, value: this.getValue()});
        });
    }


    render() {
        super.render();

        this.on('change', e => {
            this.triggerRoot('input.change', e);
        });
    }
}