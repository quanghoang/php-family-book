/**
 * Created by hoangq on 3/2/17.
 */

import Text from './Text';
import Util from 'lib/UI/Helper/Util';
import App from 'lib/UI/App/Framework7/App';

export default class Date extends Text {


    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Date';
        this.containerClass = 'f7 uc uc-date';
        this.autocomplete   = false;
        this.readonly       = true;
        this.dateFormat     = attributes.dateFormat || 'yyyy-mm-dd';

    }

    init() {
        super.init();
        this.initCalendar();
    }

    initCalendar() {
        let settings = {
            input: '#' + this.id,
            closeOnSelect: true,
            closeByOutsideClick: true,
        };
        this.calendar  = App.getF7Instance().calendar(settings);
    }
}