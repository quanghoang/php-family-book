/**
 * Created by hoangq on 2/21/17.
 */

import Input from './Input';
import Util from 'lib/UI/Helper/Util';

export default class Text extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Text';
        this.containerClass = 'f7 uc uc-text';
        this.autocomplete   = Util.toBool(attributes.autocomplete, false);
        this.readonly       = Util.toBool(attributes.readonly, false);
    }

    createInput() {
        return `
        <input type="text" id="${this.id}" name="${this.name}" value="${this.value || ''}"
            class="${this.class}" placeholder="${this.holder}"
            autocomplete="${this.autocomplete ? 'on' : 'off'}"
            ${this.disabled ? 'disabled' : ''} ${this.readonly ? 'readonly' : ''} />`
    }
}