/**
 * Created by hoangq on 3/8/17.
 */

import Input from './Input';
import Util from 'lib/UI/Helper/Util';

export default class Textarea extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Textarea';
        this.containerClass = 'f7 uc uc-textarea';
        this.autocomplete   = Util.toBool(attributes.autocomplete, false);
        this.readonly       = Util.toBool(attributes.readonly, false);
        this.rows           = attributes.rows || 4;
        this.resizable      = Util.toBool(attributes.resizable, false);
        this.alignTop       = Util.toBool(attributes.alignTop, true);
    }

    init() {
        super.init();

        if (this.resizable) {
            this.f7app.resizeTextarea('#' + this.id);
        }
    }

    createInput() {
        return `
        <textarea id="${this.id}" name="${this.name}" 
            class="${this.class} ${this.resizable ? 'resizable' : ''}" 
            placeholder="${this.holder}" 
            rows="${this.rows}" 
            ${this.disabled ? 'disabled' : ''} 
            ${this.readonly ? 'readonly' : ''}>${this.value || ''}</textarea>`;
    }
}