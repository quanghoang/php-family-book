/**
 * Created by hoangq on 3/1/17.
 */

import BaseInput from 'lib/UI/Input/Base/Input';
import Html from 'lib/UI/Helper/Html';
import App from 'lib/UI/App/Framework7/App';
import Util from 'lib/UI/Helper/Util';

export default class Input extends BaseInput {

    constructor(attributes={}) {
        super(attributes);

        this.f7app    = App.getF7Instance();
        this.alignTop = Util.toBool(attributes.alignTop, false);
    }

    createLabel(content) {
        return `
        <div class="item-content">
            ${this.prepend ? `<div class="item-media">${Html.icon(this.prepend)}</div>` : ''}             
            <div class="item-inner">
                <div class="item-title label">${this.label}</div>
                <div class="item-input">${content}</div>                
            </div>
        </div>`;
    }
}