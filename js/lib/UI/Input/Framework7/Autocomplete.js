/**
 * Created by hoangq on 3/1/17.
 */

import Input from './Input';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';


export default class Autocomplete extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Autocomplete';
        this.containerClass = 'f7 uc uc-autocomplete';
        this.multiple       = Util.toBool(attributes.multiple, false);
        this.standalone     = this.multiple ? true : Util.toBool(attributes.standalone, false);
        this.class          = this.class + (this.standalone ? ' hidden' : ''); //not show input on standalone mode
        this.readonly       = Util.toBool(attributes.readonly, false);
        this.expandInput    = Util.toBool(attributes.expandInput, false);
        this.backOnSelect   = Util.toBool(attributes.backOnSelect, true);
        this.limit          = attributes.limit || 10;   //limit items show on dropdown
        this.top            = attributes.top   || 10;   //top items when query is empty
        this.options        = attributes.options || [];
        this.value          = this.value || null;
    }

    init() {
        super.init();

        if (this.method && !this.loaded) {
            this.loadData();
            return;
        }

        this.initAutocomplete();
    }

    initData(response) {
        this.options = response.data;
        this.initAutocomplete();
    }

    initAutocomplete() {
        let settings = {
            input:        '#' + this.id,
            openIn:       'dropdown',
            backText:     'Save',
            pageTitle:    'Select ' + this.label,
            limit:        this.limit,
            multiple:     this.multiple,
            expandInput:  this.expandInput,
            backOnSelect: this.backOnSelect,
            value:        Util.toArray(this.value),
        };

        // Open on new page standalone (select on new page instead of dropdown)
        if (this.standalone) {
            settings['openIn']   = 'page';
            settings['opener']   = Dom7('#' + this.id + '-opener');
            settings['onChange'] = (autocomplete, values) => {
                let results = [];
                for (let value of values) {
                    value = Util.isObject(value) ? value.value : value;
                    results.push(value);
                }
                this.getContainer().find('.item-after').text(results.join(', '));
                this.getInput().val(JSON.stringify(results));
                this.value = results;
                this.trigger('change', {id: this.id, name: this.name});
            };
        }
        else {
            settings['onChange'] = (autocomplete, values) => {
                this.trigger('change', {id: this.id, name: this.name});
            };
        }

        // Handle source callback
        settings['source'] = (autocomplete, query, render) => {
            var results = [];
            if (!query.trim().length) {
                for (let option of this.options) {
                    let value = Util.isObject(option) ? option.value : option;
                    results.push(value);

                    if (results.length > this.top) {
                        break;
                    }
                }
                render(results);
                return;
            }
            let regexp = new RegExp(Util.escapeRegExp(query), 'i');
            for (let option of this.options) {
                let value = Util.isObject(option) ? option.value : option;
                if (regexp.test(value)) {
                    results.push(value);
                }
            }
            render(results);
        };
        this.autocomplete = this.f7app.autocomplete(settings);
    }

    getValue() {
        if (this.multiple) {
            return this.value;
        }
        return super.getValue();
    }

    createLabel(content) {
        if (this.standalone) {
            return `
            <a href="#" id="${this.id + '-opener'}" class="item-link item-content autocomplete-opener">
                ${content}                                
                ${this.prepend ? `<div class="item-media">${Html.icon(this.prepend)}</div>` : ''}
                <div class="item-inner">
                    <div class="item-title">${this.label}</div>
                    <div class="item-after">${this.value ? Util.toArray(this.value).join(', ') : this.holder}</div>
                </div>                                                                
            </a>`;
        }
        return super.createLabel(content);
    }

    createInput() {
        return `
        <input type="text" id="${this.id}" name="${this.name}" value="${this.value || ''}"
            class="${this.class}" placeholder="${this.holder}" autocomplete="off"
            ${this.disabled ? 'disabled' : ''} ${this.readonly ? 'readonly' : ''} />`
    }
}