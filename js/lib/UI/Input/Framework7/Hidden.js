/**
 * Created by hoangq on 3/8/17.
 */


import Input from './Input';

export default class Hidden extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Hidden';
        this.containerClass = 'f7 uc uc-hidden';
    }

    markup() {
        return `<input type="hidden" id="${this.id}" name="${this.name}" value="${this.value || ''}" />`;
    }
}