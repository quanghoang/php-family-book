/**
 * Created by hoangq on 3/1/17.
 */

import Input from './Input';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';
import Arrays from 'lib/UI/Helper/Arrays';

export default class F7Select extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Select';
        this.containerClass = 'f7 uc uc-select';
        this.holder         = attributes.holder      || '';
        this.options        = attributes.options     || [];
        this.sortGroup      = attributes.sortGroup   || ''; //Can be '', 'asc', 'desc'
        this.sortOption     = attributes.sortOption  || ''; //Can be '', 'asc', 'desc'
        this.multiple       = Util.toBool(attributes.multiple, false);
        this.clearButton    = Util.toBool(attributes.clearButton, true);
        this.selectAll      = Util.toBool(attributes.selectAll, false);
        this.searchBar      = Util.toBool(attributes.searchBar, true);
        this.popup          = Util.toBool(attributes.popup, false);
        this.virtual        = Util.toBool(attributes.virtual, false);
    }

    init() {
        super.init();

        // Init select box
        let input = this.getInput();

        //init data
        if (this.method && !this.loaded) {
            this.loadData();
        }

        //show load info if found
        if (this.info) {
            this.setPlaceHolder(this.info);
            this.enableInput(false);
        }

        if (this.value) {
            this.setValue(this.value);
        }

        // Replace empty value with placeholder value
        input.change(() => {
            if (Util.isEmpty(input.val())) {
                this.setPlaceHolder(this.holder, 100);
            }
        });
    }

    setValue(value) {
        super.setValue(value);
        this.setPlaceHolder(Util.toArray(value, ', '), 100);
    }

    /**
     * Set placeholder value
     * @param placeholder string
     * @param delay int Milliseconds waiting for Framework7
     */
    setPlaceHolder(placeholder, delay=0) {
        let values = Util.toArray(this.getValue());
        if (!Util.isEmpty(values)) {
            placeholder = values.join(', ');
        }

        setTimeout(() => {
            this.getContainer().find('.item-after').html(placeholder);
        }, delay);
    }

    loadData() {
        this.enableInput(false);
        this.getInput().html('');
        this.setPlaceHolder('Loading...');
        super.loadData();
    }

    initData(response) {
        let input = this.getInput();

        if (response.info) {
            this.setPlaceHolder(response.message);
            return;
        }

        this.setOptions(response.data);
        this.setPlaceHolder(this.holder);
        this.enableInput(true);
    }

    setOptions(options) {
        this.options = options;
        this.getInput().html(this.createOptions());
    }

    createLabel(content) {
        let settings = `
            data-searchbar="${this.searchBar ? 'true' : 'false'}" 
            data-searchbar-placeholder="${this.holder}"
            data-page-title="Select ${this.label}"
            data-virtual-list="${this.virtual ? 'true' : 'false'}"
            ${this.popup ? 'data-open-in="popup"' : ''}
            data-back-on-select="true"
            data-back-text="Back"`;
        return `
        <a href="#" class="item-link smart-select" ${settings.replace("\n", ' ')}>            
            ${content}
            <div class="item-content">
                ${this.prepend ? `<div class="item-media">${Html.icon(this.prepend)}</div>` : ''}
                <div class="item-inner">                    
                    <div class="item-title">${this.label}</div>                    
                    <div class="item-after">${this.holder}</div>                    
                </div>
            </div>
        </a>`;
    }

    markup() {
        let options = this.createOptions();
        let select  = this.createInput(options);
        return this.createLabel(select);
    }

    /**
     * Create select input element
     * @param options string Options HTML content
     */
    createInput(options) {
        return `         
        <select id="${this.id}" name="${this.name}" ${this.multiple ? ' multiple' : ''} class="${this.class}">                            
            ${options}
        </select>`;
    }

    /**
     * Find groups
     * @returns {[string]}
     */
    findGroups() {
        let groups  = [''];
        let found   = {};
        let index   = -1;
        let options = this.options;

        for (let option of options) {
            index++;
            if (Util.isString(option)) {
                option = {value: option, label: option};
                options[index] = option;
            }

            let group = option.group;
            if (group && !found[group]) {
                found[group] = true;
                groups.push(group);
            }
        }

        //sort group
        switch (this.sortGroup.toUpperCase()) {
            case 'ASC':
                groups = groups.sort();
                break;

            case 'DESC':
                groups = groups.sort().reverse();
                break;
        }
        return groups;
    }

    /**
     * Create Select Options content
     */
    createOptions() {
        let value   = this.value;
        value       = Util.isArray(value) ? value : (value ? [value] : null);
        let options = this.options || [];

        if (Util.isEmpty(options)) {
            return '';
        }

        if (!this.multiple) {
            options.splice(0, 0, {value: '', label: 'None'});
        }

        //select options
        if (value) {
            let index = 0;
            for (let option of options) {
                if (Util.isString(option)) {
                    options[index] = {label: option, value: option};
                }
                options[index].selected = value.indexOf(option.value) >= 0;
                index++;
            }
        }

        //create groups
        let content1 = '', content2 = '';
        this.findGroups().forEach((group) => {
            if (!group) {
                content1 += this.createOptionGroup(options, group);
            }
            else {
                content2 += this.createOptionGroup(options, group);
            }
        });
        return content1 + content2;
    }

    /**
     * Create option group
     * @param options array
     * @param group string Group name (use '' for default group)
     * @returns {string}
     */
    createOptionGroup(options, group) {
        let content = '';
        let group_options = [];

        for (let option of options) {
            if (group == option.group || (group == '' && !option.group)) {
                group_options.push(option);
            }
        }

        //sort options
        group_options = Arrays.sort(group_options, {label: this.sortOption.toUpperCase()});

        for (let option of group_options) {
            option       = Util.isString(option) ? {value: option, label: option} : option;
            let display  = `data-display-as="${option.display || option.value}"`;
            let selected = option.selected ? 'selected' : '';
            content     += `<option value="${option.value}" ${selected} ${display}>${option.label}</option>`;
        }

        if (group) {
            content = `<optgroup label="${group}">${content}</optgroup>`;
        }
        return content;
    }
}