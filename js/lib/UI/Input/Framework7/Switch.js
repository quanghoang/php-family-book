/**
 * Created by hoangq on 3/9/17.
 */

import Checkbox from './Checkbox';
import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';

export default class Switch extends Checkbox {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Switch';
        this.containerClass = 'f7 uc uc-switch';
        this.checked        = Util.toBool(attributes.checked, false) || this.value == 1;
    }

    createLabel(content) {
        return `
        <div class="item-content">
            ${this.prepend ? `<div class="item-media">${Html.icon(this.prepend)}</div>` : ''}
            <div class="item-inner">
                <div class="item-title label">${this.label}</div>
                <div class="item-input">
                    <label class="label-switch">
                        ${content}
                        <div class="checkbox"></div>
                    </label>                                        
                </div>
            </div>
        </div>`;
    }
}