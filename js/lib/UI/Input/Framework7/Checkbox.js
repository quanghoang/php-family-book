/**
 * Created by hoangq on 3/2/17.
 */

import Input from './Input';
import Util from 'lib/UI/Helper/Util';

export default class Checkbox extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Checkbox';
        this.containerClass = 'f7 uc uc-checkbox';
        this.checked        = Util.toBool(attributes.checked, false) || this.value == 1;
    }

    init() {
        let input = this.getInput().change(event => {
            this.checked = input.prop('checked');
            this.trigger('change', {id: this.id, name: this.name, value: this.getValue(), event: event});
        });
    }

    createLabel(content) {
        return `
        <label class="label-checkbox item-content">
            ${content}
            <div class="item-media">
                <i class="icon icon-form-checkbox"></i>
            </div>
            <div class="item-inner">
                <div class="item-title">${this.label}</div>
            </div>
        </label>`;
    }

    createInput() {
        return `
        <input type="checkbox" id="${this.id}" name="${this.name}" value="1"
            class="${this.class}" ${this.disabled ? 'disabled' : ''} ${this.checked ? 'checked' : ''} />`
    }

    getValue() {
        return this.getInput().prop('checked') ? 1 : 0;
    }
}