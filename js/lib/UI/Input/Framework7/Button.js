/**
 * Created by hoangq on 3/10/17.
 */

import Input from './Input';
import Html from 'lib/UI/Helper/Html';
import Util from 'lib/UI/Helper/Util';

export default class Button extends Input {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Button';
        this.containerClass = 'f3 uc uc-button';
        this.submit         = Util.toBool(attributes.submit, false);
        this.reset          = Util.toBool(attributes.reset, false);
        this.value          = attributes.value || 'Button';
        this.label          = attributes.label || attributes.value;

        this.on('click', attributes.click);
    }

    markup() {
        let label   = Html.icon(this.label);
        let type    = this.reset ? 'reset' : (this.submit ? 'submit' : 'button');

        return `<button id="${this.id}" type="${type}" value="${this.value}" style="width: 100%" 
                    class="button ${this.class}" ${this.disabled ? 'disabled' : ''}>${label}</button>`;
    }
}