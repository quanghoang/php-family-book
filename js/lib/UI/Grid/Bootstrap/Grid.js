/**
 * Created by hoangq on 6/7/17.
 */

// import $ from 'jquery2-0-3';
//import 'jqueryui1-12-1';
// import 'jqueryui-1-8-23';
// import 'jqueryui-1-8-23/themes/cupertino/jquery-ui.css!';
// import 'slickgrid-2-1-0/lib/jquery.event.drag-2.2';
// import 'slickgrid-2-1-0/lib/jquery.event.drop-2.2';
// import 'slickgrid-2-1-0/slick.grid.css!';
// import 'lib/UI/Grid/css/Slickgrid.css!';
// import 'slickgrid-2-1-0/slick.core';
// import 'slickgrid-2-1-0/slick.dataview';
// import 'slickgrid-2-1-0';
// import 'xslickgrid-3-0-3/lib/jquery.event.drag-2.2';
// import 'xslickgrid-3-0-3/lib/jquery.event.drop-2.2';
// import 'xslickgrid-3-0-3/slick.grid.css!';
// import 'lib/UI/Grid/css/Slickgrid.css!';
// import 'xslickgrid-3-0-3/slick.core';
// import 'xslickgrid-3-0-3/slick.dataview';
// import 'xslickgrid-3-0-3/slick.grid';

import 'multiselect-1-14';
import 'multiselect-1-14/src/jquery.multiselect.filter';
import 'multiselect-1-14/jquery.multiselect.css!';
import 'multiselect-1-14/jquery.multiselect.filter.css!';
import 'slickgrid-2-1-0/slick.grid.css!';
import 'lib/UI/Grid/css/slick-default-theme.css!';
import 'lib/UI/Grid/css/Slickgrid.css!';
import {Grid, Data, Formatters, Plugins, Slick} from 'slickgrid-es6';
window.Slick = Slick;
import 'slickgrid-column-group-plugin/dist/slick.columngroup.modified';
import 'slickgrid-column-group-plugin/dist/slick.columngroup.css!';

import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Component from 'lib/UI/Component';
import Notify from 'lib/UI/Helper/Notify';

export default class SlickGrid extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.type           = 'Grid';
        this.containerClass = 'ui-grid';
        this.emptyValue     = '[Empty]'; //used for rendering dropdown label with empty value
        this.defaults       = this.getDefaults();
        this.grid           = null;
        this.dataview       = null;
        this.data           = attributes.data || [];
        this.method         = attributes.method || ''; //loading data method
        this.columns        = attributes.columns || [];
        this.formatters     = attributes.formatters || {}; //custom formatters
        this.filters        = attributes.filters || {}; //custom filters
        this.options        = $.extend(this.defaults.options, attributes.options || {});
        this.height         = attributes.height || this.defaults.height;
        this.headerHeight   = attributes.headerHeight || null;
        this.striped        = Util.toBool(attributes.striped, true); //strip row bg
        this.detailsImage   = attributes.detailsImage || this.defaults.detailsImage;
        this.exportCSV      = Util.toBool(attributes.exportCSV, false); //show/hide Export CSV button
        this.exportRaw      = Util.toBool(attributes.exportRaw, false); //export raw data (not using formatters)
        this.clearFilter    = Util.toBool(attributes.clearFilter, false); //show/hide Clear Filters button on toolbar
        this.reloadButton   = Util.toBool(attributes.reloadButton, false);  //show/hide Reload button
        this.exportFilename = attributes.exportFilename || `export${this.name ? '_' + this.name : ''}`; //filename without extension
        this.toolbarButtons = [];
        this.colIndexes     = {};
        this.colFilters     = {};
        this.totalRows      = 0;
        this.filteredRows   = 0;


        if (this.clearFilter) {
            this.addToolbarButtons({name: 'clear_filter', label: '[im:eraser] Clear Filters', class: 'warning'});
        }
        if (this.reloadButton) {
            this.addToolbarButtons({name: 'reload', label: '[im:refresh] Refresh', class: 'primary'});
        }
        if (this.exportCSV) {
            this.addToolbarButtons({name: 'export_csv', label: '[im:download] Export CSV', class: 'primary'});
        }
        this.addToolbarButtons(attributes.toolbarButtons || attributes.toolbar);
    }

    /**
     * Get default values
     * @returns object
     */
    getDefaults() {
        return {
            reservedColumnNames: ['id', 'details'],

            options: {
                enableCellNavigation:   true,
                enableColumnReorder:    true,
                explicitInitialization: true,
                showHeaderRow:          false,
                headerRowHeight:        35,
            },

            height: 500,
            columnWidth: 100,

            detailsImage: 'includes/slickGrid/images/details.png',

            detailsColumn: {
                name:      'details',
                formatter: 'details',
                width:     20,
                filter:    'none',
                sortable:  false,
                cssClass:  'ui-grid-details',
                toolTip:   'Click to open'
            },

            formatters: {
                details:(rowIndex, colIndex, value, column, data) => {
                    return `<img src="${this.detailsImage}" title="${column.toolTip}" />`;
                },
                currency: (rowIndex, colIndex, value) => {
                    value = value * 1.0;
                    return '<span style="color:#aaa">$</span>' + value.toFixed(2);
                },
                percent: (rowIndex, colIndex, value) => {
                    if (value == '') {
                        return '-';
                    }
                    value = value * 1.0;
                    return value.toFixed(2) + '<span style="color:#aaa">%</span>';
                },
                bracket: (rowIndex, colIndex, value) => {
                    return value ? '[' + value + ']' : '';
                },
                dash: (rowIndex, colIndex, value) => {
                    return value ? value : '-';
                },
                zerodash: (rowIndex, colIndex, value) => {
                    return value && value != '0' ? value : '-';
                },
                yesno: (rowIndex, colIndex, value) => {
                    return value * 1 ? 'Yes' : 'No';
                },
                striptags: (rowIndex, colIndex, value) => {
                    return Util.stripTags(value);
                },
                stripspaces:(rowIndex, colIndex, value) => {
                    return Util.stripSpaces(value);
                },
            }
        };
    }

    initGrid() {
        this.columns   = this.createColumns();
        this.totalRows = this.data.length;
        //this.dataview  = new Slick.Data.DataView();
        this.dataview  = new Data.DataView();

        let options = this.options;
        if (!options.showHeaderRow) {
            for (let column of this.columns) {
                if (column.filter) {
                    options.showHeaderRow = true;
                }
            }
        }

        let content = this.getContainer().find('.ui-grid-content').css('height', this.height);
        //this.grid   = new Slick.Grid(content, this.dataview, this.columns, options);
        this.grid   = new Grid(content, this.dataview, this.columns, options);

        //handle sort event
        this.grid.onSort.subscribe((e, args) => {
            this.filteredRows = 0;
            this.sortColumn(args.sortCol.id, args.sortAsc);
        });

        // handle header cell rendered event
        this.grid.onHeaderRowCellRendered.subscribe((e, args) => {
            this.markupFilter($(args.node), args.column.id);
        });

        // handle filter input event
        $(this.grid.getHeaderRow()).delegate(":input", "change keyup", (e) => {
            this.filteredRows = 0;
            this.handleFilterEvent($(e.target));
            this.trigger('filtered', {filters: this.colFilters, numRows: this.filteredRows});
        });

        // click event
        this.grid.onClick.subscribe((e, args) => {
            this.trigger('row.click', {row: args.row, cell: args.cell, column: this.columns[args.cell], data: this.getRow(args.row)});
        });

        // double click event
        this.grid.onDblClick.subscribe((e, args) => {
            this.trigger('row.dbclick', {row: args.row, cell: args.cell, column: this.columns[args.cell], data: this.getRow(args.row)});
        });

        this.grid.onKeyDown.subscribe((e, args) => {
            let params = {row: args.row, cell: args.cell, column: this.columns[args.cell], data: this.getRow(args.row)};
            this.trigger('keydown', params);
            this.trigger('keydown.' + e.keyCode, params);
        });

        this.initDataView();
        this.grid.init();
        this.grid.render();
        this.initToolbar();
        this.initPlugin();

        if (this.headerHeight) {
            let container = this.getContainer();
            container.find('.slick-column-name').css('white-space', 'normal');
            container.find('.slick-header-column').css('background', 'none');
            container.find('.slick-header-columns').css('background', 'none');
            container.find('.slick-header-column.ui-state-default').css('height', 32);
        }
    }


    initPlugin() {
        let groups = {};
        for (let column of this.columns) {
            if (column.groupName) {
                groups[column.groupName] = 1;
            }
        }
        if (!Util.isEmpty(groups)) {
            let plugin = new ColumnGroup();
            this.grid.registerPlugin(plugin);
            plugin.enableColumnGrouping();
        }
    }

    initDataView() {
        // handle row count changed events
        this.dataview.onRowCountChanged.subscribe((e, args) => {
            this.grid.updateRowCount();
            this.grid.render();

            //Update display percentage
            let percent = this.totalRows > 0 ? (this.filteredRows / this.totalRows) * 100 : 0;
            percent = percent != parseInt(percent) ? percent.toFixed(2) : parseInt(percent);
            percent = percent ? ' (' + percent + '%)' : '';
            this.setDisplayMessage('Displaying ' + this.filteredRows + ' of ' + this.totalRows + percent);
            this.updateFilters();
        });

        // handle row changed event
        this.dataview.onRowsChanged.subscribe((e, args) => {
            this.grid.invalidateRows(args.rows);
            this.grid.render();
        });

        this.dataview.setFilter((row) => {
            let found = this.filterRow(row);
            this.filteredRows += found ? 1 : 0;
            //console.log(this.filteredRows);
            return found;
        });

        this.setData(this.data);
        setTimeout(() => { //wait for grid finish rendering
            this.trigger('ready');
        }, 100);
    }

    addRowId(data) {
        if (!data) {
            return data;
        }
        let index = 0;
        for (let row of data) {
            if (!row.id) {
                data[index].id = ++index;
            }
        }
        return data;
    }

    setData(data) {
        if (!data) {
            return;
        }
        data              = this.addRowId(data);
        this.data         = data;
        this.filteredRows = 0;
        this.totalRows    = data.length;
        this.dataview.beginUpdate();
        this.dataview.setItems(data);
        this.dataview.endUpdate();
    }

    /**
     * Get current filtered data/rows
     * @returns {Array}
     */
    getData() {
        let length = this.dataview.getLength();
        let rows   = [];

        for (let i=0; i < length; i++) {
            rows.push(this.dataview.getItem(i));
        }
        return rows;
    }

    initToolbar() {
        for (let button of this.toolbarButtons) {
            $('#' + button.id).click(e => {
                let el   = $(e.target);
                let name = el.attr('name');
                this.trigger(`toolbar.button.click`, {name: name, button: el});
                this.trigger(`toolbar.button.${name}.click`, {button: el});
                el.blur();
            });
        }

        if (this.clearFilter) {
            this.on('toolbar.button.clear_filter.click', () => {
                this.clearFilters();
            });
        }

        if (this.reloadButton) {
            this.on('toolbar.button.reload.click', () => {
                if (!this.method) {
                    Notify.error('Reload method not found.');
                    return;
                }
                this.reload();
            });
        }

        if (this.exportCSV) {
            this.on('toolbar.button.export_csv.click', () => {
                this.exportCSVFile(this.exportFilename + '.csv');
            });
        }
    }

    clearFilters() {
        let header = this.getContainer().find('.slick-headerrow');
        header.find('.ui-check-filter').prop('checked', false);
        header.find('.ui-text-filter').val('');
        header.find('.ui-select-filter').val('');
        header.find('.ui-multi-select-filter').multiselect('uncheckAll');

        this.colFilters   = {};
        this.filteredRows = 0;
        this.dataview.refresh();
        this.trigger('filtered', {filters: []});
    }

    exportCSVFile(filename=null) {
        let rows   = this.getData();
        let data   = [];
        let rIndex = 0;
        filename   = filename || this.exportFilename + '.csv';

        for (let row of rows) {
            let cIndex = 0;
            let item   = {};
            for (let column of this.columns) {
                if (column.id == 'details') {
                    continue;
                }

                let value = row[column.id];
                if (!this.exportRaw && column.formatter && Util.isCallable(column.formatter)) {
                    value = (column.formatter)(rIndex, cIndex, value, column, row);
                    value = Util.stripTags(value);
                }
                item[column.name] = value;
                cIndex++;
            }
            data.push(item);
            rIndex++;
        }
        Util.toCSV(data, filename);
    }

    addToolbarButtons(buttons) {
        if (!buttons) {
            return;
        }

        for (let btn of Util.toArray(buttons)) {
            if (!btn) {
                continue;
            }
            let button = {
                id:    this.id + '-toolbar-' + btn.name + '-button',
                name:  btn.name,
                label: btn.label,
                class: btn.class ? 'btn-' + btn.class : 'btn-default',
            };
            this.toolbarButtons.push(button);
        }
    }

    addColumn(columns) {
        for (let column of Util.toArray(columns)) {
            this.columns.push(column);
        }
    }

    createColumns() {
        let columns  = [];
        let index    = 0;

        for (let column of this.columns) {
            if (!column || column.hide) {
                continue;
            }
            if (column.name == 'details') {
                column = $.extend(this.defaults.detailsColumn, column);
            }

            if (column.mask && !this.formatters[column.name]) {
                this.formatters[column.name] = (rIdex, cIndex, value) => {
                    return Util.toMask(value, column.mask);
                }
            }

            let cssClass = column.cssClass || column.cssclass || '';
            cssClass    += column.align ? ' ui-align-' + column.align : '';
            columns.push({
                id:             column.name,
                field:          column.name,
                name:           column.label     || (this.defaults.reservedColumnNames.includes(column.name) ? '' : column.name),
                groupName:      column.groupName || column.group,
                width:          column.width     || this.defaults.columnWidth,
                cssClass:       cssClass,
                headerCssClass: column.headerCssClass || column.headercssclass || '',
                sortable:       Util.toBool(column.sortable, true),
                sortType:       column.sort || (column.filter == 'number' ? 'number' : 'string'),
                resizable:      Util.toBool(column.resizable, true),
                toolTip:        column.toolTip   || column.tooltip || '',
                formatter:      this.formatters[column.formatter] || this.formatters[column.name] || this.defaults.formatters[column.formatter] || null,
                filter:         column.filter || '',
                filterLabels:   column.filterLabels || {},
                filterId:       `${this.id}-${column.name}-filter`,
                align:          column.align  || 'left',
                mask:           column.mask || null
            });
            this.colIndexes[column.name] = index++;
        }
        return columns;
    }

    reload(callback=null) {
        if (!this.method) {
            throw 'Load data method not found.';
        }

        this.call(this.method, this.loadParams, response => {
            let cell = this.getActiveCell();
            let data = response.data;
            if (!data) {
                return;
            }
            this.removeAllRows();
            this.setData(data);

            if (Util.isCallable(callback)) {
                callback(data);
            }

            if (cell && cell.row && cell.row.id) {
                this.setActiveRow(cell.row.id);
            }
            this.trigger('grid.reloaded');
        });
    }

    markup() {
        let buttons = '';
        for (let button of this.toolbarButtons) {
            buttons += Html.createButton(button.id, button.name, button.label, button.class);
        }
        return `
        <div class="ui-grid-toolbar">
            <div class="ui-grid-buttons">${buttons}</div>
            <div class="ui-gird-display">Displaying 0 of 0 (0%)</div>            
        </div>
        <div class="clear"></div>
        <div class="ui-grid-content ${this.striped ? 'ui-striped' : ''}"></div>`;
    }

    setDisplayMessage(message) {
        this.getContainer().find('.ui-gird-display').html(message);
    }

    render() {
        super.render();

        if (this.method && !this.data.length) {
            this.call(this.method, this.loadParams, response => {
                this.data = response.data;
                this.initGrid();
            });
            return;
        }

        this.initGrid();
    }

    getColumn(attrValue, attrKey='id') {
        if (attrKey == 'id') {
            return this.columns[this.colIndexes[attrValue]];
        }

        for (let column of this.columns) {
            if (column[attrKey] == attrValue) {
                return column;
            }
        }
        return null;
    }

    getRow(rowIndex) {
        return rowIndex < this.dataview.getLength() ? this.dataview.getItem(rowIndex) : null;
    }

    getRowById(rowId) {
        let index = this.dataview.getRowById(rowId);
        return this.getRow(index);
    }

    updateRow(data) {
        let row = this.getRowById(data.id);
        if (!row) {
            return;
        }
        for (let name in data) {
            row[name] = data[name];
        }
        this.dataview.updateItem(data.id, row);
        this.updateFilters();
    }

    removeRow(rowId) {
        this.totalRows--;
        this.dataview.deleteItem(rowId);
    }

    removeAllRows() {
        if (!this.data) {
            return;
        }

        this.dataview.beginUpdate();

        let rowIds = [];
        for (let row of this.data) {
            rowIds.push(row.id);
        }
        for (let rowId of rowIds) {
            this.dataview.deleteItem(rowId);
        }

        this.data = [];
        this.totalRows = 0;
        this.filteredRows = 0;
        this.dataview.endUpdate();
    }

    addRow(row) {
        this.totalRows++;
        this.dataview.addItem(row);
    }

    addRowAt(row, atIndex=0) {
        this.totalRows++;
        this.dataview.insertItem(atIndex, row);
    }

    saveRow(row, activeRow=true, atIndex=null) {
        if (row.id == 'new') {
            row.id = this.dataview.getLength() + 1;

            if (atIndex === null) {
                this.addRow(row);
            }
            else {
                this.addRowAt(row, atIndex)
            }
        }
        else {
            this.updateRow(row);
        }
        if (activeRow) {
            this.setActiveRow(row.id);
        }
    }

    setActiveRow(rowId, colIndex=0) {
        let index = this.dataview.getRowById(rowId);
        this.grid.setActiveCell(index, colIndex);
        //this.grid.gotoCell(index, colIndex);
        this.grid.focus();
    }

    getActiveCell() {
        if (!this.grid) {
            return;
        }
        let cell = this.grid.getActiveCell();
        let row  = cell ? cell.row  : 0;
        let col  = cell ? cell.cell : 0;
        return {
            rowIndex: row,
            colIndex: col,
            row:      this.getRow(row)
        };
    }

    focus() {
        let cell = this.getActiveCell();
        if (cell && cell.row) {
            this.setActiveRow(cell.row.id, cell.colIndex);
        }
    }

    sortColumn(columnId, sortType) {
        let column = this.getColumn(columnId);

        this.dataview.sort((row1, row2) => {
            let x, y;
            switch (column.sortType) {
                case 'number':
                    x = row1[columnId] * 1;
                    y = row2[columnId] * 1;
                    return x == y ? 0 : (x > y ? 1 : -1);

                default:
                case 'string':
                    x = row1[columnId] ? String(row1[columnId]).toLowerCase() : '';
                    y = row2[columnId] ? String(row2[columnId]).toLowerCase() : '';
                    return x == y ? 0 : (x > y ? 1 : -1);
            }
        }, sortType);
    }

    getAllRows() {
        return this.dataview.getItems();
    }

    findRow(columnName, columnValue) {
        for (let row of this.getAllRows()) {
            if (row[columnName] == columnValue) {
                return row;
            }
        }
        return null;
    }

    getColumnValues(columnId, distinct=true) {
        let column = this.getColumn(columnId);
        let items  = this.getAllRows();
        if (!column || !items.length) {
            return [];
        }

        if (distinct) {
            let result = {};
            for (let item of items) {
                let value = item[columnId];
                result[value] = true;
            }
            return Object.keys(result);
        }

        let result = [];
        for (let item of items) {
            let value = item[columnId];
            result.push(value);
        }
        return result;
    }

    getColumnFilteredValues(columnId) {
        let result = {};
        let length = this.dataview.getLength();

        for (let i=0; i < length; i++) {
            let item  = this.dataview.getItem(i);
            let value = item[columnId];
            result[value] = true;
        }
        return Object.keys(result);
    }

    getColumnFilterInput(columnId) {
        let column = this.getColumn(columnId);
        return column ? $('#' + column.filterId) : null;
    }

    markupFilter(cell, columnId) {
        let column = this.getColumn(columnId);
        if (!column || column.id !== columnId) {
            return;
        }

        let wrapper      = $('<div class="ui-grid-filter" style="width:100%; text-align: center" />');
        let columnFilter = this.colFilters[column.id];
        let filterValue  = columnFilter && columnFilter['text'] ? columnFilter['text'] : '';

        switch (column.filter) {
            case 'select':
                this.createSelectFilter(wrapper, column, filterValue, this.getColumnValues(columnId));
                break;

            case 'multiselect':
                this.createSelectFilter(wrapper, column, filterValue, this.getColumnValues(columnId), true);
                break;

            case 'yesno':
                this.createSelectFilter(wrapper, column, filterValue, {yes: 'Yes', no: 'No'});
                break;

            default:
            case 'text':
            case 'number':
            case 'none':
                let disabled = column.filter == 'none' ? ' disabled' : '';
                let text = $(`<input id="${column.filterId}" class="ui-filter-input ui-text-filter" ${disabled} />`);
                text.data("columnId", column.id);
                text.val(filterValue);
                wrapper.append(text);
                break;
        }
        cell.html(wrapper)
    }

    createSelectFilter(wrapper, column, filterValue, options, multiSelect=false) {
        let cssClass = multiSelect ? ' ui-multi-select-filter' : '';
        let select   = $(`<select id="${column.filterId}" class="ui-select-filter ui-filter-input ${cssClass}">`);
        this.setFilterOptions(column, select, options, filterValue, multiSelect);
        select.data('columnId', column.id);
        wrapper.append(select);

        if (multiSelect) {
            select.multiselect({noneSelectedText: ''}).multiselectfilter();
            select.multiselect("uncheckAll");
        }
    }

    setFilterOptions(column, select, options, filterValue='', multiSelect=false) {
        let html       = '';
        let emptyValue = this.emptyValue;
        let optionsObj = options;

        if (Array.isArray(options)) {
            optionsObj = {};
            options.sort();
            $(options).each(function () {
                let opValue = !this || this == 'null' ? '' : String(this);
                opValue     = opValue === "undefined" ? emptyValue : opValue;
                let label   = column.filterLabels[opValue] || opValue;
                optionsObj[opValue] = label;
            });
        }

        let hasEmpty = false;
        $.each(optionsObj, (value, name) => {
            let isEmpty  = !value || value == '' || value == null;
            let selected = !isEmpty && value == filterValue ? 'selected' : '';
            hasEmpty    |= isEmpty;

            if (!isEmpty) {
                html += `<option value="${value}" ${selected}>${name}</option>`;
            }
        });

        if (hasEmpty) {
            html = `<option value="${this.emptyValue}">${this.emptyValue}</option>` + html;
        }
        if (!multiSelect) {
            html = '<option value=""></option>' + html;
        }
        select.empty().append(html);
        return select;
    }

    handleFilterEvent(input) {
        let wrapper = input.parent();
        let column  = this.getColumn(input.data('columnId'));
        let value   = $.trim(input.val());

        if (!column) {
            return;
        }

        if (input.hasClass('ui-multi-select-filter')) {
            value = [];
            $(input.multiselect("getChecked")).each(function() {
                value.push($.trim($(this).val()));
            });
        }

        this.colFilters[column.id] = value;
        if (value == '') {
            delete this.colFilters[column.id];
        }

        this.dataview.refresh();
    }

    filterRow(row) {
        let match = true;

        if (Util.isEmpty(this.colFilters)) {
            return true;
        }

        for (let columnId in this.colFilters) {
            let filterValues = this.colFilters[columnId];
            let column       = this.getColumn(columnId);
            let cellValue    = row[column.field];

            if (!filterValues.length) {
                continue;
            }

            let matchValue = false;
            for (let filterValue of Util.toArray(filterValues)) {
                switch (column.filter) {
                    case 'number':
                        matchValue |= this.filterNumber(filterValue, cellValue, false);
                        break;

                    case 'yesno':
                        filterValue = filterValue == 'yes' ? 1 : (filterValue == 'no' ? 0 : this.emptyValue);
                        matchValue |= this.filterText(filterValue, cellValue, true);
                        break;

                    case 'select':
                    case 'multiselect':
                        matchValue |= this.filterText(filterValue, cellValue, true);
                        break;

                    default:
                    case 'text':
                        matchValue |= this.filterText(filterValue, cellValue, false);
                        break;
                }
            }
            match &= matchValue;
        }
        return match;
    }

    filterNumber(filterValue, cellValue) {
        if (!filterValue) {
            return true;
        }

        let compare = 'contains';
        for (let operator of ['>=', '<=', '!', '!=', '=', '>', '<']) {
            if (filterValue.indexOf(operator) === 0) {
                compare     = operator;
                filterValue = filterValue.replace(compare, '');
                break;
            }
        }

        if (filterValue.indexOf('..') > 0) {
            compare     = 'between';
            filterValue = filterValue.split('..');
            if (!filterValue[1]) {
                return true;
            }
        }

        if (!filterValue) {
            return true;
        }

        if (!['contains', 'between'].includes(compare)) {
            cellValue   = cellValue * 1;
            filterValue = Util.stripSpaces(filterValue) * 1;
        }

        switch (compare) {
            case 'contains':
                return String(cellValue).includes(filterValue);

            case 'between':
                return (cellValue >= filterValue[0] * 1) && (cellValue <= filterValue[1] * 1);

            case '=':
                return cellValue == filterValue;

            case '!=':
            case '!':
                return cellValue != filterValue;

            case '>':
                return cellValue > filterValue;

            case '>=':
                return cellValue >= filterValue;

            case '<':
                return cellValue < filterValue;

            case '<=':
                return cellValue <= filterValue;
        }
        return true;
    }

    filterText(filterValue, cellValue, extractCompare) {
        if (filterValue == this.emptyValue) {
            return !cellValue;
        }

        if (extractCompare) {
            return String(cellValue) === String(filterValue);
        }
        return String(cellValue).toLowerCase().includes(filterValue.toLowerCase());
    }

    updateColumnFilter(columnId) {
        let column = this.getColumn(columnId);
        let input  = this.getColumnFilterInput(columnId);
        if (!column || !input || !['select', 'selectmulti'].includes(column.filter)) {
            return;
        }

        let values = this.getColumnFilteredValues(columnId);
        let multi  = column.filter == 'selectmulti';
        this.setFilterOptions(column, input, values, input.val(), multi);
    }

    updateFilters() {
        this.filteredRows = 0;
        for (let column of this.columns) {
            if (['select', 'selectmulti'].includes(column.filter)) {
                this.updateColumnFilter(column.id);
            }
        }
    }

    destroy() {
        this.grid.destroy();
        this.getContainer().html('');
    }
}