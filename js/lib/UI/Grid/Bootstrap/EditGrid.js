/**
 * Created by hoangq on 7/7/17.
 */


import Component from 'lib/UI/Component';
import Util from 'lib/UI/Helper/Util';
import Container from 'lib/UI//Container/Bootstrap/Container';
import Arrays from 'lib/UI/Helper/Arrays';
import Html from 'lib/UI/Helper/Html';
import Notify from 'lib/UI/Helper/Notify';

export default class EditGrid extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.editObjectName    = attributes.editObjectName || '';
        this.loadGridParam     = attributes.loadGridParam || attributes.params || {};
        this.loadGirdMethod    = attributes.loadGridMethod || false;
        this.loadDialogMethod  = attributes.loadDialogMethod || false;
        this.saveMethod        = attributes.saveMethod || false;
        this.deleteMethod      = attributes.deleteMethod || false;
        this.deleteMessage     = attributes.deleteMessage || 'Do you want to delete this record?';
        this.validateMethod    = attributes.validateMethod || false;    //validate on server side
        this.formValidate      = attributes.formValidate || null;       //validate on client side
        this.validateInvisible = Util.toBool(attributes.validateInvisible, false); //validate invisible components or not
        this.dialogSize        = attributes.dialogSize || 'large';      //small, medium, large, larger, full, all
        this.dialogHeight      = attributes.dialogHeight || 600;
        this.gridHeight        = attributes.gridHeight || 600;
        this.headerHeight      = attributes.headerHeight || null;
        this.formatters        = attributes.formatters || {};
        this.exportCSV         = Util.toBool(attributes.exportCSV, false); //show Export CSV button on toolbar
        this.exportRaw         = Util.toBool(attributes.exportRaw, false); //export without formatting
        this.clearFilter       = Util.toBool(attributes.clearFilter, true); //show/hide Clear Filters button on toolbar
        this.canAdd            = Util.toBool(attributes.canAdd, true);
        this.canEdit           = Util.toBool(attributes.canEdit, true);
        this.canDelete         = Util.toBool(attributes.canDelete, true);
        this.canDuplicate      = Util.toBool(attributes.canDuplicate, false);
        this.canReload         = Util.toBool(attributes.canReload, false);
        this.grid              = null;
        this.dialog            = null;
        this.form              = null;
        this.isSubmitted       = false;
        this.icons             = {Add: '[i:file-o]', Duplicate: '[i:clone]', Edit: '[i:pencil]', Load: '[is:spinner]'};
    }

    initDialog() {
        if (this.dialog) {
            this.dialog.destroy();
        }
        this.dialog = Container.factory({
            type:      'dialog',
            container: `${this.id}-dialog`,
            size:      this.dialogSize,
            height:    this.dialogHeight,
            buttons:   [
                {name: 'delete', 'align': 'left', label: '[i:trash]', tooltip: 'Delete'},
                {name: 'duplicate', 'align': 'left', label: '[i:clone]', tooltip: 'Duplicate'},
                {name: 'save', 'default': true},
                {name: 'close', label: 'Cancel', click: 'close'}
            ]
        });
    }

    initGrid() {
        if (!this.loadGirdMethod) {
            Notify.error('Load grid method is not defined.', 'EditGrid');
            return;
        }

        this.call(this.loadGirdMethod, this.loadGridParam, response => {
            if (response.error) {
                return;
            }

            this.grid = Container.factory({...response.data,
                type:        'grid',
                name:        this.name,
                container:   `${this.id}-grid`,
                height:      this.gridHeight,
                formatters:  this.formatters,
                exportCSV:   this.exportCSV,
                clearFilter: this.clearFilter,
                exportRaw:   this.exportRaw,
                headerHeight:   this.headerHeight,
                toolbarButtons: [
                    this.canAdd ? {name: 'add', label: '[im:plus] Add New', 'class': 'success'} : null
                ]
            });

            this.grid.on('row.click', e => {
                if (e.column.id == 'details') {
                    this.openDialog(e.data);
                }
                this.trigger('row.click', e);
            });

            this.grid.on('row.dbclick', e => {
                this.openDialog(e.data);
                this.trigger('row.dbclick', e);
            });

            this.grid.on('keydown.13', e => {
                this.openDialog(e.data);
                this.trigger('keydown', e);
            });

            this.grid.on('toolbar.button.add.click', e => {
               this.openDialog({id: 'new'});
            });

            this.grid.on('filtered', filters => {
                this.trigger('filtered', filters);
            });

            this.grid.on('ready', () => {
                this.trigger('ready');
            });

            this.grid.render();
            this.grid.focus();
        });
    }

    findRow(columnName, columnValue) {
        return this.grid.findRow(columnName, columnValue);
    }

    updateRow(row) {
        this.grid.updateRow(row);
    }

    getRowAction(row) {
        return row.id == 'new' ? (row.duplicated ? 'Duplicate' : 'Add') : 'Edit';
    }

    getObjectName(row) {
        row = row.id == 'new' ? null : row;
        return Util.isCallable(this.editObjectName) ? this.editObjectName(row) : this.editObjectName;
    }

    setActiveRow(rowId) {
        this.grid.setActiveRow(rowId);
    }

    openDialog(row) {
        if (!this.loadDialogMethod) {
            Notify.error('Load dialog method is not defined.', 'EditGrid');
            return;
        }

        row.params  = this.loadGridParam;
        let action  = this.getRowAction(row);
        let objName = this.getObjectName(row);
        let title   = objName.indexOf('!') === 0 ? objName.substr(1) : (action + ' ' + objName);

        this.dialog.setTitleIcon(this.icons[action]);
        this.dialog.setTitle(Html.icon(title));

        this.dialog.setBody('<img src="media/image/empty-form.png" />').one('before.open', () => {
            this.loadForm(row);
        });

        this.dialog.one('button.save.click', () => {
           this.validateForm(row);
        });

        this.dialog.one('button.delete.click', () => {
            this.deleteRow(row);
        });

        this.dialog.one('button.duplicate.click', () => {
            this.duplicateRow(row);
        });

        this.dialog.one('close', () => {
            this.grid.focus();
            this.dialog.clearEvents();
            if (this.form) {
                this.form.closeDropdown();
                this.form.destroy();
            }
        });

        this.dialog.open();
    }


    loadForm(row) {
        let action   = this.getRowAction(row);
        this.canSave = this.saveMethod   && ((action == 'Add' && this.canAdd) ||
                       (action == 'Edit' && this.canEdit) || (action == 'Duplicate' && this.canDuplicate));

        this.dialog.showButton('delete', action == 'Edit' && this.canDelete && this.deleteMethod);
        this.dialog.showButton('duplicate', action == 'Edit' && this.canDuplicate);
        this.dialog.showButton('save', this.canSave);
        this.dialog.enableButton('save', false);
        this.dialog.setTitleIcon(this.icons['Load']);

        this.call(this.loadDialogMethod, row, response => {
            if (response.error) {
                this.dialog.close();
                return;
            }

            this.initForm(response.data, row);
            this.dialog.setBody(this.form);
            this.trigger('dialog.open', {dialog: this.dialog, row: row});
            this.dialog.enableButton('save', true);
            this.dialog.setTitleIcon(this.icons[action]);
        });
    }

    initForm(form, row) {
        let action             = this.getRowAction(row);
        form.validateMethod    = this.validateMethod;
        form.canSubmit         = false;
        form.validateInvisible = this.validateInvisible;
        form.validateCallback  = !this.formValidate ? null : () => {
            if (Util.isCallable(this.formValidate)) {
                return this.formValidate(this.form, this.form.getValue());
            }
            return true;
        };

        this.form = Container.factory({type: 'form', ...form});

        this.form.on('submit', () => {
            if (this.canSave && this.dialog.isButtonEnabled('save')) {
                this.validateForm(row);
            }
        });

        this.form.on('validated', e => {
            this.dialog.enableButton('save', true);

            if (!e.valid) {
                this.dialog.scrollTop();
                this.dialog.setTitleIcon(this.icons[action]);
                this.isSubmitted = false;
                return;
            }

            if (this.canSave && this.isSubmitted) {
                this.saveForm();
            }
        });

        this.form.on('validate.error', e => {
            this.dialog.close();
        });
    }

    validateForm(row) {
        this.dialog.enableButton('save', false);
        this.dialog.setTitleIcon(this.icons['Load']);
        this.isSubmitted = true;
        this.form.validate();
    }

    saveForm() {
        let data = this.form.getValue();

        this.call(this.saveMethod, data, response => {
            if (!response.error) {
                this.grid.saveRow(response.data);
                this.trigger('form.save', response.data);
            }
        });

        this.dialog.close();   
    }

    deleteRow(row) {
        let message = Util.isCallable(this.deleteMessage) ? this.deleteMessage(row) : this.deleteMessage;
        if (confirm(message)) {
            this.call(this.deleteMethod, row, response => {
                if (!response.error) {
                    if (row.id && Util.isNumeric(row.id)) {
                        this.grid.removeRow(row.id);
                    }
                    this.trigger('row.delete', response.data);
                }
            });
            this.dialog.close();
        }
    }

    duplicateRow(row) {
        this.dialog.close();
        let data = this.form.getValue();
        this.openDialog({...row, ...data, id: 'new', duplicated: true});
    }

    markup() {
        return `
        <div id="${this.id}-grid"></div>
        <div id="${this.id}-dialog"></div>`;
    }

    render() {
        super.render();
        this.initDialog();
        this.initGrid();
    }

    reload() {
        this.grid.reload();
    }

    /**
     * Get filtered rows from grid
     */
    getRows() {
        return this.grid.getData();
    }

    getAllRows() {
        return this.grid.getAllRows();
    }
}