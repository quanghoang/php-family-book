/**
 * Created by hoangq on 7/19/17.
 */

// jQuery.noConflict();
//import 'd3js-4-7-4';
// import * as d3 from 'd3-3-5-0';
// import 'C3js-0-4-11';
// import 'c3-0-4-14';
// import * as c3 from 'c3-0-4-14';
// import 'c3-0-4-14/c3.css!';
// import 'C3js-0-4-11/c3.css!';
// import 'Tabluator-2-11/tabulator.css!';

import 'furf/jquery-ui-touch-punch';
import '../css/PivotGrid.css!';
import 'pivottable-npm';
import 'pivottable-npm/dist/pivot.css!';
import 'pivottable-npm/dist/c3_renderers';

// import 'PivotTable-2-13-0';
// import 'PivotTable-2-13-0/dist/pivot.css!';
// import 'PivotTable-2-13-0/dist/c3_renderers';
// import 'pivot_nreco';
// import 'pivot_nreco/Scripts/pivottable/nrecopivottableext.css!';

import Util from 'lib/UI/Helper/Util';
import Html from 'lib/UI/Helper/Html';
import Component from 'lib/UI/Component';
import Filter from 'lib/UI/Filter/Bootstrap/Filter';

export default class PivotGrid extends Component {

    constructor(attributes={}) {
        super(attributes);

        this.containerClass = 'ui-pivot';
        this.data           = attributes.data || [];
        this.options        = attributes.options || {};
        this.hasUI          = Util.toBool(attributes.hasUI, false);
        this.hasChart       = Util.toBool(attributes.hasChart, false);
    }

    init() {
        // if (!$.pivotUI || !$.pivot) {
        //    jQuery.noConflict();
        // }

        if (this.hasChart) {
            this.options.renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.c3_renderers);
        }
        if (this.hasUI) {
            this.getTableContainer().pivotUI(this.data, this.options);
        }
        else {
            this.getTableContainer().pivot(this.data, this.options);
        }
    }

    getFilterContainer() {
        return this.getContainer().find('.ui-pivot-filter');
    }

    getTableContainer() {
        return this.getContainer().find('.ui-pivot-table');
    }

    markup() {
        return `
        <div class="ui-pivot-filter"></div>
        <div class="ui-pivot-table"></div>`;
    }
}