/**
 * Created by hoangq on 1/13/17.
 */


import Config from 'lib/UI/Config';
import Util from 'lib/UI/Helper/Util';
import Validator from 'lib/UI/Helper/Validator';
import Notify from 'lib/UI/Helper/Notify';
import Event from 'lib/UI/Helper/Event';
import Request from 'lib/UI/Helper/Request';
// import $ from 'es6-shim/jquery-noglobal-3-1-1';


export default class Component extends Event {

    constructor(attributes={}) {
        super(attributes);

        this.type           = attributes.type || 'Component';
        this.id             = attributes.id || this.uniqueId('ui');
        this.name           = attributes.name || '';
        this.label          = attributes.label || '';
        this.renderLabel    = Util.toBool(attributes.renderLabel, true);
        this.class          = attributes.class || ''; //css class
        this.style          = attributes.style || ''; //css style
        this.container      = attributes.container || this.id + 'c';
        this.parent         = attributes.parent || null;
        this.method         = attributes.method || null; //load data method
        this.isRendered     = false;
        this.containerClass = '';
        this.containerTag   = 'div';
        this.loading        = false;    //is ajax loading data?
        this.loaded         = false;    //is ajax data loaded?
        this.loadParams     = attributes.loadParams || attributes.params || {};
        this.components     = [];
        this.timeout        = null;     //setTimeout ID for waitVisible method
        this.visible        = Util.isString(attributes.visible) || Util.isCallable(attributes.visible) ? false :
                              Util.toBool(attributes.visible, true);
        this.initComponent(attributes);
    }

    /**
     * Init component
     * @param attributes object
     */
    initComponent(attributes) {
        this.add(attributes.components || attributes.ui);

        //bind event from 'on' attribute
        if (Util.isObject(attributes.on)) {
            for (let event in attributes.on) {
                let callback = attributes.on[event];
                if (Util.isCallable(callback)) {
                    this.on(event, callback);
                }
            }
        }

        if (Util.isString(attributes.visible)) {
            this.initVisibleRule(attributes.visible);
        }

        if (Util.isCallable(attributes.visible)) {
            this.initVisibleCallback(attributes.visible);
        }
    }


    /**
     * Init visible rule
     * @param rule string
     */
    initVisibleRule(rule) {
        // parse rule
        let condition = null;
        let operators = ['>=', '<=', '!==', '!=', '==', '=', '>', '<'];

        for (let op of operators) {
            let parts = rule.split(op);
            if (parts.length == 2) {
                condition = {
                    field: parts[0].trim(),
                    op:    op,
                    value: parts[1].trim(),
                };
                break;
            }
        }

        if (!condition) {
            return;
        }

        let root = this.getRoot();
        root.on('input.change', input => {
            if (input.name != condition.field) {
                return;
            }

            let visible = false;
            let value   = input.value;
            let lower   = {input: String(value).toLocaleLowerCase(), condition: String(condition.value).toLocaleLowerCase()};

            switch (condition.op) {
                case '>=':
                    visible = value >= condition.value;
                    break;
                case '<=':
                    visible = value <= condition.value;
                    break;
                case '!=':
                    visible = value != condition.value;
                    break;
                case '!=':
                    visible = lower.input != lower.condition;
                    break;
                case '=':
                    visible = lower.input == lower.condition;
                    break;
                case '==':
                    visible = value == condition.value;
                    break;
                case '>':
                    visible = value > condition.value;
                    break;
                case '<':
                    visible = value < condition.value;
                    break;
            }
            this.setVisible(visible);

            if (Util.isCallable(this.setFocus) && !input.firstInit && this.focus) {
                setTimeout(() => {
                    this.setFocus(true);
                }, 200);
            }
        });

        //trigger change event for the first time
        setTimeout(() => {
            let result = this.find(condition.field.trim(), root);
            let input  = result[0] || null;
            input      = input ? {id: input.id, name: input.name, value: input.getValue(), firstInit: true} : {};
            root.trigger('input.change', input);
        }, 100);
    }


    /**
     * Init visible callback
     * @param callback
     */
    initVisibleCallback(callback) {
        if (!Util.isCallable(callback)) {
            return;
        }

        let root = this.getRoot();

        root.on('input.change', e => {
            let data       = {};
            let inputs     = [];
            let components = this.getAllComponents();

            for (let component of components) {
                if (component.isInput) {
                    inputs.push({
                        id:    component.id,
                        name:  component.name,
                        value: component.getValue()
                    });
                    data[component.name] = component.getValue();
                }
            }

            let visible = callback(data, inputs, e);
            this.setVisible(visible);
        });

        root.trigger('input.change');
    }

    /**
     * Get root component instance
     * @returns {Component}
     */
    static getRoot() {
        if (!Component.rootInstance) {
            Component.rootInstance = new Component(({
                id:        'root',
                container: $('body')
            }));
        }
        return Component.rootInstance;
    }

    /**
     * Get root component
     * @returns {Component}
     */
    getRoot() {
        return Component.getRoot();
    }

    /**
     * Is UI component or not
     * @param object object
     * @param type null|string
     * @returns {boolean}
     */
    static isComponent(object, type=null) {
        return  object instanceof Component && (!type || (object.type && type == object.type));
    }

    /**
     * Get config value
     * @param name Config name
     * @returns {*}
     */
    config(name) {
        const flatten = Util.flattenObject(Config);
        if (!name) {
            return Config;
        }
        if (Config[name] !== undefined) {
            return Config[name];
        }
        if (flatten[name] !== undefined) {
            return flatten[name];
        }
        return null;
    }


    /**
     * Find child component(s)
     * @param selector string Component selector, similar to css selector
     * Eg1. '#form'     ==> get component with id is 'form'.
     * Eg2. '.search'   ==> get component(s) with class is 'search'
     * Eg3. '@Button'   ==> get component(s) with type is 'Button'
     * Eg4. 'my-text'   ==> get component(s) with name is 'my-text'
     */
    find(selector, parent=this, result=[]) {
        if (!selector || !parent || Util.isEmpty(parent.components)) {
            return result;
        }

        for (let component of parent.components) {
            if (this.match(selector, component)) {
                result.push(component);
            }

            if (component.components.length) {
                this.find(selector, component, result);
            }
        }
        return result;
    }

    /**
     * Select all components matching the selector
     * @param selector string
     * @returns array Return array of component or empty array if not found
     */
    select(selector) {
        return this.find(selector, this, []);
    }

    /**
     * Get all components from root node
     * @returns {*}
     */
    getAllComponents() {
        return this.find('*', this.getRoot(), []);
    }

    /**
     * Get first component matching the selector
     * @param selector string
     * @returns {*} Return component if found or null if not found
     */
    get(selector) {
        if (selector == 'root') {
            return this.getRoot();
        }
        let result = this.select(selector);
        return result ? result[0] : null;
    }

    /**
     * Check if a selector matches with a component or not
     * @param selector string
     * @param component Component
     * @returns {boolean}
     */
    match(selector, component) {
        if (!selector || !component || !component instanceof Component) {
            return false;
        }

        selector  = selector.trim().toLowerCase();
        let types = {'*': 'all', '#': 'id', '.': 'class', '@': 'type'};
        let type  = types[selector.substr(0, 1)] || 'name';
        selector  = type != 'name' ? selector.substr(1) : selector;

        return type == 'all' ||
               type == 'id'    && component.id.toLowerCase() == selector ||
               type == 'class' && component.class.toLowerCase().trim().split(' ').includes(selector) ||
               type == 'type'  && component.type.toLowerCase() == selector ||
               type == 'name'  && component.name.toLowerCase() == selector;
    }

    /**
     * Add component(s)
     * @param components
     */
    add(components) {
        if (Util.isEmpty(components)) {
            return;
        }
        for (let component of Util.toArray(components)) {
            component.parent = this;
            this.components.push(component);
        }
    }

    /**
     * Remove component from root components array
     * @param componentId string Component ID.
     */
    removeComponent(componentId) {
        let index = -1;
        for (let component of this.getRoot().components) {
            index++;
            if (component.id == componentId) {
                this.getRoot().components.splice(index, 1);
                return;
            }
        }
    }

    /**
     * Remove all components
     */
    destroy() {
        for (let component of this.components) {
            if (component.timeout) {
                clearTimeout(component.timeout);
            }
            component.clearEvents();
            component.destroy();
        }

        this.removeComponent(this.id);
        this.clearEvents();
        this.components = [];
        delete this;
    }

    /**
     * Find closest parent component
     * @param selector string
     * @returns {*}
     */
    closest(selector) {
        let parent = this.parent;

        while (parent) {
            if (this.match(selector, parent)) {
                return parent;
            }
            parent = parent.parent;
        }
        return null;
    }

    /**
     * Is child of a component
     * @param selector string
     * @returns {boolean}
     */
    isChildOf(selector) {
        return this.closest(selector) ? true : false;
    }

    /**
     * Create unique ID
     * @param prefix string
     * @param counter int
     * @returns {string}
     */
    uniqueId(prefix='comp', counter=0) {
        Component.counter = Component.counter ? Component.counter + 1 : 1;
        return prefix + Util.padString(counter || Component.counter, 3);
    }

    /**
     * Set container id
     * @param container string ID of the div container
     */
    setContainerId(container) {
        this.container = container;
    }

    /**
     * Create container & append to parent container
     * @returns {jQuery} Return container node
     */
    createContainer() {
        let target = this.getRoot().getContainer();

        if (this.parent) {
            let container = this.parent.getContainer();
            if (container.length) {
                target = container;
            }
        }
        target.append(this.createWrapper());
        return this.getContainer();
    }

    /**
     * Create container wrapper
     * @returns {string}
     */
    createWrapper(content='', cssClass='') {
        cssClass = cssClass ? `class="${cssClass}"` : '';
        return `<${this.containerTag} id="${this.container}" ${cssClass}>${content}</${this.containerTag}>`;
    }

    /**
     * Get container html element
     * @returns {jQuery}
     */
    getContainer() {
        return Util.isjQuery(this.container) ? this.container : $('#' + this.container);
    }

    /**
     * Get container ID string
     * @returns {*|string}
     */
    getContainerId() {
        return Util.isjQuery(this.container) ? this.container.attr('id') : this.container;
    }

    /**
     * Remove container
     */
    removeContainer() {
        this.getContainer().detach();
    }

    /**
     * Set form content
     * @param content string HTML content
     * @param classes string CSS class
     * @returns {*}
     */
    setContent(content, classes='') {
        let container = this.getContainer();
        classes = this.containerClass + ' ' + classes;

        if (!Util.isEmpty(classes)) {
            container.addClass(classes);
        }
        this.setVisible(this.visible);

        container.html(content);
        this.isRendered = true;
        return container;
    }

    setVisible(visible=true) {
        this.visible = visible;
        if (!visible) {
            this.getContainer().hide();
        }
        else {
            this.getContainer().css('display', '');
        }
        this.trigger('visible', {id: this.id, visible});
    }

    isVisible() {
        let container = this.getContainer();
        return container && container.is(':visible');
    }

    waitForVisibility(callback, speed=250) {
        if (!Util.isCallable(callback)) {
            return;
        }
        if (this.isVisible()) {
            return callback();
        }
        this.timeout = setTimeout(() => {
            this.waitForVisibility(callback, speed);
        }, speed);

    }

    /**
     * Check if the component has container or not
     * @returns {boolean}
     */
    hasContainer() {
        return this.getContainer().length != 0;
    }

    /**
     * Render markup for component (TBD in inherited classes)
     * @returns {string}
     */
    markup() {
        return null;
    }

    /**
     * Init after rendering (TBD in inherited classes)
     */
    init() {

    }

    /**
     * Render component content (TBD in inherited classes)
     */
    render() {
        let content = this.markup();
        if (content) {
            this.setContent(content);
            this.init();
        }
    }


    /**
     * Init after refreshing
     */
    initRefresh() {
    }

    /**
     * Refresh content
     */
    refresh() {
        let content = this.markup();
        this.setContent(content);
        this.initRefresh();
    }


    /**
     * create label
     * @param content
     * @returns {string}
     */
    createLabel(content) {
        if (!this.label || !this.renderLabel) {
            return content;
        }
        let label = `<label class="control-label">${this.label}</label>`;
        return `<div>${label}</div> ${content}`;
    }

    /**
     * Call ajax method on server side
     * @param method string
     * @param data object|callback
     * @param callback function
     * @param dataType string Response datatype (json, xml, html)
     * @returns {boolean}
     */
    call(method, data, callback, dataType='json') {
        let cfgAjax = this.config('ajax');
        let parse  = Request.parse(method, cfgAjax.folder, cfgAjax.class);
        if (!cfgAjax.url || !parse) {
            return false;
        }

        let label = 'Call method ' + parse.method;
        if (data && data['ajaxLabel']) {
            label = data['ajaxLabel'];
            delete data['ajaxLabel'];
        }

        let reqData = {
            data: Util.isCallable(data) ? data() : data,
            ajax: {
                folder:   parse.folder,
                class:    parse.class,
                method:   parse.method,
                dataType: dataType,
            }
        };

        let request = Request.getInstance();
        let queue   = request.queues[this.id];

        // remove the request with same method/class/folder name with the new request
        // in the queue before adding the new one
        if (queue && Util.isArray(queue)) {
            let reqAjax = reqData.ajax;
            for (let index=0; index < queue.length; index++) {
                let req = queue[index].data.ajax;
                if (req.folder == reqAjax.folder && req.class == reqAjax.class && req.method == reqAjax.method) {
                    queue.splice(index, 1);
                }
            }
        }

        let success = response => {
            this.ajaxSuccess(response, callback);
        };
        let fail = (jqxhr, textStatus, error) => {
            this.ajaxFail(jqxhr, textStatus, error, label);
        };

        this.loading = true;
        request.add(this.id, cfgAjax.url, reqData, success, fail, cfgAjax.method, dataType, label);
        return true;
    }

    /**
     * Handle ajax success
     */
    ajaxSuccess(response, callback) {
        this.loading = false;
        this.loaded  = true;

        if (response.error && response.message) {
            Notify.error(response.message, 'Error:', {delay: 60000});
        }

        if (response.info && response.message) {
            Notify.info(response.message, '', {delay: 8000});
        }

        if (response.success && response.message) {
            Notify.success(response.message, '', {delay: 5000});
        }

        if (Util.isCallable(callback)) {
            callback(response);
        }
    }

    /**
     * Handle ajax error
     */
    ajaxFail(jqxhr, textStatus, error, label) {
        Notify.error(error, label + ':', {delay: 60000});
    }

    /**
     * Call method from root node
     * @param method
     * @param data
     * @param callback
     * @param dataType
     * @returns {boolean}
     */
    static callMethod(method, data, callback, dataType='json') {
        return this.getRoot().call(method, data, callback, dataType);
    }

    /**
     * Trigger root event
     * @param event string
     * @param data object
     */
    triggerRoot(event, data) {
        return this.getRoot().trigger(event, data);
    }

    /**
     * On root event
     * @param event
     * @param callback
     * @param multiple
     */
    onRoot(event, callback, multiple=true) {
        return this.getRoot().on(event, callback, multiple);
    }

    /**
     * Click event
     * @param callback
     */
    click(callback) {
        this.on('click', callback);
    }

    /**
     * Change event
     * @param callback
     */
    change(callback) {
        this.on('change', callback);
    }


    /**
     * Add css rule dynamically
     * @param rule string Eg. "div {border: 2px solid black; background-color: blue;}"
     */
    addCssRule(rule) {
        var sheet = document.createElement('style')
        sheet.innerHTML = rule;
        document.body.appendChild(sheet);
    }

    /**
     * Get input components
     * @param components
     * @returns {Array}
     */
    getInputs(components=null) {
        components = components || this.components;
        let input = [];
        for (let component of components) {
            if (component.isInput) {
                input.push(component);
            }

            if (component instanceof Component) {
                if (component.components) {
                    input = input.concat(this.getInputs(component.components))
                }
            }
        }
        return input;
    }

    /**
     * Get input by name
     * @param name string
     * @returns {*}
     */
    getInput(name) {
        for (let input of this.getInputs()) {
            if (input.name == name) {
                return input;
            }
        }
        return null;
    }

    /**
     * Set form value/data
     * @param value object
     */
    setValue(value) {
        for (let component of this.getInputs()) {
            if (value[component.name]) {
                component.setValue(value[component.name]);
            }
        }
    }

    /**
     * Get form value/data
     * @returns {{}}
     */
    getValue() {
        let value = {};
        for (let component of this.getInputs()) {
            let name = component.name;
            if (!value[name]) {
                value[name] = component.getValue();
                continue;
            }

            if (!Util.isArray(value[name])) {
                value[name] = [value[name]];
            }
            value[name].push(component.getValue());
        }
        return value;
    }
}