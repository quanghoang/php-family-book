/**
 * Created by hoangq on 1/3/17.
 */

import 'jquery3-2-1';
import 'chosen1-6-2';

import 'bootstrap3-3-7';
// import 'bootstrap3-3-7/css/bootstrap.css!';
//import 'font-awesome4-7-0/css/font-awesome.css!';

import 'typeahead';
import 'bootstrap-tagsinput';
import 'bootstrap-tagsinput/dist/bootstrap-tagsinput.css!';
import 'bootstrap-tagsinput/dist/bootstrap-tagsinput-typeahead.css!';
import 'bootstrap-checkbox1-4';

import 'select2-4-0-3';
import 'select2-4-0-3/css/select2.css!';

import 'lib/UI/App/css/App.css!';
import 'lib/UI/css/ui.css!';
import 'lib/UI/css/bootstrap-chosen.css!';
import 'lib/UI/Table/css/Table.css!';
import 'lib/UI/Container/css/Container.css!';
import 'lib/UI/Input/css/Input.css!';
import 'lib/UI/List/css/List.css!';

import {factory, render, get} from 'lib/UI/Factory/Bootstrap/Factory';
export {factory, render, get};
