/**
 * Created by hoangq on 1/3/17.
 */

import $ from 'jquery3-1-1';
import 'jqueryui1-12-1';
// import 'chosen1-6-2';
//import 'jqueryui1-12-1/themes/base/jquery-ui.min.css!';
import 'bootstrap3-3-7';
// import 'bootstrap3-3-7/css/bootstrap.css!';
//import 'tinymce-4-6-4';

import 'typeahead';
import 'bootstrap-tagsinput';
import 'bootstrap-tagsinput/dist/bootstrap-tagsinput.css!';
import 'bootstrap-tagsinput/dist/bootstrap-tagsinput-typeahead.css!';
import 'select2-4-0-3';
import 'select2-4-0-3/css/select2.css!';

import 'lib/UI/App/css/App.css!';
import 'lib/UI/css/ui.css!';
import 'lib/UI/css/bootstrap-chosen.css!';
import 'lib/UI/Table/css/Table.css!';
import 'lib/UI/Container/css/Container.css!';
import 'lib/UI/Input/css/Input.css!';
import 'lib/UI/List/css/List.css!';

import {factory, render, open, get, call} from 'lib/UI/Factory/Bootstrap/Factory';
export {factory, render, open, get, call};

// import EditGrid from 'lib/UI/Grid/Bootstrap/EditGrid';
// export {EditGrid};

// import Grid from 'lib/UI/Grid/Bootstrap/Grid';
// export {Grid};
// import DartFilter from 'lib/UI/Filter/Bootstrap/DartFilter';
// export {DartFilter};

import Util from 'lib/UI/Helper/Util';
export {Util};

import Notify from 'lib/UI/Helper/Notify';
export {Notify};
