/**
 * Created by hoangq on 3/29/17.
 */

import 'framework7_1-5-3';
import $ from 'jquery3-2-1';

import 'lib/UI/App/css/App.css!';
import 'lib/UI/css/ui.css!';
import 'lib/UI/css/bootstrap-chosen.css!';
import 'lib/UI/Table/css/Table.css!';
import 'lib/UI/Container/css/Container.css!';
import 'lib/UI/Input/css/Input.css!';
import 'lib/UI/List/css/List.css!';

import App from './App';
import Request from 'lib/UI/Helper/Request';


export default function startup(settings) {

    // Request.call('Mobile/App.getAppSettings', null, response => {

        let app = new App({
            title:     'My App',
            startPage: 'dashboard',
            hasLogin:  false,
            signedin:  false,

            options: {
                material:       false,
                pushState:      true,
                dynamicPageUrl: '!{{index}}',
                swipePanel:     'left',
            },

            menu: {
                container: 'main-menu',
                label:     'Mobile',
                //method:    'Mobile/App.getMenus'
            }
        });
        app.render();

    // });
}