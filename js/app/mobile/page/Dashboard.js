/**
 * Created by hoangq on 3/29/17.
 */

import Page from 'lib/UI/App/Framework7/Page';

export default class Dashboard extends Page {
    constructor(attributes={}) {
        super(attributes);
        this.name       = 'dashboard';
        this.title      = 'Dashboard';
        this.backButton = false;
        this.content    = `Hello World!`;
    }
}