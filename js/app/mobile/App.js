/**
 * Created by hoangq on 3/29/17.
 */

import BaseApp from 'lib/UI/App/Framework7/App';
import Dashboard from './page/Dashboard';

export default class App extends BaseApp {

    constructor(attributes={}) {
        super(attributes);
        this.addView({name: 'main'});

        this.addPage({
            dashboard: Dashboard
        });
    }
}