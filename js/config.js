System.config({
  baseURL: "/php-family-book/js",
  defaultJSExtensions: true,
  transpiler: "babel",
  babelOptions: {
    "optional": [
      "runtime",
      "optimisation.modules.system"
    ]
  },
  paths: {
    "github:*": "jspm_packages/github/*",
    "npm:*": "jspm_packages/npm/*"
  },

  map: {
    "PapaParse-4-2": "github:mholt/PapaParse@4.2.0",
    "babel": "npm:babel-core@5.8.38",
    "babel-runtime": "npm:babel-runtime@5.8.38",
    "backbonejs-1-3": "github:jashkenas/backbone@1.3.3",
    "boootstrap-multiselect-0-9-13": "github:davidstutz/bootstrap-multiselect@0.9.13",
    "bootstrap-checkbox1-4": "github:vsn4ik/bootstrap-checkbox@1.4.0",
    "bootstrap-filestyle-1-2-3": "github:markusslima/bootstrap-filestyle@1.2.3",
    "bootstrap-filestyle-1-3-0": "github:markusslima/bootstrap-filestyle@1.3.0",
    "bootstrap-notify-3-1-3": "github:mouse0270/bootstrap-notify@3.1.3",
    "bootstrap-tagsinput": "github:bootstrap-tagsinput/bootstrap-tagsinput@0.7.0",
    "bootstrap3-3-7": "github:components/bootstrap@3.3.7",
    "chosen1-6-2": "github:harvesthq/chosen@1.6.2",
    "code-mirror-3-12": "github:codemirror/CodeMirror@v3.12",
    "code-mirror-npm": "npm:codemirror@5.31.0",
    "core-js": "npm:core-js@1.2.7",
    "css": "github:systemjs/plugin-css@0.1.36",
    "font-awesome4-7-0": "github:fortawesome/font-awesome@4.7.0",
    "framework7-icons0-8-9": "github:framework7io/Framework7-Icons@0.8.9",
    "framework7_1-5-3": "github:framework7io/Framework7@1.5.3",
    "highcharts5-0-2": "github:highcharts/highcharts-dist@5.0.2",
    "inputmask-3-3-10": "github:RobinHerbots/Inputmask@3.3.10",
    "jquery": "npm:jquery@3.2.1",
    "jquery-ns-autogrow-1-1-6": "github:ro31337/jquery.ns-autogrow@1.1.6",
    "jquery3-1-1": "github:components/jquery@3.1.1",
    "jquery3-2-1": "github:components/jquery@3.2.1",
    "jqueryui1-12-1": "github:components/jqueryui@1.12.1",
    "multiselect-1-13": "github:ehynds/jquery-ui-multiselect-widget@1.13",
    "multiselect-1-14": "github:ehynds/jquery-ui-multiselect-widget@1.14",
    "multiselect-1-5": "github:ehynds/jquery-ui-multiselect-widget@1.5",
    "navigo-4-6-2": "github:krasimir/navigo@4.7.2",
    "select2-4-0-3": "npm:select2@4.0.3",
    "slickgrid-2-1-0": "github:mleibman/SlickGrid@2.1.0",
    "slickgrid-column-group-plugin": "github:naresh-n/slickgrid-column-group@master",
    "slickgrid-es6": "npm:slickgrid-es6@2.1.0",
    "tinymce-4-6-4": "github:tinymce/tinymce-dist@4.6.4",
    "treeview-1-2-0": "github:jonmiles/bootstrap-treeview@1.2.0",
    "typeahead": "github:twitter/typeahead.js@0.11.1",
    "github:components/jqueryui@1.12.1": {
      "jquery": "npm:jquery@3.2.1"
    },
    "github:harvesthq/chosen@1.6.2": {
      "jquery": "npm:jquery@3.2.1"
    },
    "github:jspm/nodelibs-assert@0.1.0": {
      "assert": "npm:assert@1.4.1"
    },
    "github:jspm/nodelibs-buffer@0.1.1": {
      "buffer": "npm:buffer@5.0.8"
    },
    "github:jspm/nodelibs-path@0.1.0": {
      "path-browserify": "npm:path-browserify@0.0.0"
    },
    "github:jspm/nodelibs-process@0.1.2": {
      "process": "npm:process@0.11.10"
    },
    "github:jspm/nodelibs-util@0.1.0": {
      "util": "npm:util@0.10.3"
    },
    "github:jspm/nodelibs-vm@0.1.0": {
      "vm-browserify": "npm:vm-browserify@0.0.4"
    },
    "github:twitter/typeahead.js@0.11.1": {
      "jquery": "npm:jquery@3.2.1"
    },
    "npm:assert@1.4.1": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "buffer": "github:jspm/nodelibs-buffer@0.1.1",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "util": "npm:util@0.10.3"
    },
    "npm:babel-runtime@5.8.38": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:buffer@5.0.8": {
      "base64-js": "npm:base64-js@1.2.1",
      "ieee754": "npm:ieee754@1.1.8"
    },
    "npm:codemirror@5.31.0": {
      "buffer": "github:jspm/nodelibs-buffer@0.1.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:core-js@1.2.7": {
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "path": "github:jspm/nodelibs-path@0.1.0",
      "process": "github:jspm/nodelibs-process@0.1.2",
      "systemjs-json": "github:systemjs/plugin-json@0.1.2"
    },
    "npm:inherits@2.0.1": {
      "util": "github:jspm/nodelibs-util@0.1.0"
    },
    "npm:path-browserify@0.0.0": {
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:process@0.11.10": {
      "assert": "github:jspm/nodelibs-assert@0.1.0",
      "fs": "github:jspm/nodelibs-fs@0.1.2",
      "vm": "github:jspm/nodelibs-vm@0.1.0"
    },
    "npm:select2@4.0.3": {
      "almond": "npm:almond@0.3.3",
      "jquery-mousewheel": "npm:jquery-mousewheel@3.1.13"
    },
    "npm:slickgrid-es6@2.1.0": {
      "flatpickr": "npm:flatpickr@1.9.1",
      "jquery": "npm:jquery@3.2.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:util@0.10.3": {
      "inherits": "npm:inherits@2.0.1",
      "process": "github:jspm/nodelibs-process@0.1.2"
    },
    "npm:vm-browserify@0.0.4": {
      "indexof": "npm:indexof@0.0.1"
    }
  }
});
