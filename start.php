<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/23/17
 * Time: 4:02 PM
 */

session_start();

//error_reporting(E_ALL & ~E_NOTICE);
//ini_set("display_errors", 1);

include 'vendor/autoload.php';

$_SESSION['theme'] = System\Config::get('theme.default');
define('DS', DIRECTORY_SEPARATOR, true);
define('BASE_PATH', __DIR__ . DS, true);