<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 6/29/2016
 * Time: 12:37 PM
 */

namespace Helper;

class Validator {

	public static function isEmail($value) {
		return filter_var($value, FILTER_VALIDATE_EMAIL);
	}
}