<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 6/8/2016
 * Time: 3:39 PM
 */

namespace Helper;

/**
 * Class for handling array data
 * @package Helper
 */
class Arrays {

    /**
     * Search for a key in array
     *
     * @param string $key Use '.' to access to nested arrays
     * @param array $array
     * @return mixed|null
     */
    public static function find($key, $array) {
        $key = trim($key);
        return isset($array[$key]) ? $array[$key] : self::search($key, $array);
    }

    /**
     * Search a value in nested array
     *
     * @param string $search Search key value
     * @param array $array Array will be searched in
     * @param string $parent Parent key
     *
     * @return null|mixed
     */
    public static function search($search, $array, $parent='') {
        if (!is_array($array)) {
            return null;
        }

        foreach ($array as $key => $value) {
            $current = empty($parent) ? $key : $parent . '.' . $key;

            if ($key == $search || $current == $search) {
                return $value;
            }

            if (is_array($value)) {
                $result = self::search($search, $value, $current);
                if ($result) {
                    return $result;
                }
            }
        }
        return null;
    }


    /**
     * Flatten a multi-dimension array into a one-dimension array
     *
     * @param array $array
     * @param array $result
     * @param string $parent
     * @return array
     */
    public static function flatten($array, &$result=array(), $parent='') {
        if (!is_array($array)) {
            return null;
        }

        foreach ($array as $key => $value) {
            $current = empty($parent) ? $key : $parent . '.' . $key;
            if (is_array($value)) {
                self::flatten($value, $result, $current);
                continue;
            }
            $result[$current] = $value;
        }
        return $result;
    }

    /**
     * Copy/clone array
     * @param array $source
     * @return array
     */
    public static function copy($source) {
        $arrayObject = new \ArrayObject($source);
        return $arrayObject->getArrayCopy();
    }

    public static function toArray($value) {
        return empty($value) ? array() : (is_array($value) ? $value : array($value));
    }

	/**
	 * Extract column values
	 * @param $rows array
	 * @param $column string Column name
	 * @return array
	 */
	public static function extractValues($rows, $column) {
		$rows   = self::toArray($rows);
		$result = array();

		foreach ($rows as $row) {
			$value = $row[$column];
			if ($value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	/**
	 * Extract distinct values
	 * @param $rows array
	 * @param $column string Column name
	 * @return array
	 */
	public static function extractDistinctValues($rows, $column) {
		$rows   = self::toArray($rows);
		$result = array();

		foreach ($rows as $row) {
			$value = $row[$column];
			if ($value) {
				$result[$value] = true;
			}
		}
		return array_keys($result);
	}

	public static function toIndexArray($rows, $indexColumn, $valueColumn) {
		$rows   = self::toArray($rows);
		$result = array();

		foreach ($rows as $row) {
			if (isset($row[$indexColumn])) {
				$result[$indexColumn] = isset($row[$valueColumn]) ? $row[$valueColumn] : null;
			}
		}
		return $result;
	}
}