<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 6/8/2016
 * Time: 11:25 AM
 */

namespace Helper;


class Format {

    /**
     * Format byte value
     *
     * @param int $size
     * @param int $precision
     * @return string
     */
    public static function byte($size, $precision = 2) {
        $base = log($size, 1024);
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');
        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }

    /**
     * Format milisecond value
     *
     * @param int $milisecond
     * @return string
     */
    public static function milisecond($milisecond) {
        $second = (int)($milisecond / 1000);
        $milisecond = $milisecond - $second * 1000;
        return $second ? sprintf('%ss %sms', $second, $milisecond) : sprintf('%sms', $milisecond);
    }
}