<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/23/17
 * Time: 3:55 PM
 */

namespace Helper;


class User {

	public static function getLoginId() {
		return isset($_SESSION['user_id']) ? $_SESSION['user_id'] : 0;
	}
}