<?php

namespace Helper;

/**
 * Description of URL
 *
 * @author quanghoang
 */
class URL {
    
    /**
     * Input URL
     *
     * @var string 
     */
    public $url;
    
    /**
     * Scheme / protocol (http, https, ftp, ftps)
     *
     * @var null|string
     */
    public $scheme;

    /**
     * Host (IP or domain name)
     *
     * @var null|string
     */
    public $host;

    /**
     * @var int
     */
    public $port = 80;

    /**
     * URL path (including dir, file, query string)
     *
     * @var null|string
     */
    public $path;

    
    /**
     * Directory in path
     *
     * @var string 
     */    
    public $dir;
    
    /**
     * File in path
     *
     * @var string 
     */
    public $file;
    
    /**
     * File extension in path
     *
     * @var string 
     */
    public $ext;
    
    /**
     * Query string
     *
     * @var null|string
     */
    public $query;
    
    /**
     * Image file extensions (for filtering image links)
     * 
     * @var array
     */
    public $image_extensions = array('jpg', 'png', 'gif');

    /**
     * Create URL object from url string
     * @param string $url
     * @return URL
     */
    public static function create($url) {
        return new static($url);
    }

    /**
     * Url constructor.
     *
     * @param $url
     */
    public function __construct($url) {
        $this->url = $url;
        $urlProperties = parse_url($url);

        foreach (['scheme', 'host', 'path', 'port', 'query'] as $property) {
            if (isset($urlProperties[$property])) {
                $this->$property = $urlProperties[$property];
            }
        }
        
        $info = pathinfo($this->path);        
        $this->dir  = isset($info['dirname'])  ? ltrim($info['dirname'], '/') : '';
        $this->file = isset($info['basename'])  ? $info['basename'] : '';
        $this->ext  = isset($info['extension']) ? $info['extension'] : '';
    }

    /**
     * Determine if the url is relative.
     *
     * @return bool
     */
    public function is_relative() {
        return is_null($this->scheme);
    }

    /**
     * Determine if the url is relative.
     *
     * @return bool
     */
    public function is_absolute() {
        return !is_null($this->scheme);
    }

    /**
     * Determine if the url has protocol or not.
     *
     * @return bool
     */
    public function has_protocol() {
        return !empty($this->scheme);
    }
    
    /**
     * Determine if this is an anchor link or not
     * 
     * @return bool
     */
    public function is_anchor() {
        return strpos($this->file, '#')  === 0;
    }

    /**
     * Determine if this is a query string or not
     *
     * @return bool
     */
    public function is_query() {
        return strpos($this->file, '?')  === 0;
    }
    
    
    /**
     * Determine if this is a phone link or not
     * 
     * @return bool
     */
    public function is_phone() {
        return $this->scheme == 'tel';
    }
    
    /**
     * Determine if this is an email link or not
     * 
     * @return bool
     */
    public function is_email() {
        return $this->scheme == 'mailto';
    }
    
    
    /**
     * Determine if this is an image link or not
     * 
     * @return bool
     */
    public function is_image() {
        return in_array($this->ext, $this->image_extensions);
    }
    
       
    /**
     * Determine if this is an inline javascript.
     *
     * @return bool
     */
    public function is_javascript() {
        return $this->scheme === 'javascript';
    }

    /**
     * Set the scheme.
     *
     * @param string $scheme
     *
     * @return $this
     */
    public function scheme($scheme) {
        $this->scheme = $scheme;
        return $this;
    }

    /**
     * Set the host.
     *
     * @param string $host
     *
     * @return $this
     */
    public function host($host) {
        $this->host = $host;
        return $this;
    }

    /**
     * @param int $port
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function port($port){
        if (!is_numeric($port)) {
            throw new \Exception('Invalid port number');
        }

        $this->port = $port;
        return $this;
    }

    /**
     * Remove the fragment.
     *
     * @return $this
     */
    public function remove_fragment() {
        $this->path = explode('#', $this->path)[0];
        return $this;
    }
        
    /**
     * Convert relative url to absolute url
     * @param string $base_url
     * @return string
     */
    public function absolute_url($base_url) {
        if ($this->is_absolute()) {
            return $this->url;
        }

        $base_url = rtrim($base_url, '/') . '/';

        // relative to current base url
        if (strpos($this->url, '.') === 0) {
            return self::create($base_url)->base_url() . '/' . ltrim($this->url, '/');
        }

        // queries and anchors
        if ($this->is_anchor() || $this->is_query()) {
            return $base_url . $this->url;
        }

        // parse base URL and convert to local variables: $scheme, $host, $path
        extract(parse_url($base_url));

        // remove non-directory element from path
        $path = preg_replace('#/[^/]*$#', '', $path);

        // destroy path if relative url points to root
        if (strpos($this->url, '/') === 0) {
            $path = '';
        }

        // dirty absolute URL
        $abs = "{$host}{$path}/{$this->url}";

        // replace '//' or '/./' or '/foo/../' with '/'
        $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
        for($n=1; $n>0; $abs=preg_replace($re, '/', $abs, -1, $n)) {}

        /* absolute URL is ready! */
        return "{$scheme}://{$abs}";
    }

    /**
     * Get base url
     *
     * @return string
     */
    public function base_url() {
        $url  = $this->root_url() . "/$this->dir";
        return rtrim($url, '/');
    }

    /**
     * Get root url
     *
     * @return string
     */
    public function root_url() {
        $port = ($this->port === 80 ? '' : ":{$this->port}");
        return "{$this->scheme}://{$this->host}{$port}";
    }


    /**
     * Convert the url to string.
     *
     * @return string
     */
    public function __toString() {
        $root  = $this->root_url();
        $path  = ltrim($this->path, '/');
        $query = $this->query ? "?{$this->query}" : '';
        return "{$root}/{$path}{$query}";
    }
}
