<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Helper;

/**
 * Description of Date
 *
 * @author quanghoang
 */
class DateTime {

	const ISO_DATETIME  = 'Y-m-d H:i:s';

	/**
	 * Add days to date
	 * @param string $date
	 * @param int $numDays
	 * @return false|string
	 */
	public static function addDays($date, $numDays) {
		if ($numDays == 0) {
			return $date;
		}

		$date    = date('Y-m-d', strtotime($date));
		$sign    = $numDays > 0 ? '+' : '-';
		$numDays = abs($numDays);

		return date('Y-m-d', strtotime($date . " $sign $numDays day"));
	}


	/**
	 * Get current time in milisecond
	 *
	 * @return int
	 */
	public static function milisecond() {
		return (int)round(microtime(true) * 1000);
	}

	public static function now() {
		return date(self::ISO_DATETIME);
	}

	/**
	 * Create human readable timestamp
	 * @param $timestamp int|string
	 * @return string
	 */
	public static function relativeTime($timestamp) {
		$timestamp  = is_int($timestamp) ? $timestamp : strtotime($timestamp);
		$difference = time() - $timestamp;
		$periods    = array("sec", "min", "hour", "day", "week", "month", "years", "decade");
		$lengths    = array("60", "60", "24", "7", "4.35", "12", "10");

		if ($difference > 0) { // this was in the past
			$ending = "ago";
		}
		else { // this was in the future
			$difference = -$difference;
			$ending     = "to go";
		}
		for ($j = 0; $difference >= $lengths[$j]; $j++) {
			$difference /= $lengths[$j];
		}
		$difference = round($difference);
		if ($difference != 1) {
			$periods[$j] .= "s";
		};
		$text = "$difference $periods[$j] $ending";
		return $text;
	}

	public static function formatTime($timestamp) {
		$timestamp = is_int($timestamp) ? $timestamp : strtotime($timestamp);
		return sprintf('%s (%s)', date(self::ISO_DATETIME, $timestamp), self::relativeTime($timestamp));
	}
    
    public static function addDate($date, $interval) {
        $date=date_create($date);
        date_add($date,date_interval_create_from_date_string($interval));
        return date_format($date,"Y-m-d");
    }
    
}
