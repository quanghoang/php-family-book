<?php
/**
 * Created by PhpStorm.
 * User: hoangq
 * Date: 10/14/17
 * Time: 12:23 PM
 */

namespace Helper;


class Email {

	public static function send() {
		// Create the Transport
		$transport = (new \Swift_SmtpTransport('smtp.example.org', 25))->setUsername('your username')->setPassword('your password');

		// Create the Mailer using your created Transport
		$mailer = new \Swift_Mailer($transport);

		// Create a message
		$message = (new \Swift_Message('Wonderful Subject'))
			->setFrom(['john@doe.com' => 'John Doe'])
			->setTo(['receiver@domain.org', 'other@domain.org' => 'A name'])
			->setBody('Here is the message itself');

		// Send the message
		return $mailer->send($message);
	}
}